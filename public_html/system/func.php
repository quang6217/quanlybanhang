<?php
error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
function _sql($t)
{
    return addslashes(strip_tags($t));
}
function _sql00($t)
{
    return strip_tags(preg_replace('/[^a-zA-Z0-9- ._@]/', '', $t));
}
function _sql01($string)
{
    return addslashes(strip_tags(preg_replace('/[^a-zA-Z0-9- .!?_@áàảãạăắặằẳẵâấầẩẫậÁÀẢÃẠĂẮẶẰẲẴÂẤđĐéèẻẽẹêếềểễệÉÈẺẼẸÊẾỀỂỄỆíìỉĩịÍÌỈĨỊóòỏõọôốồổỗộơớờởỡợÓÒỎÕỌÔỐỒỔỖỘƠỚúùủũụưứừửữựÚÙỦŨỤƯỨỪỬỮỰýỳỷỹỵÝỲỶỸỴ]/', '', $string)));
}

function _sql011($string)
{
    return addslashes(strip_tags(preg_replace('/[^a-zA-Z0-9- .!?_@áàảãạăắặằẳẵâấầẩẫậÁÀẢÃẠĂẮẶẰẲẴÂẤđĐéèẻẽẹêếềể%ễệÉÈẺẼẸÊẾỀỂỄỆíìỉĩịÍÌỈĨỊóòỏõọôốồổỗộơớờởỡợÓÒỎÕỌÔỐỒỔỖỘƠỚúùủũụưứừửữựÚÙỦŨỤƯỨỪỬỮỰýỳỷỹỵÝỲỶỸỴ]/', '', $string)));
}

function _sql_chat($string)
{
    return addslashes(strip_tags(preg_replace('/[^a-zA-Z0-9- .!?_@áàảãạăắặằẳẵâấầẩẫậÁÀẢÃẠĂẮẶẰẲẴÂẤđĐéèẻẽẹêếềểễệÉÈẺẼẸÊẾỀỂỄỆíìỉĩịÍÌỈĨỊóòỏõọôốồổỗộơớờởỡợÓÒỎÕỌÔỐỒỔỖỘƠỚúùủũụưứừửữựÚÙỦ\[\]ŨỤƯỨỪỬỮỰýỳỷỹỵÝỲỶỸỴ]/', '', $string)));
}
function _sql02($string)
{
    return addslashes(strip_tags(preg_replace('/[=<>%"\']+/', '', $string)));
}
function _sql03($t)
{
    return addslashes(strip_tags($t));
}
function _sql_num($t)
{
    return preg_replace('/[^0-9]/', '', $t);
}
function base64_en($data)
{
    return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}
//Giải mã
function base64_de($data)
{
    return strip_tags(base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)));
}
function base64_html($data)
{
    return htmlentities(base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)));
}

function random_string($num)
{
    return substr(str_shuffle('0123456789abcdefghjkmnpqrstwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), -$num);
}

function cookie_to_id($cookie)
{
    $id_facebook = 0;
    if (preg_match('/c_user=(.+?);/', $cookie, $a)) {
        $id_facebook =  $a[1];
    }
    return (int) $id_facebook;
}

//Lấy số dư của member
function member_amount($id)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM member WHERE id =:id");
    $stmt->execute(array(":id" => $id));
    $data = $stmt->fetch(PDO::FETCH_ASSOC);
    return $data["money"];
}

// <-- BEGIN Lấy loại giao dịch -->
function sql_giao_dich($id)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM lich_su_giao_dich_loai WHERE id =:id");
    $stmt->execute(array(":id" => (int) $id));
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    return $member['name'];
}

// <-- END Lấy loại giao dịch -->
// <-- BEGIN Tính toán và xác nhận giao dịch, nếu chạy thẳng để xác nhận 8 -->
function lich_su_giao_dich($so_tien, $nguoi_chuyen, $nguoi_nhan, $loai_giao_dich, $ghi_chu, $mat_khau, $xac_nhan, $time_gd)
{
    global $conn;
    $ket_qua = 9;
    $ghi_chu = _sql01($ghi_chu);
    if (sql_member($nguoi_nhan, 'email') == '' and $ket_qua == 9) {
        $ket_qua = 'Người nhận không đúng';
    }
    if ($loai_giao_dich != 16) {
        if ($mat_khau != '') {
            if (sql_member($nguoi_chuyen, 'pass') != md5(md5($mat_khau)) and $ket_qua == 9) {
                $ket_qua = 'Mật khẩu không đúng';
            }
        }
        if ($nguoi_chuyen != 9) {
            if ((int) $so_tien < 0 and $ket_qua == 9) {
                $ket_qua = 'Số giao dịch không thể nhỏ hơn 0đ';
            }
            if (sql_member($nguoi_chuyen, 'money') < (int) $so_tien and $ket_qua == 9) {
                $ket_qua = 'Số dư không đủ thực hiện giao dịch';
            }
        }
    }
    if ($nguoi_nhan == $nguoi_chuyen and $ket_qua == 9) {
        $ket_qua = 'Người nhận không thể là chính bạn';
    }
    if ($xac_nhan != 8 and $ket_qua == 9) {
        $ket_qua = 'oke';
    }
    if ($ket_qua == 9) {
        //Thêm tổng số tiền đã nạp vào SEVER
        if ($loai_giao_dich == 1) {
            $tong_tien_nap = $so_tien;
        } else {
            $tong_tien_nap = 0;
        }
        // Trừ tiền của người gửi
        $sql = "UPDATE member SET money=money-$so_tien WHERE id='$nguoi_chuyen'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        // Cộng tiền cho người nhận.
        $sql = "UPDATE member SET money=money+$so_tien, tong_tien_nap=tong_tien_nap+$tong_tien_nap WHERE id='$nguoi_nhan'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        //Lưu trữ lịch sữ giao dịch
        $so_du_nguoi_chuyen = sql_member($nguoi_chuyen, 'money');
        $so_du_nguoi_nhan = sql_member($nguoi_nhan, 'money');
        $ngay_giao_dich = date('Ymd', $time_gd);
        $sql = "INSERT INTO `lich_su_giao_dich`(`id_chuyen`, `id_nhan`, `so_tien`, `so_du_chuyen`, `so_du_nhan`, `loai_giao_dich`, `ghi_chu`, `ngay_giao_dich`, `thoi_gian`) VALUES ($nguoi_chuyen,$nguoi_nhan,$so_tien,$so_du_nguoi_chuyen,$so_du_nguoi_nhan,$loai_giao_dich,'$ghi_chu',$ngay_giao_dich,$time_gd)";
        $conn->exec($sql);
    }
    return $ket_qua;
}
// <-- END Tính toán và xác nhận giao dịch -->
// <-- BEGIN Lấy thông tin member -->
function sql_member($id, $gia_tri)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM member WHERE id =:id");
    $stmt->execute(array(":id" => (int) $id));
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    return $member[$gia_tri];
}
// <-- END Lấy thông tin member -->
// <-- BEGIN Lấy thông tin member -->
function sql_data($id, $gia_tri, $table)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM $table WHERE id =:id");
    $stmt->execute(array(":id" => (int) $id));
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    return $member[$gia_tri];
}
// <-- END Lấy thông tin member -->
// <-- BEGIN Kiểm tra xem nó có tồn tại hay ko -->
function check_tt($sql, $value_1, $value_2)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM $sql WHERE $value_1=$value_2 LIMIT 1");
    $stmt->execute();
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($member['id'] > 0) {
        $ketqua = 1;
    } else {
        $ketqua = 0;
    }
    return $ketqua;
}
// <-- END Kiểm tra xem nó có tồn tại hay ko -->
function get_data($url, $cookie = null, $useragent = null)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM thu_vien_user_agent WHERE `ma` = 3 ORDER BY RAND() limit 1");
    $stmt->execute(array());
    $a2552 = $stmt->fetch(PDO::FETCH_ASSOC);
    $useragent_auto =  $a2552['user_agent'];
    $ch = @curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    $head[] = "Connection: keep-alive";
    $head[] = "Keep-Alive: 300";
    $head[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
    $head[] = "Accept-Language: en-us,en;q=0.5";
    $useragent = $useragent == null ? $useragent_auto : $useragent;
    curl_setopt($ch, CURLOPT_USERAGENT, $useragent);
    curl_setopt($ch, CURLOPT_ENCODING, '');
    curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Expect:'
    ));
    $page = curl_exec($ch);
    curl_close($ch);
    return $page;  //echo htmlentities($url);	
}
function token_eaag($trang_thai, $time_php, $update)
{
    global $conn;
    if ($update == "get") {
        $stmt1 =  $conn->prepare("SELECT * FROM token_eaag_facebook WHERE status=$trang_thai ORDER BY time_use ASC LIMIT 1"); //Có thể chọn RAND ()
        $stmt1->execute();
        $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
        $token = 'update';
        foreach ($list_code as $show_code) {
            $token =  $show_code['token_eaag'];
            $sql = "UPDATE token_eaag_facebook SET time_use='$time_php', use_num=use_num+1 WHERE id='" . $show_code['id'] . "'";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
        }
    } else {
        $sql = "UPDATE token_eaag_facebook SET time_use='$time_php', status=$trang_thai WHERE token_eaag='$update'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    }
    return $token;
}
function cookie_fb($subject)
{
    $c_user =  explode('c_user=', $subject);
    $c_user = explode(';', $c_user[1]);
    if ($c_user[0] < 1000) {
        return '';
    }
    $c_user = 'c_user=' . _sql01($c_user[0]) . ';';
    $xs =  explode('xs=', $subject);
    $xs = explode(';', $xs[1]);
    $xs = $xs[0] != '' ? 'xs=' . _sql01($xs[0]) . ';' : '';
    $fr =  explode('fr=', $subject);
    $fr = explode(';', $fr[1]);
    $fr = $fr[0] != '' ? 'fr=' . _sql01($fr[0]) . ';' : '';
    $datr =  explode('datr=', $subject);
    $datr = explode(';', $datr[1]);
    $datr = $datr[0] != '' ? 'datr=' . _sql01($datr[0]) . ';' : '';
    $sb =  explode('sb=', $subject);
    $sb = explode(';', $sb[1]);
    $sb = $sb[0] != '' ? 'sb=' . _sql01($sb[0]) . ';' : '';
    $cookie =  $c_user . '' . $xs . '' . $fr . '' . $datr . '' . $sb;
    return $cookie;
}

function load_page($url_page_load, $total_page, $limit, $total_records, $total_page_max)
{
    $url_page_load = explode('&page', $url_page_load);
    $url_page_load = $url_page_load[0];
    $html_pa_3 = $_GET['page'] - 3;
    $html_page_3 = $html_pa_3 > 0 ? '<li class="paginate_button page-item "><a href="' . $url_page_load . '&page=0" aria-controls="datatables-multi" data-dt-idx="2" tabindex="0" class="page-link">Đầu</a></li>' : '';
    $html_pa_2 = $_GET['page'] - 2;
    $html_page_2 = $html_pa_2 > 0 ? '<li class="paginate_button page-item "><a href="' . $url_page_load . '&page=' . $html_pa_2 . '" aria-controls="datatables-multi" data-dt-idx="2" tabindex="0" class="page-link">' . $html_pa_2 . '</a></li>' : '';
    $html_pa_1 = $_GET['page'] - 1;
    $html_page_1 = $_GET['page'] > 0 ? '<li class="paginate_button page-item "><a href="' . $url_page_load . '&page=' . $html_pa_1 . '" aria-controls="datatables-multi" data-dt-idx="2" tabindex="0" class="page-link">' . $html_pa_1 . '</a></li>' : '';
    $html_pas_1 = $_GET['page'] + 1;
    $html_pages_1 = $total_page > $html_pas_1 ? '<li class="paginate_button page-item "><a href="' . $url_page_load . '&page=' . $html_pas_1 . '" aria-controls="datatables-multi" data-dt-idx="2" tabindex="0" class="page-link">' . $html_pas_1 . '</a></li>' : '';
    $html_pas_2 = $_GET['page'] + 2;
    $html_pages_2 = $total_page > $html_pas_2 ? '<li class="paginate_button page-item "><a href="' . $url_page_load . '&page=' . $html_pas_2 . '" aria-controls="datatables-multi" data-dt-idx="2" tabindex="0" class="page-link">' . $html_pas_2 . '</a></li>' : '';
    $html_pas_3 = $_GET['page'] + 3;
    $html_pages_3 = $total_page > $html_pas_3 ? '<li class="paginate_button page-item "><a href="' . $url_page_load . '&page=' . $total_page_max . '" aria-controls="datatables-multi" data-dt-idx="2" tabindex="0" class="page-link">Cuối</a></li>' : '';
    echo '<div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="datatables-multi_info" role="status" aria-live="polite"><button type="button" id="show-popup" class="btn btn-round btn-info" data-toggle="modal" data-target="#edit_page">' . number_format($limit, 0) . ' hàng/trang</button> - tổng ' . number_format($total_records, 0) . ' hàng!</div></div>';
    if ($total_page_max > 0) {
        echo '
      <div class="col-sm-12 col-md-7 d-flex justify-content-end"><div class="dataTables_paginate paging_simple_numbers" id="datatables-multi_paginate">
      <ul class="pagination">
      ' . $html_page_3 . '
      ' . $html_page_2 . '
      ' . $html_page_1 . '
      <li class="paginate_button page-item "><button style="color:red;" data-toggle="modal" data-target="#set_page" aria-controls="datatables-multi" data-dt-idx="2" tabindex="0" class="page-link">' . (int) $_GET['page'] . '</button></li>
      ' . $html_pages_1 . '
      ' . $html_pages_2 . '
      ' . $html_pages_3 . '
      </ul>
      </div>
      ';
    }
    echo '</div></div>';
}
function load_dialog($total_page_max, $member_id)
{
    echo '
      <div class="modal fade" id="edit_page" role="dialog">
      <div class="modal-dialog">
      <div class="modal-content">
      <div class="modal-header bg_xanh_duong_nhat">
      <h2 style="text-align:center;">Số hàng trên mổi trang</h2>
      <button type="button" class="btn btn-outline-danger" data-dismiss="modal">&times;</button>
      </div>         
      <div class="modal-body">     <center>Cho phép trong khoảng 5 -> 200 hàng mổi trang</center>
      <form method="post" action="' . _sql01($_SERVER['REQUEST_URI']) . '">
      <div class="card-body">   
      <input type="number" min="5" max="200" step="5" class="form-control" name="number_page" value=""  placeholder="20">
      <input type="hidden" name="member" value="' . $member_id . '"></div>
      <div class="col-12 text-center">
      <button type="submit" name="post_limit_page" class="btn btn-block btn-flat btn-info">Xác nhận</button></form>
      </div>
      </div>     
      </div>
      </div>
      </div>            
      <div class="modal fade" id="set_page" role="dialog">
      <div class="modal-dialog">
      <div class="modal-content">
      <div class="modal-header bg_xanh_duong_nhat">
      <h2 style="text-align:center;">Chuyển nhanh đến số trang</h2>  
      <button type="button" class="btn btn-outline-danger" data-dismiss="modal">&times;</button>   </div>  
      <div class="modal-body">
      <form method="post" action="' . _sql01($_SERVER['REQUEST_URI']) . '">
      <div class="card-body">
      <input type="number" min="0" max="' . $total_page_max . '" step="1" class="form-control" name="go_to_page" value=""  placeholder="Tất cả có ' . $total_page_max . ' trang">  
      <input type="hidden" name="member" value="' . $member_id . '"><br>
      <button type="submit" name="post_url_load" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
      </div>
       </form>    
      </div>
      </div>
      </div>
      </div> <br>
        ';
}
// Xũ lý giá trị chuyển hàng - trang
if (isset($_POST['post_url_load'])) {
    $url_load = explode('&page=', $_SESSION['REQUEST_URI']);
    header('Location: ' . _sql01($url_load[0]) . '&page=' . _sql01($_POST['go_to_page']));
}
if (isset($_POST['post_limit_page'])) {
    $limit_new = (int) $_POST['number_page'];
    $nguoi_luu = (int) $_POST['member'];
    $sql = "UPDATE member SET limit_page=$limit_new WHERE id=$nguoi_luu";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $url_load = explode('&page=', $_SESSION['REQUEST_URI']);
    header('Location: ' . _sql01($url_load[0]) . '');
}

function info_vn_xa($id)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM vn_tinh_huyen_xa WHERE id =:id");
    $stmt->execute(array(":id" => (int) $id));
    $xa = $stmt->fetch(PDO::FETCH_ASSOC);
    $json['id_xa'] = $id;
    $json['id_huyen'] = $xa['id_huyen'];
    $json['id_tinh'] = $xa['id_tinh'];
    $json['name_xa'] = $xa['name'];
    $json['name_huyen'] = info_vn_huyen($xa['id_huyen']);
    $json['name_tinh'] = info_vn_tinh($xa['id_tinh']);
    $json['full_name'] = $xa['name'] . ', ' . $json['name_huyen'] . ', ' . $json['name_tinh'];

    return json_encode($json, JSON_UNESCAPED_UNICODE);
}

function info_vn_xa_v2($id)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM vn_tinh_huyen_xa WHERE id =:id");
    $stmt->execute(array(":id" => (int) $id));
    $xa = $stmt->fetch(PDO::FETCH_ASSOC);
    $json['id_xa'] = $id;
    $json['id_huyen'] = $xa['id_huyen'];
    $json['id_tinh'] = $xa['id_tinh'];
    $json['name_xa'] = $xa['name'];
    $json['name_huyen'] = info_vn_huyen($xa['id_huyen']);
    $json['name_tinh'] = info_vn_tinh($xa['id_tinh']);
    $json['full_name'] = $xa['name'] . ', ' . $json['name_huyen'] . ', ' . $json['name_tinh'];
    return $json['full_name'];
}

function info_vn_xa_v3($id)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM vn_tinh_huyen_xa WHERE id =:id");
    $stmt->execute(array(":id" => (int) $id));
    $xa = $stmt->fetch(PDO::FETCH_ASSOC);
    return $xa['full_info'];
}
function date_to_str($date)
{
    $date = (int)$date;
    if ($date < 10000000) {
        return "***";
    }
    $nam = substr($date, 0, 4);
    $thang =  substr($date, 4, 2);
    $ngay =  substr($date, 6, 8);
    return $ngay . '-' . $thang . '-' . $nam;
}
function info_vn_huyen($id)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM vn_tinh_huyen WHERE id =:id");
    $stmt->execute(array(":id" => (int) $id));
    $xa = $stmt->fetch(PDO::FETCH_ASSOC);
    return $xa['name'];
}

function info_vn_tinh($id)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM vn_tinh WHERE id =:id");
    $stmt->execute(array(":id" => (int) $id));
    $xa = $stmt->fetch(PDO::FETCH_ASSOC);
    return $xa['name'];
}

function info_mau_sac($id)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM vn_mau_sac WHERE id =:id");
    $stmt->execute(array(":id" => (int) $id));
    $xa = $stmt->fetch(PDO::FETCH_ASSOC);
    return $xa['name'];
}

function info_size($id)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id");
    $stmt->execute(array(":id" => (int) $id));
    $xa = $stmt->fetch(PDO::FETCH_ASSOC);
    return $xa['name_1'];
}

function convert_vi_to_en($str)
{
    $str = preg_replace("/(à|á|ạ|ả|ả|ã|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", "a", $str);
    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ệ|ể|ễ)/", "e", $str);
    $str = preg_replace("/(ì|í|ị|ỉ|ỉ|ĩ)/", "i", $str);
    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", "o", $str);
    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", "u", $str);
    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", "y", $str);
    $str = preg_replace("/(đ)/", "d", $str);
    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", "A", $str);
    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", "E", $str);
    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", "I", $str);
    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", "O", $str);
    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", "U", $str);
    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", "Y", $str);
    $str = preg_replace("/(Đ)/", "D", $str);
    return $str;
}

function check_string($key, $string)
{
    $dem = count(explode($key, $string));
    $kq = $dem > 1 ? $dem : 0;
    return $kq;
}

function check_nha_mang($sdt, $v = 0)
{
    $chuoi = (int)substr($sdt, 0, 4);
    $ar_sdt["296"] = "An Giang";
    $ar_sdt["254"] = "Bà Rịa - Vũng Tàu";
    $ar_sdt["209"] = "Bắc Cạn";
    $ar_sdt["204"] = "Bắc Giang";
    $ar_sdt["291"] = "Bạc Liêu";
    $ar_sdt["222"] = "Bắc Ninh";
    $ar_sdt["275"] = "Bến Tre";
    $ar_sdt["256"] = "Bình Định";
    $ar_sdt["274"] = "Bình Dương";
    $ar_sdt["271"] = "Bình Phước";
    $ar_sdt["252"] = "Bình Thuận";
    $ar_sdt["290"] = "Cà Mau";
    $ar_sdt["292"] = "Cần Thơ";
    $ar_sdt["206"] = "Cao Bằng";
    $ar_sdt["236"] = "Đà Nẵng";
    $ar_sdt["262"] = "Đắk Lắk";
    $ar_sdt["261"] = "Đắk Nông";
    $ar_sdt["215"] = "Điện Biên";
    $ar_sdt["251"] = "Đồng Nai";
    $ar_sdt["277"] = "Đồng Tháp";
    $ar_sdt["269"] = "Gia Lai";
    $ar_sdt["219"] = "Hà Giang";
    $ar_sdt["226"] = "Hà Nam";
    $ar_sdt["239"] = "Hà Tĩnh";
    $ar_sdt["220"] = "Hải Dương";
    $ar_sdt["225"] = "Hải Phòng";
    $ar_sdt["293"] = "Hậu Giang";
    $ar_sdt["218"] = "Hòa Bình";
    $ar_sdt["221"] = "Hưng Yên";
    $ar_sdt["258"] = "Khánh Hoà";
    $ar_sdt["297"] = "Kiên Giang";
    $ar_sdt["260"] = "Kon Tum";
    $ar_sdt["213"] = "Lai Châu";
    $ar_sdt["263"] = "Lâm Đồng";
    $ar_sdt["205"] = "Lạng Sơn";
    $ar_sdt["214"] = "Lào Cai";
    $ar_sdt["272"] = "Long An";
    $ar_sdt["228"] = "Nam Định";
    $ar_sdt["238"] = "Nghệ An";
    $ar_sdt["229"] = "Ninh Bình";
    $ar_sdt["259"] = "Ninh Thuận";
    $ar_sdt["210"] = "Phú Thọ";
    $ar_sdt["257"] = "Phú Yên";
    $ar_sdt["232"] = "Quảng Bình";
    $ar_sdt["235"] = "Quảng Nam";
    $ar_sdt["255"] = "Quảng Ngãi";
    $ar_sdt["203"] = "Quảng Ninh";
    $ar_sdt["233"] = "Quảng Trị";
    $ar_sdt["299"] = "Sóc Trăng";
    $ar_sdt["212"] = "Sơn La";
    $ar_sdt["276"] = "Tây Ninh";
    $ar_sdt["227"] = "Thái Bình";
    $ar_sdt["208"] = "Thái Nguyên";
    $ar_sdt["237"] = "Thanh Hóa";
    $ar_sdt["234"] = "Thừa Thiên - Huế";
    $ar_sdt["273"] = "Tiền Giang";
    $ar_sdt["294"] = "Trà Vinh";
    $ar_sdt["207"] = "Tuyên Quang";
    $ar_sdt["270"] = "Vĩnh Long";
    $ar_sdt["211"] = "Vĩnh Phúc";
    $ar_sdt["216"] = "Yên Bái";
    $check_mang = $ar_sdt[$chuoi] != "" ? $ar_sdt[$chuoi] : "0";
    if ($check_mang == "0") {
        $chuoi = (int)substr($sdt, 0, 3);
    }
    if ($chuoi == 28) {
        $check_mang = "Hồ Chí Minh";
    }
    if ($chuoi == 24) {
        $check_mang = "Hà Nội";
    }
    if ($check_mang == "0") {
        $mang_viettel = '86|96|97|98|32|33|34|35|36|37|38|39';
        $mang_mobi = '89|90|93|70|79|77|76|78';
        $mang_vina = '88|91|94|83|84|85|81|82';
        $mang_vietnamobile = '92|56|58';
        $mang_gmobile = '99|59';
        if ($v == 0) {
            $check_mang = check_string($chuoi, $mang_viettel) > 0 ? 'Viettel' : "0";
            if ($check_mang == "0") {
                $check_mang = check_string($chuoi, $mang_mobi) > 0 ? 'Mobifone' : "0";
            }
            if ($check_mang == "0") {
                $check_mang = check_string($chuoi, $mang_vina) > 0 ? 'Vinaphone' : "0";
            }
            if ($check_mang == "0") {
                $check_mang = check_string($chuoi, $mang_vietnamobile) > 0 ? 'Vietnamobile' : "0";
            }
            if ($check_mang == "0") {
                $check_mang = check_string($chuoi, $mang_gmobile) > 0 ? 'Gmobile' : "0";
            }
            if ($check_mang == "0") {
                $check_mang = 'Nhà mạng';
            }
        } else {
            $check_mang = check_string($chuoi, $mang_viettel) > 0 ? 'Viettel' : "0";
            if ($check_mang == "0") {
                $check_mang = check_string($chuoi, $mang_mobi) > 0 ? 'Mobi' : "0";
            }
            if ($check_mang == "0") {
                $check_mang = check_string($chuoi, $mang_vina) > 0 ? 'Vina' : "0";
            }
            if ($check_mang == "0") {
                $check_mang = check_string($chuoi, $mang_vietnamobile) > 0 ? 'Vietnamobi' : "0";
            }
            if ($check_mang == "0") {
                $check_mang = check_string($chuoi, $mang_gmobile) > 0 ? 'Gmobi' : "0";
            }
            if ($check_mang == "0") {
                $check_mang = 'Ko biết';
            }
        }
    }
    if (strlen($sdt) != 10) {
        $check_mang = "ERROR";
    }
    return $check_mang;
}

function dem_nguoc_url($url, $ly_do, $chu_y, $so_giay = 5)
{
    echo '
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
        <script>
            $(document).ready(function() {
                var interval;
                $("#processbar").val("0");
                $("#count-down").html("' . $so_giay . '");
                clearInterval(interval);
                interval = null;
                var count = ' . $so_giay . ';

                function loadlink() {
                    window.location = "' . $url . '";
                }
                interval = setInterval(function() {
                    count--;
                    if (count === 0) {
                        count = ' . $so_giay . ';
                        loadlink()
                    }
                    $("#count-down").html(count);
                    $("#processbar").val(' . $so_giay . ' - count);
                }, 1000);
            });
        </script>
        <center>
            <title>error</title>
            <div class="content"><span class="has-text-grey is-size-7">' . $ly_do . '<br><br>
            Chuyển hướng sau: <span id="count-down">' . $so_giay . 's</span> giây</span><br>
                <progress value="0" max="' . $so_giay . '" id="processbar"></progress></div>
            ' . $chu_y . '
        </center>';
    exit;
}
function _check_sdt($sdt, $i = 10)
{
    $t1 = str_replace(array("o", "O"), "0", strtolower(convert_vi_to_en($sdt)));
    $pattern = '/[0-9]{' . $i . '}/';
    if (preg_match($pattern, $t1, $matches)) {
        $report =  $matches[0];
    } else {
        $report =  '0';
    }

    if ($report == '0') {
        $t1 = str_replace(array(".", ",", " ", "-"), "", $sdt);
        $pattern = '/[0-9]{' . $i . '}/';
        if (preg_match($pattern, $t1, $matches)) {
            $report =  $matches[0];
        } else {
            $report =  '0';
        }
    }
    return $report;
}

function check_ma_hang($id, $gia_tri)
{
    global $conn;
    $gia_tri = _sql01($gia_tri);
    $stmt =  $conn->prepare("SELECT * FROM vn_san_pham WHERE id =:id");
    $stmt->execute(array(":id" => (int)$id));
    $sanp = $stmt->fetch(PDO::FETCH_ASSOC);
    return $sanp[$gia_tri];
}

function sql_kho_hang($mau, $size, $ma_hang, $id_team, $xuat = 1)
{
    global $conn;
    $don_vi = $xuat == 1 ? -1 : 1;
    $arr_ms = explode('|', str_replace('0|', '', $mau));
    $arr_sz = explode('|', str_replace('0|', '', $size));
    for ($a = 0; $a <= count($arr_ms); $a++) {
        if ($arr_ms[$a] > 0 and $arr_sz[$a] > 0) {
            $mau_sac = (int)$arr_ms[$a];
            $size_a = (int)$arr_sz[$a];
            // --> Begin: Dữ liệu kho chính
            $stmt =  $conn->prepare("SELECT * FROM vn_ton_kho WHERE team=$id_team AND ma_hang =$ma_hang AND mau_sac=$mau_sac AND size=$size_a");
            $stmt->execute();
            $kho_hang = $stmt->fetch(PDO::FETCH_ASSOC);
            $id_kho = (int)$kho_hang['id'];
            if ($id_kho > 0) {
                $sql = "UPDATE vn_ton_kho SET so_luong=so_luong+$don_vi WHERE id=$id_kho";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            } else {
                $sql = "INSERT INTO `vn_ton_kho`(`team`, `ma_hang`, `mau_sac`, `size`, `so_luong`) VALUES ($id_team,$ma_hang,$mau_sac,$size_a,$don_vi)";
                $conn->exec($sql);
            }
            // --> End: Dữ liệu kho chính

            // --> Begin: Thêm vào kho dự phòng để tính tỷ lệ %
            $stmt1 =  $conn->prepare("SELECT * FROM vn_hang_kho_add WHERE team=$id_team AND ma_hang =$ma_hang AND mau_sac=$mau_sac AND size=$size_a");
            $stmt1->execute();
            $kho_hang_add = $stmt1->fetch(PDO::FETCH_ASSOC);
            $id_kho_add = (int)$kho_hang_add['id'];
            if ($id_kho_add > 0) {
                $sql = "UPDATE vn_hang_kho_add SET so_luong=so_luong+$don_vi WHERE id=$id_kho_add";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            } else {
                $sql = "INSERT INTO `vn_hang_kho_add`(`team`, `ma_hang`, `mau_sac`, `size`, `so_luong`) VALUES ($id_team,$ma_hang,$mau_sac,$size_a,$don_vi)";
                $conn->exec($sql);
            }
            // --> End: Thêm vào kho dự phòng để tính tỷ lệ %
        }
    }
}

function sql_level($id, $gia_tri)
{
    global $conn;
    $gia_tri = _sql01($gia_tri);
    $stmt =  $conn->prepare("SELECT * FROM chuc_vu WHERE id =:id");
    $stmt->execute(array(":id" => (int)$id));
    $sanp = $stmt->fetch(PDO::FETCH_ASSOC);
    return $sanp[$gia_tri];
}

function sql_sale($team, $gia_tri)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM `vn_gia_sale` WHERE `team`='$team' AND `name`='$gia_tri'");
    $stmt->execute();
    $sanp = $stmt->fetch(PDO::FETCH_ASSOC);
    return $sanp['money'];
}
function i_eaag($token = '0')
{
    if ($token == '0') {
        $token = "EAAGNO4a7r2wBAMHGD37V5s9ZCPAfv6ToJ3ZAXZCr96GSE1BEBXdSdn2XILyRlnENv9UovXSKfyL26R3U5JRdvMTVNbDYjMPyMRZBLUXZCW71Fxc47gGWyiFd34p1gZAZCSzp0D3ctCHZBllEZCgTTAloUlr2cd4p6FTPE5koFYrhQhiUS7hHVD7sd";
    }
    return $token;
}

function conten_nd_chat($nd_show)
{
    if ((int) stripos('a' . $nd_show, '[/img]') > 0) {
        if ((int) stripos('a' . $nd_show, '.mp4') > 0) {
            $nd_show = str_replace('[img]', '<audio controls><source src="', $nd_show);
            $nd_show = str_replace('[/img]', '" type="audio/ogg"></audio>', $nd_show);
        } else {
            $nd_show = str_replace('[img]', '<img src="', $nd_show);
            $nd_show = str_replace('[/img]', '">', $nd_show);
        }
    }
    return $nd_show;
}
function new_mess_c($team)
{
    global $conn;
    $sqlAll = "SELECT COUNT(`mem_chot`) FROM `vn_log_chat_id_fanpage` WHERE team=$team AND `mem_chot`=0";
    $stmt = $conn->query($sqlAll);
    $aja = (int)$stmt->fetchColumn();
    return '<h4><a href="/chat_bot_api&get_mess">New (' . $aja . ')</a></h4>';
}

function name_sanpham_01($id)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM sale_sanpham_01 WHERE id =:id");
    $stmt->execute(array(":id" => (int) $id));
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    return $member['name'];
}

function sql_sanpham_02($id, $gia_tri)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM sale_sanpham_02 WHERE id =:id");
    $stmt->execute(array(":id" => (int) $id));
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    return $member[$gia_tri];
}

function sql_web($id, $table, $gia_tri)
{
    global $conn;
    $string  = "SELECT * FROM $table WHERE id = ";
    $stmt =  $conn->prepare("SELECT * FROM $table WHERE id =:id");
    $stmt->execute(array(":id" => (int) $id));
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    // echo $string;
    // echo $id;
    // echo "<br>";
    return $member[$gia_tri];
}
function check_ten_combo($id_combo)
{
    global $conn;
    $stmt1 =  $conn->prepare("SELECT * from sale_sanpham_combo WHERE id_combo = '$id_combo' ");
    $stmt1->execute();
    // $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    return $stmt1->fetchALL(PDO::FETCH_ASSOC);
}
function get_data_sale_02($id)
{
    global $conn;
    $stmt1 =  $conn->prepare("SELECT * from gia_xuat_nhap where id = $id  ORDER by id desc limit 1");
    $stmt1->execute();
    return $stmt1->fetch();
}
function kho_update()
{
    global $conn;
    $time_check = 0;
    $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`sl` ) as `total_sl`, sum(`a`.`tien` ) as `total_tien` 
    FROM `sale_sanpham_04` AS  `a` 
    WHERE `time_num`>$time_check GROUP BY  `sp_02`  ORDER BY `total_sl` DESC");
    $stmt1->execute(array());
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $show_code) {
        $ma_kem = (int)$show_code['sp_02'];
        $tong_sl = (int)$show_code['total_sl'];
        // kiểm tra đúng sẽ là một sản phẩm bình thường
        $data_combo = check_ten_combo($ma_kem);
        if(count($data_combo)==0)
        {
            $id_san_pham = (int)$show_code['sp_01'];
            $tong_tien = (int)$show_code['total_tien'];
            $stmt =  $conn->prepare("SELECT * FROM kho_ban_ra WHERE ma_kem=$ma_kem");
            $stmt->execute();
            $asan_pham = $stmt->fetch(PDO::FETCH_ASSOC);
            $ia_sp = (int)$asan_pham['id'];
            if ($ia_sp > 0) 
            {
                $sql = "UPDATE kho_ban_ra SET so_luong='$tong_sl', tong_tien_ban='$tong_tien' WHERE id='$ia_sp'";
                // echo $sql."<br>"."===================="."<br>";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            } else {
                $sql = "INSERT INTO `kho_ban_ra`(`san_pham`, `ma_kem`, `so_luong`, `tong_tien_ban`) VALUES ($id_san_pham,$ma_kem,$tong_sl,$tong_tien)";
                $conn->exec($sql);
            }
        }else
        {
            // xử lý nếu là một combo
            foreach ($data_combo as $combo_code) {
                $id_sp_sale_02 = $combo_code["id_sp_sale_02"];
                $stmt1_get =  $conn->prepare("SELECT * , sum(`a`.`sl` ) as `total_sl`, sum(`a`.`tien` ) as `total_tien` 
                FROM `sale_sanpham_04` AS  `a` 
                WHERE `time_num`>$time_check and `sp_02` =  $id_sp_sale_02 GROUP BY  `sp_02`  ORDER BY `total_sl` DESC");
                $stmt1_get->execute();
                // =================================
                $tong_sl_else = (int)$stmt1_get->fetch()['total_sl'] +$tong_sl;
                $id_san_pham_else = (int)$stmt1_get->fetch()['sp_01'];
                $gia_xuat_sp = get_data_sale_02($id_sp_sale_02)["gia_xuat"]*$tong_sl;
                $tong_tien_else = (int)$stmt1_get->fetch()['total_tien']+$gia_xuat_sp;
                // echo $tong_sl_else." ".$tong_tien_else;
                // =====================================
                $stmt =  $conn->prepare("SELECT * FROM kho_ban_ra WHERE ma_kem=$id_sp_sale_02");
                $stmt->execute();
                $asan_pham = $stmt->fetch(PDO::FETCH_ASSOC);
                $ia_sp = (int)$asan_pham['id'];
                if ($ia_sp > 0) 
                {
                    $sql = "UPDATE kho_ban_ra SET so_luong='$tong_sl_else', tong_tien_ban='$tong_tien_else' WHERE id='$ia_sp'";
                    // echo $sql;
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                } else
                {
                    $sql = "INSERT INTO `kho_ban_ra`(`san_pham`, `ma_kem`, `so_luong`, `tong_tien_ban`) 
                    VALUES ($id_san_pham_else,$id_sp_sale_02,$tong_sl_else,$tong_tien_else)";
                    $conn->exec($sql);
                }
            }
        }
        
    }
    $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`so_luong`) as `total_sl`, sum(`a`.`tong_tien_ban`) as `total_tien` FROM `kho_ban_ra` AS  `a` GROUP BY  `ma_kem`  ORDER BY `id` ASC");
    $stmt1->execute(array());
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $show_fp) {
        $json_br_sl[$show_fp['ma_kem']] = (int)$show_fp['total_sl'];
        $json_br_tien[$show_fp['ma_kem']] = (int)$show_fp['total_tien'];
    }

    $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`so_luong`) as `total_sl`, sum(`a`.`tien_nhap`) as `total_tien` FROM `kho_nhap_vao` AS  `a` GROUP BY  `ma_kem`  ORDER BY `id` ASC");
    $stmt1->execute(array());
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $show_fp) {
        $json_nv_sl[$show_fp['ma_kem']] = (int)$show_fp['total_sl'];
        $json_nv_tien[$show_fp['ma_kem']] = (int)$show_fp['total_tien'];
    }
    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_02 ORDER BY id ASC"); //Có thể chọn RAND ()
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $show_fp) {
        if(count($data_combo)==0)
        {
            $ton_sl = $json_nv_sl[$show_fp['id']] - $json_br_sl[$show_fp['id']];
            $ton_tien = $json_br_tien[$show_fp['id']] - $json_nv_tien[$show_fp['id']];
            $stmt =  $conn->prepare("SELECT * FROM kho_update WHERE id =:id");
            $stmt->execute(array(":id" => (int)$show_fp['id']));
            $member = $stmt->fetch(PDO::FETCH_ASSOC);
            $id_ma = (int)$member['id'];
            if ($id_ma > 0) {
                $sql = "UPDATE kho_update SET sl='$ton_sl', tien='$ton_tien' WHERE id='" . $id_ma . "'";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            } else {
                $sql = "INSERT INTO `kho_update`(`id`, `sl`, `tien`) VALUES ($id_ma,$ton_sl,$ton_tien)";
                $conn->exec($sql);
            }
        }
    }
}

function sql_update($table, $value, $where)
{
    global $conn;
    try {
        $sql = "UPDATE $table SET $value WHERE $where";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return 'oke';
    } catch (Exception $e) {
        return 'error';
    }
}
function dem_so_ngay($bt, $kt)
{
    $a1 = explode('-', $bt);
    $a1t = mktime(0, 0, 0, $a1[1], $a1[2], $a1[0]);
    $a2 = explode('-', $kt);
    $a2t = mktime(0, 0, 0, $a2[1], $a2[2], $a2[0]);
    if ($a2t > 1000000) {
        $i_date = ($a2t - $a1t) / (60 * 60 * 24) + 1;
    } else {
        $i_date = 0;
    }
    return $i_date;
}
function get_id_nhap_xuat()
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM gia_nhap_xuat ORDER BY id DESC LIMIT 1");
    $stmt->execute();
    return $stmt->fetch()["id"];
}
// lấy số lần nhập giá tiền băng id sản phẩm
function get_so_lan_by_id($id)
{
    global $conn;
    $stmt1 =  $conn->prepare("SELECT * from gia_xuat_nhap WHERE id = $id");
    $stmt1->execute();
    return $stmt1->fetch()["so_lan"];
}
// tham số truyền vào là 1 id của sản pphaamr
function get_id_so_lan_max($id)
{
    global $conn;
    $stmt1 =  $conn->prepare("SELECT * from gia_xuat_nhap where id_sp = $id ORDER BY so_lan desc limit 1");
    $stmt1->execute();
    return $stmt1->fetch();
}
function get_data_by_id_gia_nhap_xuat($id)
{
    global $conn;
    $stmt1 =  $conn->prepare("SELECT * from gia_xuat_nhap where id = $id ORDER BY so_lan desc limit 1");
    $stmt1->execute();
    return $stmt1->fetch();
}
function get_max_id_sale_sp_2()
{
    
    global $conn;
    $stmt1 =  $conn->prepare("SELECT * from sale_sanpham_02 ORDER by id desc limit 1");
    $stmt1->execute();
    return $stmt1->fetch()["id"];
}
function insert_gia_xuat_nhap($id_sp,$gia_xuat,$gia_nhap,$so_lan)
{
    global $conn;
    $sql = "INSERT INTO `gia_xuat_nhap`(`id_sp`,`gia_xuat`,`gia_nhap`,`so_lan`) VALUES ($id_sp,$gia_xuat,$gia_nhap,$so_lan)";
    $conn->exec($sql);
}
function check_admin($email)
{
    if($email == "congnt2989@gmail.com")
    {
        return true;
    }else{
        return false;
    }
}
function get_chot_xit($id_chot_xit)
{
    global $conn;
    $stmt1 =  $conn->prepare("SELECT * from admin_ly_do_chot_xit where id = $id_chot_xit");
    $stmt1->execute();
    return $stmt1->fetch()["name"];
}
function get_list_code_xuat($time_php,$data_time)
{
    global $conn;
    $num =0;
    $stmt1 =  $conn->prepare("SELECT * FROM `sale_sanpham_03_xuat` where $data_time  ORDER by id desc");
    $stmt1->execute(array());
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $show_fp) 
    {
        
        if($data_time == "1=1 AND 1=1")
        {
            $ngay_hien_tai = date('d-m-Y', $time_php);
            $ngay_them = date('d-m-Y', $show_fp["time_xuat"]);
            if($ngay_hien_tai - $ngay_them <= 1)
            {
                $data[$num] = $show_fp["code_xuat"];
                $num++;
            }else
            {
                break;
            }
        }else{
            $data[$num] = $show_fp["code_xuat"];
            $num++;
        }
    }
    return $data;

}
function convert_to_date_int($data)
{
  $year =  substr( $data,  0, 4);
  $month = substr( $data,  4, 2);
  $day = substr( $data,  6, 2);
  return  $datetime =  $year."/".$month."/".$day;
//   return $timestamp = strtotime($datetime);
}
