<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>TRÌNH TẠO MÃ 2FA</title>
      <link rel="stylesheet" href="./css/bulma-0.7.1.min.css">
      <style>
         @media screen and (min-width: 1068px) {
         .container {
         max-width: 600px;
         width: 600px;
         }
         }
      </style>
      <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
	  <script>
			$(document).ready(function () {
				var interval;
				$("#btn-save").click(function(){
					$("#processbar").val("0");
					$("#count-down").html("30");
					clearInterval(interval);   
					interval = null;
					var count = 30;
					function loadlink(){
						$('#res-code').load('res.php', {
							   secrect: $("#secrect-key").val()
						});
					 }
					 loadlink();
					 interval = setInterval(function(){
						count--;
						if (count === 0) 
						{
							count = 30;
							loadlink()
						}
						$("#count-down").html(count);
						$("#processbar").val(30 - count);
					 }, 1000);
				});
			});
			</script>
      <script>
         
          
      </script>
   </head>
   <body>
      <section id="app" class="section">
         <div class="container">
            <h1 class="title">TRÌNH TẠO MÃ 2FA</h1>
            <div class="field">
               <label class="label is-uppercase">Code 2FA</label> 
               <div class="control"><input type="text" id="secrect-key" placeholder="AAAA BBBB CCCC D63M V7U4 VT2S EDVA XXXX" class="input" value=""></div>
			   <div class="control"><input type="button" id="btn-save" class="button" value="Lấy mã"></div>
            </div>
            <div class="content"><span class="has-text-grey is-size-7">Hết hạn sau: <span id="count-down">30s</span> giây</span> <progress value="20" max="30" id="processbar" class="progress is-info is-small"></progress></div>
            <div class="box">
               <p id="res-code" class="title is-size-1 has-text-centered">...</p>
            </div>
            <div class="field" style="visibility: hidden;">
               <label class="label is-uppercase">Số ký tự</label> 
               <div class="control"><input type="text" placeholder="Usually 6" class="input"></div>
            </div>
            <div class="field" style="visibility: hidden;">
               <label class="label is-uppercase">Thời gian khả dụng</label> 
               <div class="control"><input type="text" placeholder="Usually 30" class="input"></div>
            </div>
         </div>
      </section>
   </body>
</html>