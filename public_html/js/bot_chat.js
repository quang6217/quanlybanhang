myFunction_hide_leftmenu();
function myFunction_hide_leftmenu() {
    var element = document.getElementsByClassName("sidebar sidebar-sticky")[0];
    element.setAttribute("class", "sidebar sidebar-sticky d-none");
}
function cuon_chat() {
    var element = document.getElementsByClassName("msg_history")[0];
    element.scrollTop = element.scrollHeight;
}
$(".sidebar-toggle.d-flex.mr-2").click(function () {
    var element = document.getElementsByClassName("sidebar sidebar-sticky d-none")[0];
    element.setAttribute("class", "sidebar sidebar-sticky");
});
window.onload = function () {
    //  myFunction_hide_leftmenu();
    cuon_chat();

};

function load_huyen(id_huyen) {
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var html = xmlhttp.responseText.trim();
            document.getElementById("vn_huyen").innerHTML = html;
        }
    };
    xmlhttp.open("GET", "don_hang&huyen=" + id_huyen, true);
    xmlhttp.send();
}

function my_tinh() {
    var x = document.getElementById("vn_tinh");
    var i = x.selectedIndex;
    load_huyen(x.options[i].value);
}

function load_xa(id_xa) {
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var html = xmlhttp.responseText.trim();
            document.getElementById("vn_xa").innerHTML = html;
        }
    };
    xmlhttp.open("GET", "don_hang&xa=" + id_xa, true);
    xmlhttp.send();
}

function my_huyen() {
    var x = document.getElementById("vn_huyen");
    var i = x.selectedIndex;
    load_xa(x.options[i].value);
}

function load_ma_hang(id_mahang) {
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var html_full = xmlhttp.responseText.trim();
            var arr = html_full.split("|");
            document.getElementById("vn_mau_sac").innerHTML = arr[0];
            document.getElementById("vn_size").innerHTML = arr[1];
            document.getElementById("tong_tien").value = arr[2];
            document.getElementById("gia_tien_cd").value = arr[2];
            document.getElementById("name_sp").value = arr[3];
            document.getElementById("sl_mua").value = 1;
        }
    };
    if (id_mahang > 999 && id_mahang < 10000) {
        xmlhttp.open("GET", "don_hang&sp=" + id_mahang, true);
        xmlhttp.send();
    }

}

function sc_ma_hang() {
    load_ma_hang(document.getElementById("id_ma_hang").value);
}

function edit_sl1() {
    var gia_tien = document.getElementById("gia_tien_cd").value;
    var sl_mua = document.getElementById("sl_mua").value;
    if (sl_mua < 1) {
        return;
    }
    var don_gia = gia_tien * sl_mua;
    document.getElementById("tong_tien").value = don_gia;
    var html_mau_sac = document.getElementById("html_mau_sach").innerHTML.split("</select>")[0] + "</select>"; // 2 - 3
    var html_size = document.getElementById("html_size_a").innerHTML.split("</select>")[0] + "</select>";
    var html_in_mau_sac = "";
    var html_in_size_1 = "";
    if (sl_mua > 10) {
        sl_mua = 10;
    }
    for (i = 0; i < sl_mua; i++) {
        html_in_size_1 = html_in_size_1 + html_size;
        html_in_mau_sac = html_in_mau_sac + html_mau_sac;
        var i1 = i - 1;
        var i2 = i + 1;
        html_mau_sac = html_mau_sac.replace("name=\"mau_sac\"", "name=\"mau_sac_" + i + "\"");
        html_mau_sac = html_mau_sac.replace("name=\"mau_sac_" + i1 + "\"", "name=\"mau_sac_" + i + "\"");
        html_size = html_size.replace("name=\"size\"", "name=\"size_" + i + "\"");
        html_size = html_size.replace("name=\"size_" + i1 + "\"", "name=\"size_" + i + "\"");
    }
    document.getElementById("html_size_a").innerHTML = html_in_size_1;
    document.getElementById("html_mau_sach").innerHTML = html_in_mau_sac;
}

function check_dia_chi(dia_chi) {
    var post_dc = encodeURI("dia_chi=" + dia_chi);
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        var html = xmlhttp.responseText.trim();
        var post_id = html[1];
        var post_string = html[0];
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            if (html.length > 10) {
                document.getElementById("dia_chi_giao_hang").innerHTML = html;
            }
        }
    };
    xmlhttp.open("POST", "don_hang", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(post_dc);
}

function dx_dia_chi() {
    var dia_chi = document.getElementById("goi_y_dia_chi").value;
    if (dia_chi.length > 4) {
        check_dia_chi(dia_chi);
    }
}

function load_full_info(id_mahang) {
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var html_full = xmlhttp.responseText.trim();
            var arr = html_full.split("|");
            document.getElementById("vn_tinh").innerHTML = arr[0];
            document.getElementById("vn_huyen").innerHTML = arr[1];
            document.getElementById("vn_xa").innerHTML = arr[2];
            document.getElementById("dia_chi_giao_hang").innerHTML = "";
            document.getElementById("goi_y_dia_chi").value = arr[3];
        }
    };
    xmlhttp.open("GET", "don_hang&full_xa=" + id_mahang, true);
    xmlhttp.send();
}

function my_p_dia_chi() {
    var id_xa = document.getElementById("id_p_dia_chi").value;
    load_full_info(id_xa);
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function show_hiden_p() {
    var cookie_don_hang = "<?= $_COOKIE['don_hang'] ?>";
    if (cookie_don_hang == "show") {
        setCookie("don_hang", "off", 10);
    } else {
        setCookie("don_hang", "show", 10);
    }
}

function check_sdt_2(sdt_get) {
    var xmlhttp;
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var html_full = xmlhttp.responseText.trim();
            if (html_full != '') {
                var arr = html_full.split("|");
                document.getElementById("id_a_namekh").value = arr[0];
                document.getElementById("id_address_detail").value = arr[1];
                document.getElementById("goi_y_dia_chi").value = arr[2];
                document.getElementById("vn_xa").innerHTML = arr[3];
                document.getElementById("id_button_td").innerHTML = arr[4];
                document.getElementById("vn_huyen").innerHTML = arr[5];
                document.getElementById("vn_tinh").innerHTML = arr[6];
                document.getElementById("sl_mua").value = "";
                document.getElementById("button_xn_k").innerHTML = "Xác nhận không trùng (khách cần thêm đơn)";
                document.getElementById("id_edit_ghi_chu").innerHTML = "<textarea class=\"form-control\" rows=\"4\" name=\"ghi_chu\" placeholder=\"Mô tả đơn hàng (ghi chú)\">Khách hàng đã có lịch sữ mua hàng, cẩn thận kẻo trùng đơn</textarea>";
            }
        }
    };
    xmlhttp.open("GET", "don_hang&chdh_sdt=" + sdt_get, true);
    xmlhttp.send();
}

function check_sdt() {
    var sdt_get = document.getElementById("id_c_sdta").value;
    if (sdt_get.length > 8) {
        check_sdt_2(sdt_get);
    }
}
