var vitri_chuot_id = "";

function my_vitri() {
    var x = document.activeElement.id;
    vitri_chuot_id = x;
}
document.addEventListener('keydown', function(event) {
    if ((event.which == 13 || event.keyCode == 13) && vitri_chuot_id == "sms_can_sent") {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        var info = document.getElementById("info_can_sent").value;
        if (keycode == '13') {
            var a_info = info.split(',');
            sent_mess(a_info[0], a_info[1], a_info[2]);
        }
    }
}, false);

function load_from_web() {
    var position = window.location.href.lastIndexOf('mess_ad');
    if (position > 0) { position = 99; } else { position = 0; }
    var info = document.getElementById("info_can_sent").value;
    var a_info = info.split(',');
    var post_data = encodeURI("view_mess_post=" + a_info[0] + "&uid_facebook=" + a_info[2] + "&position=" + position);
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        var html = xmlhttp.responseText.trim().split('||||');
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            try { document.getElementById("list_sms_mess").innerHTML = html[1]; } catch {}
            try {
                var time_chat_log_reload = document.getElementById("time_chat_log_reload").value;
                var ss_aa = html[0].lastIndexOf(time_chat_log_reload);
                if (ss_aa < 10 || time_chat_log_reload == "") {
                    document.getElementById("view_chat_log").innerHTML = html[0];
                    cuon_chat();
                }
            } catch {
                document.getElementById("view_chat_log").innerHTML = html[0];
                cuon_chat();
            }
            try { document.getElementById("check_sum_ne_mess").innerHTML = html[2]; } catch {}
        }
    };
    xmlhttp.open("POST", "000_api", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(post_data);
}
setInterval(function() {
    load_from_web();
}, 5000);

function sent_mess(uid, uid_fan, uid_mem) {
    var nd_sms = document.getElementById("sms_can_sent").value;
    var post_data = encodeURI("uid_fanpage=" + uid_fan + "&uid_facebook=" + uid_mem + "&uid=" + uid + "&mess_post=" + nd_sms);
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        var html = xmlhttp.responseText.trim();
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            document.getElementById("view_chat_log").innerHTML = html;
            cuon_chat();
        }
    };
    xmlhttp.open("POST", "000_api", true);
    document.getElementById("sms_can_sent").value = "";
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(post_data);
}

function view_chat(id_fan, id_mem, full_name, sdt, uid, id_don_hang, cc_ma_hang) {
    var position = window.location.href.lastIndexOf('mess_ad');
    if (position > 0) { window.history.pushState('a', 'b', '/mess_ad&uid=' + uid); } else { window.history.pushState('a', 'b', '/mess_nv&uid=' + uid); }
    document.getElementById("id_address_detail").value = "";
    document.getElementById("goi_y_dia_chi").value = "";
    document.getElementById("id_edit_ghi_chu").innerHTML = "<textarea class=\"form-control\" rows=\"4\" name=\"ghi_chu\" placeholder=\"Mô tả đơn hàng (ghi chú)\"></textarea>";
    var post_dc = encodeURI("uid_fanpage=" + id_fan + "&uid_facebook=" + id_mem + "&uid=" + uid + "&id_don_hang=" + id_don_hang);
    document.getElementById("vn_huyen").innerHTML = "";
    document.getElementById("vn_xa").innerHTML = "";
    document.getElementById("id_check_mang_v2").value = "Nhà mạng";
    document.getElementById("button_xn_k").innerHTML = "Xác nhận";
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        var html = xmlhttp.responseText.trim();
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            var i_html = html.split("|||||");
            document.getElementById("view_chat_log").innerHTML = i_html[0];
            document.getElementById("don_hang_vua_len").innerHTML = i_html[1];
            document.getElementById("html_sent_sms").innerHTML = i_html[2];
            document.getElementById("id_a_namekh").value = full_name;
            document.getElementById("id_sc_linkFB").value = uid + "|" + id_fan + "|" + id_mem;
            if (sdt > 1000) {
                document.getElementById("id_c_sdta").value = '0' + sdt;
                check_sdt_2('0' + sdt);
            } else { document.getElementById("id_c_sdta").value = ''; }
            if (cc_ma_hang > 999) {
                document.getElementById("id_ma_hang").value = cc_ma_hang;
                load_ma_hang(cc_ma_hang);
            }
            cuon_chat();
        }
    };
    xmlhttp.open("POST", "000_api", true);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(post_dc);
}

function cuon_chat() {
    var element = document.getElementsByClassName("msg_history")[0];
    element.scrollTop = element.scrollHeight;
}
window.onload = function() {
    cuon_chat();
};
$(document).on('click', '.goi_y_btn', function() {
    var _bubble = $(this).parent().find('.bubble');
    if (_bubble.is(':visible')) {
        $(this).parent().find('.bubble').slideToggle(100);
    } else {
        $('.bubble').hide();
        $(this).parent().find('.bubble').slideToggle(100);
    }
});
$(document).on('click', '.note_chat', function() {
    var _bubble = $(this).parent().find('.bubble');
    if (_bubble.is(':visible')) {
        $(this).parent().find('.bubble').slideToggle(100);
    } else {
        $('.bubble').hide();
        $(this).parent().find('.bubble').slideToggle(100);
    }
});

function goi_y_chat(e) {
    document.getElementById("sms_can_sent").value = e.text;
}

function abc(e) {
    e.setAttribute("style", "background-color: #f3f7f7!important");
}
$(".msg_history").click(function() {
    $('.bubble').hide();
});
$(".card").click(function() {
    $('.bubble').hide();
});
$(".inbox_chat").click(function() {
    $('.bubble').hide();
});
$(".write_msg").click(function() {
    $('.bubble').hide();
});