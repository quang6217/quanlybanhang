
var vitri_chuot_id = "";
var html_js = "";
function my_vitri() {
        var x = document.activeElement.id;
        vitri_chuot_id = x;
}
function load_html(data, url_post) {
        var post_dc = encodeURI(data);
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            var html = xmlhttp.responseText.trim();
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                html_js = html;
        }
    };
    xmlhttp.open("POST", url_post, false);
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlhttp.send(post_dc);
}
document.addEventListener('keydown', function(event) {
        if ((event.which == 13 || event.keyCode == 13) && vitri_chuot_id == "search_sp_001") {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                var a01 = document.getElementById("search_sp_001").value;
            a01 = a01.trim().replace(/\s+/g, ' ');
            document.getElementById("search_sp_001").value = a01;
            if (a01.length > 0) {
                    load_html("input_001=" + a01, "js_047");
                document.getElementById("search_sp_002").innerHTML = html_js;
            } else {
                    document.getElementById("search_sp_002").innerHTML = "";
            }
        }
    }
}, false);
function isEmpty(val) {
        return (val === undefined || val == null || val.length <= 0) ? true : false;
}
/////quynh
// ************************************************
// Shopping Cart API
// ************************************************
var shoppingCart = (function() {
        // =============================
    // Private methods and propeties
    // =============================
    cart = [];
    // Constructor
    function Item(name, price, count) {
            this.name = name;
        this.price = price;
        this.count = count;
    }
    // Save cart
    function saveCart() {
            sessionStorage.setItem('shoppingCart', JSON.stringify(cart));
    }
    // Load cart
    function loadCart() {
            cart = JSON.parse(sessionStorage.getItem('shoppingCart'));
    }
    if (sessionStorage.getItem("shoppingCart") != null) {
            loadCart();
    }
    // =============================
    // Public methods and propeties
    // =============================
    var obj = {};
    // Add to cart
    obj.addItemToCart = function(name, price, count) {
                for (var item in cart) {
                    if (cart[item].name === name) {
                        cart[item].count++;
                    saveCart();
                    return;
                }
            }
            var item = new Item(name, price, count);
            cart.push(item);
            saveCart();
        }
        // Set count from item
    obj.setCountForItem = function(name, count) {
            for (var i in cart) {
                if (cart[i].name === name) {
                    cart[i].count = count;
                break;
            }
        }
    };
    // Remove item from cart
    obj.removeItemFromCart = function(name) {
            for (var item in cart) {
                if (cart[item].name === name) {
                    cart[item].count--;
                if (cart[item].count === 0) {
                        cart.splice(item, 1);
                }
                break;
            }
        }
        saveCart();
    }
    // Remove all items from cart
    obj.removeItemFromCartAll = function(name) {
            for (var item in cart) {
                if (cart[item].name === name) {
                    cart.splice(item, 1);
                break;
            }
        }
        saveCart();
    }
    // Clear cart
    obj.clearCart = function() {
            cart = [];
        saveCart();
    }
    // Count cart 
    obj.totalCount = function() {
            var totalCount = 0;
        for (var item in cart) {
                totalCount += cart[item].count;
        }
        return totalCount;
    }
    // Total cart
    obj.totalCart = function() {
            var totalCart = 0;
        for (var item in cart) {
                totalCart += cart[item].price * cart[item].count;
        }
        return Number(totalCart.toFixed(2));
    }
    // List cart
    obj.listCart = function() {
            var cartCopy = [];
        for (i in cart) {
                item = cart[i];
            itemCopy = {};
            for (p in item) {
                    itemCopy[p] = item[p];
            }
            itemCopy.total = Number(item.price * item.count);
            cartCopy.push(itemCopy)
        }
        return cartCopy;
    }
    // cart : Array
    // Item : Object/Class
    // addItemToCart : Function
    // removeItemFromCart : Function
    // removeItemFromCartAll : Function
    // clearCart : Function
    // countCart : Function
    // totalCart : Function
    // listCart : Function
    // saveCart : Function
    // loadCart : Function
    return obj;
})();
// *****************************************
// Triggers / Events
// ***************************************** 
// Add item
// $('.add-to-cart').click(function(event) {
    //     event.preventDefault();
//     var name = $(this).data('name');
//     var price = Number($(this).data('price'));
//     shoppingCart.addItemToCart(name, price, 1);
//     displayCart();
// });
function add_to_cart(e) {
        console.log(e);
    // e.preventDefault();
    var name = e.getAttribute("data-name").toString();
    var price = Number(e.getAttribute("data-price"));
    shoppingCart.addItemToCart(name, price, 1);
    displayCart();
}
// Clear items
$('.clear-cart').click(function() {
        shoppingCart.clearCart();
    displayCart();
});
function displayCart() {
        var cartArray = shoppingCart.listCart();
    var output = "";
    for (var i in cartArray) {
            output += "<tr>" +
            "<td>" + cartArray[i].name + "</td>" +
            "<td>(" + cartArray[i].price.toLocaleString('vi', { style: 'currency', currency: 'VND' }) + ")</td>" +
            "<td style='width:25%'><div class='input-group'><button class='minus-item input-group-addon btn btn_primary'  onclick=\"minus_item('" + cartArray[i].name + "')\">-</button>" +
            "<input type='number' class='item-count form-control'  data-name='" + cartArray[i].name + "' value='" + cartArray[i].count + "'>" +
            "<button class='plus-item btn btn_primary input-group-addon' onclick=\"plus_item('" + cartArray[i].name + "')\">+</button></div></td>" +
            "<td><button class='delete-item btn btn-danger' data-name=" + cartArray[i].name + " onclick=\"delete_item('" + cartArray[i].name + "')\">X</button></td>" +
            " = " +
            "<td>" + cartArray[i].total.toLocaleString('vi', { style: 'currency', currency: 'VND' }) + "</td>" +
            "</tr>";
    }
    var discount = Number(document.getElementById("discount").value) * shoppingCart.totalCart() / 100;
    var vat = Number(document.getElementById("vat").value) * shoppingCart.totalCart() / 100;
    var thanh_tien = shoppingCart.totalCart() - discount + vat;
    output = output + '' +
        '        <tr>' +
        '            <td colspan="2" align="right">Tổng cộng</td>' +
        '            <td colspan="2" align="right" class="fz16"><strong>' + shoppingCart.totalCart().toLocaleString('vi', { style: 'currency', currency: 'VND' }) + '</strong></td>' +
        '            <td></td>' +
        '        </tr>' +
        '        <tr>' +
        '            <td colspan="2" align="right">Giảm giá</td>' +
        '            <td colspan="2" align="right" class="fz16"><strong>' + discount.toLocaleString('vi', { style: 'currency', currency: 'VND' }) + '</strong></td>' +
        '            <td></td>' +
        '        </tr>' +
        '        <tr>' +
        '            <td colspan="2" align="right">VAT</td>' +
        '            <td colspan="2" align="right" class="fz16"><strong>' + vat.toLocaleString('vi', { style: 'currency', currency: 'VND' }) + '</strong></td>' +
        '            <td></td>' +
        '        </tr>' +
        '        <tr class="font-weight-bold" style="font-size: 14px;">' +
        '            <td colspan="2" align="right">Thành tiền</td>' +
        '            <td colspan="2" align="right" class="cp fz16"><strong>' + thanh_tien.toLocaleString('vi', { style: 'currency', currency: 'VND' }) + '</strong></td>' +
        '            <td></td>' +
        '        </tr>';
    $('.show-cart').html(output);
    $('.total-cart').html(shoppingCart.totalCart().toLocaleString('vi', { style: 'currency', currency: 'VND' }));
    $('.total-count').html(shoppingCart.totalCount());
}
// Item count input
$('.show-cart').on("change", ".item-count", function(event) {
        var name = $(this).data('name');
    var count = Number($(this).val());
    shoppingCart.setCountForItem(name, count);
    displayCart();
});
displayCart();
//-1
function minus_item(name) {
        console.log(name);
    shoppingCart.removeItemFromCart(name);
    displayCart();
}
//+1
function plus_item(name) {
        console.log(name);
    shoppingCart.addItemToCart(name);
    displayCart();
}
// Delete item button
function delete_item(name) {
        shoppingCart.removeItemFromCartAll(name);
    displayCart();
}