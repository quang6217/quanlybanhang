var vitri_chuot_id = "";
var html_js = "";

function my_vitri() {
    var x = document.activeElement.id;
    vitri_chuot_id = x;
}
js_search_sp();

function js_search_sp() {
    var a01 = document.getElementById("search_sp_001").value;
    a01 = a01.trim().replace(/\s+/g, ' ');
    document.getElementById("search_sp_001").value = a01;
    if (a01.length > 0) { sessionStorage.setItem('js_ivar_search_sp', a01); } else { if (sessionStorage.getItem("js_ivar_search_sp") != null) { a01 = sessionStorage.getItem('js_ivar_search_sp'); } }
    if (a01.length > 0) {
        $.post("js_047", { "input_001": a01 }, function(data, status) {
            document.getElementById("search_sp_002").innerHTML = data;
        });
    }
}
document.addEventListener('keydown', function(event) {
    if ((event.which == 13 || event.keyCode == 13) && vitri_chuot_id == "search_sp_001") {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            js_search_sp();
        }
    }
}, false);

function isEmpty(val) {
    return (val === undefined || val == null || val.length <= 0) ? true : false;
}
var shoppingCart = (function() {
    cart = [];

    function Item(uid, name, price, count) {
        this.uid = uid;
        this.name = name;
        this.price = price;
        this.count = count;
    }

    function saveCart() {
        sessionStorage.setItem('shoppingCart', JSON.stringify(cart));
    }

    function loadCart() {
        cart = JSON.parse(sessionStorage.getItem('shoppingCart'));
    }
    if (sessionStorage.getItem("shoppingCart") != null) {
        loadCart();
    }
    var obj = {};
    obj.addItemToCart = function(uid, name, price, count) {
        for (var item in cart) {
            if (cart[item].uid === uid) {
                cart[item].count++;
                saveCart();
                return;
            }
        }
        var item = new Item(uid, name, price, count);
        cart.push(item);
        saveCart();
    }
    obj.setCountForItem = function(uid, count) {
        for (var i in cart) {
            if (cart[i].uid === Number(uid)) {
                cart[i].count = count;
                break;
            }
        }
        saveCart();
    };
    obj.removeItemFromCart = function(uid) {
        for (var item in cart) {
            if (cart[item].uid === uid) {
                cart[item].count--;
                if (cart[item].count === 0) {
                    cart.splice(item, 1);
                }
                break;
            }
        }
        saveCart();
    }
    obj.removeItemFromCartAll = function(uid) {
        for (var item in cart) {
            if (cart[item].uid === Number(uid)) {
                cart.splice(item, 1);
                break;
            }
        }
        saveCart();
    }
    obj.clearCart = function() {
        cart = [];
        saveCart();
    }
    obj.totalCount = function() {
        var totalCount = 0;
        for (var item in cart) {
            totalCount += cart[item].count;
        }
        return totalCount;
    }
    obj.totalCart = function() {
        var totalCart = 0;
        for (var item in cart) {
            totalCart += cart[item].price * cart[item].count;
        }
        return Number(totalCart.toFixed(2));
    }
    obj.listCart = function() {
        var cartCopy = [];
        for (i in cart) {
            item = cart[i];
            itemCopy = {};
            for (p in item) {
                itemCopy[p] = item[p];
            }
            itemCopy.total = Number(item.price * item.count);
            cartCopy.push(itemCopy)
        }
        return cartCopy;
    }
    return obj;
})();

function add_to_cart(e) {
    //console.log(e);
    var uid = Number(e.getAttribute("data-uid"));
    var name = e.getAttribute("data-name").toString();
    var price = Number(e.getAttribute("data-price"));
    shoppingCart.addItemToCart(uid, name, price, 1);
    displayCart();
}
$('.clear-cart').click(function() {
    shoppingCart.clearCart();
    displayCart();
});

function displayCart() {
    var cartArray = shoppingCart.listCart();
    var output = "<tr><td><strong>Sản phẩm</strong></td><td><strong>Số lượng</strong></td><td><strong>Tổng tiền</strong></td><td></td></tr>";
    var num = 0;
    for (var i in cartArray) {
        num++;
        output += "<tr>" +
            "<td>" + cartArray[i].name + "</td>" +
            "<td style='width:20%'><div class='input-group'><input type='number' min='1' class='item-count form-control' data-uid='" + cartArray[i].uid + "' value='" + cartArray[i].count + "'>" +
            "<td>" + cartArray[i].total.toLocaleString('vi', { style: 'currency', currency: 'VND' }) + "</td>" +
            "<td><button class='delete-item btn btn-danger' onclick=\"delete_item('" + cartArray[i].uid + "')\"><i class='far fa-fw fa-trash-alt'></i></button></td>" +
            "</tr>";
    }
    var discount = Number(document.getElementById("discount").value) * shoppingCart.totalCart() / 100;
    var vat = Number(document.getElementById("vat").value) * shoppingCart.totalCart() / 100;
    var gia_ship = Number(document.getElementById("ship").value);
    var thanh_tien = shoppingCart.totalCart() - discount + vat + gia_ship;
    output = output + '' +
        '        <tr>' +
        '            <td></td>' +
        '            <td colspan="1" align="right">Tổng đơn</td>' +
        '            <td colspan="2" align="right" class="fz16"><strong>' + shoppingCart.totalCart().toLocaleString('vi', { style: 'currency', currency: 'VND' }) + '</strong></td>' +
        '        </tr>' +
        '        <tr>' +
        '            <td></td>' +
        '            <td colspan="1" align="right">VAT</td>' +
        '            <td colspan="2" align="right" class="fz16"><strong>' + vat.toLocaleString('vi', { style: 'currency', currency: 'VND' }) + '</strong></td>' +
        '        </tr>' +
        '        <tr>' +
        '            <td></td>' +
        '            <td colspan="1" align="right">Giảm giá</td>' +
        '            <td colspan="2" align="right" class="fz16"><strong>' + discount.toLocaleString('vi', { style: 'currency', currency: 'VND' }) + '</strong></td>' +
        '        </tr>' +
        '        <tr>' +
        '            <td></td>' +
        '            <td colspan="1" align="right">Phí ship</td>' +
        '            <td colspan="2" align="right" class="fz16"><strong>' + gia_ship.toLocaleString('vi', { style: 'currency', currency: 'VND' }) + '</strong></td>' +
        '        </tr>' +
        '        <tr class="font-weight-bold" style="font-size: 14px;">' +
        '            <td></td>' +
        '            <td colspan="1" align="right">Thành tiền</td>' +
        '            <td colspan="2" align="right" class="cp fz16"><strong>' + thanh_tien.toLocaleString('vi', { style: 'currency', currency: 'VND' }) + '</strong></td>' +
        '        </tr>';
    $('.show-cart').html(output);
    $('.total-cart').html(shoppingCart.totalCart().toLocaleString('vi', { style: 'currency', currency: 'VND' }));
    $('.total-count').html(shoppingCart.totalCount());
}
$('.show-cart').on("mouseout", ".item-count", function(event) {
    var uid = $(this).data('uid');
    var count = Number($(this).val());
    shoppingCart.setCountForItem(uid, count);
    displayCart();
});
displayCart();

function minus_item(name) {
    console.log(name);
    shoppingCart.removeItemFromCart(name);
    displayCart();
}

function plus_item(name) {
    console.log(name);
    shoppingCart.addItemToCart(name);
    displayCart();
}

function delete_item(name) {
    shoppingCart.removeItemFromCartAll(name);
    displayCart();
}
$('.vn_tinh').select2({
    placeholder: '(Bấm để chọn tỉnh/thành phố)',
    ajax: {
        url: '/js_047&tinh',
        dataType: 'json',
        delay: 250,
        data: function(data) {
            return {
                searchTinh: data.term
            };
        },
        processResults: function(response) {
            return {
                results: response
            };
        },
        cache: true
    }
});
$('.vn_huyen').select2({
    placeholder: '(Bấm để chọn quận/huyện)',
    ajax: {
        url: '/js_047&huyen',
        dataType: 'json',
        delay: 250,
        data: function(data) {
            return {
                searchTerm: data.term,
                vn_tinh: Number(document.getElementById("vn_tinh").value)
            };
        },
        processResults: function(response) {
            return {
                results: response
            };
        },
        cache: true
    }

});
$('.vn_xa').select2({
    placeholder: '(Gõ để chọn đường/phường/xã)',
    ajax: {
        url: '/js_047&xa',
        dataType: 'json',
        delay: 250,
        data: function(data) {
            return {
                searchTerm: data.term,
                vn_huyen: Number(document.getElementById("vn_huyen").value)
            };
        },
        processResults: function(response) {
            return {
                results: response
            };
        },
        cache: true
    }

});
$('.js_post_dh').click(function() {
    id_xa = Number(document.getElementById("vn_xa").value);
    id_huyen = Number(document.getElementById("vn_huyen").value);
    id_tinh = Number(document.getElementById("vn_tinh").value);
    customer_tel = Number(document.getElementById("customer_tel").value);
    customer_fullname = document.getElementById("customer_fullname").value;
    customer_first_address = document.getElementById("customer_first_address").value;
    message = document.getElementById("message").value;
    id_ly_do_xit = document.getElementById("id_ly_do_xit").value;
    id_nguon_khach = document.getElementById("id_nguon_khach").value;
    if (id_xa > 0 && customer_tel > 100000000 && customer_fullname.length > 1 && customer_first_address.length > 5 && id_nguon_khach>0) {
        if (sessionStorage.getItem("shoppingCart") != null) {
            cart_abc = sessionStorage.getItem('shoppingCart');
            discount = Number(document.getElementById("discount").value);
            vat = Number(document.getElementById("vat").value);
            ship = Number(document.getElementById("ship").value);
            url_xl_as = window.location.href;
            $.post("add_donhang", { name: "name", cart: cart_abc, discount: discount, vat: vat, ship: ship, id_xa: id_xa, id_huyen: id_huyen, id_tinh: id_tinh, customer_tel: customer_tel, customer_fullname: customer_fullname, customer_first_address: customer_first_address, message: message, url_xl_as: url_xl_as, id_ly_do_xit: id_ly_do_xit, id_nguon_khach: id_nguon_khach }, function(data, status) {
                if (data == "trung_don") {
                    alert("Phát hiện trùng đơn hàng: 0" + customer_tel + "\nKhông thể thêm 2 đơn giống nhau trong 60 phút");
                } else {
                    // alert(data);
                    shoppingCart.clearCart();
                    window.location.replace("/list_hang");
                }
            });
        } else {
            alert("Giỏ hàng trống");
        }
    } else {
        error = "";
        if (customer_tel < 100000000) { error = error + "\nSố điện thoại chưa đúng"; }
        if (customer_fullname.length < 1) { error = error + "\nBạn chưa nhập tên khách hàng"; }
        if (customer_first_address.length < 6) { error = error + "\nĐịa chỉ chi tiết nhận hàng còn thiếu"; }
        if (id_xa < 1) { error = error + "\nThiếu thông tin phường/xã"; }
        if (id_nguon_khach < 1) { error = error + "\nThiếu thông tin nguồn hàng"; }
        alert(error);
        return;
    }
});

function load_from_edit(cart, giam, vat, ship, message, sdt, nameKH, diaChi, vnXa) {
    json_cat = "";
    if (cart.length > 10) {
        shoppingCart.clearCart();
        json_cat = atob(cart);
        sessionStorage.setItem('shoppingCart', json_cat);
        cart = JSON.parse(sessionStorage.getItem('shoppingCart'));
    }
    if (giam.length > 0) { document.getElementById("discount").value = giam; }
    if (vat.length > 0) { document.getElementById("vat").value = vat; }
    if (ship.length > 0) { document.getElementById("ship").value = ship; }
    if (message.length > 0) { document.getElementById("message").value = message; }
    if (sdt.length > 0) { document.getElementById("customer_tel").value = "0" + sdt; }
    if (nameKH.length > 0) { document.getElementById("customer_fullname").value = nameKH; }
    if (diaChi.length > 0) { document.getElementById("customer_first_address").value = diaChi; }
    if (Number(vnXa) > 0) {
        $.post("js_047", { vnXa: vnXa }, function(data) {
            $arr = data.split('||');
            document.getElementById("vn_xa").innerHTML = $arr[0];
            document.getElementById("vn_huyen").innerHTML = $arr[1];
            document.getElementById("vn_tinh").innerHTML = $arr[2];
        });
    }
    if (json_cat.length > 10) { load_from_edit_1(json_cat); }
}

function load_from_edit_1(json_cat) {
    if (json_cat.length > 10) {
        sessionStorage.setItem('shoppingCart', json_cat);
        cart = JSON.parse(sessionStorage.getItem('shoppingCart'));
        displayCart();
    }
}

function check_sdt() {
    customer_tel = Number(document.getElementById("customer_tel").value);
    if (customer_tel > 100000000) {
        $.post("js_047", { sdt_quen: customer_tel }, function(data) {
            $arr = data.split('||');
            if ($arr.length > 2) {
                document.getElementById("show_khach_quen").innerHTML = $arr[0];
                document.getElementById("customer_fullname").value = $arr[1];
                document.getElementById("customer_first_address").value = $arr[2];
                document.getElementById("vn_xa").innerHTML = $arr[3];
                document.getElementById("vn_huyen").innerHTML = $arr[4];
                document.getElementById("vn_tinh").innerHTML = $arr[5];
                document.getElementById("ska_5").innerHTML = "Chú ý khách quen";
            }
        });
    }

}