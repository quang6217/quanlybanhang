<?php
// $title = 'Liên hệ cho zui';
// require 'site/widget/header.php';
?>
<!DOCTYPE html>

<html lang="en">

<head>

    <title><?= $title ?> - <?= $_SERVER['HTTP_HOST'] ?></title>

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--===============================================================================================-->

    <link rel="icon" type="image/png" href="csslogin/images/icons/favicon.ico" />

    <!--===============================================================================================-->

    <!-- <link rel="stylesheet" type="text/css" href="csslogin/vendor/bootstrap/css/bootstrap.min.css"> -->

    <!--===============================================================================================-->

    <link rel="stylesheet" type="text/css" href="csslogin/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

    <!--===============================================================================================-->

    <!-- <link rel="stylesheet" type="text/css" href="csslogin/vendor/animate/animate.css"> -->

    <!--===============================================================================================-->

    <!-- <link rel="stylesheet" type="text/css" href="csslogin/vendor/css-hamburgers/hamburgers.min.css"> -->

    <!--===============================================================================================-->

    <!-- <link rel="stylesheet" type="text/css" href="csslogin/vendor/select2/select2.min.css"> -->

    <!--===============================================================================================-->

    <script src="./csslogin/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="./csslogin/vendor/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="lq_css/lienhe.css">

    <!-- <link rel="stylesheet" type="text/css" href="csslogin/css/util.css">

	<link rel="stylesheet" type="text/css" href="csslogin/css/main.css?v1"> -->

    <!--===============================================================================================-->

</head>

<body>

    <div class="limiter">

        <div class="container-fluid">

            <div class="container-login100">

                <div class="wrap-login100">

                    <div id="contacts-new" class="container minh220 image">
                        <div class="white-card rel">
                            <div id="main_" class="row">
                                <div id="contacts" class="col-sm-4">
                                    <div class="left-side side c1 image ">
                                        <h2 class="fs38r mt0 mb40r">Contacts</h2>

                                        <div class="icon-box d-flex pb30">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <div class="text-block">
                                                <a class="db pb10 a8" href="mailto:support@directadmin.com">support@directadmin.com</a>
                                                <a class="db pb10 a8" href="mailto:billing@directadmin.com">billing@directadmin.com</a>
                                                <a class="db pb10 a8 " href="mailto:sales@directadmin.com.com">sales@directadmin.com</a>
                                            </div>
                                        </div>
                                        <div class="icon-box d-flex pb30">
                                            <i class="fa fa-map" aria-hidden="true"></i>
                                            <div class="text-block">
                                                <p class="c1 lh15">
                                                    JBMC Software #173,<br>
                                                    3-11 Bellerose Drive <br>
                                                    St Albert AB T8N 5C9, Canada
                                                </p>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div id="message" class="col-sm-8">
                                    <div class="right-side side">
                                        <h2 class="fs38r mt0 mb40r">Send us a message</h2>
                                        <form id="contact_form" action="?" method="POST" class="d-flex d-wrap has-validation-callback">
                                            <input type="hidden" name="action" value="mail">
                                            <div class="input-el w50pr mb20">
                                                <label class="db fw500">Your Name</label>
                                                <input class="input fw600 fs16 pb10" name="name" type="text" required="">
                                                <small class="db error"></small>
                                            </div>
                                            <div class="input-el w50pr mb20">
                                                <label class="db fw500">Email Address</label>
                                                <input class="input fw600 fs16 pb10" name="email" type="email" required="">
                                                <small class="db error"></small>
                                            </div>
                                            <div class="input-el w100pr">
                                                <label class="db fw500">Subject</label>
                                                <input class="input fw600 fs16 pb10" name="subject" type="text" required="">
                                                <small class="db error"></small>
                                            </div>
                                            <div class="input-el w100pr">
                                                <label class="db fw500">Message</label>
                                                <div class="d-flex">
                                                    <textarea class="w100pr fw600 fs16 pb10" name="message" rows="10" required="" placeholder="Enter Your message.

If your question involves an active install, include your License ID, Client ID, or your License IP."></textarea>
                                                    <button title="Submit Message" class="btn btn-link submit" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                                </div>
                                                <small class="db error"></small>
                                            </div>
                                        </form>

                                        <script>
                                            window.onload = function() {
                                                $('#contact_form').submit(function(event) {
                                                    event.preventDefault();
                                                    grecaptcha.ready(function() {
                                                        grecaptcha.execute('6LfSn_IUAAAAAALRCuNir24TT5HgV4AhEerGrseb', {
                                                            action: 'contacts'
                                                        }).then(function(token) {
                                                            console.log("Set token " + token);
                                                            $('#contact_form').prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');
                                                            $('#contact_form').prepend('<input type="hidden" name="action" value="contacts">');
                                                            $('#contact_form').unbind('submit').submit();
                                                        });;
                                                    });
                                                });
                                            }
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    var onresize = function() {
                        var widthclinet = document.body.clientWidth;
                        var contacts = '<div id="contacts" class="col-sm-4">' + document.getElementById("contacts").innerHTML + '</div>';
                        var message = ' <div id="message" class="col-sm-8">' + document.getElementById("message").innerHTML + '</div>';
                        if (widthclinet < 770) {
                            $("#contacts").remove();
                            $("#main_").append(contacts);
                        } else{
                            $("#message").remove();
                            $("#main_").append(message);
                        }
                    }
                    window.addEventListener("resize", onresize);
                </script>