<?php
function check_token($link)
{    
    for ($i1 = 0; $i1 < 5; $i1++) {
        $html = get_data($link);                        
        if ((int) stripos('a' . $html, 'working on getting this fixed') < 1 OR (int) stripos('a' . $html, 'business.facebook.com') > 0 OR (int) stripos('a' . $html, 'Facebook - Log In or Sign Up')>0) {
            break;
        }
    } 

    if (preg_match('/requestStatus":(.+?),/', $html, $a)) {
        $requestStatus =  $a[1];
    }
    $requestStatus = $requestStatus > 0 ? $requestStatus : 0;
    return $requestStatus;
}

if ($_COOKIE['id_member'] < 1) {
    echo '0';
    exit;
}

if (isset($_POST['link'])) {
    echo check_token($_POST['link']);
}
if (isset($_GET['link'])) {
    echo check_token($_GET['link']);
}
