<?php
$url_load = _sql02(str_replace("/", "", $_SERVER['REQUEST_URI']));
if($url_load=="new"){
    $str = substr(md5(base64_en(rand(10000000000,9999999999999))),1,20);
    header('Location: /'.$str );
    exit;
}
?>
<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Ghi chú</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <style type="text/css">
        .card-header {
            padding: .55rem 1.25rem;
            margin-bottom: 0rem;
        }

        .card {
            position: relative;
            display: -webkit-box;
            display: flex;
            -webkit-box-orient: vertical;
            -webkit-box-direction: normal;
            flex-direction: column;
            min-width: 0;
            word-wrap: break-word;
            background-color: #fff;
            background-clip: border-box;
            border: 1px solid #e5e9f2;
            border-radius: .2rem;
            margin-bottom: 2rem;
            box-shadow: none;
        }

        body {
            margin: 0;
            font-family: Nunito Sans, -apple-system, BlinkMacSystemFont, Segoe UI, Helvetica Neue, Arial, sans-serif;
            font-size: .875rem;
            font-weight: 400;
            line-height: 1.5;
            color: #495057;
            text-align: left;
            background-color: #f5f9fc;
        }
        .btn_primary {
            color: black !important;
            background-color: #aacbed !important;
            border-color: #aacbed !important;
        }
        .btn_primary:hover {
            color: #fff !important;
            background-color: #007bff !important;
            border-color: #007bff !important;
        }
        .p_stt {
            margin: 0 !important;
            margin-bottom: 0 !important;
        }
    </style>
</head>
<?php
$url_load = _sql02(str_replace("/", "", $_SERVER['REQUEST_URI']));
$stmt1 =  $conn->prepare("SELECT * FROM ghichu_viewfb WHERE href='$url_load'");
$stmt1->execute();
$ghichu = $stmt1->fetch(PDO::FETCH_ASSOC);
$ndghichu = base64_html($ghichu['noi_dung']);
?>
<body style="background-color: #eaeef6">
    <div class="wrapper">
        <main class="content p-4">
            <div class="container-fluid p-0">
                <div class="row">
                    <div class="card-body">
                        <div class="form-group">
                            <textarea class="form-control" style="margin: auto;height: 90vh;width: 99%;" onchange="mysend()" value="<?= $ndghichu ?>" id="ghichu" name="list_email" rows="20" placeholder=""><?= $ndghichu ?></textarea><br>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">    
    var name = "<?=$url_load?>";
    function mysend() {
        post_chat();
    }
    setInterval(function() {
      post_chat();
    }, 5000);

    function post_chat() {        
        var ghichu = encodeURI("name=" + name + "&ndghichu=" + document.getElementById("ghichu").value);        
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("POST", "/?get=notepad_post", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.onreadystatechange = function() {            
            var html = xmlhttp.responseText;
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {                
            }
        };
        xmlhttp.send(ghichu);
    }
</script>