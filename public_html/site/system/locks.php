
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Tài khoản bị khóa</title>
	<style>
		body {
			opacity: 0;
		}
	</style>
	<script src="js/settings.js"></script>
	<!-- END SETTINGS -->
</head>
<?php
 $stmt =  $conn->prepare("SELECT * FROM danh_sach_team WHERE team =:team" );
 $stmt->execute(array(":team" => $member['team']));
 $my_team= $stmt->fetch(PDO::FETCH_ASSOC);  
 ?>
<body>
	<main class="main d-flex w-100">
		<div class="container d-flex flex-column">
			<div class="row h-100">
				<div class="mx-auto d-table h-100">
					<div class="d-table-cell align-middle">
						<div class="text-center">
							<h1 class="display-1 font-weight-bold">Bị khóa</h1>
							<p class="h3"><?=$member['notes_lock']?></p>
							<p class="h3 font-weight-normal mt-3 mb-4"><font color=blue><?=$my_team['footer_1']?><font></p>
							<a href="/?sys=login&exit=ok" class="btn btn-primary btn-lg">Đăng xuất</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

</body>

</html>