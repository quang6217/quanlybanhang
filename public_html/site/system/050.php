<?php
function check_token($link)
{
    $html = get_data($link);
    if (preg_match('/businessID":(.+?),/', $html, $a)) {
        $businessID =  $a[1];
    }
    if (preg_match('/businessName":"(.+?)"/', $html, $a)) {
        $businessName =  $a[1];
    }
    if (preg_match('/inviterName":"(.+?)"/', $html, $a)) {
        $inviterName =  $a[1];
    }
    if (preg_match('/inviteeEmail":"(.+?)"/', $html, $a)) {
        $inviteeEmail =  $a[1];
        $inviteeEmail =  str_replace("\u0040", "@", $inviteeEmail);
    }
    if (preg_match('/requestStatus":(.+?),/', $html, $a)) {
        $requestStatus =  $a[1];
    }
    if ($businessID > 1000) {
        $requestStatus = $requestStatus == 1 ? '1' : '2';
        $jaaaa = '{
            "name" : "' . $inviterName . '",
            "businessName" : "' . $businessName . '"
        }';
        $json = json_decode($jaaaa, true);
        $inviterName = $json['name'];
        $businessName = $json['businessName'];
        return $requestStatus . '|' . $businessID . '|' . $link . '|' . $businessName . '|' . $inviterName . '|' . $inviteeEmail;
    } else {
        return '0';
    }
}

if ($_COOKIE['id_member'] < 1) {
    echo '0';
    exit;
}

if (isset($_GET['link'])) {
    $html = check_token($_GET['link']);
    $arr = explode('|', $html);
    $type = explode('|', $_GET['type']);
    $y = count($type);
    $gia_tri = "";
    for ($i = 0; $i < $y; $i++) {
        if ($type[$i] != "") {
            $gia_tri = $gia_tri . '' . $arr[$type[$i]] . '|';
        }
    }
    $gia_tri = $gia_tri . '' . $arr[0];
    echo $gia_tri;
}
