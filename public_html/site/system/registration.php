<?php
//Khai báo các biến ban đầu
$error_1 = '';
$error_2 = '';
$error_3 = '';
$email = '';
$pass = '';
$code = '';
$full_name = '';
$donot = "Những mật khẩu không khớp";
$donot_code = "Những mã đó không khớp";

//Hàm xữ lý khi người dùng nhấn POST
if (isset($_POST['reg'])) {
	//Lưu lại giá trị post
	//echo 'dayne'; exit;
	$email = _sql01(($_POST['email']));
	$pass = _sql00(($_POST['pass']));
	$pass_1 = _sql00(($_POST['pass_1']));
	$code = (int) $_POST['code'];
	$full_name = _sql01(($_POST['full_name']));

	//Kiểm tra sự tồn tại của email
	$sqlAll = "SELECT COUNT(`email`) FROM `member`WHERE `email`='$email' LIMIT 1";
	$stmt = $conn->query($sqlAll);
	$num_email = $stmt->fetchColumn();

	$id_referral = 0;
	$team_referral = 0;
	//Lấy thông tin người giới thiệu
	if ($code > 100) {
		$stmt =  $conn->prepare("SELECT * FROM member WHERE `code_reg`='$code'");
		$stmt->execute(array());
		$referral = $stmt->fetch(PDO::FETCH_ASSOC);
		$id_referral = (int) $referral['id'];
		$team_referral = (int) $referral['team'];
	}

	// Thông báo các lỗi khi gặp phải
	$error_1 = $num_email > 0 ? '<small style="padding: 20px; color:red">Email này đã là thành viên</small>' : '';
	$error_2 = $pass != $pass_1 ? '<small style="padding: 20px; color:red">' . $donot . '</small>' : '';
	$error_3 = $referral['id'] < 1 ? '<small style="padding: 20px; color:red">' . $donot_code . '</small>' : '';
	if ($code > 0) {
		$error_3 = '<small style="padding: 20px; color:red">' . $donot_code . '</small>';
	} else {
		$error_3 = '<small style="padding: 20px; color:red">Nếu ko có: điền số 0</small>';
	}

	//Mọi thứ đã oke, cho phép đăng ký thành viên
	if ($num_email == 0 and $pass == $pass_1) {
		$pass_md5 = md5(md5($pass_1));
		$key_num = rand(1000000, 9999999);
		$code_reg = rand(1000, 999999);
		$key_string = md5(md5(md5($key_num)));

		//Thêm dữ liệu lên SQL
		try {
			$sql = "INSERT INTO `member`(`name`, `phone`, `email`, `pass`, `level`, `team`, `money`, `key_num`, `key_string`, `code_reg`, `id_referral`, `pass_error`, `time_login`, `time_reg`, `locks`, `full_info`) VALUES  
			('$full_name','','$email','$pass_md5',0,$team_referral,0,$key_num,'$key_string',$code_reg,$id_referral,0,0,$time_php,0,'')";
			$conn->exec($sql);
			$_SESSION['email'] = $email;
			header('Location: /?get=login');
			exit;
		} catch (Exception $e) {
			$error_1 = '<small style="padding: 20px; color:red">Đã xảy ra lỗi, xin hảy thử lại lần nữa...</small>';
		}
	}
}
$title = 'Đăng ký';
require 'site/widget/header_r.php';
?>

<form method="post" action="<?= $_SERVER['REQUEST_URI'] ?>">
	<span class="login100-form-title">
		Đăng ký thành viên
	</span>
	<div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
		<input class="input100" type="text" name="full_name" value="<?= $full_name ?>" placeholder="Tên Của Bạn" required>
		<span class="focus-input100"></span>
		<span class="symbol-input100">
			<i class="fa fa-user" aria-hidden="true"></i>
		</span>
	</div>

	<?= $error_1 ?>
	<div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
		<input class="input100" type="email" name="email" value="<?= $email ?>" placeholder="Email" required>
		<span class="focus-input100"></span>
		<span class="symbol-input100">
			<i class="fa fa-envelope" aria-hidden="true"></i>
		</span>
	</div>

	<div class="wrap-input100 validate-input" data-validate="Password is required">
		<input class="input100" type="password" name="pass" value="<?= $pass ?>" placeholder="Mật khẩu" required>
		<span class="focus-input100"></span>
		<span class="symbol-input100">
			<i class="fa fa-lock" aria-hidden="true"></i>
		</span>
	</div>

	<?= $error_2 ?>
	<div class="wrap-input100 validate-input" data-validate="Password is required">
		<input class="input100" type="password" name="pass_1" placeholder="Nhập lại mật khẩu" required>
		<span class="focus-input100"></span>
		<span class="symbol-input100">
			<i class="fa fa-lock" aria-hidden="true"></i>
		</span>
	</div>

	<?= $error_3 ?>
	<div class="wrap-input100" data-validate="Password is required">
		<input class="input100" type="text" name="code" value="<?= $code ?>" placeholder="Mã giới thiệu">
		<span class="focus-input100"></span>
		<span class="symbol-input100">
			<i class="fa fa-key" aria-hidden="true"></i>
		</span>
	</div>

	<div class="container-login100-form-btn">
		<button class="login100-form-btn" type="submit" name="reg">
			Đăng ký
		</button>
	</div>

	<div class="text-center p-t-136">
		<a class="txt2" href="/?sys=login">
			Đăng nhập nếu bạn đã có tài khoản
			<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
		</a>
	</div>
</form>
</div>
</div>
</div>
<?php require 'site/widget/footer_r.php'; ?>
