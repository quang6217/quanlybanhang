<?

$ad_url = 0;

$mes_url = '/mess_nv';

$title = "Chốt đơn";

if(explode('&',$_SERVER['REQUEST_URI'])[0]=='/mess_ad'){

    $ad_url = 5;

    $mes_url = '/mess_ad';

    $title = "Admin check đơn";

}

$in_id_sp = (int) $_COOKIE['cc_ma_hang'];

if ($in_id_sp > 999 and $in_id_sp < 10000) {

    $stmt =  $conn->prepare("SELECT * FROM vn_san_pham WHERE id =:id");

    $stmt->execute(array(":id" => $in_id_sp));

    $sanp = $stmt->fetch(PDO::FETCH_ASSOC);

    $in_name_sp = _sql01($sanp['name']);

    $in_sl = 1;

    $in_gia_sp1 = (int) $sanp['gia_ban'] / 1000;

    $in_tien_ship = (int) $sanp['phi_ship'] / 1000;

    $in_gia_sp = (int) $sanp['gia_ban'] / 1000 + $in_tien_ship;

    // ------------> Lấy dữ liệu màu sắc

    $ar_mau = explode('|', $sanp['mau_sac']);

    $in_mau_sac = '';

    for ($i = 0; $i < count($ar_mau); $i++) {

        $stmt =  $conn->prepare("SELECT * FROM vn_mau_sac WHERE id =:id");

        $stmt->execute(array(":id" => (int) $ar_mau[$i]));

        $mau = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($mau['mau'] != "") {

            $in_mau_sac = $in_mau_sac . '<option value="' . $mau['id'] . '" title="' . $mau['mo_ta'] . '">' . $mau['mau'] . '</option>';

        }

    }

    if ($in_mau_sac == "") {

        $in_mau_sac = '<option value="4444">Không có màu sắc để chọn</option>';

    }

    // ------------> Lấy dữ liệu size

    $ar_size = explode('|', $sanp['size']);

    $in_size = '';

    for ($i = 0; $i < count($ar_size); $i++) {

        $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id AND sex =:sex");

        $stmt->execute(array(":id" => (int) $ar_size[$i], ":sex" => (int) $sanp['sex']));

        $mau = $stmt->fetch(PDO::FETCH_ASSOC);

        $in_size = $in_size . '<option value="' . $mau['id'] . '" title="Cao: ' . $mau['chieu_cao'] . ' - Nặng: ' . $mau['can_nang'] . ' - Eo: ' . $mau['vong_eo'] . ' - Ngực: ' . $mau['vong_nguc'] . '">' . $mau['name'] . '</option>';

    }

    $html_i_ms = '<select class="custom-select flex-grow-1" id="vn_mau_sac" name="mau_sac" onchange="vn_mau_sac()" required>' . $in_mau_sac . '</select>';

    $html_i_sz = '<select class="custom-select flex-grow-1" id="vn_size" name="size" onchange="vn_size()" required>' . $in_size . '</select>';

} else {

    $in_id_sp = '';

}

if (isset($_POST['xac_nhan'])) {

    $sdt = (int) $_POST['sdt'];

    $nameKH = _sql($_POST['nameKH']);

    $linkFB = _sql($_POST['linkFB']);

    $ma_hang = (int) $_POST['ma_hang'];

    $nameSP = _sql($_POST['nameSP']);

    $sl_mua = (int) $_POST['sl_mua'];

    $total_money = (int) $_POST['total_money'] * 1000;

    $mau_sac = (int) $_POST['mau_sac'];

    $size = (int) $_POST['size'];

    setcookie('cc_ma_hang', $ma_hang, $time_php + 10 * 60 * 60 * 24);

    for ($a = 0; $a < 100; $a++) {

        $ms_i = isset($_POST['mau_sac_' . $a]) ? '|' . (int) $_POST['mau_sac_' . $a] : '';

        $sz_i = isset($_POST['size_' . $a]) ? '|' . (int) $_POST['size_' . $a] : '';

        $mau_sac = $mau_sac . '' . $ms_i;

        $size = $size . '' . $sz_i;

    }

    $xaKH = (int) $_POST['xaKH'];

    $address_detail = _sql($_POST['address_detail']);

    $ghi_chu = _sql($_POST['ghi_chu']);

    $time_giam_sdt =  $time_php - 120 * 60;

    $sqlAll = "SELECT COUNT(`sdt`) FROM `vn_don_hang_ship` WHERE `sdt` = $sdt and `time_add`>$time_giam_sdt AND `trang_thai`!='44' AND `ma_hang`=$ma_hang";

    $stmt = $conn->query($sqlAll);

    $sum_sdt_a = $stmt->fetchColumn();    

    if ($xaKH > 0 and $mau_sac > 0 and $sl_mua > 0 and $ma_hang > 0 and $sum_sdt_a < 1) {

        sql_kho_hang($mau_sac, $size, $ma_hang, $id_team);

        $sql = "INSERT INTO `vn_don_hang_ship`(`mem`, `team`, `sdt`, `nameSP`, `nameKH`, `linkFB`, `ma_hang`, `sl_mua`, `total_money`, `mau_sac`, `size`, `xaKH`, `address_detail`, `ghi_chu`, `trang_thai`, `time_add`, `time_num`) VALUES ('$id_member','$id_team','$sdt','$nameSP','$nameKH','$linkFB','$ma_hang','$sl_mua','$total_money','$mau_sac','$size','$xaKH','$address_detail','$ghi_chu','0','$time_php','$time_num')";

        $conn->exec($sql);

        //Lấy thông tin id đơn hàng để check

        $stmt =  $conn->prepare("SELECT * FROM vn_don_hang_ship WHERE sdt =:sdt ORDER BY id DESC limit 1");

        $stmt->execute(array(":sdt" => $sdt));

        $id_dn_sdt = $stmt->fetch(PDO::FETCH_ASSOC);

        $id_dh_k = (int)$id_dn_sdt['id'];

        $sotien_lend = sql_sale($id_team, 'add_dh');

        if ($sotien_lend > 0 AND $ad_url==0) {

            lich_su_giao_dich($sotien_lend, 9, $id_member, 4, 'ĐH ' . $id_dh_k, '', 8, $time_php);

        }

        $id_fab = (int)explode('|', $linkFB)[0];

        if ($id_fab > 0) {

            $sql = "UPDATE vn_log_chat_id_fanpage SET id_don_hang=$id_dh_k, sdt=$sdt WHERE id=$id_fab";

            $stmt = $conn->prepare($sql);

            $stmt->execute();

        }

        header('Location: '.$mes_url.'&id=' . $ma_hang);

        exit;

    }

    $title = "Lổi đơn hàng";

    $tao_don_hang = '<font color=red>Bạn chưa điền đầy đủ thông tin</font>';

    if ($sum_sdt_a > 0) {

        $tao_don_hang = 'Số: 0' . $sdt . ' đã lên đơn';

        dem_nguoc_url($mes_url, 'Lổi không thể lên đơn', 'Số điện thoại 0' . $sdt . ' đã lên đơn');

    }

    dem_nguoc_url($mes_url, 'Lổi không thể lên đơn', 'Bạn chưa điền đầy đủ thông tin cần thiết');

}

$sdt = '';

$nmang_sdt = '';

$nameKH = '';

$linkFB = '';

$address_detail = '';

$ghi_chu = '';

$block_edit = '';

$ct_xa_kh = '';

$in_mau_sac = '<option value="0">Chọn màu sắc</option>';

$in_size = '<option value="0">Chọn size</option>';

$html_i_ms = '<select class="custom-select flex-grow-1" id="vn_mau_sac" name="mau_sac" onchange="vn_mau_sac()" required>' . $in_mau_sac . '</select>';

$html_i_sz = '<select class="custom-select flex-grow-1" id="vn_size" name="size" onchange="vn_size()" required>' . $in_size . '</select>';

$selected_tinh = '<option value="0">Chọn Tỉnh/Thành phố</option>';

$selected_huyen = '<option>Chọn Huyện/Quận</option>';

$selected_xa = '<option>Chọn Phường/Xã</option>';

$stmt1 =  $conn->prepare("SELECT * FROM vn_tinh ORDER BY id ASC");

$stmt1->execute();

$list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);

foreach ($list_code as $show) {

    $selected_tinh = $selected_tinh . '<option value="' . $show['id'] . '">' . $show['name'] . '</option>';

}

// -->Begin: chọn sms mặc định

if (isset($_GET['uid'])) {

    $uid = (int)$_GET['uid'];

}

// $stmt->execute();

if ($uid < 1) {

    $stmt =  $conn->prepare("SELECT * FROM vn_log_chat_id_fanpage WHERE mem_chot=$id_member ORDER BY timestamp DESC, time_update DESC LIMIT 1");

    if ($ad_url == 5) {

        $stmt =  $conn->prepare("SELECT * FROM vn_log_chat_id_fanpage WHERE team =$id_team ORDER BY timestamp DESC, time_update DESC LIMIT 1");

    }

    $stmt->execute();

    $isha = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($isha['id'] < 1) {

        $isha['id'] = 9999999;

    }

    header('Location: '.$mes_url.'&uid=' . $isha['id']);    

    exit;

}

$don = "";

$tt = "";

if (isset($_GET['don'])) {

    $don = _sql01(isset($_GET['don']));

}

if (isset($_GET['tt'])) {

    $tt = _sql01(isset($_GET['tt']));

}

// -->End: chọn sms mặc định

$mac_dinh = 'sidebar sidebar-sticky toggled';