<?php
function check_ten_sp($name)
{
    global $conn;
    $sql = "SELECT * from sale_sanpham_02 WHERE ma_kem = '$name' ";
    $stmt1 =  $conn->prepare($sql);
    $stmt1->execute();
    return $stmt1->fetch();
}
if ($id_level < 6) {
    echo "Bạn không có quyền truy cập vào chức năng này!!!";
    exit;
}
$thong_bao_chuan = '';
if (isset($_POST['name_ct_sanp'])) {
    $id_sp = (int) $_POST['ma_hang'];
    $thong_bao_kq = '';
    $array_3 = explode('<br />', nl2br(trim($_POST['name_ct_sanp'])));
    foreach ($array_3 as $na) {
        $arr = explode('|', trim(_sql($na)));
        $name = _sql01($arr[0]);
        $phan_loai = _sql($arr[1]);
        $thuoc_tinh = (int)$arr[2];
        $gia_tien = (int)$arr[3];
        $gia_nhap = (int)$arr[4];
        $stmt =  $conn->prepare("SELECT * FROM sale_sanpham_02 WHERE ma_kem='$name' AND id_san_pham=$id_sp");
        $stmt->execute();
        $ia_na = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($ia_na['id'] > 0) {
            $thong_bao_kq = '' . $thong_bao_kq . '<li>  Tên: <font color=red>' . $name . '</font> - <font color=red>Trùng tên</font> </li>';
        } else {
            $sql = "INSERT INTO `sale_sanpham_02`(`id_san_pham`, `ma_kem`, `phan_loai`, `thuoc_tinh`, `gia_tien`, `gia_nhap`, `hien_an`) 
            VALUES ($id_sp,'$name','$phan_loai',$thuoc_tinh,$gia_tien,$gia_nhap,0)";
            $conn->exec($sql);
            $id_max = get_max_id_sale_sp_2();
            insert_gia_xuat_nhap($id_max,$gia_tien,$gia_nhap,1);
            $thong_bao_kq = '' . $thong_bao_kq . '<li>  Tên: <font color=blue>' . $name . '</font> - <font color=blue>Thêm thành công</font> </li>';
        }
    }
    $thong_bao_chuan = '<div class="row"><div class="col-12"><div class="card"><div class="card-header"><h5 class="card-title">Kết quả thêm tên sản phẩm</h5></div><ol>' . $thong_bao_kq . '</ol></div></div></div></div>';
}

if (isset($_POST['load_ajax'])) {
    $id_post = (int)$_POST['load_ajax'];
    echo '<div class="input-group mb-3 col-md-12"><div class="input-group-prepend"><span class="input-group-text">Mã hàng</span></div><select name="ma_hang" id="sale_sanpham_01" class="custom-select flex-grow-1" onchange="show_id_sp()"><option>.........</option>';
    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_01 ORDER BY id ASC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    foreach ($list_code as $show) {
        $selected_a = $id_post == $show['id'] ? 'selected' : '';
        echo '<option value="' . $show['id'] . '" ' . $selected_a . '>' . $show['id'] . ' - ' . $show['name'] . '</option>';
    }
    echo '</select></div><div class="form-group col-md-12"><textarea type="text" class="form-control" rows="5" value="" name="name_ct_sanp" placeholder="Tên|Phân loại|Thuộc tính|Giá bán|Giá nhập"></textarea></div>';
    echo '|||||<div class="card"><div class="card-header alert-info"><div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size: px;color:white;">Sản phẩm của bạn</div></div><div class=""><div class="table-responsive flex-row flex-nowrap"><table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%"><thead><tr><th style="text-align:center; width:5%;">#</th>
    <th style="text-align:left;">Tên mã kèm</th>
    <th style="text-align:center;">Phân loại</th>
    <th style="text-align:right;">Giá tiền</th>
    <th style="text-align:right;">Giá nhập</th><th style="text-align:center; width:7%;">Actions</th></tr></thead><tbody>';
    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_02 WHERE id_san_pham=$id_post ORDER BY id ASC");
    $stmt1->execute(array());
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    foreach ($list_code as $show_fp) {
        $num++;
        $a_on = 'on';
        if ($show_fp['hien_an'] == 44) {
            $a_on = 'off';
        }
        // chức năng kiểm tra dòng sp này có phải là combo hay không, nếu là combo thì không cho chỉnh sửa
        $data_combo = check_ten_combo($show_fp['id']);
        $quyen_edit_1 = "";
        // if(count($data_combo)==0)
        // {
            $quyen_edit_1 = '<a href="javascript:void(0)" onclick="edit_show_name_2(' . $show_fp['id'] . ')"><i class="align-middle mr-2 fas fa-fw fa-pen"></i></a>';
        // }
        $quyen_edit_2 = '<a href="javascript:void(0)" onclick="hidden_show_name_2(' . $show_fp['id'] . ')"><i class="align-middle mr-2 fas fa-fw fa-toggle-' . $a_on . '"></i></a>';
        $ngay_hien_tai = date('d/m/Y', $time_php);
        $ngay_them = date('d/m/Y', $show_fp['time_add']);
        $ngay_show = $ngay_them == $ngay_hien_tai ? date('H:i:s', $show_fp['time_add']) : $ngay_them;
        echo '<tr>
            <td style="text-align:center;" title="' . $show_fp['id'] . '">' . $num . '</td>                        
            <td title="' . $show_fp['ma_kem'] . '" id="show_name0_' . $show_fp['id'] . '">' . substr($show_fp['ma_kem'],  0, 100)  . '</td>
            <td style="text-align:center" id="show_name1_' . $show_fp['id'] . '">' . substr($show_fp['phan_loai'],  0, 100)  . '</td>            
            <td style="text-align:right;" id="show_name2_' . $show_fp['id'] . '">' . number_format($show_fp['gia_tien'])  . 'đ</td>  
            <td style="text-align:right;" id="show_name3_' . $show_fp['id'] . '">' . number_format($show_fp['gia_nhap'])  . 'đ</td>  
            <td style="text-align:center;" id="quyen_edit2_' . $show_fp['id'] . '">
            ' . $quyen_edit_1 .$quyen_edit_2. '
            </td>
            </tr>';
    }
    echo '</tbody></table> </div> </div>';
    exit;
}

//Xoá dữ liệu
if (isset($_GET['hidden'])) {
    $idCheck = $_GET['hidden'];
    header('Location: /add_sanpham');
    exit;
}
if (isset($_POST['xacnhan_tao'])) {
    $thong_bao_kq = '';
    $array_3 = explode('<br />', nl2br(trim($_POST['name_new'])));
    foreach ($array_3 as $na) {
        $name = _sql01($na);
        $stmt =  $conn->prepare("SELECT * FROM sale_sanpham_01 WHERE name='$name'");
        $stmt->execute();
        $ia_na = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($ia_na['id'] > 0) {
            $thong_bao_kq = '' . $thong_bao_kq . '<li>  Tên: <font color=red>' . $name . '</font> - <font color=red>Trùng tên</font> </li>';
        } else {
            $sql = "INSERT INTO `sale_sanpham_01`(`mem`, `team`, `name`, `hien_an`, `time_add`)  VALUES ('$id_member', '$id_team', '$name', '0', $time_php)";
            $conn->exec($sql);
            $thong_bao_kq = '' . $thong_bao_kq . '<li>  Tên: <font color=blue>' . $name . '</font> - <font color=blue>Thêm thành công</font> </li>';
        }
    }
    $thong_bao_chuan = '<div class="row"><div class="col-12"><div class="card"><div class="card-header"><h5 class="card-title">Kết quả thêm tên sản phẩm</h5></div><ol>' . $thong_bao_kq . '</ol></div></div></div></div>';
}
if (isset($_POST['xacnhan_gop'])) 
{
    $check_dieu_kien_them_combo = true;
    $arr_t = explode('|',trim($_POST['name_new']));
    //kiểm tra nhập đúng định dang hay chưa?
    if($arr_t[0] == NULL||$arr_t[1] == NULL||$arr_t[2] == NULL || $arr_t[3] == NULL )
    {
        $thong_bao_kq = '' . $thong_bao_kq . '<li>  Tên: <font color=red>' . $name . '</font> - <font color=red>Nhập không đúng định dạng</font> </li>';
    }else
    {
    // nếu đúng định dạng tiếp tục
    $leng = count($arr_t);
    $name_loai_sp = _sql01($arr_t[0]);
    $name_combo = _sql01($arr_t[1]);
    $loai = _sql01($arr_t[2]);
    // kiểm tra tồn tại loại sản phẩm hay không?
    $stmt_loai_sp =  $conn->prepare("SELECT * FROM sale_sanpham_01 WHERE name='$name_loai_sp'");
    $stmt_loai_sp->execute();
    $ia_na = $stmt_loai_sp->fetch();
    if ($ia_na['id'] == 0) {
        $thong_bao_kq = '' . $thong_bao_kq . '<li>  Tên: <font color=red>' . $name_loai_sp . '</font> - <font color=red>Không tồn tại tên</font> </li>';
    } 
    else
    {   
        $id_loai_sp = $ia_na['id'];
        $stmt_loai_sp_ct =  $conn->prepare("SELECT * FROM sale_sanpham_02 WHERE id_san_pham ='$id_loai_sp' AND ma_kem = '$name_combo'");
        $stmt_loai_sp_ct->execute();
        $ia_loai_sp_chi_tiet = $stmt_loai_sp_ct->fetch()["id"];
        // kiểm tra xem có tồn tại sản phẩm hay không
        if($ia_loai_sp_chi_tiet ==0)
        {
            for($k =3;$k<$leng;$k++)
            {
                if(check_ten_sp($arr_t[$k])["id"]<=0)
                {
                    $check_dieu_kien_them_combo = false;
                    $thong_bao_kq = '' . $thong_bao_kq . '<li>  Tên: <font color=red>' . $name . '</font> - <font color=red>Không tồn tại sản phẩm '.$arr_t[$k].'</font> </li>';
                    break;
                }
            }
            // thêm dữ liệu vào
            if($check_dieu_kien_them_combo)
            {
                //tìm id loai sp
                $stmt_get_id =  $conn->prepare("SELECT * from sale_sanpham_01 where name = '$name_loai_sp'");
                $stmt_get_id->execute();
                $id_sp_01 =  $stmt_get_id->fetch()["id"];
                // ===============================================================
                $sql = "INSERT INTO `sale_sanpham_02`(`id_san_pham`, `ma_kem`, `phan_loai`, `thuoc_tinh`, `gia_tien`,`gia_nhap`,`hien_an`) 
                 VALUES ('$id_sp_01', '$name_combo', '$loai', '0', '0','0','0')";
                $conn->exec($sql);
                // ===============================================================
                // tìm id combo mới thêm vào
                $stmt_get_id =  $conn->prepare("SELECT * from sale_sanpham_02 ORDER by id desc limit 1");
                $stmt_get_id->execute();
                $id_new =  $stmt_get_id->fetch()["id"];
                // var_dump($id_new);
                // // ================================================================
                $gia_tien = 0;
                $gia_nhap = 0;
                for($k =3;$k<$leng;$k++)
                {
                    $data_sp = check_ten_sp($arr_t[$k]);
                    // var_dump($data_sp);
                    $gia_tien +=  $data_sp["gia_tien"];
                    $gia_nhap += $data_sp["gia_nhap"];
                    $id_sp_sale_02 = $data_sp["id"];
                    $sql = "INSERT INTO `sale_sanpham_combo`(`id_combo`, `id_sp_sale_02`)  VALUES ('$id_new', '$id_sp_sale_02')";
                    $conn->exec($sql);
                }
                // cập nhật nốt giá còn lại
                $sql = "UPDATE sale_sanpham_02 SET gia_tien= '$gia_tien', gia_nhap = '$gia_nhap' WHERE id='$id_new'";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                // thêm vào giá nhập xuất
                $id_max = get_max_id_sale_sp_2();
                insert_gia_xuat_nhap($id_max,0,0,1);
                $thong_bao_kq = '' . $thong_bao_kq . '<li>  Tên: <font color=blue></font> - <font color=blue>Thêm thành công</font> </li>';
            }
        }
        else
        {
            $thong_bao_kq = '' . $thong_bao_kq . '<li>  Tên: <font color=red>' . $name_loai_sp . '</font> - <font color=red>Trùng tên combo</font> </li>';
        }
        

    }
    

    }
    $thong_bao_chuan = '<div class="row"><div class="col-12"><div class="card"><div class="card-header"><h5 class="card-title">Kết quả thêm tên sản phẩm</h5></div><ol>' . $thong_bao_kq . '</ol></div></div></div></div>';
}
$status_a = (int) $status_a;
$title = 'Thêm sản phẩm';
require 'site/widget/header.php';
$list_name =  $conn->prepare("SELECT * FROM sale_sanpham_01");
$list_name->execute(array());
?>
<main class="content">
    <div class="container-fluid p-0">
        <?= $thong_bao_chuan ?>
        <div class="row">
            <div class="ml-3" id="headingTwo" style="display: inline-flex;">
                <h3 class="mt-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Thêm sản phẩm
                    </a>
                </h3>
            </div>
            <div class="ml-3" id="headingTwo" style="display: inline-flex;">
                <h3 class="mt-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_ct" aria-expanded="false" aria-controls="collapse_ct">
                        Chi tiết sản phẩm
                    </a>
                </h3>
            </div>
            <div class="ml-3" id="headingTwo" style="display: inline-flex;">
                <h3 class="mt-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#heading_gop" aria-expanded="false" aria-controls="heading_gop">
                        Gộp sản phẩm
                    </a>
                </h3>
            </div>
            <!--  -->
            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_ct" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-row" id="show_du_lieu">
                                        <div class="input-group mb-3 col-md-12">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Mã hàng</span>
                                            </div>
                                            <select name="ma_hang" id="sale_sanpham_01" class="custom-select flex-grow-1" onchange="show_id_sp()">
                                                <option>.........</option>
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_01 ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                $num = 0;
                                                foreach ($list_code as $show) {
                                                    echo '<option value="' . $show['id'] . '">' . $show['id'] . ' - ' . $show['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit" name="xac_nhan" id="button_xn_k" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapseTwo" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <textarea type="text" class="form-control" rows="5" value="" name="name_new" placeholder="Mổi tên sản phẩm là 1 dòng"></textarea>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <button type="submit" name="xacnhan_tao" class="btn btn-block btn-flat btn_xam text_black">Xác nhận</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="heading_gop" class="card collapse" aria-labelledby="heading_gop" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <textarea type="text" class="form-control" rows="5" value="" name="name_new" placeholder="Loại sản phẩm|Tên Combo|Loại|Tên sản phẩm 1|Tên sản phẩm 2|..."></textarea>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <button type="submit" name="xacnhan_gop" class="btn btn-block btn-flat btn_xam text_black">Xác nhận</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-2" id="list_show_sanp">
                <div class="card">
                    <div class="card-header alert-info">
                        <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size: px;color:white;">
                            Sản phẩm của bạn
                        </div>
                    </div>
                    <div class="">
                        <div class="table-responsive flex-row flex-nowrap">
                            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align:center; width:5%;">#</th>
                                        <th style="text-align:left;">Tên sản phẩm</th>
                                        <th style="text-align:center;">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sqlAll = "SELECT COUNT(*) FROM `sale_sanpham_01`";
                                    $stmt5 = $conn->query($sqlAll);
                                    $total_records  = $stmt5->fetchColumn();
                                    $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                                    $total_page = ceil($total_records / $limit);
                                    $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                                    $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                                    if ($total_page > 0) {
                                        $total_page_max = $total_page - 1;
                                    } else {
                                        $total_page_max = $total_page;
                                    }
                                    $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                                    $start_page = $_GET['page'] * $limit;
                                    $num_1 = 0;
                                    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_01 ORDER BY id ASC LIMIT $start_page, $limit");
                                    $stmt1->execute(array());
                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                    $num = 0;
                                    foreach ($list_code as $show_fp) {
                                        $num++;
                                        $a_on = 'on';
                                        if ($show_fp['hien_an'] == 44) {
                                            $a_on = 'off';
                                        }
                                        $quyen_edit = '<a href="javascript:void(0)" onclick="edit_show_name(' . $show_fp['id'] . ')"><i class="align-middle mr-2 fas fa-fw fa-pen"></i></a>
                                        <a href="javascript:void(0)" onclick="hidden_show_name(' . $show_fp['id'] . ')"><i class="align-middle mr-2 fas fa-fw fa-toggle-'.$a_on.'"></i></a>';
                                        $num_1 = $num_1 + 1;
                                        $num = $num_1 + $_GET['page'] * $limit;
                                        $ngay_hien_tai = date('d/m/Y', $time_php);
                                        $ngay_them = date('d/m/Y', $show_fp['time_add']);
                                        $ngay_show = $ngay_them == $ngay_hien_tai ? date('H:i:s', $show_fp['time_add']) : $ngay_them;
                                        echo '<tr>
                                            <td style="text-align:center;" title="' . $show_fp['mem'] . ' - ' . $show_fp['id'] . '">' . $num . '</td>
                                            <td style="font-family: center" title="' . $show_fp['name'] . '" id="show_name_' . $show_fp['id'] . '">' . substr($show_fp['name'],  0, 100)  . '</td>                                            
                                            <td style="text-align:center;" id="quyen_edit_' . $show_fp['id'] . '">
                                            ' . $quyen_edit . '
                                            </td>
                                            </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
                $i = 1;
                for ($i; $i <= $num; $i++) {
                    echo $chuoi_popup_edit[$i];
                }
                load_page(_sql01($_SERVER['REQUEST_URI']), $total_page, $limit, $total_records, $total_page_max);
                load_dialog($total_page_max, $member['id']); ?>
            </div>
        </div>
    </div>
    </div>
    <script>
        function hidden_show_name(uid) {
            a_string = document.getElementById("quyen_edit_" + uid).innerHTML;
            var abc = a_string.split('fa-toggle-on');
            if (abc.length > 1) {
                type_toggle = 44;
                a_string = a_string.replace("fa-toggle-on", "fa-toggle-off");
            } else {
                type_toggle = 0;
                a_string = a_string.replace("fa-toggle-off", "fa-toggle-on");
            }
            document.getElementById("quyen_edit_" + uid).innerHTML = a_string;
            $.post("js_047", {
                type_toggle: type_toggle,
                uid: uid
            }, function(data, status) {});
        }

        function hidden_show_name_2(uid) {
            a_string = document.getElementById("quyen_edit2_" + uid).innerHTML;
            var abc = a_string.split('fa-toggle-on');
            if (abc.length > 1) {
                type_toggle2 = 44;
                a_string = a_string.replace("fa-toggle-on", "fa-toggle-off");
            } else {
                type_toggle2 = 0;
                a_string = a_string.replace("fa-toggle-off", "fa-toggle-on");
            }
            document.getElementById("quyen_edit2_" + uid).innerHTML = a_string;
            $.post("js_047", {
                type_toggle2: type_toggle2,
                uid: uid
            }, function(data, status) {});
        }

        function edit_show_name(uid) {
            name = document.getElementById("show_name_" + uid).innerHTML;
            var abc = name.split('<input class=');
            if (abc.length > 1) {
                return;
            }

            document.getElementById("show_name_" + uid).innerHTML = "<input class=\"form-control\" onblur=\"update_show_name(" + uid + ")\" id=\"show_name1_" + uid + "\" value=\"" + name + "\">";
        }

        function update_show_name(uid) {
            name = document.getElementById("show_name1_" + uid).value;
            document.getElementById("show_name_" + uid).innerHTML = name;
            $.post("js_047", {
                sale_sanpham_01: uid,
                name: name
            }, function(data, status) {});
        }

        function edit_show_name_2(uid) {
            name = document.getElementById("show_name0_" + uid).innerHTML;
            var abc = name.split('<input class=');
            if (abc.length > 1) {
                return;
            }
            document.getElementById("show_name0_" + uid).innerHTML = "<input class=\"form-control\" onblur=\"update_show_name2(" + uid + ")\" id=\"show_nam_1_" + uid + "\" value=\"" + name + "\">";

            name = document.getElementById("show_name1_" + uid).innerHTML;
            document.getElementById("show_name1_" + uid).innerHTML = "<input class=\"form-control\" onblur=\"update_show_name2(" + uid + ")\" id=\"show_nam_2_" + uid + "\" value=\"" + name + "\">";

            name = document.getElementById("show_name2_" + uid).innerHTML;
            document.getElementById("show_name2_" + uid).innerHTML = "<input class=\"form-control\" onblur=\"update_show_name2(" + uid + ")\" id=\"show_nam_3_" + uid + "\" value=\"" + name + "\">";

            name = document.getElementById("show_name3_" + uid).innerHTML;
            document.getElementById("show_name3_" + uid).innerHTML = "<input class=\"form-control\" onblur=\"update_show_name2(" + uid + ")\" id=\"show_nam_4_" + uid + "\" value=\"" + name + "\">";
        }

        function update_show_name2(uid) {
            name = document.getElementById("show_nam_1_" + uid).value;
            document.getElementById("show_name0_" + uid).innerHTML = name;

            loai = document.getElementById("show_nam_2_" + uid).value;
            document.getElementById("show_name1_" + uid).innerHTML = loai;

            gia = document.getElementById("show_nam_3_" + uid).value;
            document.getElementById("show_name2_" + uid).innerHTML = gia;

            ship = document.getElementById("show_nam_4_" + uid).value;
            document.getElementById("show_name3_" + uid).innerHTML = ship;
            $.post("js_047", {
                sale_sanpham_02: uid,
                name: name,
                loai: loai,
                gia: gia,
                ship: ship
            }, function(data, status) {
                // alert(data);
            });
        }

        function show_id_sp() {
            $.post("add_sanpham", {
                load_ajax: document.getElementById("sale_sanpham_01").value
            }, function(data, status) {
                var html = data.trim().split('|||||');
                document.getElementById("show_du_lieu").innerHTML = html[0];
                document.getElementById("list_show_sanp").innerHTML = html[1];
            });
        }
    </script>