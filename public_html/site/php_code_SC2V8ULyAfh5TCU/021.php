<?php
$kq_sa[-1] = "Hủy đơn hàng";
$kq_sa[1] = "Chưa tiếp nhận";
$kq_sa[2] = "Đã tiếp nhận";
$kq_sa[3] = "Đã nhập kho";
$kq_sa[4] = "Đang giao hàng";
$kq_sa[5] = "Chưa đối soát";
$kq_sa[6] = "Đã đối soát";
$kq_sa[7] = "Không lấy được hàng";
$kq_sa[8] = "Hoãn lấy hàng";
$kq_sa[9] = "Không giao được hàng";
$kq_sa[10] = "Delay giao hàng";
$kq_sa[11] = "Trả lại hàng cho shop";
$kq_sa[12] = "Đang lấy hàng";
$kq_sa[13] = "Đơn hàng bồi hoàn";
$kq_sa[20] = "COD cầm hàng đi trả";
$kq_sa[21] = "COD đã trả xong hàng";
$kq_sa[123] = "Shipper báo đã lấy hàng";
$kq_sa[127] = "Shipper báo không lấy được hàng";
$kq_sa[128] = "Shipper báo delay lấy hàng";
$kq_sa[45] = "Shipper báo đã giao hàng";
$kq_sa[49] = "Shipper báo không giao được giao hàng";
$kq_sa[410] = "Shipper báo delay giao hàng";

if (isset($_GET['delfp'])) {
    $id_xoa = (int) $_GET['delfp'];
    $ma_hang = (int) $_GET['id'];

      // -- >Trả lại kho hàng khi xóa sản phẩm    
      $stmt =  $conn->prepare("SELECT * FROM vn_don_hang_ship WHERE id =:id");
      $stmt->execute(array(":id" => $id_xoa));
      $edit = $stmt->fetch(PDO::FETCH_ASSOC);
      if (($edit['mem'] == $id_member or $id_level > 3) and $edit['trang_thai'] != 44) {
          sql_kho_hang($edit['mau_sac'], $edit['size'], $edit['ma_hang'], $id_team, 4); //Xóa dữ liệu hàng hóa ở kho
      }
      // -- >Trả lại kho hàng khi xóa sản phẩm

    $sql = "UPDATE vn_don_hang_ship SET trang_thai=44 WHERE id=$id_xoa AND mem=$id_member";
    if ($id_level > 3) {
        $sql = "UPDATE vn_don_hang_ship SET trang_thai=44 WHERE id=$id_xoa";
    }
    $stmt = $conn->prepare($sql);
    $stmt->execute();  
    header('Location: /list_hang');
    exit;
}

if (isset($_GET['search'])) {
    if ($_GET['search'] == 0) {
        $_SESSION['tim_kiem_list_hang'] = '1=1';
        header('Location: /list_hang');
        exit;
    }
}
$post_value = "";
if (isset($_POST['tim_kiem_v2'])) {
    $post_value = _sql01(trim($_POST['value_tim_kiem']));
    $int_post_value = _sql_num(trim($_POST['value_tim_kiem']));
    if ($int_post_value > 10000) {
        $int_post_value = (int)$int_post_value;
    }
    $do_dai_chuoi = strlen($_POST['value_tim_kiem']);
    if ($do_dai_chuoi > 3) {
        $_SESSION['tim_kiem_list_hang'] = '`sdt` LIKE \'%' . $int_post_value . '%\' OR `nameKH` LIKE \'%' . $post_value . '%\' OR `ma_ghtk` LIKE \'%' . $post_value . '%\'';
        if ($int_post_value == "") {
            $_SESSION['tim_kiem_list_hang'] = '`nameKH` LIKE \'%' . $post_value . '%\'';
        }
    } else {
        $_SESSION['tim_kiem_list_hang'] = '`mem`=' . $int_post_value;
    }
    if($_POST['value_tim_kiem']>9999 AND $_POST['value_tim_kiem']<99999){
        $_SESSION['tim_kiem_list_hang'] = '`id` = ' . $int_post_value;        
    }
}

if (!isset($_SESSION['tim_kiem_list_hang'])) {
    $_SESSION['tim_kiem_list_hang'] = '1=1';
}

$timkiem = $_SESSION['tim_kiem_list_hang'];
$title = 'Danh sách đơn hàng';
require 'site/widget/header.php'; ?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-header bg_xanh_duong_nhat">
                        <h5 class="card-title mb-0">Tìm kiếm nhanh chóng..!</h5>
                    </div>
                    <form method="post" action="<?= _sql03($_SERVER['REQUEST_URI']) ?>">
                        <div class="card-body">
                            <div class="input-group mb-3">
                                <input name="value_tim_kiem" class="form-control" value="<?= $post_value ?>" placeholder="Gõ sdt, tên khách, id nhân viên" autocomplete="off">
                                <span class="input-group-append"><button type="submit" name="tim_kiem_v2" class="btn btn_primary" type="button">Tìm kiếm!</button></span>
                            </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 mt-2">
            <div class="card">
                <div class="card-header alert-info">
                    <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                        Danh sách đơn hàng
                    </div>
                </div>
                <div class="">
                    <div class="table-responsive flex-row flex-nowrap">
                        <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                            <thead>
                                <tr>
                                    <th style="text-align:center; width:5%;">#</th>
                                    <th style="text-align:center;">Member</th>
                                    <th style="text-align:center;">Tên KH</th>
                                    <th style="text-align:center;">SĐT</th>
                                    <th style="text-align:center;">Trạng thái</th>
                                    <th style="text-align:center;">Sản phẩm</th>
                                    <th style="text-align:center;">Tổng tiền - GHTK - Sl</th>
                                    <th style="text-align:center;">Ghi chú</th>
                                    <th style="text-align:center;">Ngày thêm</th>
                                    <th style="text-align:center; width:7%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sqlAll = "SELECT COUNT(`mem`) FROM `vn_don_hang_ship` WHERE team=$id_team AND trang_thai!=44 AND $timkiem";
                                $stmt5 = $conn->query($sqlAll);
                                $total_records  = $stmt5->fetchColumn();
                                $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                                $total_page = ceil($total_records / $limit);
                                $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                                $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                                if ($total_page > 0) {
                                    $total_page_max = $total_page - 1;
                                } else {
                                    $total_page_max = $total_page;
                                }
                                $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                                $start_page = $_GET['page'] * $limit;
                                $num_1 = 0;

                                $stmt1 =  $conn->prepare("SELECT * FROM vn_don_hang_ship WHERE team=$id_team AND trang_thai!=44 AND $timkiem ORDER BY id DESC LIMIT $start_page, $limit");
                                $stmt1->execute(array());
                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                $num = 0;
                                foreach ($list_code as $show_fp) {
                                    $mas_tk = "";
                                    $arr = (array) $show_fp;
                                    $num_1 = $num_1 + 1;
                                    $num = $num_1 + $_GET['page'] * $limit;
                                    $ngay_hien_tai = date('d/m/Y', $time_php);
                                    $ngay_them = date('d/m/Y', $show_fp['time_add']);
                                    $ngay_show = $ngay_them == $ngay_hien_tai ? date('H:i:s', $show_fp['time_add']) : $ngay_them;
                                    $sho_ghi = _sql($show_fp['ghi_chu']) != '' ? _sql($show_fp['ghi_chu']) : _sql($show_fp['address_detail']);
                                    $sho_ghi_a = mb_substr($sho_ghi, 0, 20, 'UTF-8');
                                    if (($id_member == $show_fp['mem'] or $id_level > 3) and $show_fp['code_xuat'] == "") {
                                        $quyen_edit = '<a href="don_hang&edit_id=' . $show_fp['id'] . '"><i class="align-middle" data-feather="edit" style="color:#495057"></i></a>
                                            <a href="' . _sql03($_SERVER['REQUEST_URI']) . '&delfp=' . $show_fp['id'] . '" onclick="return confirm(\'#' . $num . ' - Đơn hàng của: ' . $show_fp['nameKH'] . ', sẽ bị xóa?\')"><i class="align-middle" data-feather="trash" style="color:#495057"></i></a>';
                                        $d_sdt = $show_fp['sdt'];
                                    } else {
                                        $quyen_edit = '<a href="don_hang&edit_id=' . $show_fp['id'] . '"><i class="align-middle" data-feather="edit" style="color:#495057"></i></a>
                                            <a href="' . _sql03($_SERVER['REQUEST_URI']) . '&delfp=' . $show_fp['id'] . '" onclick="return confirm(\'#' . $num . ' - Đơn hàng của: ' . $show_fp['nameKH'] . ', sẽ bị xóa?\')"><i class="align-middle" data-feather="trash" style="color:#495057"></i></a>';
                                        $d_sdt = substr($show_fp['sdt'], 0, 7) . '***';
                                    }

                                    if ($id_level > 3) {
                                        $d_sdt = $show_fp['sdt'];
                                    }
                                    $trang_thai_v1 = 'Chưa xuất hàng';
                                    if ($show_fp['code_xuat'] > 100) {
                                        $mas_tk = "color:black;";
                                        $trang_thai_v1 = 'Đang gói hàng';
                                    }
                                    if ($show_fp['ma_ghtk'] != "0") {
                                        $mas_tk = "color:navy;";
                                        $trang_thai_v1 = 'Đang gửi hàng';
                                    }
                                    if ($show_fp['gh_status'] != "0") {
                                        $mas_tk = "color:blue;";
                                        if ($show_fp['gh_status'] == 11 or $show_fp['gh_status'] == 9 or $show_fp['gh_status'] == 10) {
                                            $mas_tk = "color:red;";
                                        }
                                        if ($show_fp['gh_status'] == 3) {
                                            $mas_tk = "color:black;";
                                        }
                                        $trang_thai_v1 = $kq_sa[$show_fp['gh_status']];
                                    }

                                    if (isset($int_post_value)) {
                                        $mas_dd = $do_dai_chuoi;
                                        if ($int_post_value == substr($d_sdt, -$mas_dd)) {
                                            $mas_tk = "color:red;";
                                        }
                                    }
                                    $tien_ghtk = number_format((int)$show_fp['gh_pick_money'], 0);
                                    echo '<tr>
                                        <td style="text-align:center; ' . $mas_tk . '">' . $num . '</td>
                                        <td style="text-align:center; ' . $mas_tk . '">' . ucwords(sql_member($show_fp['mem'], 'name')) . '</td>
                                        <td style="text-align:center; ' . $mas_tk . '">' . $show_fp['nameKH'] . '</td>                                                                                
                                        <td style="text-align:center; ' . $mas_tk . '">0' . $d_sdt . ' - (' . check_nha_mang('0' . $d_sdt, 2) . ')</td>
                                        <td style="text-align:center; ' . $mas_tk . '">' . $trang_thai_v1 . '</td>      
                                        <td style="text-align:center; ' . $mas_tk . '">' . $show_fp['nameSP'] . '</td>                                        
                                        <td style="text-align:center; ' . $mas_tk . '">' . number_format($show_fp['total_money'], 0) . ' - ' . $tien_ghtk . ' - ' . $show_fp['sl_mua'] . '</td>
                                        <td style="text-align:center; ' . $mas_tk . '" title="' . $sho_ghi . '">' . $sho_ghi_a . '</td>
                                        <td style="text-align:center; ' . $mas_tk . '" title="' . date('d/m/Y H:i:s', $show_fp['time_add']) . '">' .  $ngay_show . '</td>
                                        <td style="text-align:center; ' . $mas_tk . '">' . $quyen_edit . '</td>
                                        </tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php
            load_page(_sql02($_SERVER['REQUEST_URI']), $total_page, $limit, $total_records, $total_page_max);
            load_dialog($total_page_max, $member['id']);
            ?>
        </div>
    </div>
    </div>
</main>