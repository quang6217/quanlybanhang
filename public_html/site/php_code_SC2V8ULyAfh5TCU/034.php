<?php
if (isset($_POST['post_sc_dh'])) {
    if ($id_level < 2) {
        dem_nguoc_url('/ke_hang', 'Chỉ MOD trở lên mới có quyền báo cáo sự cố đơn hàng', 'Cần cố gắng hơn nữa để được thăng LEVEL bạn nhé!', 8);
        exit; //Thoát nếu không đủ quyền để update sự cố
    }
    $sc_id_post = (int)$_POST['sc_id_post'];
    $sc_status = (int)$_POST['sc_status'];
    $sc_notes = _sql($_POST['sc_notes']);
    if ($sc_status < 1) {
        $so_giay = 8;
        dem_nguoc_url('/ke_hang', 'Bạn chưa cho biết sự cố của đơn hàng!', 'Chú ý: Đền đầy đủ thông tin trước khi xác nhận!', 8);
        exit;
    }

    if ($sc_status == 3) { //Trừ tiền nếu nhân viên lên sai đơn hàng
        $thanh_tien = sql_sale($id_team, 'saidh');
        if ($thanh_tien != 0) {
            $stmt =  $conn->prepare("SELECT * FROM vn_don_hang_ship WHERE id =:id");
            $stmt->execute(array(":id" => $sc_id_post));
            $edit = $stmt->fetch(PDO::FETCH_ASSOC);
            $mem_sai = (int)$edit['mem'];
            if ($edit['sc_dh'] == 0) {
                lich_su_giao_dich($thanh_tien, 9, $mem_sai, 7, 'Sai ĐH ' . $sc_id_post, '', 8, $time_php);
            }
        }
    }

    $sql = "UPDATE vn_don_hang_ship SET time_sc=$time_php, sc_mem_up=$id_member, sc_dh=$sc_status, sc_note='$sc_notes' WHERE id='$sc_id_post'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    $_SESSION['tim_kiem_list_hang'] = '1=1';
    header('Location: /ke_hang');
    exit;
}
if (isset($_GET['search'])) {
    if ($_GET['search'] == 0) {
        $_SESSION['tim_kiem_list_hang'] = '1=1';
        header('Location: /ke_hang');
        exit;
    }
}
$post_value = "";
if (isset($_POST['tim_kiem_v2'])) {

    $post_value = _sql01(trim($_POST['value_tim_kiem']));
    $int_post_value = _sql_num(trim($_POST['value_tim_kiem']));
    if ($int_post_value > 10000) {
        $int_post_value = (int)$int_post_value;
    }

    $do_dai_chuoi = strlen($_POST['value_tim_kiem']);
    if ($do_dai_chuoi > 3) {
        $_SESSION['tim_kiem_list_hang'] = '`sdt` LIKE \'%' . $int_post_value . '%\' OR `nameKH` LIKE \'%' . $post_value . '%\' OR `ma_ghtk` LIKE \'%' . $post_value . '%\'';
        if ($int_post_value == "") {
            $_SESSION['tim_kiem_list_hang'] = '`nameKH` LIKE \'%' . $post_value . '%\'';
        }
    } else {
        $_SESSION['tim_kiem_list_hang'] = '`mem`=' . $int_post_value;
    }
    if($_POST['value_tim_kiem']>9999 AND $_POST['value_tim_kiem']<99999){
        $_SESSION['tim_kiem_list_hang'] = '`id` = ' . $int_post_value;        
    }
}

if (!isset($_SESSION['tim_kiem_list_hang'])) {
    $_SESSION['tim_kiem_list_hang'] = '1=1';
}

$timkiem = $_SESSION['tim_kiem_list_hang'];
$title = 'Ke hàng';
require 'site/widget/header.php';
$su_co_a[0] = '...........';
$su_co_a[1] = 'SHOP chịu';
$su_co_a[2] = 'ĐÓNG GÓI';
$su_co_a[3] = 'LÊN ĐƠN';
$su_co_a[4] = 'Xưởng sản xuất';
$su_co_a[5] = 'Cố tình bom hàng';
$su_co_a[6] = 'Lý do khác';

// $su_co_a[1] = 'Không đúng size (mặc không vừa)';
// $su_co_a[2] = 'Nhân viên đóng gói';
// $su_co_a[12] = 'Nhân viên lên đơn sai';
// $su_co_a[13] = 'Hàng bị rách, bị lỗi';
// $su_co_a[3] = 'Cố tình bom hàng';
// $su_co_a[4] = 'Khách không đặt hàng (nhầm số điện thoại)';
// $su_co_a[5] = 'Nhận hàng 1 phần trong đơn hàng';
// $su_co_a[6] = 'Yêu cầu giảm tiền SHIP mới nhận hàng';
// $su_co_a[7] = 'Trùng đơn, cướp đơn (khách đã nhận hàng)';
// $su_co_a[8] = 'Không liên lạc được với khách';
// $su_co_a[9] = 'Sản phẩm không giống quảng cáo';
// $su_co_a[10] = 'Lý do khác';
// $su_co_a[11] = 'Giao thiếu hàng, khách không nhận';
?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-header bg_xanh_duong_nhat">
                        <h5 class="card-title mb-0">Tìm kiếm nhanh chóng..!</h5>
                    </div>
                    <form method="post" action="<?= _sql03($_SERVER['REQUEST_URI']) ?>">
                        <div class="card-body">
                            <div class="input-group mb-3">
                                <input name="value_tim_kiem" class="form-control" value="<?= $post_value ?>" placeholder="Gõ sdt, tên khách, id đơn hàng" autocomplete="off">
                                <span class="input-group-append"><button type="submit" name="tim_kiem_v2" class="btn btn_primary" type="button">Tìm kiếm!</button></span>
                            </div>
                    </form>
                </div>
            </div>
        </div>
        <? if($timkiem!="1=1") {?>
        <div class="col-12 mt-2">
            <div class="card">
                <div class="card-header alert-info">
                    <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                        Kết quả tìm kiếm
                    </div>
                </div>
                <div class="">
                    <div class="table-responsive flex-row flex-nowrap">
                        <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                            <thead>
                                <tr>
                                    <th style="text-align:center;  width:5%">#</th>
                                    <th style="text-align:center;">Tên KH</th>
                                    <th style="text-align:center;">SĐT</th>
                                    <th style="text-align:center;">Sản phẩm</th>
                                    <th style="text-align:center;">Địa chỉ</th>
                                    <th style="text-align:center; width:7%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $stmt1 =  $conn->prepare("SELECT * FROM vn_don_hang_ship WHERE team=$id_team AND trang_thai!=44 AND `code_xuat` IS NOT NULL AND $timkiem ORDER BY id DESC LIMIT 100");
                                $stmt1->execute(array());
                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                $num = 0;
                                foreach ($list_code as $show_fp) {
                                    $mas_tk = "";
                                    $arr = (array) $show_fp;
                                    $num++;
                                    $ngay_hien_tai = date('d/m/Y', $time_php);
                                    $ngay_them = date('d/m/Y', $show_fp['time_add']);
                                    $ngay_show = $ngay_them == $ngay_hien_tai ? date('H:i:s', $show_fp['time_add']) : $ngay_them;
                                    $sho_ghi = _sql($show_fp['ghi_chu']) != '' ? _sql($show_fp['ghi_chu']) : _sql($show_fp['address_detail']);
                                    $sho_ghi_a = mb_substr($sho_ghi, 0, 20, 'UTF-8');

                                    $trang_thai_v1 = 'Chưa xuất hàng';
                                    if ($show_fp['code_xuat'] != "") {
                                        $mas_tk = "color:black;";
                                        $trang_thai_v1 = 'Đang gói hàng';
                                    }
                                    if ($show_fp['ma_ghtk'] != "") {
                                        $mas_tk = "color:navy;";
                                        $trang_thai_v1 = 'Đang gửi hàng';
                                    }
                                    if ($show_fp['gh_status'] != "") {
                                        $mas_tk = "color:blue;";
                                        if ($show_fp['gh_status'] == 11 or $show_fp['gh_status'] == 9 or $show_fp['gh_status'] == 10) {
                                            $mas_tk = "color:red;";
                                        }
                                        if ($show_fp['gh_status'] == 3) {
                                            $mas_tk = "color:black;";
                                        }
                                        $trang_thai_v1 = $kq_sa[$show_fp['gh_status']];
                                    }

                                    if (isset($int_post_value)) {
                                        $mas_dd = $do_dai_chuoi;
                                        if ($int_post_value == substr($d_sdt, -$mas_dd)) {
                                            $mas_tk = "color:red;";
                                        }
                                    }
                                    $tien_ghtk = number_format((int)$show_fp['gh_pick_money'], 0);
                                    $quyen_edit = '                                    
                                    <button type="button" id="show-popup" class="button_a" data-toggle="modal" data-target="#set_' . $num . '"><i class="align-middle" data-feather="edit"></i></button> 
                                    <a href="https://khachhang.giaohangtietkiem.vn/khachhang?code=' . $show_fp['ma_ghtk'] . '" target="_blank"><img src="/img/ghtk-favicon.png" alt="Giaohangtietkiem.vn" style="width="20" height="20""></a>
                                    ';
                                    $d_sdt = $show_fp['sdt'];
                                    $dia_chi_kh = info_vn_xa_v3($show_fp['xaKH']);
                                    echo '<tr>
                                        <td style="text-align:center;">' . $num . '</td>                                        
                                        <td style="text-align:center;">' . $show_fp['id'] . ' - ' . $show_fp['nameKH'] . '</td>                                                                                
                                        <td style="text-align:center;">0' . $d_sdt . ' - (' . check_nha_mang('0' . $d_sdt, 2) . ')</td>                                        
                                        <td style="text-align:center;">' . $show_fp['nameSP'] . ' - (' . $show_fp['sl_mua'] . ')</td>                                                                                
                                        <td style="text-align:center;">' . $dia_chi_kh . '</td>                                        
                                        <td style="text-align:center;">' . $quyen_edit . '</td>
                                        </tr>';
                                    $mau_size = '';
                                    $arr_ms = explode('|', str_replace('0|', '', $show_fp['mau_sac']));
                                    $arr_sz = explode('|', str_replace('0|', '', $show_fp['size']));
                                    for ($a = 0; $a <= $show_fp['sl_mua']; $a++) {
                                        if ($arr_ms[$a] > 0 and $arr_sz[$a] > 0) {
                                            $mau_size = $mau_size . '' . info_mau_sac($arr_ms[$a]) . ' - ' . info_size($arr_sz[$a]) . '<br>';
                                        }
                                    }
                                    $selected[0] = '';
                                    $selected[1] = '';
                                    $selected[2] = '';
                                    $selected[3] = '';
                                    $selected[4] = '';
                                    $selected[5] = '';
                                    $selected[6] = '';
                                    $selected[7] = '';
                                    $selected[8] = '';
                                    $selected[9] = '';
                                    $selected[10] = '';
                                    $selected[$show_fp['sc_dh']] = 'selected';
                                    $sho_ghica = '';
                                    if ($show_fp['ghi_chu'] != '') {
                                        $sho_ghica = '<div class="alert alert-secondary alert-outline-coloured alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                        <div class="alert-icon">
                                        <i class="align-middle mr-2 fas fa-fw fa-marker"></i>
                                        </div>
                                        <div class="alert-message">
                                            <strong>' . $show_fp['ghi_chu'] . '</strong>
                                        </div>
                                    </div>';
                                    }


                                    $chuoi_popup[$num] = '
                                        <div class="modal fade" id="set_' . $num . '" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header bg_xanh_duong_nhat">
                                                        <h2 style="text-align:center;">#' . $show_fp['id'] . ' - kh: ' . $show_fp['nameKH'] . ' - sđt: 0' . $d_sdt . '</h2>
                                                        <button type="button" class="btn btn-outline-danger" data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form method="post" action="' . _sql03($_SERVER['REQUEST_URI']) . '">
                                                            <div class="card-body">                                                                                                                                                                                               
                                                                <input type="hidden" name="sc_id_post" value="' . $show_fp['id'] . '">    
                                                                
                                                                <div class="alert alert-secondary alert-outline-coloured alert-dismissible" role="alert">
                                                                
                                                                <div class="alert-icon">
                                                                <i class="align-middle mr-2 fas fa-fw fa-tired"></i>
                                                                </div>
                                                                <div class="alert-message">
                                                                    <strong>' . ucwords(sql_member($show_fp['mem'], 'name')) . '</strong>
                                                                </div>
                                                            </div>   


                                                                <div class="alert alert-secondary alert-outline-coloured alert-dismissible" role="alert">                                                                    
                                                                    <div class="alert-icon">
                                                                    <i class="align-middle mr-2 fas fa-fw fa-address-card"></i>
                                                                    </div>
                                                                    <div class="alert-message">                                                                        
                                                                        ' . _sql($show_fp['address_detail']) . '<br>
                                                                        <strong>' . $dia_chi_kh . '</strong>
                                                                        </div>
                                                                    </div>   
                                                                <div class="alert alert-secondary alert-outline-coloured alert-dismissible" role="alert">                                                                    
                                                                    <div class="alert-icon">
                                                                    <i class="align-middle mr-2 fas fa-fw fa-cart-plus"></i>
                                                                    </div>
                                                                    <div class="alert-message">
                                                                        <strong>' . $mau_size . '</strong>
                                                                    </div>
                                                                </div>    
                                                                
                                                                <div class="alert alert-secondary alert-outline-coloured alert-dismissible" role="alert">                                                                    
                                                                    <div class="alert-icon">
                                                                    <i class="align-middle mr-2 fas fa-fw fa-dollar-sign"></i>
                                                                    </div>
                                                                    <div class="alert-message">
                                                                        <strong>' . number_format($show_fp['total_money'], 0) . 'đ</strong>
                                                                    </div>
                                                                </div>
                                                                ' . $sho_ghica . '
                                                                <div class="alert alert-secondary alert-outline-coloured alert-dismissible" role="alert">                                                                    
                                                                    <div class="alert-icon">
                                                                    <i class="align-middle mr-2 fas fa-fw fa-frown"></i>
                                                                    </div>
                                                                    <div class="alert-message">
                                                                    <select class="form-control" name="sc_status"> 
                                                                        <option value="0" ' . $selected[0] . '> ' . $su_co_a[0] . '</option>
                                                                        <option value="1" ' . $selected[1] . '>1 - ' . $su_co_a[1] . '</option>
                                                                        <option value="2" ' . $selected[2] . '>2 - ' . $su_co_a[2] . '</option>
                                                                        <option value="3" ' . $selected[3] . '>3 - ' . $su_co_a[3] . '</option>                                                                        
                                                                        <option value="4" ' . $selected[4] . '>4 - ' . $su_co_a[4] . '</option>
                                                                        <option value="5" ' . $selected[5] . '>5 - ' . $su_co_a[5] . '</option>
                                                                        <option value="6" ' . $selected[6] . '>6 - ' . $su_co_a[6] . '</option>
                                                                    </select>
                                                                    </div>
                                                                </div>  
                                                                <div class="alert alert-secondary alert-outline-coloured alert-dismissible" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                                                <div class="alert-icon">
                                                                <i class="align-middle mr-2 fas fa-fw fa-comment-dots"></i>
                                                                </div>
                                                                <div class="alert-message">
                                                                    <textarea class="form-control" name="sc_notes" rows="3">' . _sql($show_fp['sc_note']) . '</textarea>
                                                                    </div>
                                                                </div>                                                                 
                                                                <button type="submit" name="post_sc_dh" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php
            $i = 1;
            for ($i; $i <= $num; $i++) {
                echo $chuoi_popup[$i];
            }
            ?>
        </div>
        <?}?>
        <div class="col-12 mt-2">
            <div class="card">
                <div class="card-header alert-info">
                    <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                        Đơn hàng gặp sự cố
                    </div>
                </div>
                <div class="">
                    <div class="table-responsive flex-row flex-nowrap">
                        <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                            <thead>
                                <tr>
                                    <th style="text-align:center; width:5%;">#</th>
                                    <th style="text-align:center;">Nhân viên</th>
                                    <th style="text-align:center;">Tên KH</th>
                                    <th style="text-align:center;">SĐT</th>
                                    <th style="text-align:center;">Sản phẩm - (SL)</th>
                                    <th style="text-align:center;">Trách nhiệm</th>
                                    <th style="text-align:center;">Ghi chú</th>
                                    <th style="text-align:center;">Thời gian</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $sqlAll = "SELECT COUNT(`mem`) FROM `vn_don_hang_ship` WHERE team=$id_team AND `sc_dh`!=0";
                                $stmt5 = $conn->query($sqlAll);
                                $total_records  = $stmt5->fetchColumn();
                                $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                                $total_page = ceil($total_records / $limit);
                                $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                                $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                                if ($total_page > 0) {
                                    $total_page_max = $total_page - 1;
                                } else {
                                    $total_page_max = $total_page;
                                }
                                $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                                $start_page = $_GET['page'] * $limit;
                                $num_1 = 0;

                                $stmt1 =  $conn->prepare("SELECT * FROM vn_don_hang_ship WHERE team=$id_team AND `sc_dh`!=0 ORDER BY time_sc DESC LIMIT $start_page, $limit");
                                $stmt1->execute(array());
                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                $num = 0;
                                foreach ($list_code as $show_fp) {
                                    $mas_tk = "";
                                    $arr = (array) $show_fp;
                                    $num_1 = $num_1 + 1;
                                    $num = $num_1 + $_GET['page'] * $limit;
                                    $d_sdt = $show_fp['sdt'];
                                    $dia_chi_kh = info_vn_xa_v3($show_fp['xaKH']);
                                    $su_alsks = _sql($show_fp['sc_note']) != '' ? _sql($show_fp['sc_note']) : '...';
                                    echo '<tr>
                                        <td style="text-align:center;">' . $num . '</td>   
                                        <td style="text-align:center;"><a title="' . $show_fp['id'] . '">' . ucwords(sql_member($show_fp['mem'], 'name')) . '</a></td>                                     
                                        <td style="text-align:center;">' . $show_fp['nameKH'] . '</td>                                                                                
                                        <td style="text-align:center;">0' . $d_sdt . ' - (' . check_nha_mang('0' . $d_sdt, 2) . ')</td>                                        
                                        <td style="text-align:center;">' . $show_fp['nameSP'] . ' - (' . $show_fp['sl_mua'] . ')</td>                                                                                
                                        <td style="text-align:center;">' . $su_co_a[$show_fp['sc_dh']] . '</td>                                                                                
                                        <td style="text-align:center;">' . $su_alsks . '</td>     
                                        <td style="text-align:center;">' . date('H:i d/m', $show_fp['time_sc']) . '</td>  
                                        </tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php
            load_page(_sql02($_SERVER['REQUEST_URI']), $total_page, $limit, $total_records, $total_page_max);
            load_dialog($total_page_max, $member['id']);
            $i = 1;
            for ($i; $i <= $num; $i++) {
                echo $chuoi_popup[$i];
            }
            ?>
        </div>
    </div>
    </div>
</main>