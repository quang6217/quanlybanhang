</div>
<div class="type_msg">
    <div class="input_msg_write" id="html_sent_sms">
        <? echo type_msg($uid, $memfacebook['uid_fanpage'], $memfacebook['uid_member']); ?>
    </div>
</div>
</div>
<div class=" card" style="margin-bottom: -0.1rem;background-color: #fdfdfd;height: calc(100vh - 112px);overflow-y: scroll;">
    <div class="card" id="don_hang_vua_len">
        <?php
        if ($memfacebook['id_don_hang'] > 0) {
            echo '<div class="card-header alert-secondary">
                                <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                                    Đơn hàng gần đây
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive flex-row flex-nowrap">';
            $stmt =  $conn->prepare("SELECT * FROM vn_don_hang_ship WHERE id =:id");
            $stmt->execute(array(":id" => (int)$memfacebook['id_don_hang']));
            $edit = $stmt->fetch(PDO::FETCH_ASSOC);
            $mau_size = '';
            $arr_ms = explode('|', str_replace('0|', '', $edit['mau_sac']));
            $arr_sz = explode('|', str_replace('0|', '', $edit['size']));
            for ($a = 0; $a <= $edit['sl_mua']; $a++) {
                if ($arr_ms[$a] > 0 and $arr_sz[$a] > 0) {
                    $mau_size = $mau_size . '' . info_mau_sac($arr_ms[$a]) . ' - ' . info_size($arr_sz[$a]) . '<br>';
                }
            }
            echo ' 
                                    <div class="alert alert-secondary alert-outline-coloured alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                        <div class="alert-icon">
                                        <i class="align-middle mr-2 fas fa-fw fa-cart-plus"></i>
                                        </div>
                                        <div class="alert-message">
                                            <strong>' . $edit['nameSP'] . ' - SL: ' . $edit['sl_mua'] . '<br>' . $mau_size . '</strong>
                                        </div>
                                    </div>    

                                    <div class="alert alert-secondary alert-outline-coloured alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                        <div class="alert-icon">
                                        <i class="align-middle mr-2 fas fa-fw fa-dollar-sign"></i>
                                        </div>
                                        <div class="alert-message">
                                            <strong>' . number_format($edit['total_money'], 0) . 'đ</strong>
                                        </div>
                                    </div> </div>
                                    </div>';
            // Tự động nhận diện và điền thông tin dựa vào UID có sẳn
            $sdt = '0' . _sql00($edit['sdt']);
            $nmang_sdt = check_nha_mang($sdt);
            $nameKH = _sql01($edit['nameKH']);
            $linkFB = _sql00($edit['linkFB']);
            $in_id_sp = (int) $edit['ma_hang'];
            $in_name_sp = _sql($edit['nameSP']);
            $in_sl = (int) $edit['sl_mua'];
            $in_gia_sp = (int) $edit['total_money'] / 1000;
            $address_detail = _sql($edit['address_detail']);
            $ghi_chu = _sql($edit['ghi_chu']);
            $json_xa = json_decode(info_vn_xa((int) $edit['xaKH']), true);
            $ct_xa_kh = $json_xa['full_name'];
            $stmt =  $conn->prepare("SELECT * FROM vn_san_pham WHERE id =:id");
            $stmt->execute(array(":id" => $in_id_sp));
            $sanp = $stmt->fetch(PDO::FETCH_ASSOC);
            $in_name_sp = _sql01($sanp['name']);
            $in_gia_sp1 = (int) $sanp['gia_ban'] / 1000;
            $in_tien_ship = (int) $sanp['phi_ship'] / 1000;
            // ------------> Lấy dữ liệu màu sắc
            $html_i_ms = '';
            $ar_ms1 = explode('|', $edit['mau_sac']);
            for ($a = 0; $a < count($ar_ms1); $a++) {
                $ar_mau = explode('|', $sanp['mau_sac']);
                $in_mau_sac = '';
                for ($i = 0; $i < count($ar_mau); $i++) {
                    $stmt =  $conn->prepare("SELECT * FROM vn_mau_sac WHERE id =:id");
                    $stmt->execute(array(":id" => (int) $ar_mau[$i]));
                    $mau = $stmt->fetch(PDO::FETCH_ASSOC);
                    $selected_a = $mau['id'] == $ar_ms1[$a] ? 'selected' : '';
                    $in_mau_sac = $in_mau_sac . '<option value="' . $mau['id'] . '" title="' . $mau['mo_ta'] . ' - ' . $ar_ms1[$a] . '" ' . $selected_a . '>' . $mau['mau'] . '</option>';
                }
                if ($a == 0) {
                    $name_ms_s = 'mau_sac';
                } else {
                    $a1 = $a - 1;
                    $name_ms_s = 'mau_sac_' . $a1;
                }
                $html_i_ms = $html_i_ms . '<select class="custom-select flex-grow-1" id="vn_mau_sac" name="' . $name_ms_s . '" onchange="vn_mau_sac()" required>' . $in_mau_sac . '</select>';
            }
            // ------------> Lấy dữ liệu size
            $html_i_sz = '';
            $ar_ms1 = explode('|', $edit['size']);
            for ($a = 0; $a < count($ar_ms1); $a++) {
                $ar_size = explode('|', $sanp['size']);
                $in_size = '';
                for ($i = 0; $i < count($ar_size); $i++) {
                    $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id AND sex =:sex");
                    $stmt->execute(array(":id" => (int) $ar_size[$i], ":sex" => (int) $sanp['sex']));
                    $mau = $stmt->fetch(PDO::FETCH_ASSOC);
                    $selected_a = $mau['id'] == $ar_ms1[$a] ? 'selected' : '';
                    $in_size = $in_size . '<option value="' . $mau['id'] . '" title="Cao: ' . $mau['chieu_cao'] . ' - Nặng: ' . $mau['can_nang'] . ' - Eo: ' . $mau['vong_eo'] . ' - Ngực: ' . $mau['vong_nguc'] . '" ' . $selected_a . '>' . $mau['name'] . '</option>';
                }
                $name_ms_s = '';
                if ($a == 0) {
                    $name_ms_s = 'size';
                } else {
                    $a1 = $a - 1;
                    $name_ms_s = 'size_' . $a1;
                }
                $html_i_sz = $html_i_sz . '<select class="custom-select flex-grow-1" id="vn_size" name="' . $name_ms_s . '" onchange="vn_size()" required>' . $in_size . '</select>';
            }
            $ar_size = explode('|', $sanp['size']);
            $in_size = '';
            for ($i = 0; $i < count($ar_size); $i++) {
                $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id AND sex =:sex");
                $stmt->execute(array(":id" => (int) $ar_size[$i], ":sex" => (int) $sanp['sex']));
                $mau = $stmt->fetch(PDO::FETCH_ASSOC);
                $selected_a = $i + 1 == $edit['size'] ? 'selected' : '';
                $in_size = $in_size . '<option value="' . $mau['id'] . '" title="Cao: ' . $mau['chieu_cao'] . ' - Nặng: ' . $mau['can_nang'] . ' - Eo: ' . $mau['vong_eo'] . ' - Ngực: ' . $mau['vong_nguc'] . '" ' . $selected_a . '>' . $mau['name'] . '</option>';
            }
            // ------------> Lấy dữ liệu tỉnh
            $selected_tinh = '';
            $selected_huyen = '<option value="' . $json_xa['id_huyen'] . '">' . $json_xa['name_huyen'] . '</option>';
            $selected_xa = '<option value="' . $json_xa['id_xa'] . '">' . $json_xa['name_xa'] . '</option>';
            $stmt1 =  $conn->prepare("SELECT * FROM vn_tinh ORDER BY id ASC");
            $stmt1->execute();
            $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
            foreach ($list_code as $show) {
                $selected_ck_t = $show['id'] == $json_xa['id_tinh'] ? 'selected' : '';
                $selected_tinh = $selected_tinh . '<option value="' . $show['id'] . '" ' . $selected_ck_t . '>' . $show['name'] . '</option>';
            }
        }
        ?>
    </div>
    <div class="card-header alert-info">
        <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
            Thêm đơn hàng
        </div>
    </div>
    <div class="card-body">
        <form method="post">
            <div class="form-row">
                <div class="form-row">
                    <div class="form-group mb-3  col-md-6">
                        <input class="form-control h38" type="number" id="id_c_sdta" name="sdt" value="<?= $sdt ?>" placeholder="SĐT khách hàng" onchange="check_sdt()" autocomplete="off" required></input>
                    </div>
                    <div class="form-group mb-3  col-md-6">
                        <input class="form-control h38" type="text" id="id_check_mang_v2" name="check_mang" value="<?= $nmang_sdt ?>" placeholder="Mạng di động" readonly></input>
                    </div>
                    <div class="form-group mb-3 col-md-12">
                        <input class="form-control h38" name="nameKH" id="id_a_namekh" value="<?= $nameKH ?>" placeholder="Tên khách hàng" autocomplete="off" required></input>
                    </div>
                    <div class="form-group mb-3  col-md-12">
                        <input class="form-control h38" type="text" name="address_detail" id="id_address_detail" value="<?= $address_detail ?>" placeholder="Nhập địa chỉ chi tiết (nhà, ngõ, đường)" autocomplete="off" required></input>
                    </div>
                    <div class="input-group mb-3 col-md-12">
                        <input class="form-control h38" id="goi_y_dia_chi" value="<?= $ct_xa_kh ?>" placeholder="Gõ tên Phường/Xã, Quận/Huyện, Tỉnh để tìm kiếm nhanh chóng" onkeyup="dx_dia_chi()" onchange="dx_dia_chi()" autocomplete="off"></input>
                    </div>
                    <div class="form-group mb-3  col-md-12" id="dia_chi_giao_hang">
                    </div>
                    <div class="form-group mb-3  col-md-6">
                        <input class="form-control h38" type="number" id="id_ma_hang" value="<?= $in_id_sp ?>" name="ma_hang" placeholder="Mã hàng" onkeyup="sc_ma_hang()" onchange="sc_ma_hang()" required <?= $block_edit ?>></input>
                    </div>
                    <div class="form-group mb-3  col-md-6">
                        <input class="form-control h38" type="number" min="1" max="50" id="sl_mua" value="<?= $in_sl ?>" name="sl_mua" placeholder="Số lượng" onchange="edit_sl1()" onkeyup="edit_sl1()" required></input>
                    </div>
                    <div class="form-group mb-3  col-md-6">
                        <input class="form-control h38" type="number" id="tien_ship_hang" value="<?= $in_tien_ship ?>" name="tien_ship_hang" placeholder="Phí ship" onchange="tien_ship_ne()" onkeyup="tien_ship_ne()"></input>
                        <input hidden class="form-control h38" type="number" id="tien_ship_hang_v1" value="<?= $in_tien_ship ?>" name="tien_ship_hang_v1"></input>
                    </div>
                    <div class="form-group mb-3  col-md-6">
                        <input hidden class="form-control h38" id="gia_tien_cd" value="<?= $in_gia_sp1 ?>"></input>
                        <input class="form-control h38" id="tong_tien" value="<?= $in_gia_sp ?>" name="total_money" type="number" placeholder="Tổng tiền" required></input>
                    </div>
                    <div class="form-group mb-3  col-md-6" id="html_mau_sach">
                        <?= $html_i_ms ?>
                    </div>
                    <div class="form-group mb-3  col-md-6" id="html_size_a">
                        <?= $html_i_sz ?>
                    </div>
                    <div class="form-group mb-3  col-md-6">
                        <input class="form-control h38" type="text" id="name_sp" value="<?= $in_name_sp ?>" name="nameSP" placeholder="Tên sản phẩm" required <?= $block_edit ?>></input>
                    </div>
                    <div class="form-group mb-3  col-md-6">
                        <select class="custom-select flex-grow-1" id="vn_tinh" onchange="my_tinh()">
                            <?= $selected_tinh ?>
                        </select>
                    </div>
                    <div class="form-group mb-3  col-md-6">
                        <select class="custom-select flex-grow-1" id="vn_huyen" onchange="my_huyen()">
                            <?= $selected_huyen ?>
                        </select>
                    </div>
                    <div class="form-group mb-3  col-md-6">
                        <select id="vn_xa" onchange="my_xa()" name="xaKH" class="custom-select flex-grow-1" required>
                            <?= $selected_xa ?>
                        </select>
                    </div>
                    <div class="form-group mb-3  col-md-6">
                        <input hidden class="form-control h38" id="id_sc_linkFB" name="linkFB" value="" placeholder="Link or id Face" autocomplete="off"></input>
                    </div>
                    <div class="form-group  mb-3 col-md-12" id="id_edit_ghi_chu">
                        <textarea class="form-control" rows="2" name="ghi_chu" placeholder="Mô tả đơn hàng (ghi chú)"><?= $ghi_chu ?></textarea>
                    </div>
                    <div class="form-group  mb-3 col-md-12">
                        <button type="submit" name="xac_nhan" id="button_xn_k" class="btn btn-block btn-info">Xác nhận</button>
                    </div>
                </div>
        </form>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</main>
<script src="/js/don_hang.js"></script>
<script src="/js/chat_bot_fb.js"></script>