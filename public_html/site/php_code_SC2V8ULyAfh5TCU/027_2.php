<?php
require 'api_func_bot.php';
if (isset($_POST['update_bot'])) {
    $id_post = (int) $_POST['id_post_name'];
    $name_y_cau_hoi = _sql($_POST['name_y_cau_hoi']);
    $name_tu_khoa = _sql(strtolower(convert_vi_to_en($_POST['name_tu_khoa'])));
    $full_cau_hoi = _sql($_POST['full_cau_hoi']);
    if ($id_post > 0) {
        $stmt =  $conn->prepare("SELECT * FROM vn_chat_bot WHERE id =:id");
        $stmt->execute(array(":id" => $id_post));
        $vn_bot = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($id_member == $vn_bot['mem'] or $id_level > 3) {
            $sql = "UPDATE vn_chat_bot SET question='$name_y_cau_hoi',key_a='$name_tu_khoa',key_full='$full_cau_hoi' WHERE id=$id_post";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
        } else {
            echo 'Bạn ko có quyền điều chỉnh bài học này, bởi câu hỏi này dược dạy bởi: ' . sql_member($vn_bot['mem'], 'name');
            exit;
        }
    } else {
        if ($name_y_cau_hoi != '' and $name_tu_khoa != '' and $full_cau_hoi != '') {
            try {
                $sql = "INSERT INTO `vn_chat_bot`(`question`, `key_a`, `key_full`, `mem`) VALUES ('$name_y_cau_hoi','$name_tu_khoa','$full_cau_hoi',$id_member)";
                $conn->exec($sql);
            } catch (Exception $e) {
            }
        }
    }
    header('Location: /update_bot');
    exit;
}
if (isset($_GET['cau_hoi'])) {
    $stmt1 =  $conn->prepare("SELECT * FROM vn_chat_bot ORDER BY id DESC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    foreach ($list_code as $vn_chat_bot) {
        $num++;
        echo $num . ' - ' . $vn_chat_bot['question'] . '<br>';
    }
    exit;
}
if (isset($_POST['chat_bot'])) {
    $tin_nhan_kh = _sql01(strtolower(convert_vi_to_en($_POST['nd'])));
    $stmt1 =  $conn->prepare("SELECT * FROM vn_chat_bot ORDER BY id DESC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $vn_chat_bot) {
        $key_a = explode('|', $vn_chat_bot['key_a']);
        for ($ka = 0; $ka < count($key_a); $ka++) {
            $ndka = trim($key_a[$ka]);
            if ($ndka != "") {
                if (check_string($ndka, $tin_nhan_kh) > 0) {
                    $bot_rep = $vn_chat_bot['key_full'];
                    break;
                }
            }
            if ($bot_rep != "") {
                break;
            }
        }
        if ($bot_rep != "") {
            break;
        }
    }
    $id_ma_hang = "1001";
    if (isset($_GET['id'])) {
        if ($_GET['id'] > 999) {
            $id_ma_hang = (int)$_GET['id'];
        }
    }
    if (_sql_num($_POST['nd']) > 1000000) {
        $bot_rep = "Cảm ơn chị, tầm 10 - 15p nữa em sẽ gọi!";
    } else {
        $bot_rep = hoi_bot_tra_loi($id_ma_hang, $_POST['nd']);
    }
    $nguoi_day = sql_member($vn_chat_bot['mem'], 'name');
    if (check_string('ót ko hiểu',$bot_rep)>0) {
        echo "<textarea class=\"form-control\" rows=\"10\" placeholder=\"Bót trả lời\" disabled></textarea>;;0;;0;;<label class=\"form-label\">Cú pháp trả lời</label><textarea class=\"form-control\" rows=\"5\" name=\"full_cau_hoi\"></textarea>;;0;;";
    } else {
        echo "<textarea class=\"form-control\" rows=\"10\" placeholder=\"Bót trả lời\" disabled>Người dạy: " . $nguoi_day . "\n" . $bot_rep . "</textarea>;;" . $vn_chat_bot['question'] . ";;" . $vn_chat_bot['key_a'] . ";;<label class=\"form-label\">Cú pháp trả lời</label><textarea class=\"form-control\" rows=\"5\" name=\"full_cau_hoi\">" . $vn_chat_bot['key_full'] . "</textarea>;;" . $vn_chat_bot['id'];
    }
    exit;
}
$title = "Dạy BOT cách chát";
require 'site/widget/header.php';
?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_TaoDonHang" class="card collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <form method="post" action="<?= _sql03($_SERVER['REQUEST_URI']) ?>">
                                <div class="card-body">
                                    <div class="form-row">
                                        <div class="form-group mb-3 col-md-12">
                                            <label class="form-label">Ý nghĩa câu hỏi</label>
                                            <input type="text" class="form-control" value="" name="name_y_cau_hoi" id="id_y_cau_hoi">
                                            <input hidden value="" name="id_post_name" id="id_post_vn">
                                        </div>
                                        <div class="form-group mb-3 col-md-12">
                                            <label class="form-label">Từ khóa</label>
                                            <input type="text" class="form-control" value="" name="name_tu_khoa" id="id_tu_khoa">
                                        </div>
                                        <div class="form-group mb-3 col-md-12" id="id_cau_hoi">
                                            <label class="form-label">Cú pháp trả lời</label>
                                            <textarea class="form-control" rows="5" name="full_cau_hoi"></textarea>
                                        </div>
                                        <div class="form-group mb-3 col-md-12"><button type="submit" name="update_bot" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button></div><br><br>
                            </form>
                            <div class="form-group  mb-3 col-md-12" id="id_edit_ghi_chu">
                                <textarea class="form-control" rows="10" placeholder="Bót trả lời" disabled></textarea>
                            </div>
                            <div class="input-group mb-3 col-md-12">
                                <input class="form-control h38" name="tra_loi" id="id_tra_loi" onkeyup="fu_tra_loi()" onchange="fu_tra_loi()" placeholder="Nội dung hội thoại" autocomplete="off"></input>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function post_tra_loi(cau_hoi) {
            var post_dc = encodeURI("chat_bot=vbd&ma_sp=1000&nd=" + cau_hoi);
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                var html = xmlhttp.responseText.trim();
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var a_html = html.split(";;");
                    if (html.length > 1) {
                        document.getElementById("id_edit_ghi_chu").innerHTML = a_html[0];
                        if (a_html[1] == "0") {
                            a_html[1] = "";
                            a_html[2] = "";
                            a_html[4] = "";
                        }
                        document.getElementById("id_y_cau_hoi").value = a_html[1];
                        document.getElementById("id_tu_khoa").value = a_html[2];
                        document.getElementById("id_cau_hoi").innerHTML = a_html[3];
                        document.getElementById("id_post_vn").value = a_html[4];
                    }
                }
            };
            xmlhttp.open("POST", "update_bot", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(post_dc);
        }

        function fu_tra_loi() {
            var tl = document.getElementById("id_tra_loi").value;
            if (tl.length > 1) {
                post_tra_loi(tl);
            }
        }
    </script>