<?php
if ($member['level'] <=7) {
    header('Location: /');
    exit;
}

$_SESSION['tb'] = $_SESSION['tb'] != "" ? $_SESSION['tb'] : "";

$value_tim_kiem  = "";
if (isset($_POST['tim_kiem_v2'])) {
    $_SESSION['value_tim_kiem'] = _sql01($_POST['value_tim_kiem']);
    $value_tim_kiem = _sql01($_POST['value_tim_kiem']);
}
$value_tim_kiem = $_SESSION['value_tim_kiem'] != '' ? _sql01(trim($_SESSION['value_tim_kiem'])) : '';
if (isset($_GET['status'])) {
    $_SESSION['status'] = (int) $_GET['status'];
    $_SESSION['value_tim_kiem'] = '';
    header('Location: /ql_member');
    exit;
}
$status_a = isset($_SESSION['status']) ? $_SESSION['status'] : 888;
$status_a = (int) $status_a;

if (isset($_POST['new_code_reg'])) {
    $key_nhap = rand(10000, 999999);
    $sql = "UPDATE member SET code_reg='$key_nhap' WHERE id='" . $member['id'] . "'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    header('Location: /ql_member');
    exit;
}

if (isset($_POST['status_update_mem'])) {
    $name = _sql01($_POST['name']);
    $facebook = _sql01($_POST['facebook']);
    $phone = _sql01($_POST['phone']);
    $dia_chi = _sql01($_POST['dia_chi']);
    $notes_lock = _sql01($_POST['notes_lock']);
    $locks = (int) ($_POST['locks']);
    $level = (int) ($_POST['level']);
    $id_luu = (int) $_POST['id_post'];
    $team = (int)$_POST['team'];
    if ($id_level <= $level) {
        $_SESSION['tb'] = "Bảo mật không cho phép thay đổi level người khác lớn hơn level hiện tại của bạn!";
        header('Location: /ql_member');
        exit;
    }

    //Bảo mật không cho phép thay đổi thông tin người khác người team
    $stmt =  $conn->prepare("SELECT * FROM member WHERE id =:id");
    $stmt->execute(array(":id" => $id_luu));
    $mem_luu = $stmt->fetch(PDO::FETCH_ASSOC);
    $check = $id_level > 8 ? "true" : ($id_team != $mem_luu['team'] ? "fasle" : "true");
    if ($check == "fasle") {
        $_SESSION['tb'] = "Bảo mật không cho phép thay đổi thông tin người khác người team!";
        header('Location: /ql_member');
        exit;
    }

    if (isset($_POST['pass_3']) && $id_level > 6) {
        $_SESSION['tb'] = "";
        $pass_moi = md5(md5($_POST['pass_3']));
        if ($level < $id_level) {
            $sql = "UPDATE member SET name='$name', facebook='$facebook', phone='$phone', dia_chi='$dia_chi', pass='$pass_moi',`level`='$level', notes_lock='$notes_lock', locks='$locks' WHERE id='" . $id_luu . "'";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
        }
    } else {
        $_SESSION['tb'] = "";
        if ($level < $id_level) {
            $sql = "UPDATE member SET name='$name',`level`='$level', facebook='$facebook', phone='$phone', dia_chi='$dia_chi', notes_lock='$notes_lock', locks='$locks' WHERE id='" . $id_luu . "'";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
        }
    }
    header('Location: /ql_member');
    exit;
}

if (isset($_GET['delmem'])) {
    $idCheck = $_GET['delmem'];
    if (strlen($idCheck) <= 9 or $id_level < 9) {
        header('Location: /ql_member');
        exit;
    } else {
        $idCheck = substr($idCheck, 6);
        $idCheck = substr($idCheck, 0, -3);
        $id_mem = (int) _sql01($idCheck);
        $sql = "DELETE FROM member WHERE id=$id_mem";
        $conn->exec($sql);
        header('Location: /ql_member');
        exit;
    }
}
$thongbao = $_SESSION['tb'] == "" ? "" : $_SESSION['tb'];
$title = 'ADMIN - Danh sách member';
require 'site/widget/header.php';
?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">

            <div id="id_button_td">
                <h3 class="mt-3 ml-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link" data-toggle="collapse" data-target="#collapse_TaoDonHang" aria-expanded="false" aria-controls="collapseTwo">
                        Mã đăng ký thành viên: <b>
                            <font color=yellow><?= $member['code_reg'] ?></font>
                        </b>
                    </a>
                </h3>
            </div>

            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_TaoDonHang" class="card collapse <?= $show_t ?>" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <form method="post" action="<?= _sql($_SERVER['REQUEST_URI']) ?>">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="1"><?= $member['code_reg'] ?></textarea><br>
                                        <div class="text-xs-right">
                                            <button type="submit" name="new_code_reg" class="btn btn-block btn-flat margin-top-10 btn-info">Lấy mã mới</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12">
                <div class="card">
                    <div class="card-header bg_xanh_duong_nhat">
                        <h5 class="card-title mb-0">Tìm kiếm nhanh chóng..!</h5>
                    </div>
                    <form method="post">
                        <div class="card-body">
                            <div class="input-group mb-3">
                                <input name="value_tim_kiem" class="form-control" value="<?= $value_tim_kiem ?>" placeholder="Gõ cụm từ , từ, thông tin cần lấy..." autocomplete="off">
                                <span class="input-group-append">
                                    <button type="submit" name="tim_kiem_v2" class="btn btn-info" type="button">Tìm kiếm!</button>
                    </form>
                    </span>
                </div>
            </div>
            <div class="">
                <div class="card" style="margin-bottom: 0px!important;">
                    <div class="card-header alert-info">
                        <div class="navbar-collapse collapse d-flex justify-content-between fs_18" style="float:left;"> Danh sách thànhviên</div>
                        <div class="bd-highlight" style="color:white; font-size: 18px; float:right"><a href="/ql_member&status=4" style="color:magenta;" title="Tài khoản bị khóa"><i data-feather="lock"></i></a></div>
                        <div class="bd-highlight mr-2" style="color:white; font-size: 18px; float:right"><a href="/ql_member&status=3" style="color:white ;" title="Tài khoản đang hoạt động"><i data-feather="unlock"></i></a></div>
                        <div class="bd-highlight mr-2" style="color:white; font-size: 18px; float:right"><a href="/ql_member&status=888" style="color:yellow;" title="Tất cả tài khoản"><i data-feather="refresh-cw"></i></a></div>
                        <h5 style="margin-top: 27px;color: #fce200;margin-bottom: -10px;"><?= $thongbao ?></h5>
                    </div>
                    <div class="">
                        <div class="table-responsive">
                            <table id="datatables-basic" class="table table-bordered table-striped mb-0" style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align:center; width:5%;">#</th>
                                        <th style="text-align:left;">Member</th>
                                        <th style="text-align:left;">Email</th>
                                        <?php if ($id_level > 8) {
                                            echo '<th style="text-align:center;">Team</th>';
                                        } ?>
                                        <th style="text-align:center;">Thời gian</th>
                                        <th style="text-align:center;">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $stt = $status_a - 3;
                                    $team_list = 'id!=9';
                                    if ($id_level < 9) {
                                        $team_list = 'id!=9 and team=' . $id_team;
                                    }
                                    $sqlAll = "SELECT COUNT(*) FROM member WHERE  $team_list and  locks='$stt'";

                                    if ($status_a == 999 or $status_a == 888) {
                                        $sqlAll = "SELECT COUNT(*) FROM member WHERE  $team_list ";
                                    }

                                    if ($value_tim_kiem != '') {
                                        $sqlAll = "SELECT COUNT(*) FROM `member` WHERE (`name` LIKE '%$value_tim_kiem%' OR `email` LIKE '$value_tim_kiem' OR  `id` LIKE '%$value_tim_kiem%' ) and $team_list";

                                        if ($value_tim_kiem == 'new') {
                                            $sqlAll = "SELECT COUNT(*) FROM `member` WHERE $team_list";
                                        }
                                    }
                                    $stmt5 = $conn->query($sqlAll);
                                    $total_records  = $stmt5->fetchColumn();
                                    $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                                    $total_page = ceil($total_records / $limit);
                                    $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                                    $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                                    if ($total_page > 0) {
                                        $total_page_max = $total_page - 1;
                                    } else {
                                        $total_page_max = $total_page;
                                    }
                                    $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                                    $start_page = $_GET['page'] * $limit;
                                    $num_1 = 0;

                                    $stt = $status_a - 3;
                                    $stmt1 =  $conn->prepare("SELECT * FROM member WHERE  $team_list and  locks='$stt' ORDER BY id DESC LIMIT $start_page, $limit");
                                    if ($status_a == 999 or $status_a == 888) {
                                        $stmt1 =  $conn->prepare("SELECT * FROM member WHERE $team_list  ORDER BY id DESC LIMIT $start_page, $limit");
                                    }

                                    if ($value_tim_kiem != '') {
                                        $stmt1 =  $conn->prepare("SELECT * FROM member WHERE  $team_list and  (`name` LIKE '%$value_tim_kiem%' OR `email` LIKE '$value_tim_kiem' OR  `id` LIKE '%$value_tim_kiem%' )  ORDER BY id DESC LIMIT $start_page, $limit");

                                        if ($value_tim_kiem == 'new') {
                                            $stmt1 =  $conn->prepare("SELECT * FROM member WHERE  $team_list  ORDER BY id DESC LIMIT $start_page, $limit");
                                        }
                                    }
                                    $stmt1->execute(array());
                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                    $stmt2 =  $conn->prepare("SELECT * FROM danh_sach_team");
                                    $stmt2->execute();
                                    $list_code2 = $stmt2->fetchALL(PDO::FETCH_ASSOC);
                                    foreach ($list_code2 as $show_team) {
                                        $arr_team[$show_team['id']] = $show_team['name_team'];
                                    }
                                    $num = 0;
                                    foreach ($list_code as $show_pc) {
                                        $num_1 = $num_1 + 1;
                                        $num = $num_1 + $_GET['page'] * $limit;
                                        if ($show_pc['level'] > 8) {
                                            $trang_thai = 'BOSS';
                                        } else if ($show_pc['level'] >= $id_level) {
                                            $trang_thai = sql_level($show_pc['level'], 'name');
                                        } else {
                                            $trang_thai = $show_pc['locks'] == 0 ? '<font color=blue>' . sql_level($show_pc['level'], 'name') . '</font> 
                                                        - <button type="button" id="show-popup" class="button_a" data-toggle="modal" data-target="#set_' . $num . '">
                                                        <i class="align-middle" data-feather="edit"></i></button>' : '<font color=red>Bị khóa nè</font> -
                                                        <button type="button" id="show-popup" class="button_a" data-toggle="modal" data-target="#set_' . $num . '">
                                                        <i class="align-middle" data-feather="edit"></i></button>';
                                        }
                                        $ngay_hien_tai = date('d-m-Y', $time_php);
                                        $ngay_them = date('d-m-Y', $show_pc['time_reg']);
                                        $ngay_show = $ngay_them == $ngay_hien_tai ? date('H:i:s', $show_pc['time_reg']) : $ngay_them;
                                        $style =  $show_pc['id'] == $id_member  ? 'style="background-color:#e1eee3c9"' : "";
                                        echo '<tr ' . $style . '> 
                                                    <td style="text-align:center;" title="id: ' . $show_pc['id'] . '">' . $num . '</td> 
                                                     <td>' . $show_pc['id'] . ' - ' . $show_pc['name'] . '</td> 
                                                    <td>' . $show_pc['email'] . '</td>';
                                        if ($id_level > 8) {
                                            echo '<td style="text-align:center;">' . $arr_team[$show_pc['team']] . '</td> ';
                                        }
                                        echo '<td style="text-align:center;" title="' . date('H:i:s d-m-Y', $show_pc['time_reg']) . '">' . $ngay_show . '</td> 
                                                    <td style="text-align:center;">' . $trang_thai . '</td> 
                                                    </tr>';
                                        $selected[0] = '';
                                        $selected[1] = '';
                                        $selected[$show_pc['locks']] = 'selected';
                                        $stmt4 =  $conn->prepare("SELECT * FROM chuc_vu WHERE id<$id_level ORDER BY id ASC");
                                        $stmt4->execute(array());
                                        $list_code4 = $stmt4->fetchALL(PDO::FETCH_ASSOC);
                                        $level[$show_pc['level']] = 'selected';

                                        $stmt5 =  $conn->prepare("SELECT * FROM danh_sach_team ORDER BY id ASC");
                                        if ($id_level < 9) {
                                            // $stmt5 =  $conn->prepare("SELECT * FROM danh_sach_team WHERE team=$id_team ORDER BY id ASC");
                                        }
                                        $stmt5->execute(array());
                                        $list_code5 = $stmt5->fetchALL(PDO::FETCH_ASSOC);
                                        $chuoi_popup[$num] = '                
                                                    <div class="modal fade" id="set_' . $num . '" role="dialog">
                                                    <div class="modal-dialog">                
                                                    <div class="modal-content">
                                                    <div class="modal-header bg_xanh_duong_nhat">
                                                    <h2 style="text-align:center;">ID: ' . $show_pc['id'] . ' - Nick: ' . $show_pc['name'] . '</h2>                 
                                                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal">&times;</button></div>                  
                                                    <div class="modal-body">                        
                                                    <form method="post" action="' . _sql01($_SERVER['REQUEST_URI']) . '">                
                                                    <div class="card-body">                Tên:                
                                                    <input type="text" class="form-control" name="name" value="' . $show_pc['name'] . '"><br>  
                                                    Level:                <select class="custom-select mb-3" name="level">  ';
                                        foreach ($list_code4 as $show_pc4) {
                                            $level[$show_pc4['id']] = '';
                                            $level[$show_pc['level']] = 'selected';
                                            $chuoi_popup[$num] = $chuoi_popup[$num] . '<option  value="' . $show_pc4['id'] . '" ' . $level[$show_pc4['id']]  . '>' . $show_pc4['name'] . '</option>';
                                        }

                                        $chuoi_popup[$num] = $chuoi_popup[$num] . '   </select><br>   
                                                    Phone: <input type="text" class="form-control" name="phone" value="' . $show_pc['phone'] . '"><br>                
                                                    Facebook:<input type="text" class="form-control" name="facebook" value="' . $show_pc['facebook'] . '"><br>                
                                                    Địa chỉ:  <input type="text" class="form-control" name="dia_chi" value="' . $show_pc['dia_chi'] . '">  <br> ';
                                        if ($id_level > 8) {
                                            $chuoi_popup[$num] = $chuoi_popup[$num] . '     
                                            Team:                <select class="custom-select mb-3" name="team">  ';
                                            // $chuoi_popup[$num] = $chuoi_popup[$num] .' <option ' . $selectteam[$show_pc5['id']] . '>...</option>    ';
                                            foreach ($list_code5 as $show_pc5) {
                                                $idteam = $show_pc5['id'];
                                                $selectteam[$show_pc5['id']] = '';
                                                $selectteam[$show_pc['team']] = 'selected';
                                                $chuoi_popup[$num] = $chuoi_popup[$num] . '<option disabled value="' . $show_pc5['id'] . '" ' . $selectteam[$show_pc5['id']] . ' title="Mã số team: ' . $show_pc5['id'] . '">' . $show_pc5['name_team'] . '</option> ';
                                            }
                                            $chuoi_popup[$num] = $chuoi_popup[$num] . '  </select><br>   ';
                                        }

                                        if ($id_level > 6) {
                                            $chuoi_popup[$num] = $chuoi_popup[$num] . 'Lý do khóa nick: <input type="text" class="form-control" name="notes_lock" value="' . $show_pc['notes_lock'] . '"><br>
                                                    <input type="hidden" name="id_post" value="' . $show_pc['id'] . '">   
                                                    Trạng thái:                <select class="custom-select mb-3" name="locks">               
                                                     <option value="0" ' . $selected[0] . '>Hoạt động</option>                
                                                     <option value="1" ' . $selected[1] . '>Khóa nick</option>                
                                                     </select><br>
                                                    <div class="form-group">                
                                                    <label class="custom-control custom-checkbox m-0" style="color:black;float:left">                
                                                    <input onclick="new_pass(this)" type="checkbox" value="' . $show_pc['id'] . '" id="action' . $show_pc['id'] . '" class="custom-control-input">  
                                                    <span class="custom-control-label">Cấp mật khẩu mới</span></label>';
                                        }

                                        if ($id_level > 8) {
                                            $chuoi_popup[$num] = $chuoi_popup[$num] . ' <a class="btn btn_xam"  style="color:black;float:right" href="/ql_member&delmem=' . rand(100000, 999999) . $show_pc['id'] . rand(100, 999) . '" onclick="return confirm(\'Id: ' . $show_pc['id'] . ' - Name: ' . $show_pc['name'] . ', sẽ bị xóa? \nHệ thống không thể khôi phục!\')">Xóa member</a>';
                                        }
                                        $chuoi_popup[$num] = $chuoi_popup[$num] . '</div><p class="pt-4" id="demo' . $show_pc['id'] . '"></p>  <button type="submit" name="status_update_mem" class="btn btn-block btn-flat margin-top-10 btn-info">Thay đổi thông tin</button>
                                                     </div></form></div></div></div>
                                                     </div> ';
                                    }
                                    ?> </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $i = 1;
        for ($i; $i <= $num; $i++) {
            echo $chuoi_popup[$i];
        }
        load_page(_sql01($_SERVER['REQUEST_URI']), $total_page, $limit, $total_records, $total_page_max);
        load_dialog($total_page_max, $member['id']); ?>

    </div>
</main>

<script>
    function new_pass(event) {
        var number = event.value;
        if (event.checked) {
            document.getElementById("demo" + number).innerHTML = '<div class="form-row"><input type="text" name="pass_3" class="form-control" placeholder="Chú ý: Member không thể login mật khẩu cũ" required></div></div>';
        } else {
            document.getElementById("demo" + number).innerHTML = '';
        }
    };
</script>