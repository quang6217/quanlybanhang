<?php
function get_tong_tien_chi($id_nguon,$timkiem_ngay_thang)
{
    global $conn;
    $stmt1 =  $conn->prepare("SELECT ke_toan_02 ,sum(so_tien) as `tien_chi` FROM `ke_toan_03` where mem_xoa =0 and ke_toan_02 = $id_nguon and so_tien <0 
    and $timkiem_ngay_thang GROUP by ke_toan_02 limit 1");
    $stmt1->execute();
    return $stmt1->fetch()["tien_chi"];

}
function get_name_nguonKH($id)
{
    if($id == 0)return "Không xác định";
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM admin_nguon_khach_hang where id = $id limit 1");
    $stmt->execute();
    return $stmt->fetch()["name"];
}
if ($id_level == 5) {
    header('Location: /');
    exit;
}
function _sql_date($t)
{
    return preg_replace('/[^0-9-]/', '', $t);
}
if (isset($_GET['search'])) {
    if ($_GET['search'] == 0) {
        $_SESSION['tim_kiem_list_hang'] = '1=1';
        $_SESSION['tim_kiem_ngay_thang'] = '1=1';
        $_SESSION['data_tim_kiem'] = '';
        $_SESSION['data_ngay_thang'] = '';
        header('Location: /tk_don_nguon');
        exit;
    }
}
if (isset($_POST['button_xn_tim_kiem'])) {
    $in_member = (int)$_POST['in_member'];
    $data_in_member = '1=1';
    if ($in_member > 0) {
        $data_in_member = '(`mem`=' . $in_member . ')';
    }
    $data_in_team = '1=1';
    $in_team = (int)$_POST['in_team'];
    if ($in_team > 0) {
        $data_in_team = 'team=' . $in_team;
    }
    $a_date_bd = '1=1';
    if ($_POST['date_bd'] != '') {
        $i_date_bd = _sql_date($_POST['date_bd']);
        $date_bd = _sql_num($_POST['date_bd']);
        $a_date_bd = '(`time_num`>=' . (int)$date_bd . ')';
    }
    $a_date_kt = '1=1';
    if ($_POST['date_kt'] != '') {
        $i_date_kt = _sql_date($_POST['date_kt']);
        $date_kt = _sql_num($_POST['date_kt']);
        $a_date_kt = '(`time_num`<=' . (int)$date_kt . ')';
    }
    $_SESSION['data_tim_kiem'] = $in_team . '|' . $i_date_bd . '|' . $i_date_kt . '|' . $in_member;
    $_SESSION['tim_kiem_list_hang'] =  $a_date_bd. ' AND ' . $a_date_kt . ' AND ' . $data_in_team .' AND '.$data_in_member;
    $_SESSION['data_ngay_thang'] = $i_date_bd . '|' . $i_date_kt;
    $_SESSION['tim_kiem_ngay_thang'] = $a_date_bd . ' AND ' . $a_date_kt;
}
if (!isset($_SESSION['tim_kiem_list_hang'])) {
    $_SESSION['tim_kiem_list_hang'] = '1=1';
    $_SESSION['tim_kiem_ngay_thang'] = '1=1';
    $class_show = '';
} else {
    if ($_SESSION['tim_kiem_list_hang'] != '1=1') {
        $arr_t = explode('|', $_SESSION['data_tim_kiem']);
        $class_show = 'show';
    }
}
if ($i_date_bd < 1) {
    $i_date_bd = date('Y-m', $time_php) . '-1';
}
if ($i_date_kt < 1) {
    $i_date_kt = date('Y-m-d', $time_php);
}
$timkiem = $_SESSION['tim_kiem_list_hang'];
$timkiem_ngay_thang = $_SESSION['tim_kiem_ngay_thang'];
$title = "Đơn hàng đã lên nguồn";
$list_tk = 'active';
$list_tk_1 = 'show';

require 'site/widget/header.php'; ?>
<div class="content">
    <div class="row">
        <div class="col-12 mb-3" id="accordion">
            <div id="collapse_TaoDonHang" class="card collapse <?= $class_show ?>" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card" style="margin-bottom: -0.5rem;">
                    <div class="card-body">
                        <form method="post">
                            <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12">
                                <div class="row">
                                <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Member</span>
                                            </div>
                                            <select class="custom-select flex-grow-1" name="in_member" id="member_js_sha">
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM member WHERE id!=9 ORDER BY id  ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                echo '<option value="0">...</option>';
                                                foreach ($list_code as $show) {
                                                    $selected_ck_2 = $arr_t[3] == $show['id'] ? 'selected' : '';
                                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Team</span>
                                            </div>
                                            <select class="custom-select flex-grow-1" name="in_team" id="check_team_on" onchange="check_member_team()">
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM danh_sach_team ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                echo '<option value="0">...</option>';
                                                foreach ($list_code as $show) {
                                                    $selected_ck_2 = $arr_t[0] == $show['id'] ? 'selected' : '';
                                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name_team'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Ngày bắt đầu</span>
                                            </div>
                                            <input type="date" name="date_bd" value="<?= $arr_t[1] ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Kết thúc</span>
                                            </div>
                                            <input type="date" name="date_kt" value="<?= $arr_t[2] ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-xs-right">
                                <button type="submit" name="button_xn_tim_kiem" id="button_xn_tim_kiem" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card mb-3">
                <div class="card-header alert-info">
                    <h5 class="card-title mb-0  text-white fs_18 d-flex justify-content-between" style="font-weight: bold;">
                        <div>Đơn hàng đã lên nguồn</div><i class="align-middle mr-2 fas fa-fw fa-search" data-toggle="collapse" data-target="#collapse_TaoDonHang"></i></small>
                    </h5>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Nguồn</th>
                            <th style="text-align:center;">Đơn</th>
                            <th style="text-align:center;">Doanh số</th>
                            <th style="text-align:center;">Doanh thu</th>
                            <th style="text-align:center;">Chi phí</th>
                            <th style="text-align:center;">%</th>
                        </tr>
                    </thead>
                    <tbody id="top_online">
                        <?php
                        $stmt1 =  $conn->prepare("SELECT nguonKH,count(nguonKH) as `soLuong`,sum(thuc_nhan) as `thucNhan`,sum(tong_tien) as `tongTien` FROM `sale_sanpham_03` AS  `a` WHERE $timkiem AND  trang_thai!=44 AND (type_dh<1 OR type_dh>8) GROUP BY nguonKH");
                        $stmt1->execute(array());
                        $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                        $tong = count($list_code);
                        // sắp xếp mảng theo giảm dần theo số lượng đơn
                        for($i = 0;$i< $tong;$i++)
                        {
                            for($k=$i+1;$k<$tong;$k++)
                            {
                                if($list_code[$i]['soLuong'] < $list_code[$k]['soLuong'])
                                {
                                    $list_tam = $list_code[$i];
                                    $list_code[$i] = $list_code[$k];
                                    $list_code[$k] = $list_tam;

                                }
                            }
                        }
                        $tong_thuc_Nhan = 0;
                        $tong_tong_Tien = 0;
                        $tong_don = 0;
                        $tong_tien_chi = 0;
                        foreach ($list_code as $show_fp) {
                            $tien_chi = 0;
                            if($id_level > 0)
                            {
                                if(get_tong_tien_chi($show_fp['nguonKH'],$timkiem_ngay_thang)!= NULL )
                                {
                                    $tien_chi = (int)get_tong_tien_chi($show_fp['nguonKH'],$timkiem_ngay_thang)*-1;
                                    $tong_tien_chi += $tien_chi;
                                }
                            }
                            $nguonKH = get_name_nguonKH($show_fp['nguonKH']);
                            $tong_don += $show_fp['soLuong'];
                            $tong_thuc_Nhan += (int)$show_fp['thucNhan'];
                            $tong_tong_Tien  += (int)$show_fp['tongTien'];
                            echo '<tr>                        
                            <td style="text-align:center;">' .  $nguonKH . '</td>
                            <td style="text-align:center;">' . number_format((int)$show_fp['soLuong']) . '</td>
                            <td style="text-align:center;">'.number_format((int)$show_fp['tongTien']).'</td>
                            <td style="text-align:center;">'.number_format((int)$show_fp['thucNhan']).'</td>
                            <td style="text-align:center;">'.number_format($tien_chi).'</td>
                            <td style="text-align:center;">'.number_format($tien_chi*100/(int)$show_fp['thucNhan']).'%</td>
                            </tr>';
                        }
                        echo '<tr>
                          <td style="text-align:center; color:red">Tổng</td>
                          <td style="text-align:center; color:red">'.number_format($tong_don).'</td>
                          <td style="text-align:center; color:red">'.number_format($tong_tong_Tien).'</td>
                          <td style="text-align:center; color:red">'.number_format($tong_thuc_Nhan).'</td>
                          <td style="text-align:center; color:red">'.number_format($tong_tien_chi).'</td>
                          <td style="text-align:center; color:red">'.number_format($tong_tien_chi*100/$tong_thuc_Nhan).'%</td>
                          </tr>';
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
