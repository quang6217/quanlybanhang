<?php
if (isset($_GET['tinh'])) {
    $json = [];
    $url = $_SERVER['REQUEST_URI'];
    $parts = parse_url($url);
    parse_str($parts['query'], $query);
    $search_tinh =  _sql01($query['searchTinh']);
    $stmt1 =  $conn->prepare("SELECT * FROM vn_tinh where `name` like'%" . $search_tinh . "%' ORDER BY id ASC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $show) {
        $json[] = ['id' => $show['id'], 'text' => $show['name']];
    }
    echo json_encode($json);
    exit;
}

$title = "Tạo đơn hàng";
require 'site/widget/header.php'; ?>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha256-OFRAJNoaD8L3Br5lglV7VyLRf0itmoBzWUoM+Sji4/8=" crossorigin="anonymous"></script> -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<!-- <link rel="stylesheet" type="text/css" href="css/dp_css/cache_css_customer.v1602691437.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="https://s.giaohangtietkiem.vn/cache_css/customer.v1602691437.css" /> -->

<!-- <link rel="stylesheet" type="text/css" href="css/dp_css/donhang_css_styles.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="https://khachhang.giaohangtietkiem.vn/customer/css/styles.css?v=1.0" /> -->

<style>
     .aui-group {
        display: table;
        box-sizing: border-box;
        border-spacing: 0;
        table-layout: fixed;
        width: 100%;
    }
    .row-field .select_shif_time label {
        width: 100%
    }
    .table th,
    .table td {
        padding: 8px;
        line-height: 20px;
        text-align: left;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }
    .pull-right {
        float: right;
    }

    select,
    textarea,
    input[type="text"],
    input[type="password"],
    input[type="datetime"],
    input[type="datetime-local"],
    input[type="date"],
    input[type="month"],
    input[type="time"],
    input[type="week"],
    input[type="number"],
    input[type="email"],
    input[type="url"],
    input[type="search"],
    input[type="tel"],
    input[type="color"],
    .uneditable-input {
        display: inline-block;
        height: 34px;
        padding: 6px 6px;
        margin-bottom: 10px;
        font-size: 12px;
        line-height: 20px;
        color: #555;
        vertical-align: middle;
    }

    .input-icon textarea,
    .input-icon input[type="text"],
    .input-icon input[type="password"],
    .input-icon input[type="datetime"],
    .input-icon input[type="datetime-local"],
    .input-icon input[type="date"],
    .input-icon input[type="month"],
    .input-icon input[type="time"],
    .input-icon input[type="week"],
    .input-icon input[type="number"],
    .input-icon input[type="email"],
    .input-icon input[type="url"],
    .input-icon input[type="search"],
    .input-icon input[type="tel"],
    input[type="color"] {
        padding: 6px 6px 6px 30px;
        background-color: white;
        width: 100%;
    }

    .select2-container .select2-selection--single {
        box-sizing: border-box;
        cursor: pointer;
        display: block;
        height: 34px;
        user-select: none;
        -webkit-user-select: none;
    }

    .select2-container {
        box-sizing: border-box;
        display: inline-block;
        margin: 0;
        position: relative;
        vertical-align: middle;
        width: 100% !important;
    }

    .sidebar-nav {
        margin: 0 0 0 0 !important;

    }

    button.minus-item.input-group-addon.btn.btn_primary,
    button.plus-item.input-group-addon.btn.btn_primary,
    button.delete-item.btn.btn-danger {
        margin: 0px 3px 0px 3px;
        padding: 0px 6px;
    }

    input.item-count.form-control {
        margin: 0px 0px -10px 0px;
        height: 22px;
    }


    .aui-group .input-icon i {
    position: absolute;
    top: 10px;
    left: 60px;
    font-size: 16px;
    color: #666;
    z-index: 1;
}
[class^="icon-"], [class*=" icon-"] {
    display: inline;
    /* width: auto;
    height: auto; */
    line-height: normal;
    vertical-align: baseline;
    background-image: none;
    background-position: 0 0;
    background-repeat: repeat;
    /* margin-top: 0; */
    font-family: FontAwesome;
    font-weight: normal;
    font-style: normal;
    text-decoration: inherit;
    -webkit-font-smoothing: antialiased;
    /* height: 18px !important;
    width: 18px !important; */
    position: absolute;
    margin: 7px;
    color: gray;
}
</style>



<div class="content" onclick=my_vitri()>

    <div class="row">

        <div class="col-12 col-lg-7">
            <div class="card mb-3">
                <div class="card-header bg_xanh_duong_nhat">
                    <h5 class="card-title mb-0">Tìm kiếm mã sản phẩm..!</h5>
                </div>
                <div class="card-body">
                    <div class="input-group mb-3">
                        <input class="form-control" type="text" value="" id="search_sp_001" placeholder="Gõ tên hoặc id mã sản phẩm" autocomplete="off">
                    </div>
                </div>
            </div>
            <div id="search_sp_002">
            </div>
        </div>
        <div class="col-12 col-lg-5">
            <div class="card mb-3">
                <div class="card-header bg_xanh_duong_nhat">
                    <h5 class="card-title mb-0" style="float: left;">Tổng sản phẩm: (<span class="total-count"></span>) - <button class="clear-cart btn btn-danger">Clear Cart</button></h5>
                    <div id="navbar-cart" class="navbar-collapse collapse-show pull-right">
                        <button type="button" class="btn btn_primary">Cart (<span class="total-count"></span>)</button>
                        <button class="clear-cart btn btn-danger">Clear Cart</button>
                    </div>
                </div>
                <div class="card-body">
                    <div id="popover_content_wrapper">
                        <span id="cart_details">
                            <div class="table-responsive" id="order_table">
                                <table class="table show-cart mb-2">

                                </table>
                            </div>
                        </span>
                        <div class="btnbox-vbd">
                            <div class="form-group">
                                <div class="form-row align-items-center">
                                    <div class="col-md-6 input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Giảm: %</div>
                                        </div>
                                        <input type="text" class="form-control" order_id="31768" name="discount" style="font-size: 13px;" onchange="displayCart()" id="discount" value="0">
                                    </div>
                                    <div class="col-md-6 input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">VAT: %</div>
                                        </div>
                                        <select class="form-control" order_id="31768" name="vat" id="vat" onchange="displayCart()" style="height:34px;">
                                            <option value="0">0</option>
                                            <option value="5">5</option>
                                            <option value="10">10</option>
                                            <option value="15">15</option>
                                            <option value="20">20</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <textarea id="message" class="form-control" name="message" placeholder="Ghi chú"></textarea>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-12 col-xs-12">
                                    <a class="btn btn-lg btn-block btn_primary" id="cart_save" order_id="31768">
                                        <i class="align-middle mr-2 fas fa-fw fa fa-thumb-tack"></i> Lưu
                                    </a>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>



            <div class="card mb-3">

                <div class="card-header bg_xanh_duong_nhat">

                    <h5 class="card-title mb-0">Thông tin người nhận hàng</h5>

                </div>
                <form method="post">
                    <div class="aui-group form-row">
                        <div class="input-icon row-field" style="margin-left: 15px; margin-top: 5px;">
                            <i class="align-middle icon-phone" data-feather="phone"></i>
                            <input name="customer_tel" required="required" class="left-stripe s-tooltip customer-tel ui-autocomplete-input" maxlength="15" placeholder="Nhập SĐT để tự động điền thông tin khách hàng quen" type="tel" id="Package0CustomerTel" autocomplete="off" data-original-title="Nhập chính xác số điện thoại sẽ giúp bạn điền thông tin khách hàng."><span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span> </div>

                        <div class="input-icon row-field customer-fullname-text" style="margin-left: 15px; margin-top: 0px;">
                            <i class="align-middle icon-user" data-feather="user-plus"></i>
                            <input name="customer_fullname" required="required" class="s-tooltip customer-fullname" maxlength="500" placeholder="Tên khách hàng" type="text" id="Package0CustomerFullname" data-original-title="Tên khách hàng"> </div>

                        <div id="customer-address-detect" class="input-icon row-field customer-first-address-group" style="margin-left: 15px; margin-top: 0px;">
                            <i class="align-middle icon-home" data-feather="home"></i>
                            <input name="customer_first_address" required="required" placeholder="Địa chỉ chi tiết (nhà/ngõ/ngách...)" class="left-stripe s-tooltip customer-first-address" maxlength="100" type="text" id="Package0CustomerFirstAddress" data-original-title="Địa chỉ chi tiết giúp chúng tôi giao hàng chính xác hơn">
                            <div class="loader-address"></div>
                        </div>
                        <div class="row-field customer-province-item" style="margin-left: 15px; margin-top: 0px;">
                            <select class="vn_tinh left-stripe s-tooltip flex-grow-1" type="text" style="width:100% !important" name="vn_tinh"></select>
                        </div>
                        <div class="row-field customer-province-item" style="margin-left: 15px; margin-top: 10px;">
                            <select class="vn_huyen left-stripe s-tooltip flex-grow-1" type="text" style="width:100% !important" name="vn_huyen"></select>
                        </div>
                        <div class="row-field customer-province-item" style="margin-left: 15px; margin-top: 10px;">
                            <select class="vn_xa left-stripe s-tooltip flex-grow-1" type="text" style="width:100% !important" name="vn_xa"></select>
                        </div>
                        <div class="row-field customer-province-item" style="margin-left: 15px; margin-top: 10px;">
                            <select class="vn_thon left-stripe s-tooltip flex-grow-1" type="text" style="width:100% !important" name="vn_thon"></select>
                        </div>
                        <div style=" display: block;padding: 4px 0 4px 15px;float: left;width: 100%;">
                            <i>Để nhập nhanh, shop có thể chọn Đường/Ấp hoặc Phường/Xã trước !</i>
                        </div>
                    </div>
                    <button id="submit-packages" href="#" class="btn btn_primary pull-right mr-3" data-loading-text="Đăng đơn hàng...!" data-original-title="Đăng tất cả đơn hàng lên hệ thống GHTK">
                        <i class="icon-upload"></i>Đăng đơn hàng
                    </button>
                </form>

            </div>

        </div>

    </div>

</div>

<script src="/js/d_h/add_dh1.js"></script>
<script type="text/javascript">
    $('.vn_tinh').select2({
        placeholder: '(Bấm để chọn tỉnh/thành phố)',
        ajax: {
            url: '/add_donhang1&tinh',
            dataType: 'json',
            delay: 250,
            data: function(data) {
                // console.log(data.term);
                return {
                    searchTinh: data.term // search term
                };
            },
            processResults: function(response) {
                // console.log(response);
                return {
                    results: response
                };
            },
            cache: true
        }

    });
    $('.vn_huyen').select2({
        placeholder: '(Bấm để chọn quận/huyện)',
        ajax: {
            url: '/add_donhang1&huyen',
            dataType: 'json',
            delay: 250,
            data: function(data) {
                return {
                    searchTerm: data.term // search term
                };
            },
            processResults: function(response) {
                return {
                    results: response
                };
            },
            cache: true
        }

    });
    $('.vn_xa').select2({
        placeholder: '(Gõ để chọn đường/phường/xã)',
        ajax: {
            url: '/add_donhang1&xa',
            dataType: 'json',
            delay: 250,
            data: function(data) {
                return {
                    searchTerm: data.term // search term
                };
            },
            processResults: function(response) {
                return {
                    results: response
                };
            },
            cache: true
        }

    });
    $('.vn_thon').select2({
        placeholder: '(Gõ để chọn thôn ấp, đường phố, trường học ...)',
        ajax: {
            url: '/add_donhang1&xa',
            dataType: 'json',
            delay: 250,
            data: function(data) {
                return {
                    searchTerm: data.term // search term
                };
            },
            processResults: function(response) {
                return {
                    results: response
                };
            },
            cache: true
        }

    });

    function isEmpty(val) {
        return (val === undefined || val == null || val.length <= 0) ? true : false;
    }
</script>