<?php
$error_1 = 'Email:';
$error_2 = 'Nhập lại mật khẩu:';
$error_3 = 'Mã giới thiệu:';
if (isset($_POST['reg'])) {
    $email = _sql00($_POST['email']);
    $stmt =  $conn->prepare("SELECT * FROM member WHERE email =:email");
    $stmt->execute(array(":email" => $email));
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    $id_member = (int) $member['id'];
    $stmt_code =  $conn->prepare("SELECT * FROM member WHERE code_reg  =:code_reg");
    $stmt_code->execute(array(":code_reg" => (int) $_POST['code']));
    $member_code = $stmt_code->fetch(PDO::FETCH_ASSOC);
    $id_referral = (int) $member_code['id'];
    $id_team = (int) $member_code['team'];
    if ($id_referral > 0 and $id_member == 0 and $_POST['pass'] == $_POST['pass_1']) {
        $full_name = _sql01($_POST['full_name']);
        $password = md5(md5($_POST['pass']));
        $key_num = rand(1000000000, 99999999999);
        $code_reg = rand(100000, 999999);
        $key_string = random_string(10);
        $sent_email = get_data('http://data.vbdan.com/email_danplay/danplay.php?email=' . $email . '&name=' . urlencode($full_name) . '&code=' . $code_reg . '&web=hiryo.vn');        
        if ($sent_email == 'oke') {
            try {
                $sql = "INSERT INTO `member`(`name`, `phone`, `email`, `pass`, `level`, `money`, `team`, `key_num`, `key_string`, `code_reg`, `id_referral`, `pass_error`, `time_login`, `time_reg`, `locks`, `full_info`) VALUES ('$full_name','','$email','$password',0,0,$id_team,$key_num,'$key_string',$code_reg,$id_referral,0,$code_reg,$time_php,0,'')";
                if ($id_referral == 11) {
                    $sql = "INSERT INTO `member`(`name`, `phone`, `email`, `pass`, `level`, `money`, `team`, `key_num`, `key_string`, `code_reg`, `id_referral`, `pass_error`, `time_login`, `time_reg`, `locks`, `full_info`) VALUES ('$full_name','','$email','$password',0,0,$id_team,$key_num,'$key_string',$code_reg,$id_referral,0,$time_php,$time_php,0,'')";
                }
                $conn->exec($sql);
            } catch (Exception $e) {
            }
            $stmt =  $conn->prepare("SELECT * FROM member WHERE `email`='$email'");
            $stmt->execute(array());
            $uid = $stmt->fetch(PDO::FETCH_ASSOC);
            header('Location: /very_email&id=' . $uid['id']);
            exit;
        } else {
            $error_1 = '<font color=red>Không gửi được email:</font>';
        }
    }

    if ($member['id'] > 0) {
        $error_1 = '<font color=red>Email này đã đăng ký:</font>';
    }
    if ($_POST['pass'] != $_POST['pass_1']) {
        $error_2 = '<font color=red>Mật khẩu không khớp:</font>';
    }
    if ($id_referral < 1) {
        $error_3 = '<font color=red>Mã của bạn không đúng:</font>';
    }
}


$title = 'Đăng ký';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <title><?= $title ?> - <?= $_SERVER['HTTP_HOST'] ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="css/csslogin/images/icons/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="css/csslogin/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <script src="css/csslogin/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="css/csslogin/vendor/bootstrap/js/popper.js"></script>
    <script src="css/csslogin/vendor/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/lienhe.css">

</head>

<body>

    <div class="limiter">
        <div class="container-fluid">
            <div class="container-login100">
                <div class="wrap-login100">
                    <div id="contacts-new" class="container minh220 image">
                        <div class="white-card rel">
                            <div id="main_" class="row">
                            <div id="contacts" class="col-sm-4">
                                    <div class="left-side side c1 image ">
                                        <h2 class="fs38r mt0 mb40r">Contacts</h2>
                                        <div class="icon-box d-flex pb30">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <div class="text-block">
                                                <a class="db pb10 a8" href="mailto:khachhang@hiryo.vn">khachhang@hiryo.vn</a>
                                                <a class="db pb10 a8" target="_blank" href="https://t.me/danplaynet">Zalo: 0844.156.888</a>
                                                <a class="db pb10 a8 " href="">Phone: 0844.156.888</a>
                                            </div>
                                        </div>
                                        <div class="icon-box d-flex pb30">
                                            <i class="fa fa-map" aria-hidden="true"></i>
                                            <div class="text-block">
                                                <p class="c1 lh15">
                                                    108 Lý Tự Trọng,<br>
                                                    phường Hà Huy Tập <br>
                                                    Thành phố Vinh, Nghệ An
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="message" class="col-sm-8">
                                    <div class="right-side side">
                                        <h2 class="fs38r mt0 mb40r" style="text-align: center;color:#0083b4;">Đăng ký</h2>
                                        <form id="contact_form" class="login100-form validate-form" method="post" action="<?= _sql01($_SERVER['REQUEST_URI']) ?>">

                                            <div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                                                <label style="text-align: left;">Tên Của Bạn:</label>
                                                <input class="input100" type="text" name="full_name" value="<?= _sql01($_POST['full_name']) ?>" placeholder="Vũ Bảo Duy" required>
                                            </div>

                                            <div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                                                <label style="text-align: left;"><?= $error_1 ?></label>
                                                <input class="input100" type="email" name="email" value="<?= $email ?>" placeholder="ex@abc.xyz" required>
                                            </div>

                                            <div class="wrap-input100 validate-input" data-validate="Password is required">
                                                <label style="text-align: left;">Mật khẩu:</label>
                                                <input class="input100" type="password" name="pass" value="<?= _sql($_POST['pass']) ?>" placeholder="" required>
                                            </div>

                                            <div class="wrap-input100 validate-input" data-validate="Password is required">
                                                <label style="text-align: left;"><?= $error_2 ?></label>
                                                <input class="input100" type="password" name="pass_1" placeholder="" required>
                                            </div>

                                            <div class="wrap-input100" data-validate="Password is required">
                                                <label style="text-align: left;"><?= $error_3 ?></label>
                                                <input class="input100" type="text" name="code" value="<?= _sql00($_POST['code']) ?>" placeholder="">
                                            </div>

                                            <div class="container-login100-form-btn" style="padding-bottom: 21px; ">
                                                <button class="login100-form-btn button--primary button button--icon button--icon--login" style="height: 50px;" type="submit" name="reg">
                                                    Đăng ký
                                                </button>
                                            </div>

                                            <div class="text-center p-t-12">
                                                <a class="txt2" href="/login">Đăng nhập nếu bạn đã có tài khoản</a>
                                                <i class="fa fa-long-arrow-right m-l-5" style="color: #0093ce;" aria-hidden="true"></i>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    var onresize = function() {
                        var widthclinet = document.body.clientWidth;
                        var contacts = '<div id="contacts" class="col-sm-4">' + document.getElementById("contacts").innerHTML + '</div>';
                        var message = ' <div id="message" class="col-sm-8">' + document.getElementById("message").innerHTML + '</div>';
                        if (widthclinet < 770) {
                            $("#contacts").remove();
                            $("#main_").append(contacts);
                        } else {
                            $("#message").remove();
                            $("#main_").append(message);
                        }
                    }
                    window.addEventListener("resize", onresize);
                    onresize();
                </script>