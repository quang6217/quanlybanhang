<?php
require 'api_func_bot.php';
if (isset($_GET['cau_hoi'])) {
    $stmt1 =  $conn->prepare("SELECT * FROM vn_chat_bot ORDER BY id DESC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    foreach ($list_code as $vn_chat_bot) {
        $num++;
        echo $num . ' - ' . $vn_chat_bot['question'] . '<br>';
    }
    exit;
}

if (isset($_POST['chat_bot'])) {
    $id_ma_hang = "1001";
    if(isset($_GET['id'])){
        if($_GET['id']>999){
            $id_ma_hang = (int)$_GET['id'];
        }
    }
    if (_sql_num($_POST['nd']) > 10000000) {
        $bot_rep = "Cảm ơn chị, tầm 10 - 15p nữa em sẽ gọi!";
    } else {
        $bot_rep = hoi_bot_tra_loi($id_ma_hang, $_POST['nd']);
    }
    echo '<textarea class="form-control" rows="10" placeholder="Bót trả lời" disabled>' . $bot_rep . '</textarea>';
    exit;    }

$title = "Bót trả lời";
require 'site/widget/header.php';
?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_TaoDonHang" class="card collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group  mb-3 col-md-12" id="id_edit_ghi_chu">
                                        <textarea class="form-control" rows="10" placeholder="Bót trả lời" disabled></textarea>
                                    </div>
                                    <div class="input-group mb-3 col-md-12">
                                        <input class="form-control h38" name="tra_loi" id="id_tra_loi" onkeyup="fu_tra_loi()" onchange="fu_tra_loi()" placeholder="Nội dung hội thoại" autocomplete="off"></input>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                function post_tra_loi(cau_hoi) {
                    var post_dc = encodeURI("chat_bot=vbd&ma_sp=1001&nd=" + cau_hoi);
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        var html = xmlhttp.responseText.trim();
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            if (html.length > 1) {
                                document.getElementById("id_edit_ghi_chu").innerHTML = html;
                            }
                        }
                    };
                    xmlhttp.open("POST", "<?= _sql($_SERVER['REQUEST_URI']) ?>", true);
                    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    xmlhttp.send(post_dc);
                }

                function fu_tra_loi() {
                    var tl = document.getElementById("id_tra_loi").value;
                    if (tl.length > 1) {
                        post_tra_loi(tl);
                    }
                }
            </script>