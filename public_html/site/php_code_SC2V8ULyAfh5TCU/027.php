<?php
if (isset($_POST['xac_nhan'])) {
    $cau_hoi = _sql($_POST['cau_hoi']);
    $question_type = _sql($_POST['question_type']);
    $key_word = _sql($_POST['key_word']);
    $tra_loi = _sql($_POST['tra_loi']);
    try {
        $sql = "INSERT INTO `vn_chat_bot`(`level`, `question_type`, `key_word`, `noi_dung`, `cau_hoi`) VALUES (0,'$question_type','$key_word','$tra_loi','$cau_hoi')";
        $conn->exec($sql);
    } catch (Exception $e) {
    }
    header('Location: /rep_chat');
    exit;
}
$title = "Trả lời khách hàng";
require 'site/widget/header.php';

$stmt1 =  $conn->prepare("SELECT * FROM 00_cau_hoi_kh WHERE da_rep=0 ORDER BY id ASC LIMIT 1"); //Có thể chọn RAND ()
$stmt1->execute();
$list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
foreach ($list_code as $show) {
    $cau_hoi = $show['noi_dung'];
    // $sql = "UPDATE 00_cau_hoi_kh SET da_rep=1 WHERE id='" . $show['id'] . "'";
    // $stmt = $conn->prepare($sql);
    // $stmt->execute();
}

$stmt1 =  $conn->prepare("SELECT * FROM 00_cau_hoi_kh WHERE da_rep=1 ORDER BY id DESC LIMIT 3"); //Có thể chọn RAND ()
$stmt1->execute();
$list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
$cau_hoi_gan_nhat='';
foreach ($list_code as $show) {
    $cau_hoi_gan_nhat = $show['noi_dung']."\n".$cau_hoi_gan_nhat;
}
?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_TaoDonHang" class="card collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-row">
                                        <div class="form-group  mb-3 col-md-12" id="id_edit_ghi_chu">
                                            <textarea class="form-control" rows="4" placeholder="Đã trả lời gần nhất" disabled><?=$cau_hoi_gan_nhat?></textarea>
                                        </div>

                                        <div class="input-group mb-3 col-md-12">
                                            <input class="form-control h38" name="cau_hoi" value="<?= $cau_hoi ?>" placeholder="Câu đang hỏi" autocomplete="off"></input>
                                        </div>

                                        <div class="form-group mb-3  col-md-6">
                                            <input class="form-control h38" name="question_type" value="" placeholder="Loại câu hỏi" autocomplete="off"></input>
                                        </div>

                                        <div class="form-group mb-3 col-md-6">
                                            <input class="form-control h38" name="key_word" value="" placeholder="Từ khóa" autocomplete="off"></input>
                                        </div>

                                        <div class="input-group mb-3 col-md-12">
                                            <input class="form-control h38" name="tra_loi" value="" placeholder="Trả lời" autocomplete="off"></input>
                                        </div>

                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit" name="xac_nhan" id="button_xn_k" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>