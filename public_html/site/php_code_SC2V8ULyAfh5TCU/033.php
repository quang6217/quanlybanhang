<?php
if ($id_level < 1) {
    exit;
}
if (isset($_POST['add_acc_facebook'])) {
    $conten_list_id_video = nl2br(trim($_POST['list_data']));
    $array_data = explode('<br />', $conten_list_id_video);
    $y_cmt = count($array_data);
    for ($i = 0; $i < $y_cmt; $i++) {
        $array_data[$i] = str_replace('|', '	', $array_data[$i]);
        $ar_2 = explode('	', $array_data[$i]);
        $ma_ghtk = _sql(trim($ar_2[1]));
        $id_dh = (int)$ar_2[2];
        if ($id_dh > 0) {
            if ($id_dh > 11000 and $id_dh<99999 and trim($ar_2[3]) != 'Không lấy được hàng' and trim($ar_2[3]) != '') {
                echo '<font color=blue>' . $i . ' - '.$id_dh.' - ' . $ma_ghtk . ' - (' . $ar_2[3] . ') - oke</font><br>';
                try {
                    $sql = "UPDATE vn_don_hang_ship SET ma_ghtk='$ma_ghtk' WHERE `id`=$id_dh";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                } catch (Exception $e) {
                }
            } else {
                echo '<font color=red>' . $i . ' - '.$id_dh.' - ' . $ma_ghtk . ' - (' . $ar_2[3] . ') - error</font><br>';
            }
        }
    }
    exit;
}
$title = "Mã đơn hàng";
require 'site/widget/header.php'; ?>
<main class="content">
    <div class="container-fluid p-0">
        <?= $thong_bao_chuan ?>
        <div id="accordion">
            <div id="collapseTwo" class="row collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header bg_xanh_duong_nhat">
                            <h5 class="card-title mb-0">Thêm dữ liệu của bạn</h5>
                        </div>
                        <div class="card-body">
                            <form method="post" action="<?= _sql($_SERVER['REQUEST_URI']) ?>">
                                <div class="form-group"><textarea class="form-control" name="list_data" rows="5" placeholder="Dữ liệu được lấy từ file excel
stt|mã GHTK|ĐC lấy hàng|Tên KH|SĐT KH|ĐC giao hàng|Trạng thái đơn"></textarea></br>
                                    <div class="text-xs-right"><button type="submit" name="add_acc_facebook" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>