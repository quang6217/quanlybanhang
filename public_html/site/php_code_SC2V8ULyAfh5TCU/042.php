<?php
if ($id_level <8) {
    echo " Bạn không có quyền truy cập vào chức năng này!!!";
    exit;
}
if (isset($_POST['xac_nhan_key'])) {
    $tieu_de = _sql01($_POST['tieu_de']);
    $mau_td = (int) $_POST['mau_td'];
    $noidung = _sql01($_POST['noidung']);
    $mau_nd = (int) $_POST['mau_nd'];
    $link = _sql01($_POST['link']);
    try {
        $sql = "INSERT INTO `thong_bao`(`mem`, `team`, `tieude`, `tieude_color`, `noidung`, `noidung_color`, `time_add`,  `link`, `type`)
            VALUES ('$id_member', '$id_team', '$tieu_de', '$mau_td', '$noidung',' $mau_nd','$time_php','$link','0')";
        $conn->exec($sql);
    } catch (Exception $e) {
    }
    header('Location: /nhap_thong_bao');
    exit;
}

if (isset($_GET['delfp'])) {
    $idCheck = $_GET['delfp'];
    if (strlen($idCheck) <= 9) {
        header('Location: /nhap_thong_bao');
        exit;
    } else {
        $idCheck = substr($idCheck, 6);
        $idCheck = substr($idCheck, 0, -3);
        $id_key = (int) _sql01($idCheck);
        try {

            $sql = "DELETE FROM thong_bao WHERE id=$id_key and type='0'";
            $conn->exec($sql);
        } catch (Exception $e) {
        }
        header('Location: /nhap_thong_bao');
        exit;
    }
}
$status_a = (int) $status_a;
$status[0] = '<font class="p_stt" title="Đen" style="color:black; ">Đen</font>';
$status[1] = '<font class="p_stt" title="Xanh" style="color:Blue; ">Xanh</font>';
$status[2] = '<font class="p_stt" title="Đỏ" style="color:Red;">Đỏ</font>';
$status[3] = '<font class="p_stt text_nhap_nhay" title="Nhấp nháy" >Nhấp nháy</font>';


$title = 'Admin - Nhập thông báo';
require 'site/widget/header.php';
?>

<main class="content">
    <div class="container-fluid p-0">
        <div class="row">

            <div class="ml-3" id="headingTwo" style="display: inline-flex;">
                <h3 class="mt-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Thêm thông báo
                    </a>
                </h3>
            </div>

            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapseTwo" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card"  style="margin-bottom: -0.5rem;">
                            <div class="card-header bg_xam">
                                <h5 class="card-title fs_18">
                                    Thêm Thông báo của bạn
                                </h5>
                            </div>
                            <div class="card-body">
                                <form method="post" action="<?= _sql01($_SERVER['REQUEST_URI']) ?>">
                                    <div class="form-row">

                                        <div class="form-group col-9">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend  h38">
                                                    <span class="input-group-text">Tiêu đề</span>
                                                </div>
                                                <input class="form-control h38" type="text" name="tieu_de" value="" placeholder="Tiêu đề" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group col-3">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend  h38">
                                                    <span class="input-group-text">Màu tiêu đề</span>
                                                </div>
                                                <select class="form-control h38" name="mau_td" style="color:black" value="">
                                                    <option value="0">Đen</option>
                                                    <option value="1">Xanh</option>
                                                    <option value="2">Đỏ</option>
                                                    <option value="3">Nhấp nháy</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-9">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend  h38">
                                                    <span class="input-group-text">Nội dung</span>
                                                </div>
                                                <input class="form-control h38" type="text" name="noidung" value="" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group col-3">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend  h38">
                                                    <span class="input-group-text">Màu nội dung</span>
                                                </div>
                                                <select class="form-control h38" name="mau_nd" style="color:black" value="">
                                                    <option value="0">Đen</option>
                                                    <option value="1">Xanh</option>
                                                    <option value="2">Đỏ</option>
                                                    <option value="3">Nhấp nháy</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-12">
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend  h38">
                                                    <span class="input-group-text">Link</span>
                                                </div>
                                                <input class="form-control h38" type="text" name="link" value="" placeholder="">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-row text-xs-right">
                                        <button type="submit" name="xac_nhan_key" class="btn btn-block btn-flat margin-top-10 btn_xam text_black">Xác nhận</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-header alert-info">
                        <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size: px;color:white;">
                            Các thông báo của bạn
                        </div>
                    </div>
                    <div class="">
                        <div class="table-responsive flex-row flex-nowrap">
                            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align:center; width:5%;">#</th>
                                        <th style="text-align:left;">Tiêu đề</th>
                                        <th style="text-align:left;">Nội dung</th>
                                        <th style="text-align:left;">Link</th>
                                        <th style="text-align:left;width:15%;">Màu</th>
                                        <th style="text-align:left;">Ngày</th>
                                        <th style="text-align:center; width:7%;">Xóa</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sqlAll = "SELECT COUNT(*) FROM `thong_bao` WHERE  type='0' ";
                                    $stmt5 = $conn->query($sqlAll);
                                    $total_records  = $stmt5->fetchColumn();
                                    $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                                    $total_page = ceil($total_records / $limit);
                                    $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                                    $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                                    if ($total_page > 0) {
                                        $total_page_max = $total_page - 1;
                                    } else {
                                        $total_page_max = $total_page;
                                    }
                                    $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                                    $start_page = $_GET['page'] * $limit;
                                    $num_1 = 0;
                                    $stmt1 =  $conn->prepare("SELECT * FROM thong_bao WHERE  type='0'  ORDER BY id DESC LIMIT $start_page, $limit");
                                    $stmt1->execute(array());
                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);

                                    $num = 0;
                                    foreach ($list_code as $show_fp) {
                                        //`thong_bao`(`mem`, `team`, `tieude`, `tieude_color`, `noidung`, `noidung_color`, `time_add`,  `link`, `type`)

                                        $num_1 = $num_1 + 1;
                                        $num = $num_1 + $_GET['page'] * $limit;
                                        $ngay_hien_tai = date('d/m/Y', $time_php);
                                        $ngay_them = date('d/m/Y', $show_fp['time_add']);
                                        $ngay_show = $ngay_them == $ngay_hien_tai ? date('H:i:s', $show_fp['time_add']) : $ngay_them;
                                        echo '<tr>
                                        <td style="text-align:center;" title="' . $show_fp['mem'] . ' - ' . $show_fp['id'] . '">' . $num . '</td>
                                        <td style="font-family: initial" title="' . $show_fp['tieude'] . '">' . substr($show_fp['tieude'],  0, 45)  . '...</td>
                                        <td style="font-family: initial" title="' . $show_fp['noidung'] . '">' . substr($show_fp['noidung'],  0, 40) . '...</td>
                                        <td style="font-family: initial" title="' . $show_fp['link'] . '">' . substr($show_fp['link'],  0, 30) . '...</td>
                                         <td >' . $status[$show_fp['tieude_color']] . ' - ' . $status[$show_fp['noidung_color']] . '</td>
                                         <td title = "' . date('H:i:s d-m-Y ', $show_fp['time_add']) . '">' . $ngay_show . '</td>
                                         
                                        <td style="text-align:center;">
                                           <a href="/nhap_thong_bao&delfp=' . rand(100000, 999999) . $show_fp['id'] . rand(100, 999) . '" onclick="return confirm(\'#' . $num . ' - Thông báo: ' . htmlentities($show_fp['tieude']) . ', sẽ bị xóa?\')"><i class="align-middle" data-feather="trash"></i></a>
                                        </td>
                                           </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <?php
                $i = 1;
                for ($i; $i <= $num; $i++) {
                    echo $chuoi_popup_edit[$i];
                }
                load_page(_sql01($_SERVER['REQUEST_URI']), $total_page, $limit, $total_records, $total_page_max);
                load_dialog($total_page_max, $member['id']); ?>


            </div>
        </div>
    </div>
    </div>