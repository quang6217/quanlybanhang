<?
if (isset($_GET['full_xa'])) {
    $json = json_decode(info_vn_xa((int) (int) $_GET['full_xa']), true);
    echo '<option value="' . $json['id_tinh'] . '">' . $json['name_tinh'] . '</option>|<option value="' . $json['id_huyen'] . '">' . $json['name_huyen'] . '</option>|<option value="' . $json['id_xa'] . '">' . $json['name_xa'] . '</option>|' . $json['full_name'];
    exit;
}

if (isset($_POST['dia_chi'])) {
    $po_diachi_v1 = convert_vi_to_en(str_replace(array(".", "/", "-", ":", ";", "_"), ",", _sql($_POST['dia_chi'])));
    $po_diachi = strtolower($po_diachi_v1);
    $arr = explode(',', $po_diachi);
    $xa = _sql01(trim($arr[0]));
    $huyen = _sql01(trim($arr[1]));
    $tinh = _sql01(trim(str_replace("tinh", "", $arr[2])));
    $tim_kiem = '`full_info_en` LIKE \'%' . $huyen . '%\' AND `full_info_en` LIKE \'%' . $xa . '%\' AND `full_info_en` LIKE \'%' . $tinh . '%\'';
    $stmt1 =  $conn->prepare("SELECT * FROM vn_tinh_huyen_xa WHERE $tim_kiem ORDER BY id DESC LIMIT 500"); //Có thể chọn RAND ()
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    echo '<select multiple class="custom-select flex-grow-1" id="id_p_dia_chi" onchange="my_p_dia_chi()">';
    foreach ($list_code as $show) {
        echo '<option value="' . $show['id'] . '">' . $show['full_info'] . '</option>';
    }
    echo '</select>';    
    exit;
}
if (isset($_GET['chdh_sdt'])) {
    if (strlen($_GET['chdh_sdt']) > 5) {
        $stmt =  $conn->prepare("SELECT * FROM vn_don_hang_ship WHERE sdt =:sdt ORDER BY id DESC");
        $stmt->execute(array(":sdt" => (int) $_GET['chdh_sdt']));
        $a_sdt = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($a_sdt['id'] > 0) {
            $json = json_decode(info_vn_xa((int) $a_sdt['xaKH']), true);
            $selected_tinh = '';
            $selected_huyen = '<option value="' . $json_xa['id_huyen'] . '">' . $json_xa['name_huyen'] . '</option>';
            $selected_xa = '<option value="' . $json_xa['id_xa'] . '">' . $json_xa['name_xa'] . '</option>';
            $stmt1 =  $conn->prepare("SELECT * FROM vn_tinh ORDER BY id ASC");
            $stmt1->execute();
            $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
            foreach ($list_code as $show) {
                $selected_ck_t = $show['id'] == $json['id_tinh'] ? 'selected' : '';
                $selected_tinh = $selected_tinh . '<option value="' . $show['id'] . '" ' . $selected_ck_t . '>' . $show['name'] . '</option>';
            }
            echo _sql($a_sdt['nameKH']) . '|' . _sql($a_sdt['address_detail']) . '|' . _sql($json['full_name']) . '|<option value="' . $json['id_xa'] . '">' . $json['name_xa'] . '</option>|<h3 class="mt-3 ml-3 btn btn-danger mr-2" style="float: left;"><a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_TaoDonHang" aria-expanded="false" aria-controls="collapseTwo" onclick="show_hiden_p()">Khách quen (' . ucwords(sql_member($a_sdt['mem'], 'name')) . '): ' . $a_sdt['nameKH'] . '</a> </h3>|<option value="' . $json['id_huyen'] . '">' . $json['name_huyen'] . '</option>|' . $selected_tinh . '|' . check_nha_mang($_GET['chdh_sdt']);
        } else {
            echo check_nha_mang($_GET['chdh_sdt']);
        }
    } else {
        echo check_nha_mang($_GET['chdh_sdt']);
    }
    exit;
}

if (isset($_GET['sp'])) {
    $id = (int) $_GET['sp'];
    $stmt =  $conn->prepare("SELECT * FROM vn_san_pham WHERE id =:id");
    $stmt->execute(array(":id" => $id));
    $sanp = $stmt->fetch(PDO::FETCH_ASSOC);
    $ar_mau = explode('|', $sanp['mau_sac']);
    // ------------> Lấy dữ liệu màu sắc
    for ($i = 0; $i < count($ar_mau); $i++) {
        $stmt =  $conn->prepare("SELECT * FROM vn_mau_sac WHERE id =:id");
        $stmt->execute(array(":id" => (int) $ar_mau[$i]));
        $mau = $stmt->fetch(PDO::FETCH_ASSOC);
        echo '<option value="' . $mau['id'] . '" title="' . $mau['mo_ta'] . '">' . $mau['mau'] . '</option>';
    }
    echo '|';
    // ------------> Lấy dữ liệu size
    $ar_size = explode('|', $sanp['size']);
    for ($i = 0; $i < count($ar_size); $i++) {
        $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id AND sex =:sex");
        $stmt->execute(array(":id" => (int) $ar_size[$i], ":sex" => (int) $sanp['sex']));
        $mau = $stmt->fetch(PDO::FETCH_ASSOC);
        echo '<option value="' . $mau['id'] . '" title="Cao: ' . $mau['chieu_cao'] . ' - Nặng: ' . $mau['can_nang'] . ' - Eo: ' . $mau['vong_eo'] . ' - Ngực: ' . $mau['vong_nguc'] . '">' . $mau['name'] . '</option>';
    }
    $gia_banss = $sanp['gia_ban'] / 1000;
    $gia_shosss = $sanp['phi_ship'] / 1000;
    echo '|' . $gia_banss . '|' . $sanp['name'] . '|' . $gia_shosss;
    exit;
}

if (isset($_GET['huyen'])) {
    $id = (int) $_GET['huyen'];
    $stmt1 =  $conn->prepare("SELECT * FROM vn_tinh_huyen WHERE id_tinh=$id ORDER BY id ASC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    echo '<option>Chọn Huyện/Quận</option>';
    foreach ($list_code as $show) {
        echo '<option value="' . $show['id'] . '">' . $show['name'] . '</option>';
    }
    exit;
}
if (isset($_GET['xa'])) {
    $id = (int) $_GET['xa'];
    $stmt1 =  $conn->prepare("SELECT * FROM vn_tinh_huyen_xa WHERE id_huyen=$id ORDER BY id ASC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $show) {
        echo '<option value="' . $show['id'] . '">' . $show['name'] . '</option>';
    }
    exit;
}
?>