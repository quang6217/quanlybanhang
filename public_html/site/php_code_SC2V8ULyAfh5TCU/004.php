<?php
$title = 'Quên mật khẩu';
$text_reset = '  <div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                <label style="text-align: left;">Email:</label>
                <input class="input100" type="email" name="email" value="" placeholder="ex@abc.xyz" required>
                </div>
                <div class="container-login100-form-btn">
                <button class="login100-form-btn button--primary button button--icon button--icon--login">
                    Lấy lại mật khẩu
                </button>
                </div>';

if (isset($_POST['email'])) {
    $email = _sql00($_POST['email']);
    $stmt =  $conn->prepare("SELECT * FROM member WHERE `email`='$email'");
    $stmt->execute(array());
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    $id_member = (int) $member['id'];
    if ($id_member < 1) {
        $text_reset = '  <div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                        <label style="text-align: left;">Email: <font color=red>' . $email . '</font> không đúng</label>
                        <input class="input100" type="email" name="email" value="" placeholder="ex@abc.xyz" required>
                        </div>
                        <div class="container-login100-form-btn">
                        <button class="login100-form-btn button--primary button button--icon button--icon--login">
                            Lấy lại mật khẩu
                        </button>
                        </div>';
    } else {
        $code_reg = random_string(28);
        $sql = "UPDATE member SET code_xac_minh='$code_reg' WHERE id='" . $id_member . "'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $sent_email = get_data('http://data.vbdan.com/email_danplay/reset_pass.php?email=' . $member['email'] . '&id=' . $id_member . '&name=' . urlencode($member['name']) . '&code=' . $code_reg . '&web=hiryo.vn');
        $text_reset = '  <div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                        <label style="text-align: left;">Liên kết để đặt mật khẩu mới đã gửi đến <font color=red>' . $email . '</font> của bạn!</label>                        
                        </div> ';
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $title ?> - <?= $_SERVER['HTTP_HOST'] ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="css/csslogin/images/icons/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="css/csslogin/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <script src="css/csslogin/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="css/csslogin/vendor/bootstrap/js/popper.js"></script>
    <script src="css/csslogin/vendor/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/lienhe.css">
</head>

<body>
    <div class="limiter">
        <div class="container-fluid">
            <div class="container-login100">
                <div class="wrap-login100">
                    <div id="contacts-new" class="container minh220 image">
                        <div class="white-card rel">
                            <div id="main_" class="row">
                                <div id="contacts" class="col-sm-4">
                                    <div class="left-side side c1 image ">
                                        <h2 class="fs38r mt0 mb40r">Contacts</h2>
                                        <div class="icon-box d-flex pb30">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <div class="text-block">
                                                <a class="db pb10 a8" href="mailto:khachhang@hiryo.vn">khachhang@hiryo.vn</a>
                                                <a class="db pb10 a8" target="_blank" href="https://t.me/danplaynet">Zalo: 0844.156.888</a>
                                                <a class="db pb10 a8 " href="">Phone: 0844.156.888</a>
                                            </div>
                                        </div>
                                        <div class="icon-box d-flex pb30">
                                            <i class="fa fa-map" aria-hidden="true"></i>
                                            <div class="text-block">
                                                <p class="c1 lh15">
                                                    108 Lý Tự Trọng,<br>
                                                    phường Hà Huy Tập <br>
                                                    Thành phố Vinh, Nghệ An
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="message" class="col-sm-8">
                                    <div class="right-side side">
                                        <h2 class="fs38r mt0 mb40r" style="text-align: center; color: #0083b4">Quên mật khẩu</h2>
                                        <form id="contact_form" class="login100-form validate-form" method="post" action="<?= _sql01($_SERVER['REQUEST_URI']) ?>">
                                            <?= $text_reset ?>
                                            <div class="text-center p-t-12">
                                                <a class="txt2" href="login">Đăng nhập</a>
                                                <i class="fa fa-long-arrow-right m-l-5" style="color: #0093ce;" aria-hidden="true"></i>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    var onresize = function() {
                        var widthclinet = document.body.clientWidth;
                        var contacts = '<div id="contacts" class="col-sm-4">' + document.getElementById("contacts").innerHTML + '</div>';
                        var message = ' <div id="message" class="col-sm-8">' + document.getElementById("message").innerHTML + '</div>';
                        if (widthclinet < 770) {
                            $("#contacts").remove();
                            $("#main_").append(contacts);
                        } else {
                            $("#message").remove();
                            $("#main_").append(message);
                        }
                    }
                    window.addEventListener("resize", onresize);
                    onresize();
                </script>