<?php
if ($id_level < 7) {
    header('Location: /');
    exit;
}
if (isset($_GET['check_id'])) {
    $id_check = (int) $_GET['check_id'];
    if ($id_check > 0) {
        echo sql_member($id_check, 'name');
    }
    exit;
}

if (isset($_GET['delfp'])) {
    $id_xoa = (int) $_GET['delfp'];
    $sql = "DELETE FROM vn_money_admin WHERE id=$id_xoa AND mem=$id_member";
    $conn->exec($sql);
    header('Location: /admin_money');
    exit;
}

if (isset($_POST['xac_nhan'])) {
    $so_tien = (int) $_POST['so_tien'];
    $id_mem = (int) $_POST['id_mem'];
    $loai_gd = (int) $_POST['loai_gd'];
    $id_edit = (int) $_POST['id_edit'];
    $type_gd = (int) $_POST['type_gd'];    
    $name_nhan = _sql01($_POST['name_nhan']);
    $ghi_chu = _sql($_POST['ghi_chu']);
    if ($so_tien > 0 and $id_edit == 0 and $type_gd>0) {
        $am_duong = $loai_gd == 0 ? -1 : 1;
        $so_tien = $so_tien * $am_duong * 1000;
        try {
            $sql = "INSERT INTO `vn_money_admin`(`mem`, `team`, `id_mem_nhan`, `name_mem`,`so_tien`, `type`, `ghi_chu`, `time_nume`, `time_add`) VALUES ($id_member,$id_team,$id_mem,'$name_nhan',$so_tien,$type_gd,'$ghi_chu',$time_num,$time_php)";
            $conn->exec($sql);
        } catch (Exception $e) {
        }
    }
    header('Location: /admin_money');
    exit;
}
$title = "Thu chi";
$tao_don_hang_ms = 'info';
$tao_don_hang = 'Thêm khoản thu - chi';
$button_a = 'Xác nhận';
$id_edit = '0';
require 'site/widget/header.php';
$sqlAll = "SELECT SUM(`so_tien`) FROM `vn_money_admin` WHERE team=$id_team";
$stmt = $conn->query($sqlAll);
$tong_tien_team = $stmt->fetchColumn(); ?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div id="id_button_td">
                <h3 class="mt-3 ml-3 btn btn-<?= $tao_don_hang_ms ?> mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_TaoDonHang" aria-expanded="false" aria-controls="collapseTwo" onclick="show_hiden_p()">
                        <?= $tao_don_hang ?>
                    </a>
                </h3>
            </div>

            <div id="id_button_td">
                <h3 class="mt-3 ml-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_THUCHI" aria-expanded="false" aria-controls="collapseTwo">
                        Tổng quát thu - chi
                    </a>
                </h3>
            </div>

            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_TaoDonHang" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-row">
                                        <div class="form-group mb-3  col-md-3">
                                            <select name="loai_gd" class="custom-select flex-grow-1" required>
                                                <option value="0">Chi (tiền ra)</option>
                                                <option value="1">Thu (tiền vào)</option>
                                            </select>
                                        </div>

                                        <div class="form-group mb-3  col-md-3">
                                            <select name="type_gd" class="custom-select flex-grow-1" required>
                                                <option value="0">Chọn khoản cần chi</option>
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM vn_money_admin_ct ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                $num = 0;
                                                foreach ($list_code as $show) {
                                                    echo '<option value="' . $show['id'] . '">' . $show['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="form-group mb-3  col-md-3">
                                            <select name="id_mem" class="custom-select flex-grow-1" id="id_mem_2" onchange="check_id_mem_2()" required>
                                                <option value="0">Tìm đối tác</option>
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM member WHERE team=$id_team and time_login>1000000 ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                foreach ($list_code as $show) {
                                                    echo '<option value="' . $show['id'] . '">' . $show['id'] . ' - ' . ucwords($show['name']) . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group mb-3 col-md-3">
                                            <input class="form-control h38" name="name_nhan" value="" placeholder="Đối tác" autocomplete="off"></input>
                                        </div>

                                        <div class="form-group mb-3  col-md-12">
                                            <input class="form-control h38" type="number" max="500000" value="" name="so_tien" placeholder="Số tiền (k) ví dụ: 1.500.000đ = 1500" required></input>
                                            <input hidden value="<?= $id_edit ?>" name="id_edit"></input>
                                        </div>
                                        <div class="form-group  mb-3 col-md-12">
                                            <textarea class="form-control" rows="4" name="ghi_chu" placeholder="Mô tả đơn hàng (ghi chú)"></textarea>
                                        </div>
                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit" name="xac_nhan" id="button_xn_k" class="btn btn-block btn-flat margin-top-10 btn-info"><?= $button_a ?></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_THUCHI" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-header alert-info">
                            <h5 class="card-title mb-0  text-white fs_18" style="font-weight: bold;">Tổng quát thu chi</small></h5>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width:5%; text-align:center;">#</th>
                                    <th style="width:30%; text-align:center;">Giao dịch</th>
                                    <th style="text-align:center;">Tiền (x1000đ)</th>
                                    <th style="text-align:center;">Chi tiết</th>
                                </tr>
                            </thead>
                            <tbody id="top_online">
                                <?php
                                $stmt1 =  $conn->prepare("SELECT * FROM vn_money_admin_ct ORDER BY id ASC");
                                $stmt1->execute();
                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                $num = 0;
                                foreach ($list_code as $show) {
                                    $ten_khoan_chi[$show['id']] = $show['name'];
                                    $num++;
                                    $sqlAll = "SELECT SUM(`so_tien`) FROM `vn_money_admin`WHERE `team`=$id_team AND `type`='" . $show['id'] . "'";
                                    $stmt = $conn->query($sqlAll);
                                    $tong_tien = $stmt->fetchColumn();
                                    echo '<tr>
                          <td style="text-align:center;">' . $num . '</td>
                          <td style="text-align:center;">' . $show['name'] . '</td>
                          <td style="text-align:center;">' . number_format($tong_tien/1000, 0) . '</td>                          
                          <td style="text-align:center;"><a href="/admin_money&type=' . $show['id'] . '">Xem</a></td> 
                          </tr>';
                                }
                                $num++;
                                echo '<tr>
                                <td style="text-align:center; color: red;">' . $num . '</td>
                                <td style="text-align:center; color: red;">Tổng tất cả </td>
                                <td style="text-align:center; color: red;">' . number_format($tong_tien_team, 0) . ' đ</td>                          
                                </tr>'

                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-header alert-info">
                        <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                            Danh sách khoản chi
                        </div>
                    </div>
                    <div class="">
                        <div class="table-responsive flex-row flex-nowrap">
                            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align:center; width:5%;">#</th>
                                        <th style="text-align:center;">Member</th>
                                        <th style="text-align:center;">Đối tác</th>
                                        <th style="text-align:center;">Loại</th>
                                        <th style="text-align:center;">Tiền (x1000đ)</th>
                                        <th style="text-align:center;">Ghi chú</th>
                                        <th style="text-align:center;">Ngày thêm</th>
                                        <th style="text-align:center; width:7%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $type_sql = 'type>0';
                                    if(isset($_GET['type'])){
                                        if($_GET['type']>0){
                                            $in_type = (int)$_GET['type'];
                                            $type_sql = 'type='.$in_type;
                                        }
                                    }

                                    $sqlAll = "SELECT COUNT(`mem`) FROM `vn_money_admin` WHERE team=$id_team and $type_sql";
                                    $stmt5 = $conn->query($sqlAll);
                                    $total_records  = $stmt5->fetchColumn();
                                    $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                                    $total_page = ceil($total_records / $limit);
                                    $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                                    $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                                    if ($total_page > 0) {
                                        $total_page_max = $total_page - 1;
                                    } else {
                                        $total_page_max = $total_page;
                                    }
                                    $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                                    $start_page = $_GET['page'] * $limit;
                                    $num_1 = 0;

                                    $stmt1 =  $conn->prepare("SELECT * FROM vn_money_admin WHERE team=$id_team and $type_sql ORDER BY id DESC LIMIT $start_page, $limit");
                                    $stmt1->execute(array());
                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                    $num = 0;
                                    foreach ($list_code as $show_fp) {
                                        $arr = (array) $show_fp;
                                        $num_1 = $num_1 + 1;
                                        $num = $num_1 + $_GET['page'] * $limit;
                                        $ngay_hien_tai = date('d/m/Y', $time_php);
                                        $ngay_them = date('d/m/Y', $show_fp['time_add']);
                                        $ngay_show = $ngay_them == $ngay_hien_tai ? date('H:i:s', $show_fp['time_add']) : $ngay_them;
                                        $sho_ghi = _sql($show_fp['ghi_chu']) != '' ? _sql($show_fp['ghi_chu']) : _sql($show_fp['address_detail']);
                                        $sho_ghi_a = mb_substr($sho_ghi, 0, 20, 'UTF-8');
                                        if ($id_member == $show_fp['mem']) {
                                            $quyen_edit = '<a href="' . _sql03($_SERVER['REQUEST_URI']) . '&delfp=' . $show_fp['id'] . '" onclick="return confirm(\'#' . $num . ' - Đơn hàng của: ' . $show_fp['nameKH'] . ', sẽ bị xóa?\')"><i class="align-middle" data-feather="trash" style="color:#495057"></i></a>';
                                        } else {
                                            $quyen_edit = '<i class="align-middle" data-feather="alert-circle"></i>';
                                        }
                                        $loai_mau = $show_fp['mem'] == $id_member ? 'blue' : '';
                                        $loai_mau = $show_fp['so_tien'] > 0 ? 'red' : $loai_mau;
                                        $so_tien = abs($show_fp['so_tien']);
                                        $loai_ti = $ten_khoan_chi[$show_fp['type']];
                                        echo '<tr>
                                        <td style="text-align:center; color: ' . $loai_mau . ';">' . $num . '</td>                                        
                                        <td style="text-align:center; color: ' . $loai_mau . ';">' . sql_member($show_fp['mem'], 'name') . '</td>      
                                        <td style="text-align:center; color: ' . $loai_mau . ';">' . $show_fp['name_mem'] . '</td>                                        
                                        <td style="text-align:center; color: ' . $loai_mau . ';">' . $loai_ti . '</td>
                                        <td style="text-align:center; color: ' . $loai_mau . ';">' .  number_format($so_tien/1000, 0) . '</td>  
                                        <td style="text-align:center; color: ' . $loai_mau . ';" title="' . $sho_ghi . '">' . $sho_ghi_a . '</td>
                                        <td style="text-align:center; color: ' . $loai_mau . ';" title="' . date('d/m/Y H:i:s', $show_fp['time_add']) . '">' .  $ngay_show . '</td>
                                        <td style="text-align:center; color: ' . $loai_mau . ';">' . $quyen_edit . '</td>
                                        </tr>';
                                    }
                                    $sqlAll = "SELECT SUM(`so_tien`) FROM `vn_money_admin`WHERE `mem`=$id_member";
                                    $stmt = $conn->query($sqlAll);
                                    $tong_tien_mem = $stmt->fetchColumn();
                                    echo '<tr>
                                        <td style="text-align:center;">' . number_format($num + 1) . '</td>                                        
                                        <td style="text-align:right;" colspan="3">Tổng tiền của bạn</td>
                                        <td style="text-align:left; colspan="4"">' .  number_format($tong_tien_mem, 0) . 'đ</td>  
                                        </tr>';

                                    $sqlAll = "SELECT SUM(`so_tien`) FROM `vn_money_admin` WHERE team=$id_team";
                                    $stmt = $conn->query($sqlAll);
                                    $tong_tien_team = $stmt->fetchColumn();
                                    echo '<tr>
                                            <td style="text-align:center;">' . number_format($num + 2) . '</td>                                        
                                            <td style="text-align:right;" colspan="3">Tổng tiền của TEAM</td>
                                            <td style="text-align:left; colspan="4"">' .  number_format($tong_tien_team, 0) . 'đ</td>  
                                            </tr>';

                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
                load_page(_sql($_SERVER['REQUEST_URI']), $total_page, $limit, $total_records, $total_page_max);
                load_dialog($total_page_max, $member['id']);
                ?>
            </div>
        </div>
    </div>
</main>

<script>
    function check_id_mem_2() {
        var x = document.getElementById("id_mem_2");
        var i = x.selectedIndex;
        check_id_mem(x.options[i].value);
    }

    function check_id_mem(id_mem = null) {
        if (id_mem > 0) {
            var xmlhttp;
            if (window.XMLHttpRequest) {
                xmlhttp = new XMLHttpRequest();
            } else {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var html_full = xmlhttp.responseText.trim();
                    if (html_full != '') {
                        document.getElementsByName("name_nhan")[0].value = html_full;
                    } else {
                        document.getElementsByName("name_nhan")[0].value = "ko";
                    }
                }
            };
            xmlhttp.open("GET", "admin_money&check_id=" + id_mem, true);
            xmlhttp.send();
        }
    }
</script>