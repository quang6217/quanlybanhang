<?php
$title = 'Home';
require 'site/widget/header.php';
$status[0] = 'text_black';
$status[1] = 'text_blue';
$status[2] = 'text_red';
$status[3] = 'text_nhap_nhay';
?>
<style>
    #chat_box {
        height: 315px;
        overflow-y: scroll;
    }

    #chat_box>p {
        margin: 0 !important;
        margin-bottom: 0 !important;
    }
</style>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-defaul">
                        <h4 class="block-title">Thông Báo</h4>
                        <div class="block-options">
                            <button type="button" class="btn-block-option collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo"><i class="si si-arrow-down"></i></button>
                        </div>
                    </div>
                    <div id="accordion">
                        <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                            <?php
                            $stmt1 =  $conn->prepare("SELECT * FROM `thong_bao` WHERE  type='0'  ORDER BY `id` DESC");
                            $stmt1->execute(array());
                            $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                            $num = 0;
                            foreach ($list_code as $show_fp) {
                                echo '<div class="font-w600 animated fadeIn bg-body-light border-3x px-3 py-2 mb-2 shadow-sm mw-100 border-left border-success rounded-right '
                                    . $status[$show_fp['tieude_color']] . '" style="font-size:16px;">';
                                if (trim($show_fp['link'] != "")) {
                                    echo '<a class="link-fx ' . $status[$show_fp['tieude_color']] . '" href="' . $show_fp['link'] .  '" target="_blank" style="font-size:16px;">';
                                }
                                echo $show_fp['tieude'];
                                echo '<br><span style="font-size:14px;" class="' . $status[$show_fp['noidung_color']] . '">' . $show_fp['noidung'] . '</span>';
                                if (trim($show_fp['link'] != "")) {
                                    echo ' </a>';
                                }
                                echo '</div> ';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-6 col-xl d-flex">
                <div class="card flex-fill">
                    <div class="card-body py-4">
                        <div class="media">
                            <div class="d-inline-block mt-2 mr-3">
                                <i class="feather-lg text-primary" data-feather="shopping-cart"></i>
                            </div>
                            <div class="media-body">
                                <h3 class="mb-2">2.562</h3>
                                <div class="mb-0">Tổng đơn</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-xl d-flex">
                <div class="card flex-fill">
                    <div class="card-body py-4">
                        <div class="media">
                            <div class="d-inline-block mt-2 mr-3">
                                <i class="feather-lg text-warning" data-feather="activity"></i>
                            </div>
                            <div class="media-body">
                                <h3 class="mb-2">17.212</h3>
                                <div class="mb-0">Sl sản phẩm</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-xl d-flex">
                <div class="card flex-fill">
                    <div class="card-body py-4">
                        <div class="media">
                            <div class="d-inline-block mt-2 mr-3">
                                <i class="feather-lg text-success" data-feather="dollar-sign"></i>
                            </div>
                            <div class="media-body">
                                <h3 class="mb-2">$ 24.300</h3>
                                <div class="mb-0">Tiền hàng</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-xl d-flex">
                <div class="card flex-fill">
                    <div class="card-body py-4">
                        <div class="media">
                            <div class="d-inline-block mt-2 mr-3">
                                <i class="feather-lg text-danger" data-feather="dollar-sign"></i>
                            </div>
                            <div class="media-body">
                                <h3 class="mb-2">$ 18.700</h3>
                                <div class="mb-0">Tổng doanh thu</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-8 d-flex">
                <div class="card flex-fill w-100">
                    <div class="card-header bg_xanh_duong_nhat">
                        <span class="badge badge-primary float-right">Tuần qua</span>
                        <h5 class="card-title mb-0">Thành tích bán hàng</h5>
                    </div>
                    <div class="card-body">
                        <div class="chart chart-lg">
                            <canvas id="chartjs-dashboard-line"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4 d-flex">
                <div class="card flex-fill w-100">
                    <div class="card-header bg_xanh_duong_nhat">
                        <h5 class="card-title mb-0  text-black fs_18" style="font-weight: bold;">Phòng trò chuyện</h5>
                    </div>
                    <div class="card-body" style="padding: 10px;">

                        <body data-spy="scroll" data-offset="5">
                            <div id="chat_box">
                                <?php
                                $sqlAll = "SELECT COUNT(`mem`) FROM `024_chat`";
                                $stmt = $conn->query($sqlAll);
                                $id_max = $stmt->fetchColumn();
                                $id_start = $id_max - 50;
                                $id_start = $id_start > 0 ? $id_start : 0;
                                $stmt1 =  $conn->prepare("SELECT * FROM 024_chat ORDER BY time ASC LIMIT $id_start, 50");
                                $stmt1->execute(array());
                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                foreach ($list_code as $show_code) {
                                    $level_aa = sql_member($show_code['mem'], 'level');
                                    $admin_a = $level_aa > 6 ? '<sup>(Admin)<sup>' : '';
                                    if ($level_aa > 2 and $level_aa < 7) {
                                        $admin_a = '<sup>(Mod)<sup>';
                                    }
                                    if ($level_aa > 8) {
                                        $admin_a = '<sup>(Boss)<sup>';
                                    }
                                    $mau_sac = $level_aa > 6 ? '#DF013A' : '#4000FF';
                                    echo '<p><font color=' . $mau_sac . '>' . sql_member($show_code['mem'], 'name') . '' . $admin_a . '</font>: ' . base64_de($show_code['text']) . '</p>';
                                }
                                ?>
                            </div>
                            <div id="id_post_chat" hidden><?= $show_code['id'] ?></div>

                            <body><br>
                                <div class="input-group">
                                    <input type="text" id="chat_box_1" name="chat_box" class="form-control" value="" placeholder="Điền nội dung chát" autocomplete="off">
                                    <span class="input-group-append">
                                        <button onclick="post_chat()" name="chat_box_ok" style="background-color: #5fc27e;" class="btn btn-success" type="button">Gửi!</button>
                                    </span>
                                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<script>
    $(function() {
        new Chart(document.getElementById("chartjs-dashboard-line"), {
            type: "line",
            data: {
                labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
                datasets: [{
                    label: "Sales ($)",
                    fill: true,
                    backgroundColor: "transparent",
                    borderColor: window.theme.primary,
                    data: [2015, 1465, 1487, 1796, 1387, 2123, 2866]
                }, {
                    label: "Orders",
                    fill: true,
                    backgroundColor: "transparent",
                    borderColor: window.theme.tertiary,
                    borderDash: [4, 4],
                    data: [928, 734, 626, 893, 921, 1202, 1396]
                }]
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                tooltips: {
                    intersect: false
                },
                hover: {
                    intersect: true
                },
                plugins: {
                    filler: {
                        propagate: false
                    }
                },
                scales: {
                    xAxes: [{
                        reverse: true,
                        gridLines: {
                            color: "rgba(0,0,0,0.05)"
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            stepSize: 500
                        },
                        display: true,
                        borderDash: [5, 5],
                        gridLines: {
                            color: "rgba(0,0,0,0)",
                            fontColor: "#fff"
                        }
                    }]
                }
            }
        });
    });
</script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script language="javascript">
    setInterval(function() {
        load_ajax2();
    }, 5000);
    cuon_chat();

    function cuon_chat() {
        var element = document.getElementById("chat_box");
        element.scrollTop = element.scrollHeight;
    }

    function load_ajax2() {
        var noi_dung = document.getElementById("chat_box").innerHTML;
        var id_post_chat = document.getElementById("id_post_chat").innerHTML;
        var xmlhttp;
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var html = xmlhttp.responseText.trim().split('chat_box_number:');
                var post_id = html[1];
                var post_string = html[0];
                if (post_id != id_post_chat) {
                    document.getElementById("chat_box").innerHTML = noi_dung + post_string;
                    document.getElementById("id_post_chat").innerHTML = post_id;
                    if (thanh_cuon() * 100 > 85) {
                        cuon_chat();
                    }
                }
            }
        };
        xmlhttp.open("GET", "load_ajax&chat_box=" + id_post_chat, true);
        xmlhttp.send();

    }

    $('#chat_box_1').keypress(function(event) {
        var id_post_chat = document.getElementById("id_post_chat").innerHTML;
        var noi_dung = document.getElementById("chat_box").innerHTML;
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            var post_chat = encodeURI("chat_box_post=" + id_post_chat + "&chat_box_string=" + document.getElementById("chat_box_1").value);
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                document.getElementById("chat_box_1").value = "";
                var html = xmlhttp.responseText.trim().split('chat_box_number:');
                var post_id = html[1];
                var post_string = html[0];
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    if (post_id != id_post_chat) {
                        document.getElementById("chat_box").innerHTML = noi_dung + post_string;
                        document.getElementById("id_post_chat").innerHTML = post_id;
                        cuon_chat();
                    }
                }
            };
            xmlhttp.open("POST", "load_ajax", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send(post_chat);
        }
    });

    function post_chat() {
        var id_post_chat = document.getElementById("id_post_chat").innerHTML;
        var post_chat = encodeURI("chat_box_post=" + id_post_chat + "&chat_box_string=" + document.getElementById("chat_box_1").value);
        var noi_dung = document.getElementById("chat_box").innerHTML;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            document.getElementById("chat_box_1").value = "";
            var html = xmlhttp.responseText.trim().split('chat_box_number:');
            var post_id = html[1];
            var post_string = html[0];
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("chat_box").innerHTML = noi_dung + post_string;
                document.getElementById("id_post_chat").innerHTML = post_id;
                cuon_chat();
            }
        };
        xmlhttp.open("POST", "load_ajax", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlhttp.send(post_chat);
    }

    function thanh_cuon() {
        var element = document.getElementById("chat_box");
        var a = element.scrollTop;
        var b = element.scrollHeight - element.clientHeight;
        var c = a / b;
        return c;
    }
</script>