<?php
$homqua = date('Ymd', $time_php - 60 * 60 * 24);
if (isset($_POST['xac_nhan'])) {
    $ads_id = (int)$_POST['ads_id'];
    $ads_sms = (int)$_POST['ads_sms'];
    $ads_cmt = (int)$_POST['ads_cmt'];
    $ads_money = (int)$_POST['ads_money'];
    $ads_date = _sql_num($_POST['ads_date']);
    $ghi_chu = _sql($_POST['ghi_chu']);
    if ($ads_id > 100000) {        
        if ($ads_date == $time_num or $ads_date == $homqua) {
            $stmt =  $conn->prepare("SELECT * FROM vn_tk_ads WHERE mem=$id_member AND id_ads=$ads_id AND time_num=$ads_date");
            $stmt->execute();
            $ads_show = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($ads_show['id'] > 0) {
                $sql = "UPDATE vn_tk_ads SET time_update='$time_php', sms='$ads_sms', cmt='$ads_cmt', money='$ads_money', ghi_chu='$ghi_chu' WHERE id='" . $ads_show['id'] . "'";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            } else {
                $sql = "INSERT INTO `vn_tk_ads`(`mem`, `team`, `id_ads`, `sms`, `cmt`, `money`, `ghi_chu`, `time_num`, `time_add`, `time_update`) VALUES ($id_member,$id_team,$ads_id,$ads_sms,$ads_cmt,$ads_money,'$ghi_chu',$ads_date,$time_php,$time_php)";
                $conn->exec($sql);
            }
        }
    }
    header('Location: /ks_ads');
    exit;
}
$title = "Kiểm soát ADS";
require 'site/widget/header.php';
$show_t = '';
$block_edit = '';
$i_ads_date = date('Y.m.d', $time_php);
$i_ads_id = '';
$i_ads_sms = '';
$i_ads_cmt = '';
$i_ads_money = '';
$i_ads_date = '';
$i_ghi_chu = '';
if (isset($_GET['edit_id'])) {
    $id_edit = (int)$_GET['edit_id'];
    $show_t = 'show';
    $block_edit = 'readonly';
    $stmt =  $conn->prepare("SELECT * FROM vn_tk_ads WHERE id =:id" );
    $stmt->execute(array(":id" => $id_edit));
    $eidta= $stmt->fetch(PDO::FETCH_ASSOC);
    $i_ads_id = $eidta['id_ads'];
    $i_ads_sms = $eidta['sms'];
    $i_ads_cmt = $eidta['cmt'];
    $i_ads_money = $eidta['money'];
    $i_ads_date = $eidta['time_num'];
    $i_ghi_chu = _sql($eidta['ghi_chu']);
}
?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div id="id_button_td">
                <h3 class="mt-3 ml-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link" data-toggle="collapse" data-target="#collapse_TaoDonHang" aria-expanded="false" aria-controls="collapseTwo">
                        Thêm thống kê của bạn
                    </a>
                </h3>
            </div>

            <div id="id_button_ls">
                <h3 class="mt-3 ml-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_lichsu" aria-expanded="false" aria-controls="collapseTwo">
                        Lịch sữ ADS của bạn
                    </a>
                </h3>
            </div>

            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_TaoDonHang" class="card collapse <?=$show_t?>" aria-labelledby="headingTwo" data-parent="#accordion">                        
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-row" id="show_du_lieu">
                                        <div class="input-group mb-3 col-md-12">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Ads_id</span>
                                            </div>
                                            <input class="form-control h38" type="number" value="<?= $i_ads_id ?>" name="ads_id" required <?= $block_edit ?>></input>
                                        </div>
                                        <div class="input-group mb-3 col-md-12">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Tin nhắn</span>
                                            </div>
                                            <input class="form-control h38" type="number" value="<?= $i_ads_sms ?>" name="ads_sms" required></input>
                                        </div>
                                        <div class="input-group mb-3 col-md-12">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Bình luận</span>
                                            </div>
                                            <input class="form-control h38" type="number" value="<?= $i_ads_cmt ?>" name="ads_cmt" required></input>
                                        </div>
                                        <div class="input-group mb-3 col-md-12">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Số tiền</span>
                                            </div>
                                            <input class="form-control h38" type="number" value="<?= $i_ads_money ?>" name="ads_money" required></input>
                                        </div>
                                        <div class="input-group mb-3 col-md-12">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Ngày thống kê</span>
                                            </div>
                                            <input class="form-control h38" value="<?= $i_ads_date ?>" name="ads_date" required <?= $block_edit ?>></input>
                                        </div>
                                        <div class="form-group  mb-3 col-md-12" id="id_edit_ghi_chu">
                                            <textarea class="form-control" rows="2" name="ghi_chu" placeholder="Ghi chú rõ ràng giúp BOSS dễ hiểu"><?= $i_ghi_chu ?></textarea>
                                        </div>
                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit" name="xac_nhan" id="button_xn_k" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_lichsu" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-header alert-info">
                                <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                                    ADS của bạn
                                </div>
                            </div>
                            <div class="">
                                <div class="table-responsive flex-row flex-nowrap">
                                    <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center; width:5%;">#</th>
                                                <th style="text-align:center;">Ads_id</th>
                                                <th style="text-align:center;">SMS</th>
                                                <th style="text-align:center;">CMT</th>
                                                <th style="text-align:center;">Số tiền</th>
                                                <th style="text-align:center;">Ghi chú</th>
                                                <th style="text-align:center;">Ngày</th>
                                                <th style="text-align:center;">Update</th>
                                                <th style="text-align:center; width:7%">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $stmt1 =  $conn->prepare("SELECT * FROM vn_tk_ads WHERE  mem=$id_member ORDER BY id DESC LIMIT 20");
                                            $stmt1->execute(array());
                                            $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                            $num = 0;
                                            foreach ($list_code as $show_fp) {
                                                $num = $num + 1;
                                                $ngay_show = date('Ymd', $show_fp['time_update']) == $time_num ? date('H:i:s', $show_fp['time_update']) : date('d-m-Y', $show_fp['time_update']);
                                                $show_fp['ghi_chu'] = $show_fp['ghi_chu'] == '' ? '...' : $show_fp['ghi_chu'];
                                                $quyen_edit = '<i class="align-middle" data-feather="alert-circle"></i>';
                                                if ($ads_date == $time_num or $ads_date == $homqua){
                                                    $quyen_edit = '<a href="ks_ads&edit_id=' . $show_fp['id'] . '"><i class="align-middle" data-feather="edit" style="color:#495057"></i></a>';
                                                }
                                                echo '<tr>
                                                    <td style="text-align:center;">' . $num . '</td>                                        
                                                    <td style="text-align:center;">' . $show_fp['id_ads'] . '</td>                                        
                                                    <td style="text-align:center;">' . $show_fp['sms'] . '</td>                                        
                                                    <td style="text-align:center;">' . $show_fp['cmt'] . '</td>                                                                      
                                                    <td style="text-align:center;">' . number_format($show_fp['money']) . 'đ</td>   
                                                    <td style="text-align:center;">' . _sql($show_fp['ghi_chu']) . '</td> 
                                                    <td style="text-align:center;">' . date('Y.m.d', $show_fp['time_add']) . '</td>  
                                                    <td style="text-align:center;" title="' . date('H:i:s', $show_fp['time_update']) . '">' . $ngay_show . '</td>
                                                    <td style="text-align:center;">'.$quyen_edit.'</td>
                                                    </tr>';
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-header alert-info">
                        <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                            Danh sách ADS của team
                        </div>
                    </div>
                    <div class="">
                        <div class="table-responsive flex-row flex-nowrap">
                            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align:center; width:5%;">#</th>
                                        <th style="text-align:center;">Member</th>
                                        <th style="text-align:center;">Ads_id</th>
                                        <th style="text-align:center;">SMS</th>
                                        <th style="text-align:center;">CMT</th>
                                        <th style="text-align:center;">Số tiền</th>
                                        <th style="text-align:center;">Kết quả</th>
                                        <th style="text-align:center;">Ghi chú</th>
                                        <th style="text-align:center;">Ngày</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sqlAll = "SELECT COUNT(`id`) FROM `vn_tk_ads` WHERE team=$id_team";
                                    $stmt5 = $conn->query($sqlAll);
                                    $total_records  = $stmt5->fetchColumn();
                                    $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                                    $total_page = ceil($total_records / $limit);
                                    $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                                    $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                                    if ($total_page > 0) {
                                        $total_page_max = $total_page - 1;
                                    } else {
                                        $total_page_max = $total_page;
                                    }
                                    $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                                    $start_page = $_GET['page'] * $limit;
                                    $num_1 = 0;
                                    $stmt1 =  $conn->prepare("SELECT * FROM vn_tk_ads WHERE team=$id_team ORDER BY id DESC LIMIT $start_page, $limit");
                                    $stmt1->execute(array());
                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                    $num = 0;
                                    foreach ($list_code as $show_fp) {
                                        $arr = (array) $show_fp;
                                        $num_1 = $num_1 + 1;
                                        $num = $num_1 + $_GET['page'] * $limit;
                                        $ngay_hien_tai = date('d/m/Y', $time_php);
                                        $ngay_them = date('d/m/Y', $show_fp['time_add']);
                                        $ngay_show = $ngay_them == $ngay_hien_tai ? date('H:i:s', $show_fp['time_add']) : $ngay_them;
                                        echo '<tr>
                                        <td style="text-align:center;">' . $num . '</td>    
                                        <td style="text-align:center;">' . ucwords(sql_member($show_fp['mem'], 'name')) . '</td>                                                                            
                                        <td style="text-align:center;">' . $show_fp['id_ads'] . '</td>                                        
                                        <td style="text-align:center;">' . $show_fp['sms'] . '</td>                                        
                                        <td style="text-align:center;">' . $show_fp['cmt'] . '</td>                                                                      
                                        <td style="text-align:center;">' . number_format($show_fp['money']) . 'đ</td>   
                                        <td style="text-align:center;">' . number_format($show_fp['money'] / ($show_fp['sms'] + $show_fp['cmt'])) . 'đ</td>   
                                        <td style="text-align:center;">' . _sql($show_fp['ghi_chu']) . '</td> 
                                        <td style="text-align:center;" title="' . date('H:i:s d-m-Y', $show_fp['time_update']) . '">' . date('Y.m.d', $show_fp['time_add']) . '</td>                                          
                                        </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
                load_page(_sql02($_SERVER['REQUEST_URI']), $total_page, $limit, $total_records, $total_page_max);
                load_dialog($total_page_max, $member['id']);
                ?>
            </div>
        </div>
    </div>
</main>