<?php
if ($id_level == 5) {
    header('Location: /');
    exit;
}
function ds_team($id)
{
    global $conn;
    $stmt1 =  $conn->prepare("SELECT * from danh_sach_team where id = $id limit 1");
    $stmt1->execute();
    return $stmt1->fetch()["name_team"];
}
function get_name_mem($id_name)
{
    global $conn;
    $stmt1 =  $conn->prepare("SELECT * from member where id = $id_name limit 1");
    $stmt1->execute();
    return $stmt1->fetch()["name"];
}
$title = "Thu chi";
require '055_js.php';
require 'site/widget/header.php';?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div id="id_button_td">
                <h3 class="mt-3 ml-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_TaoDonHang" aria-expanded="false" aria-controls="collapseTwo">
                        Thêm khoản thu - chi
                    </a>
                </h3>
            </div>
            <div id="id_button_td">
                <h3 class="mt-3 ml-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_THUCHI" aria-expanded="false" aria-controls="collapseTwo">
                        Thống kê của bạn
                    </a>
                </h3>
            </div>
            <?php if($id_level == 9) {?>
                <div id="id_button_td">
                    <h3 class="mt-3 ml-3 btn btn-info mr-2" style="float: left;">
                        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_Xoa" aria-expanded="false" aria-controls="collapseTwo">
                            Lịch sử xóa
                        </a>
                    </h3>
                </div>
            <?php } ?>
            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_TaoDonHang" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group mb-3  col-md-3">
                                        <select id="loai_gd" class="custom-select flex-grow-1" required>
                                            <option value="0">Loại giao dịch</option>
                                            <option value="1">Chi (tiền ra)</option>
                                            <option value="2">Thu (tiền vào)</option>
                                        </select>
                                    </div>

                                    <div class="form-group mb-3  col-md-3">
                                        <select class="custom-select flex-grow-1" id="js_khoan_chi" onchange="khoan_chi_01()" required>
                                            <option value="0">Khoản chi</option>
                                            <?php
                                            $stmt1 =  $conn->prepare("SELECT * FROM ke_toan_01 ORDER BY id ASC");
                                            $stmt1->execute();
                                            $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                            $num = 0;
                                            foreach ($list_code as $show) {
                                                echo '<option value="' . $show['id'] . '">' . $show['name'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group mb-3  col-md-3">
                                        <select class="custom-select flex-grow-1" id="chi_tiet_khoan_chi" required>
                                            <option value="0">Chi tiết khoản chi</option>
                                        </select>
                                    </div>
                                    <!-- <div class="form-group mb-3 col-md-3" id="js_doi_tac">
                                        <input class="form-control h38" id="js_name_doi_tac" value="" onkeyup="js_doi_tac()" placeholder="Đối tác" autocomplete="on" required></input>
                                    </div> -->
                                    <div class="form-group mb-3  col-md-3">
                                        <select class="custom-select flex-grow-1" id="chon_team" required>
                                            <?php
                                            $stmt1 =  $conn->prepare("SELECT * FROM danh_sach_team ORDER BY id ASC");
                                            $stmt1->execute();
                                            $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                            $num = 0;
                                            foreach ($list_code as $show) {
                                                echo '<option value="' . $show['id'] . '">' . $show['name_team'] . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md mb-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">Ngày</span>
                                                </div>
                                                <input type="date" name="date_bd" id="date_bd" value="<?= $arr_t[5] ?>" class="form-control">
                                            </div>
                                        </div>
                                    <div class="form-group mb-3  col-md-12">
                                        <input class="form-control h38" type="number" max="50000000" value="" id="so_tien" placeholder="Số tiền" required></input>
                                        <input hidden value="<?= $id_edit ?>" name="id_edit"></input>
                                    </div>
                                    <div class="form-group  mb-3 col-md-12">
                                        <textarea class="form-control" rows="4" id="ghi_chu" placeholder="Mô tả đơn hàng (ghi chú)"></textarea>
                                    </div>
                                </div>
                                <div class="text-xs-right">
                                    <button type="submit" name="xac_nhan" id="button_xn_k" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_THUCHI" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-header alert-info">
                            <h5 class="card-title mb-0  text-white fs_18" style="font-weight: bold;">Tổng quát thu chi</small></h5>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width:5%; text-align:center;">#</th>
                                    <th style="width:30%; text-align:center;">Giao dịch</th>
                                    <th style="text-align:center;">Tiền (x1000đ)</th>
                                </tr>
                            </thead>
                            <tbody id="top_online">
                                <?php
                                $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`so_tien` ) as `total_st` FROM `ke_toan_03` AS  `a` WHERE mem=$id_member GROUP BY `ke_toan_01`  ORDER BY `so_tien` ASC");
                                $stmt1->execute(array());
                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                foreach ($list_code as $show_fp) {
                                    $num++;
                                    echo '<tr>
                                    <td style="text-align:center;">' . $num . '</td>
                                    <td style="text-align:center;">' . sql_web($show_fp['ke_toan_01'], 'ke_toan_01', 'name') . '</td>
                                    <td style="text-align:center;">' . number_format($show_fp['total_st'] / 1000, 0) . '</td>                                                              
                                    </tr>';
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php if($id_level == 9){?>
                <div class="col-12 mb-3">
                    <div id="accordion">
                        <div id="collapse_Xoa" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                            <div class="card-header alert-info">
                                <h5 class="card-title mb-0  text-white fs_18" style="font-weight: bold;">Lích sử xóa</small></h5>
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width:5%; text-align:center;">#</th>
                                        <th style="text-align:center;">Member</th>
                                        <th style=" text-align:center;">Team</th>
                                        <th style=" text-align:center;">Khoản chi</th>
                                        <th style="text-align:center;">Tiền (x1000đ)</th>
                                        <th style=" text-align:center;">Ghi chú</th>
                                        <th style=" text-align:center;">Ngày thêm</th>
                                        <th style=" text-align:center;">Người xóa</th>
                                        <th style=" text-align:center;">Ngày xóa</th>
                                        
                                    </tr>
                                </thead>
                                <tbody id="top_online">
                                    <?php
                                    $stmt1 =  $conn->prepare("SELECT * FROM ke_toan_03 WHERE mem_xoa>0 ORDER BY id DESC");
                                    $stmt1->execute(array());
                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                    $num = 0;
                                    foreach ($list_code as $show_fp) {
                                        $num++;
                                        $loai_ti = sql_web($show_fp['ke_toan_01'], 'ke_toan_01', 'name');
                                        $so_tien_show = number_format(abs($show_fp['so_tien']) / 1000, 0);
                                        $time_ngay_them = convert_to_date_int($show_fp['time_num']);
                                        echo '<tr>
                                        <td style="text-align:center;">' . $num . '</td>
                                        <td style="text-align:center;">' .  sql_member($show_fp['mem'], 'name') . '</td>
                                        <td style="text-align:center;">'.ds_team((int)$show_fp['doi_tac']).'</td>    
                                        <td style="text-align:center;">'.$loai_ti.'</td>    
                                        <td style="text-align:center;">'.$so_tien_show.'</td>    
                                        <td style="text-align:center;">'.$show_fp['ghi_chu'].'</td>    
                                        <td style="text-align:center;">'.$time_ngay_them.'</td>
                                        <td style="text-align:center;">'.get_name_mem($show_fp['mem_xoa']).'</td>
                                        <td style="text-align:center;">'.date("Y/m/d",$show_fp['ly_do_xoa'] ).'</td>                                                              
                                        </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-header alert-info">
                        <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                            Danh sách khoản chi
                        </div>
                    </div>
                    <div class="">
                        <div class="table-responsive flex-row flex-nowrap">
                            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align:center; width:5%;">#</th>
                                        <th style="text-align:center;">Member</th>
                                        <th style="text-align:center;">Team</th>
                                        <th style="text-align:center;">Khoản chi</th>
                                        <th style="text-align:center;">Tiền</th>
                                        <th style="text-align:center;">Ghi chú</th>
                                        <th style="text-align:center;">Ngày thêm</th>
                                        <th style="text-align:center; width:7%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $type_sql = '1=1';
                                    $sqlAll = "SELECT COUNT(`mem`) FROM `ke_toan_03` WHERE $type_sql AND mem_xoa=0";
                                    $stmt5 = $conn->query($sqlAll);
                                    $total_records  = $stmt5->fetchColumn();
                                    $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                                    $total_page = ceil($total_records / $limit);
                                    $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                                    $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                                    if ($total_page > 0) {
                                        $total_page_max = $total_page - 1;
                                    } else {
                                        $total_page_max = $total_page;
                                    }
                                    $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                                    $start_page = $_GET['page'] * $limit;
                                    $num_1 = 0;
                                    $stmt1 =  $conn->prepare("SELECT * FROM ke_toan_03 WHERE $type_sql AND mem_xoa=0 ORDER BY id DESC LIMIT $start_page, $limit");
                                    $stmt1->execute(array());
                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                    $num = 0;
                                    foreach ($list_code as $show_fp) {
                                        $arr = (array) $show_fp;
                                        $num_1 = $num_1 + 1;
                                        $num = $num_1 + $_GET['page'] * $limit;
                                        $ngay_hien_tai = date('d-m-Y', $time_php);
                                        $ngay_them = date('d-m-Y', $show_fp['time_add']);
                                        $ngay_show = $ngay_them == $ngay_hien_tai ? date('H:i:s', $show_fp['time_add']) : $ngay_them;
                                        $time_ngay_them = convert_to_date_int($show_fp['time_num']);
                                        $ghi_chu_show = $show_fp['ghi_chu'] != '' ? mb_substr($show_fp['ghi_chu'], 0, 25, 'UTF-8') : sql_web($show_fp['ke_toan_02'], 'ke_toan_02', 'name');
                                        if ($id_member == $show_fp['mem'] or $id_level > 7) {
                                            $quyen_edit = '<a href="' . _sql03($_SERVER['REQUEST_URI']) . '&delfp=' . $show_fp['id'] . '" onclick="return confirm(\'#' . $num . ' - Khoản chi của đối tác: ' . $show_fp['doi_tac'] . ', sẽ bị xóa?\')"><i class="align-middle" data-feather="trash" style="color:#495057"></i></a>';
                                        } else {
                                            $quyen_edit = '<i class="align-middle" data-feather="alert-circle"></i>';
                                        }
                                        $loai_mau = $show_fp['mem'] == $id_member ? 'blue' : '';
                                        $loai_mau = $show_fp['so_tien'] > 0 ? 'red' : $loai_mau;
                                        $loai_ti = sql_web($show_fp['ke_toan_01'], 'ke_toan_01', 'name');
                                        $so_tien_show = '***';
                                        $ghi_chu_show1 = '***';
                                        if ($show_fp['mem'] == $id_member or ($id_level > 6 and $id_team == $show_fp['team']) or $id_level > 7) {
                                            $so_tien_show = number_format(abs($show_fp['so_tien']) / 1000, 0);
                                            $ghi_chu_show1 = $ghi_chu_show;
                                        }
                                        echo '<tr>
                                        <td style="text-align:center; color: ' . $loai_mau . ';">' . $num . '</td>                                        
                                        <td style="text-align:center; color: ' . $loai_mau . ';">' . sql_member($show_fp['mem'], 'name') . '</td>      
                                        <td style="text-align:center; color: ' . $loai_mau . ';">' . ds_team((int)$show_fp['doi_tac']). '</td>                                        
                                        <td style="text-align:center; color: ' . $loai_mau . ';">' . $loai_ti . '</td>
                                        <td style="text-align:center; color: ' . $loai_mau . ';">' .  number_format(($show_fp['so_tien']*-1)) . '</td>  
                                        <td style="text-align:center; color: ' . $loai_mau . ';" title="' . $show_fp['ghi_chu'] . '">' . $ghi_chu_show1 . '</td>
                                        <td style="text-align:center; color: ' . $loai_mau . ';" title="' . $time_ngay_them .'">' .$time_ngay_them. '</td>
                                        <td style="text-align:center; color: ' . $loai_mau . ';">' . $quyen_edit . '</td>
                                        </tr>';
                                    }
                                    // <td style="text-align:center; color: ' . $loai_mau . ';" title="' . date('d-m-Y H:i:s', $show_fp['time_add']) . '">' .  $ngay_show . '</td>
                                    $sqlAll = "SELECT SUM(`so_tien`) FROM `ke_toan_03`WHERE `mem`=$id_member AND mem_xoa=0";
                                    $stmt = $conn->query($sqlAll);
                                    $tong_tien_mem = $stmt->fetchColumn();
                                    echo '<tr>
                                        <td style="text-align:center;">' . number_format($num + 1) . '</td>                                        
                                        <td style="text-align:right;" colspan="3">Số dư hiện tại của bạn</td>
                                        <td style="text-align:left;" colspan="4">' .  number_format($tong_tien_mem, 0) . 'đ</td>  
                                        </tr>';
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
                load_page(_sql($_SERVER['REQUEST_URI']), $total_page, $limit, $total_records, $total_page_max);
                load_dialog($total_page_max, $member['id']);
                ?>
            </div>
        </div>
    </div>
</main>
<script>
    function khoan_chi_01() {
        $.post("ke_toan", {
            js_khoan_chi: document.getElementById("js_khoan_chi").value,
            name: name
        }, function(data, status) {
            document.getElementById("chi_tiet_khoan_chi").innerHTML = data;
        });
    }

    function js_doi_tac() {
        abc = document.getElementById("js_name_doi_tac").value;
        if (abc == "mem" || abc == "team") {
            $.post("ke_toan", {
                js_doi_tac: abc
            }, function(data, status) {
                document.getElementById("js_doi_tac").innerHTML = data;
            });
        }
    }
    $('#button_xn_k').click(function() {
        loai_gd = Number(document.getElementById("loai_gd").value);
        khoan_chi = Number(document.getElementById("js_khoan_chi").value);
        chi_tiet = Number(document.getElementById("chi_tiet_khoan_chi").value);
        date_bd = document.getElementById("date_bd").value;
        chon_team = Number(document.getElementById("chon_team").value);
        sum_loi = 0;
        // name_nhan_2 = 0;
        // try {
        //     name_nhan = document.getElementById("js_name_doi_tac").value;
        // } catch {
        //     ten_nv = 1;
        //     name_nhan = "koko";
        //     name_nhan_2 = Number(document.getElementById("js_name_doi_tac_2").value);
        // }
        so_tien = Number(document.getElementById("so_tien").value);
        ghi_chu = document.getElementById("ghi_chu").value;
        show_error = "<div><table class='table table-bordered table-striped' style='width:100%'><tbody>";
        if (loai_gd < 1) {
            sum_loi++;
            show_error = show_error + "<tr><td>Bạn chưa chọn loại giao dịch</td></tr>";
        }
        if (khoan_chi < 1) {
            sum_loi++;
            show_error = show_error + "<tr><td>Bạn chưa chọn khoản cần chi</td></tr>";
        }
        // if ((name_nhan_2 < 1 && name_nhan == "koko") || name_nhan.length < 1) {
        //     sum_loi++;
        //     show_error = show_error + "<tr><td>Bạn chưa chọn đối tác</td></tr>";
        // }
        if (so_tien < 1) {
            sum_loi++;
            show_error = show_error + "<tr><td>Số tiền phải lớn hơn 1</td></tr>";
        }
        if (sum_loi > 0) {
            document.getElementById("popup_title").innerHTML = "Thông báo lỗi";
            document.getElementById("popup_noidung").innerHTML = show_error;
            $('#popup_ne').modal('show');
        } 
        else {
            $.post("ke_toan", {
                button_xn_k: "",
                loai_gd: loai_gd,
                khoan_chi: khoan_chi,
                chi_tiet: chi_tiet,
                date_bd: date_bd,
                chon_team: chon_team,
                // name_nhan: name_nhan,
                // name_nhan_2: name_nhan_2,
                so_tien: so_tien,
                ghi_chu: ghi_chu
            }, function(data, status) {
                // console.log(data);
                if (data == "oke") {
                    window.location.replace("/ke_toan");
                }
            });
        }
    });
</script>
<div class="modal fade" id="popup_ne" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg_xanh_duong_nhat">
                <h2 style="text-align:center;" id="popup_title"></h2>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="popup_noidung"></div>
        </div>
    </div>
</div>