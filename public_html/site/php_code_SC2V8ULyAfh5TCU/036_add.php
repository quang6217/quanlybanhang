<?php
$title = "Tổng đơn hàng đã lên";
require 'site/widget/header.php';  ?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <?php
            $stmt1 =  $conn->prepare("SELECT * FROM vn_san_pham ORDER BY id DESC");
            $stmt1->execute();
            $list_code01 = $stmt1->fetchALL(PDO::FETCH_ASSOC);
            foreach ($list_code01 as $show_id) {
                $ma_hang = (int)$show_id['id'];
                $sqlAll = "SELECT SUM(`so_luong`) FROM `vn_hang_kho_add`WHERE `team`=$id_team AND `ma_hang`=$ma_hang";
                $stmt = $conn->query($sqlAll);
                $tong_sl_ton = $stmt->fetchColumn();
                if ($tong_sl_ton != 0) {
                    $stmt =  $conn->prepare("SELECT * FROM vn_san_pham WHERE id =:id");
                    $stmt->execute(array(":id" => $ma_hang));
                    $sanp = $stmt->fetch(PDO::FETCH_ASSOC);
            ?>
                    <div class="col-12 mt-2">
                        <div class="card">
                            <div class="card-header alert-info">
                                <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                                    <?= $sanp['name'] ?> - Mã <?= $sanp['id'] ?> số lượng đã bán: <?= number_format(-1 * $tong_sl_ton, 0) ?>/sp
                                </div>
                            </div>
                            <div class="">
                                <div class="table-responsive flex-row flex-nowrap">
                                    <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center; width:5%;">#</th>
                                                <th style="text-align:center;">Màu sắc</th>
                                                <?php
                                                $ar_size = explode('|', $sanp['size_ban']);
                                                for ($i1 = 0; $i1 < count($ar_size); $i1++) {
                                                    $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id");
                                                    $stmt->execute(array(":id" => (int) $ar_size[$i1]));
                                                    $size = $stmt->fetch(PDO::FETCH_ASSOC);
                                                    echo '<th style="text-align:center;">Size ' . $size['name_1'] . ' - (%)</th>';
                                                }
                                                ?>
                                                <th style="text-align:center;">Tổng</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $mau_sac = explode('|', $sanp['mau_sac']);
                                            $num = 0;
                                            for ($i1 = 0; $i1 < count($mau_sac); $i1++) {
                                                $num++;
                                                $stmt =  $conn->prepare("SELECT * FROM vn_mau_sac WHERE id =:id");
                                                $stmt->execute(array(":id" => (int) $mau_sac[$i1]));
                                                $mau_sac1 = $stmt->fetch(PDO::FETCH_ASSOC);
                                                echo '<tr>
                                            <td style="text-align:center;">' . $num . '</td>                                        
                                            <td style="text-align:center;">Màu ' . $mau_sac1['name'] . '</td>';
                                                $ar_size2 = explode('|', $sanp['size_ban']);
                                                $tong_sl = 0;
                                                for ($i2 = 0; $i2 < count($ar_size2); $i2++) {
                                                    $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id");
                                                    $stmt->execute(array(":id" => (int) $ar_size2[$i2]));
                                                    $size = $stmt->fetch(PDO::FETCH_ASSOC);
                                                    $id_mau = (int)$mau_sac1['id'];
                                                    $id_size = (int)$ar_size2[$i2];
                                                    $stmt1 =  $conn->prepare("SELECT * FROM vn_hang_kho_add WHERE team=$id_team AND ma_hang=$ma_hang AND mau_sac=$id_mau AND size = $id_size");
                                                    $stmt1->execute();
                                                    $ton_kho = $stmt1->fetch(PDO::FETCH_ASSOC);
                                                    echo '<th style="text-align:center;">' . number_format(-1 * $ton_kho['so_luong'], 0) . ' - (' . number_format($ton_kho['so_luong'] / $tong_sl_ton * 100, 1) . '%)</th>';
                                                    $tong_sl = $tong_sl + $ton_kho['so_luong'];
                                                }
                                                echo '<td style="text-align:center;">' . number_format(-1 * $tong_sl, 0) . '</td>';
                                                echo '</tr>';
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php }
            } ?>