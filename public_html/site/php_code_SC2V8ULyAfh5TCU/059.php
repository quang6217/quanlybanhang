<?php
if ($id_level == 5) {
    header('Location: /');
    exit;
}
function get_tong_tien_chi($id_mem,$tim_kiem_chi_tieu)
{
    global $conn;
    $stmt1 =  $conn->prepare("SELECT mem ,sum(so_tien) as `tien_chi` FROM `ke_toan_03` where mem_xoa =0 and mem = $id_mem and so_tien <0 
    and $tim_kiem_chi_tieu GROUP by mem limit 1");
    $stmt1->execute();
    return $stmt1->fetch()["tien_chi"];

}
function _sql_date($t)
{
    return preg_replace('/[^0-9-]/', '', $t);
}
if (isset($_GET['search'])) {
    if ($_GET['search'] == 0) {
        $_SESSION['tim_kiem_list_hang'] = '1=1';
        $_SESSION['tim_kiem_ngay_thang'] = '1=1';
        $_SESSION['data_tim_kiem'] = '';
        $_SESSION['data_ngay_thang'] = '';
        header('Location: /tk_don_mem');
        exit;
    }
}
if (isset($_POST['button_xn_tim_kiem'])) {
    $data_in_team = '1=1';
    $in_team = (int)$_POST['in_team'];
    if ($in_team > 0) {
        $data_in_team = 'team=' . $in_team;
    }
    $data_nguon_khach = '1=1';
    $id_nguon_khach = (int)$_POST['id_nguon_khach'];
    if ($id_nguon_khach > 0) {
        $data_nguon_khach = 'nguonKH=' . $id_nguon_khach;
    }
    $data_status_ly_do = '1=1';
    $id_status_ly_do = (int)$_POST['id_status_ly_do'];
    if ($id_status_ly_do > 0) {
        if ($id_status_ly_do < 1000) {
            $data_status_ly_do = 'type_dh=' . $id_status_ly_do;
        } else {
            $id_stt = $id_status_ly_do - 1000;
            $data_status_ly_do = 'trang_thai=' . $id_stt;
        }
    }
    $a_date_bd = '1=1';
    if ($_POST['date_bd'] != '') {
        $i_date_bd = _sql_date($_POST['date_bd']);
        $date_bd = _sql_num($_POST['date_bd']);
        $a_date_bd = '(`time_num`>=' . (int)$date_bd . ')';
    }
    $a_date_kt = '1=1';
    if ($_POST['date_kt'] != '') {
        $i_date_kt = _sql_date($_POST['date_kt']);
        $date_kt = _sql_num($_POST['date_kt']);
        $a_date_kt = '(`time_num`<=' . (int)$date_kt . ')';
    }
    $_SESSION['data_tim_kiem'] = $in_team . '|' . $id_nguon_khach . '|' . $id_status_ly_do . '|' . $i_date_bd . '|' . $i_date_kt;
    $_SESSION['tim_kiem_list_hang'] = $data_nguon_khach . ' AND ' . $data_status_ly_do . ' AND ' . $a_date_bd . ' AND ' . $a_date_kt . ' AND ' . $data_in_team;
    $_SESSION['data_ngay_thang'] = $i_date_bd . '|' . $i_date_kt;
    $_SESSION['tim_kiem_ngay_thang'] = $a_date_bd . ' AND ' . $a_date_kt;
}
if (!isset($_SESSION['tim_kiem_list_hang'])) {
    $_SESSION['tim_kiem_list_hang'] = '1=1';
    $_SESSION['tim_kiem_ngay_thang'] = '1=1';
    $class_show = '';
} else {
    if ($_SESSION['tim_kiem_list_hang'] != '1=1') {
        $arr_t = explode('|', $_SESSION['data_tim_kiem']);
        // $arr_t_ngay_thang = explode('|', $_SESSION['data_ngay_thang']);
        $class_show = 'show';
    }
}
if ($i_date_bd < 1) {
    $i_date_bd = date('Y-m', $time_php) . '-1';
}
if ($i_date_kt < 1) {
    $i_date_kt = date('Y-m-d', $time_php);
}
$timkiem = $_SESSION['tim_kiem_list_hang'];
$timkiem_ngay_thang = $_SESSION['tim_kiem_ngay_thang'];
$title = "Đơn hàng đã lên mem";
$list_tk = 'active';
$list_tk_1 = 'show';

if (isset($_POST['chi_tiet_tt'])) {
    $uid = (int)$_POST['chi_tiet_tt'];
    $sum_dh = (int)$_POST['dh'];
    $sum_thuc_nhan  = (int)$_POST['sum_thuc_nhan'];
    $trangthai[0] = "****";
    $trangthai[70] = "Gom thành công";
    $trangthai[91] = "Phát chưa thành công";
    $trangthai[100] = "Phát thành công";
    $trangthai[110] = "Chờ trả tiền";
    $trangthai[120] = "Đã trả tiền";
    $trangthai[161] = "Phát hoàn chưa TC";
    $trangthai[170] = "Phát hoàn thành công";
    // tổng đơn đã đặt
    $stmt_sum =  $conn->prepare("SELECT * , sum(`a`.`thuc_nhan` ) as `total_thuc_nhan`, count(`a`.`id` ) as `total_dh` FROM `sale_sanpham_03` AS  `a` 
    WHERE mem= $uid  AND  $timkiem  GROUP BY `mem`  ASC");
    $stmt_sum->execute();
    $data = $stmt_sum->fetch();
    $tong_don_da_dat = $data["total_dh"];

    // =================================
    $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`thuc_nhan` ) as `total_thuc_nhan`, count(`a`.`id` ) as `total_dh` FROM `sale_sanpham_03` AS  `a` WHERE mem=$uid AND $timkiem AND trang_thai!=44 AND (type_dh<1 OR type_dh>8) GROUP BY `OrderStatusId` ORDER BY `total_thuc_nhan` ASC");
    $stmt1->execute(array());
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $tilechotdon =   $sum_dh / $tong_don_da_dat*100;
    echo '<div class="table-responsive flex-row flex-nowrap">
    <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
    <thead> <tr> <th style="text-align:center;">Tên mục</th><th colspan="2" style="text-align:center;">Thống kê</th></tr>
    </thead>
    <tbody>';
    echo '<tr><td style="text-align:center;">Tỷ lệ chốt</td>
              <td style="text-align:center;" colspan="2"> '.number_format($tilechotdon).'% <sup style="color:blue";>('.$sum_dh.'/'.$tong_don_da_dat.')</sup></td></tr>';
    echo '<tr><td style="text-align:center;">Trung bình đơn</td>
              <td style="text-align:center;" colspan="2"> '.number_format($sum_thuc_nhan/$sum_dh).'</td></tr>';
    echo '</table>';

    echo '<div class="table-responsive flex-row flex-nowrap">
    <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
    <thead>
    <tr>
    <th style="text-align:center;">Trạng thái</th>
    <th style="text-align:center;">Số đơn</th>
    <th style="text-align:center;">Thực nhận</th>
    <th style="text-align:center;">Tỷ lệ</th>
    </tr>
    </thead>
    <tbody>';
    foreach ($list_code as $show_fp) {
        echo '<tr>
            <td style="text-align:center;">' . $trangthai[$show_fp['OrderStatusId']] . '</td>
            <td style="text-align:center;">  ' . number_format($show_fp['total_dh']) . '</td>
            <td style="text-align:center;">' . number_format($show_fp['total_thuc_nhan']) . '</td>
            <td style="text-align:center;">' . number_format($show_fp['total_dh'] / $sum_dh*100) . '%</td>
            </tr>';
    }
    exit;
}
require 'site/widget/header.php'; ?>
<div class="content">
    <div class="row">
        <div class="col-12 mb-3" id="accordion">
            <div id="collapse_TaoDonHang" class="card collapse <?= $class_show ?>" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card" style="margin-bottom: -0.5rem;">
                    <div class="card-body">
                        <form method="post">
                            <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12">
                                <div class="row">
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Team</span>
                                            </div>
                                            <select class="custom-select flex-grow-1" name="in_team" id="check_team_on" onchange="check_member_team()">
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM danh_sach_team ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                echo '<option value="0">...</option>';
                                                foreach ($list_code as $show) {
                                                    $selected_ck_2 = $arr_t[0] == $show['id'] ? 'selected' : '';
                                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name_team'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Nguồn KH</span>
                                            </div>
                                            <select class="custom-select flex-grow-1" name="id_nguon_khach">
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM admin_nguon_khach_hang WHERE an=0 ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                echo '<option value="0">...</option>';
                                                foreach ($list_code as $show) {
                                                    $selected_ck_2 = $arr_t[1] == $show['id'] ? 'selected' : '';
                                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Trạng thái</span>
                                            </div>
                                            <select class="custom-select flex-grow-1" name="id_status_ly_do">
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM admin_ly_do_chot_xit WHERE an=0 ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                echo '<option value="0">...</option>';
                                                foreach ($list_code as $show) {
                                                    $selected_ck_2 = $arr_t[2] == $show['id'] ? 'selected' : '';
                                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Ngày bắt đầu</span>
                                            </div>
                                            <input type="date" name="date_bd" value="<?= $arr_t[3] ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Kết thúc</span>
                                            </div>
                                            <input type="date" name="date_kt" value="<?= $arr_t[4] ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-xs-right">
                                <button type="submit" name="button_xn_tim_kiem" id="button_xn_tim_kiem" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card mb-3">
                <div class="card-header alert-info">
                    <h5 class="card-title mb-0  text-white fs_18 d-flex justify-content-between" style="font-weight: bold;">
                        <div>Đơn hàng đã lên nhân viên</div><i class="align-middle mr-2 fas fa-fw fa-search" data-toggle="collapse" data-target="#collapse_TaoDonHang"></i></small>
                    </h5>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center;">TEAM</th>
                            <th style="text-align:center;">Đơn</th>
                            <th style="text-align:center;">Tổng tiền</th>
                            <th style="text-align:center;">VAT</th>
                            <th style="text-align:center;">Giảm giá</th>
                            <th style="text-align:center;">Ship</th>
                            <th style="text-align:center;">Chi phí</th>
                            <th style="text-align:center;">Thực nhận</th>
                            <th style="text-align:center;">%</th>
                        </tr>
                    </thead>
                    <tbody id="top_online">
                        <?php
                        $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`tong_tien` ) as `total_tong_tien`, sum(`a`.`thuc_nhan` ) as `total_thuc_nhan`, sum(`a`.`vat` ) as `total_vat`, sum(`a`.`ship` ) as `total_ship`, sum(`a`.`giam_gia` ) as `total_giam_gia`, count(`a`.`id` ) as `total_dh` FROM `sale_sanpham_03` AS  `a` WHERE $timkiem AND trang_thai!=44 AND (type_dh<1 OR type_dh>8) GROUP BY `mem` ORDER BY `total_thuc_nhan` DESC");
                        $stmt1->execute(array());
                        $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                        $tong_tien_chi = 0;
                        foreach ($list_code as $show_fp) {
                            $tien_chi = 0;
                            if($id_level > 0)
                            {
                                if(get_tong_tien_chi($show_fp['mem'],$timkiem_ngay_thang)!= NULL )
                                {
                                    $tien_chi = (int)get_tong_tien_chi($show_fp['mem'],$timkiem_ngay_thang)*-1;
                                    $tong_tien_chi += $tien_chi;
                                }
                            }
                            $s_don = $s_don + $show_fp['total_dh'];
                            $s_tien = $s_tien + $show_fp['total_tong_tien'];
                            $s_vat = $s_vat + $show_fp['total_vat'];
                            $s_giam = $s_giam + $show_fp['total_giam_gia'];
                            $s_ship = $s_ship + $show_fp['total_ship'];
                            $s_nhan = $s_nhan + $show_fp['total_thuc_nhan'];
                            $ten_nhan_vien = sql_web($show_fp['mem'], 'member', 'name');
                            echo '<tr>                        
                            <td style="text-align:center;">' .  $ten_nhan_vien . '</td>
                            <td style="text-align:center;">' . number_format($show_fp['total_dh'], 0) . '</td>
                            <td style="text-align:center;">' . number_format($show_fp['total_tong_tien']) . '</td>
                            <td style="text-align:center;">' . number_format($show_fp['total_vat']) . '</td>
                            <td style="text-align:center;">' . number_format($show_fp['total_giam_gia']) . '</td>
                            <td style="text-align:center;">' . number_format($show_fp['total_ship']) . '</td>
                            <td style="text-align:center;">'.number_format($tien_chi).'</td>
                            <td style="text-align:center;" onclick="chi_tiet(' . $show_fp['mem'] . ',\'' .  $ten_nhan_vien . '\',' . $show_fp['total_thuc_nhan'] . ',' . $show_fp['total_dh'] . ')">' . number_format($show_fp['total_thuc_nhan']) . '</td>
                            <td style="text-align:center;">'.number_format($tien_chi*100/(int)$show_fp['total_thuc_nhan']).'%</td>
                            </tr>';
                        }
                        echo '<tr>
                          <td style="text-align:center; color:red">Tổng</td>
                          <td style="text-align:center; color:red">' . number_format($s_don) . '</td>
                          <td style="text-align:center; color:red">' . number_format($s_tien) . '</td>
                          <td style="text-align:center; color:red">' . number_format($s_vat) . '</td>
                          <td style="text-align:center; color:red">' . number_format($s_giam) . '</td>
                          <td style="text-align:center; color:red">' . number_format($s_ship) . '</td>
                          <td style="text-align:center; color:red">'.number_format($tong_tien_chi).'</td>
                          <td style="text-align:center; color:red">' . number_format($s_nhan) . '</td>
                          <td style="text-align:center; color:red">' . number_format($tong_tien_chi*100/$s_nhan) . '%</td>
                          </tr>';
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function chi_tiet(uid, ten, sdt, dh) {
        document.getElementById("popup_title").innerHTML = "Nhân viên: "+ten;
        $.post("", {
            chi_tiet_tt: uid,
            dh: dh,
            sum_thuc_nhan:sdt
        }, function(data) {
            document.getElementById("popup_noidung").innerHTML = data;
            $('#popup_ne').modal('show');
        });
    }
</script>
<div class="modal fade" id="popup_ne" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg_xanh_duong_nhat">
                <h2 style="text-align:center;" id="popup_title"></h2>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="popup_noidung"></div>
        </div>
    </div>
</div>