<?php
if ($id_level == 5) {
    header('Location: /');
    exit;
}
function _sql_date($t)
{
    return preg_replace('/[^0-9-]/', '', $t);
}
if (isset($_GET['search'])) {
    if ($_GET['search'] == 0) {
        $_SESSION['tim_kiem_list_hang'] = '1=1';
        $_SESSION['data_tim_kiem'] = '';
        header('Location: /cp_tong_quat');
        exit;
    }
}
if (isset($_POST['button_xn_tim_kiem'])) {
    $data_in_team = '1=1';
    $in_team = (int)$_POST['in_team'];
    if ($in_team > 0) {
        $data_in_team = 'team=' . $in_team;
    }
    $data_nguon_khach = '1=1';
    $id_nguon_khach = (int)$_POST['id_nguon_khach'];
    if ($id_nguon_khach > 0) {
        $data_nguon_khach = 'nguonKH=' . $id_nguon_khach;
    }
    $data_status_ly_do = '1=1';
    $id_status_ly_do = (int)$_POST['id_status_ly_do'];
    if ($id_status_ly_do > 0) {
        if ($id_status_ly_do < 1000) {
            $data_status_ly_do = 'type_dh=' . $id_status_ly_do;
        } else {
            $id_stt = $id_status_ly_do - 1000;
            $data_status_ly_do = 'trang_thai=' . $id_stt;
        }
    }
    $a_date_bd = '1=1';
    if ($_POST['date_bd'] != '') {
        $i_date_bd = _sql_date($_POST['date_bd']);
        $date_bd = _sql_num($_POST['date_bd']);
        $a_date_bd = '(`time_num`>=' . (int)$date_bd . ')';
    }
    $a_date_kt = '1=1';
    if ($_POST['date_kt'] != '') {
        $i_date_kt = _sql_date($_POST['date_kt']);
        $date_kt = _sql_num($_POST['date_kt']);
        $a_date_kt = '(`time_num`<=' . (int)$date_kt . ')';
    }
    $_SESSION['data_tim_kiem'] = $in_team . '|' . $id_nguon_khach . '|' . $id_status_ly_do . '|' . $i_date_bd . '|' . $i_date_kt;
    $_SESSION['tim_kiem_list_hang'] = $data_nguon_khach . ' AND ' . $data_status_ly_do . ' AND ' . $a_date_bd . ' AND ' . $a_date_kt . ' AND ' . $data_in_team;
}
if (!isset($_SESSION['tim_kiem_list_hang'])) {
    $_SESSION['tim_kiem_list_hang'] = '1=1';
    $class_show = '';
} else {
    if ($_SESSION['tim_kiem_list_hang'] != '1=1') {
        $arr_t = explode('|', $_SESSION['data_tim_kiem']);
        $class_show = 'show';
    }
}
if ($i_date_bd < 1) {
    $i_date_bd = date('Y-m', $time_php) . '-1';
}
if ($i_date_kt < 1) {
    $i_date_kt = date('Y-m-d', $time_php);
}
$timkiem = $_SESSION['tim_kiem_list_hang'];
$title = "Chi phí tổng quát";
$list_tk = 'active';
$list_tk_1 = 'show';
require 'site/widget/header.php'; ?>
<div class="content">
    <div class="row">
        <div class="col-12 mb-3" id="accordion">
            <div id="collapse_TaoDonHang" class="card collapse <?= $class_show ?>" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card" style="margin-bottom: -0.5rem;">
                    <div class="card-body">
                        <form method="post">
                            <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12">
                                <div class="row">
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Team</span>
                                            </div>
                                            <select class="custom-select flex-grow-1" name="in_team" id="check_team_on" onchange="check_member_team()">
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM danh_sach_team ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                echo '<option value="0">...</option>';
                                                foreach ($list_code as $show) {
                                                    $selected_ck_2 = $arr_t[0] == $show['id'] ? 'selected' : '';
                                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name_team'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Member</span>
                                            </div>
                                            <select class="custom-select flex-grow-1" name="in_member" id="member_js_sha">
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM member WHERE id!=9 ORDER BY id  ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                echo '<option value="0">...</option>';
                                                foreach ($list_code as $show) {
                                                    $selected_ck_2 = $arr_t[3] == $show['id'] ? 'selected' : '';
                                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                            <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Ngày bắt đầu</span>
                                            </div>
                                            <input type="date" name="date_bd" value="<?= $arr_t[3] ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Kết thúc</span>
                                            </div>
                                            <input type="date" name="date_kt" value="<?= $arr_t[4] ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-xs-right">
                                <button type="submit" name="button_xn_tim_kiem" id="button_xn_tim_kiem" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card mb-3">
                <div class="card-header alert-info">
                    <h5 class="card-title mb-0  text-white fs_18 d-flex justify-content-between" style="font-weight: bold;">
                        <div>Tổng quát chi phí</div><i class="align-middle mr-2 fas fa-fw fa-search" data-toggle="collapse" data-target="#collapse_TaoDonHang"></i></small>
                    </h5>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Tên khoản chi</th>
                            <th style="text-align:center;">Số tiền</th>
                        </tr>
                    </thead>
                    <tbody id="top_online">
                        <?php
                        if($id_level<8){
                            $limit_team = '(`team`=' . (int)$id_team . ')';
                        } else {
                            $limit_team = '1=1';
                        }
                        $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`so_tien` ) as `total_tong_tien` FROM `ke_toan_03` AS  `a` WHERE $timkiem AND $limit_team AND mem_xoa=0 GROUP BY `ke_toan_01` ORDER BY `id` ASC");
                        $stmt1->execute(array());
                        $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                        foreach ($list_code as $show_fp) {                            
                            $s_tien = $s_tien + $show_fp['total_tong_tien'];
                            echo '<tr>                        
                            <td style="text-align:center;">' . sql_web($show_fp['ke_toan_01'], 'ke_toan_01', 'name') . '</td>
                            <td style="text-align:center;">' . number_format($show_fp['total_tong_tien'], 0) . '</td>
                            </tr>';
                        }
                        echo '<tr>                        
                          <td style="text-align:center; color:red">Tổng</td>
                          <td style="text-align:center; color:red">' . number_format($s_tien) . '</td>
                          </tr>';
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    function check_member_team() {
        abc = document.getElementById("check_team_on").value;
        $.post("list_hang", {
            check_team_on: abc
        }, function(data) {
            document.getElementById("member_js_sha").innerHTML = data;
        });

    }
</script>