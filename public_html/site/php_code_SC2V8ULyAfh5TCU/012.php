<?php
if (isset($_GET['key'])) {
    $k_arr = explode('|', base64_de($_GET['key']));
    $email = _sql00($k_arr[0]);
    $stmt =  $conn->prepare("SELECT * FROM member WHERE `email`='$email'");
    $stmt->execute(array());
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($k_arr[1] == $member['time_login'] and $member['time_login'] < 1000000 and $member['time_login'] > 1000) {
        $sql = "UPDATE member SET time_login='$time_php', pass_error='0' WHERE id='" . $id_member . "'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        setcookie('id_member', $member['id'], $time_php + 10 * 60 * 60 * 24);
        setcookie('id_pass', $member['pass'], $time_php + 10 * 60 * 60 * 24);
        $code_1 = substr($member['pass'], 0, 10);
        $code_2 = substr($member['pass'], 10, 20);
        $token_v2 = md5(md5($code_1)) . '' . md5(md5(md5($code_2)));
        setcookie('token', $token_v2, $time_php + 10 * 60 * 60 * 24);
        $_SESSION['id_member'] = $member['id'];
        $_SESSION['id_pass'] = $member['pass'];
    }
    header('Location: /');
    exit;
}

$id_member = (int) $_GET['id'];
$stmt =  $conn->prepare("SELECT * FROM member WHERE id =:id");
$stmt->execute(array(":id" => $id_member));
$member = $stmt->fetch(PDO::FETCH_ASSOC);
$title = 'Verify email';
$verify_e = 'Mã số được gửi đến: <font color=red>' . _sql01($member['email']) . '</font>';
$new_pe = 'Nhập lại mật khẩu:';
if (isset($_GET['new_pass'])) {
    $title = 'New password';
    $new_arr = explode('|', base64_de($_GET['new_pass']));
    $id_member = (int) $new_arr[0];
    $code_reset = _sql00($new_arr[1]);
    $stmt =  $conn->prepare("SELECT * FROM member WHERE id =:id");
    $stmt->execute(array(":id" => $id_member));
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($member['code_xac_minh'] != $code_reset || strlen($code_reset) < 20) {
        header('Location: /');
        exit;
    }
    if (isset($_POST['pass_1'])) {
        if ($_POST['pass'] != $_POST['pass_1']) {
            $new_pe = '<font color=red>Mật khẩu không khớp:</font>';
        } else {
            $new_pas1 = md5(md5($_POST['pass']));
            $sql = "UPDATE member SET pass='$new_pas1', code_xac_minh='' WHERE id='" . $id_member . "'";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            header('Location: /');
            exit;
        }
    }
} else {
    $id_member = (int) $_GET['id'];
    $stmt =  $conn->prepare("SELECT * FROM member WHERE id =:id");
    $stmt->execute(array(":id" => $id_member));
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($member['time_login'] > 1000000 || $id_member < 1 || $member['time_login'] == 0) {
        header('Location: /');
        exit;
    }
}

if (isset($_POST['code'])) 
{
    if ($_POST['code'] == (int)$member['time_login']) {
        $sql = "UPDATE member SET time_login='$time_php', pass_error='0' WHERE id='" . $id_member . "'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        setcookie('id_member', $member['id'], $time_php + 10 * 60 * 60 * 24);
        setcookie('id_pass', $member['pass'], $time_php + 10 * 60 * 60 * 24);
        $code_1 = substr($member['pass'], 0, 10);
        $code_2 = substr($member['pass'], 10, 20);
        $token_v2 = md5(md5($code_1)) . '' . md5(md5(md5($code_2)));
        setcookie('token', $token_v2, $time_php + 10 * 60 * 60 * 24);
        $_SESSION['id_member'] = $member['id'];
        $_SESSION['id_pass'] = $member['pass'];
        header('Location: /');
        exit;
    } else {
        $verify_e = 'Mã số: <font color=red>' . _sql00($_POST['code']) . '</font> không đúng';
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $title ?> - <?= $_SERVER['HTTP_HOST'] ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="css/csslogin/images/icons/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="css/csslogin/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <script src="css/csslogin/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="css/csslogin/vendor/bootstrap/js/popper.js"></script>
    <script src="css/csslogin/vendor/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/lienhe.css">
</head>

<body>
    <div class="limiter">
        <div class="container-fluid">
            <div class="container-login100">
                <div class="wrap-login100">
                    <div id="contacts-new" class="container minh220 image">
                        <div class="white-card rel">
                            <div id="main_" class="row">
                                <div id="contacts" class="col-sm-4">
                                    <div class="left-side side c1 image ">
                                        <h2 class="fs38r mt0 mb40r">Contacts</h2>
                                        <div class="icon-box d-flex pb30">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <div class="text-block">
                                                <a class="db pb10 a8" href="mailto:support@danplay.net">support@danplay.net</a>
                                                <a class="db pb10 a8" target="_blank" href="https://t.me/danplaynet">Telegram: DANPLAY.NET</a>
                                                <a class="db pb10 a8 " href="">Phone: 0935.456.456</a>
                                            </div>
                                        </div>
                                        <div class="icon-box d-flex pb30">
                                            <i class="fa fa-map" aria-hidden="true"></i>
                                            <div class="text-block">
                                                <p class="c1 lh15">
                                                    73 V.I. Lê Nin,<br>
                                                    phường Hà Huy Tập <br>
                                                    Thành phố Vinh, Nghệ An
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if (isset($_GET['new_pass'])) 
                                {
                                    $title = 'New password';
                                    echo '<div id="message" class="col-sm-8">
                                            <div class="right-side side">
                                                <h2 class="fs38r mt0 mb40r" style="text-align: center; color: #0083b4">Nhập mật khẩu mới</h2>
                                                <form id="contact_form" class="login100-form validate-form" method="post" action="'._sql($_SERVER['REQUEST_URI']).'">                                                    
                                                    <div class="wrap-input100 validate-input">
                                                        <label style="text-align: left;">Mật khẩu:</label>
                                                        <input class="input100" type="password" name="pass" value="" required>
                                                    </div>
                                                    <div class="wrap-input100 validate-input">
                                                        <label style="text-align: left;">'.$new_pe.'</label>
                                                        <input class="input100" type="password" name="pass_1" value="" required>
                                                    </div>
                                                    <div class="container-login100-form-btn">
                                                        <button class="login100-form-btn button--primary button button--icon button--icon--login">
                                                            Xác nhận
                                                        </button>
                                                    </div>
                                                    <div class="text-center p-t-12">
                                                        <a class="txt2" href="login">Đăng nhập</a>
                                                        <i class="fa fa-long-arrow-right m-l-5" style="color: #0093ce;" aria-hidden="true"></i>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>';                                                                
                                } 
                                else {
                                    echo '<div id="message" class="col-sm-8">
                                            <div class="right-side side">
                                                <h2 class="fs38r mt0 mb40r" style="text-align: center; color: #0083b4">Xác nhận Email</h2>
                                                <form id="contact_form" class="login100-form validate-form" method="post" action="'._sql($_SERVER['REQUEST_URI']).'">                                                    
                                                    <div class="wrap-input100 validate-input">
                                                        <label style="text-align: left;">'.$verify_e.'</label>
                                                        <input class="input100" type="text" name="code" value="" placeholder="Code" required>
                                                    </div>
                                                    <div class="container-login100-form-btn">
                                                        <button class="login100-form-btn button--primary button button--icon button--icon--login">
                                                            Xác minh
                                                        </button>
                                                    </div>
                                                    <div class="text-center p-t-12">
                                                        <a class="txt2" href="login">Đăng nhập</a>
                                                        <i class="fa fa-long-arrow-right m-l-5" style="color: #0093ce;" aria-hidden="true"></i>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>';
                                }                                   
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    var onresize = function() {
                        var widthclinet = document.body.clientWidth;
                        var contacts = '<div id="contacts" class="col-sm-4">' + document.getElementById("contacts").innerHTML + '</div>';
                        var message = ' <div id="message" class="col-sm-8">' + document.getElementById("message").innerHTML + '</div>';
                        if (widthclinet < 770) {
                            $("#contacts").remove();
                            $("#main_").append(contacts);
                        } else {
                            $("#message").remove();
                            $("#main_").append(message);
                        }
                    }
                    window.addEventListener("resize", onresize);
                    onresize();
                </script>