<?php
$title = 'API - KEY của bạn';
if (isset($_POST['xac_nhan_key'])) {
    $key_string = random_string(25);
    $sql = "UPDATE member SET key_string='$key_string' WHERE id='" . $id_member . "'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    header('Location: ' . _sql($_SERVER['REQUEST_URI']));
    exit;
}
require 'site/widget/header.php';
?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header bg_xanh_duong_nhat">
                        <h5 class="card-title fs_18">Key sữ dụng auto của bạn</h5>
                    </div>
                    <div class="card-body">
                        <form method="post" action="<?= _sql($_SERVER['REQUEST_URI']) ?>">

                            <div class="form-group">
                                <input name="new_key" value="<?= base64_en($id_member . '|' . $member['key_string']) ?>" required="required" class="form-control"><br>
                                <div class="text-xs-right">
                                    <button type="submit" name="xac_nhan_key" class="btn btn-block btn-flat margin-top-10 btn-info">Cấp Key mới</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>