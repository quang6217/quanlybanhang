<?php
function time_chat_bot($time)
{
    $gio = date('H', $time);
    $reply = "Cảm ơn {khach} {ten_khach} đã ủng hộ {shop}, shop giờ đang có quá nhiều khách nhắn tin nên quá tải, em sẽ gọi tư vấn thêm cho {khach} sau 5 - 10p nữa ạ!";
    if ($gio >= 22) {
        $reply = "Cảm ơn {khach} {ten_khach} đã ủng hộ {shop}, giờ nhân viên của {shop} đã nghỉ làm hết, nên em sẽ gọi tư vấn thêm cho {khach} {ten_khach} vào sáng mai khoảng 8-9h nhé.\nChúc {khach} ngủ ngon!";
    }
    if ($gio <= 4) {
        $reply = "Cảm ơn {khach} {ten_khach} đã ủng hộ {shop}, bây giờ cả nhà đang say giấc ngủ ko tiện gọi tư vấn, nên em sẽ gọi tư vấn thêm cho {khach} {ten_khach} vào khoảng 8-9h sáng nhé!";
    }
    if ($gio == 5) {
        $reply = "Cảm ơn {khach} {ten_khach} đã ủng hộ {shop}, em vừa ngủ dậy còn chưa tiện gọi điện tư vấn, nên em sẽ gọi tư vấn thêm cho {khach} {ten_khach} vào khoảng 8-9h sáng nhé!\nChúc {khach} buổi sáng vui khỏe!";
    }
    if ($gio == 6 or $gio == 7) {
        $reply = "Cảm ơn {khach} {ten_khach} đã ủng hộ {shop}, em đang cho các cháu ăn sáng và đi học, nên em sẽ gọi tư vấn thêm cho {khach} vào khoảng 8-9h nhé!\nChúc {khach} buổi sáng vui khỏe!";
    }
    if ($gio == 11 or $gio == 12 or $gio == 13) {
        $reply = "Cảm ơn {khach} {ten_khach} đã ủng hộ {shop}, nhân viên của {shop} đang giờ cơm trưa, nên em sẽ gọi tư vấn thêm cho {khach} vào khoảng 2h chiều nhé!\nChúc {khach} buổi trưa vui khỏe!";
    }
    if ($gio == 17 or $gio == 18 or $gio == 19) {
        $reply = "Cảm ơn {khach} {ten_khach} đã ủng hộ {shop}, nhân viên của {shop} đang giờ cơm tối, nên em sẽ gọi tư vấn thêm cho {khach} vào khoảng 30 phút nữa nhé!\nChúc {khach} buổi tối vui khỏe!";
    }
    if ($time == 0) {
        $reply = "Cảm ơn {khach} {ten_khach} đã ủng hộ {shop}, shop giờ đang quá tải tin nhắn, em sẽ gọi tư vấn thêm cho {khach} sau 5 - 10p nữa ạ!";
    }
    return $reply;
}

function tinh_tien_don_vi($chuoi, $gia_ban, $don_vi, $phi_ship = 30000, $free_ship = 3)
{
    $chuoi = _sql01(strtolower(convert_vi_to_en($chuoi)));
    $gia_ban = (int)$gia_ban;
    $phi_ship = (int)$phi_ship;
    $free_ship = (int)$free_ship;
    $don_vi_1 = _sql01(strtolower(convert_vi_to_en($don_vi)));
    $so_dv = 0;
    for ($i = 1; $i < 101; $i++) {
        $dv_1 = $i . " " . $don_vi_1 . "|" . $i . "" . $don_vi_1;
        $key_a = explode('|', $dv_1);
        for ($ka = 0; $ka < count($key_a); $ka++) {
            $ndka = trim($key_a[$ka]);
            if ($ndka != "") {
                if (check_string($ndka, $chuoi) > 0) {
                    $so_dv = $i;
                }
            }
        }
    }
    if ($so_dv >= $free_ship) {
        $phi_ship = 0;
    }
    if ($so_dv == 1) {
        $phi_ship = $phi_ship + 5000;
    }
    if ($so_dv > 9) {
        $gia_ban = $gia_ban - 10000;
    }
    if ($so_dv > 19) {
        $gia_ban = $gia_ban - 5000;
    }
    if ($so_dv > 0) {
        $thanh_tien = $so_dv * $gia_ban + $phi_ship;
        $return = $so_dv . ' ' . $don_vi . ' x ' . number_format($gia_ban) . 'đ + ' . number_format($phi_ship) . 'đ ship = ' . number_format($thanh_tien) . 'đ';
        if ($phi_ship == 0) {
            $return = $so_dv . ' ' . $don_vi . ' x ' . number_format($gia_ban) . 'đ = ' . number_format($thanh_tien) . 'đ (Miễn phí ship)';
        }
    } else {
        $return = '0';
    }
    return $return;
}

function luu_noi_dung_ib_fanpage($id_member, $id_team, $id_fan, $id_user, $noi_dung_ne, $time_php, $da_xem = 0)
{
    global $conn;
    $conten_list_id_video = nl2br(trim($noi_dung_ne));
    $array_bank = explode('<br />', $conten_list_id_video);
    $count_a = count($array_bank) + 1;
    $da_xem = (int)$da_xem;
    for ($i = -1; $i <= $count_a; $i++) {
        try {
            $stt = $count_a - $i;
            $stt = $i;
            $a1 = explode('*', $array_bank[$stt]);
            if ($a1[1] != '' and $a1[3] != '') {
                $timestamp = (int)$a1[2];
                $admin_fan = (int)$a1[0];
                $noi_dung = _sql($a1[3]);
                // ---------> Begin: Tìm UID log <---------
                $stmt1 =  $conn->prepare("SELECT * FROM vn_log_chat_id_fanpage WHERE team=$id_team AND uid_fanpage=$id_fan AND uid_member=$id_user AND mem=$id_member");
                $stmt1->execute();
                $id_up1_a = $stmt1->fetch(PDO::FETCH_ASSOC);
                $uid_log = (int) $id_up1_a['id'];
                // ---------> End: Tìm UID log <---------

                $uid_member_first_name = '';
                $uid_member_full_name = '';
                $uid_member_gender = 0;
                if ($uid_log > 0) {
                    if ($id_up1_a['uid_member_gender'] != 0) {
                        $sql = "UPDATE vn_log_chat_id_fanpage SET time_update='$time_php', `timestamp`='$timestamp', da_xem='$da_xem', noi_dung='$noi_dung' WHERE id='" . $id_up1_a['id'] . "'";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute();
                    } else {
                        $token_eaag = i_eaag();
                        $json_string_user = get_data('https://graph.facebook.com/v4.0/' . $id_user . '?fields=first_name%2Cname%2Cgender&access_token=' . $token_eaag);
                        $json =  json_decode($json_string_user, true);
                        $uid_member_first_name = $json['first_name'];
                        $uid_member_full_name = $json['name'];
                        if ($json['gender'] == 'female') {
                            $uid_member_gender = 4;
                        }
                        if ($json['gender'] == 'male') {
                            $uid_member_gender = 1;
                        }
                        $sql = "UPDATE vn_log_chat_id_fanpage SET time_update='$time_php', `timestamp`='$timestamp', da_xem='$da_xem', uid_member_gender=$uid_member_gender, uid_member_full_name='$uid_member_full_name', uid_member_first_name='$uid_member_first_name', noi_dung='$noi_dung' WHERE id='" . $id_up1_a['id'] . "'";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute();
                    }
                } else {
                    $token_eaag = i_eaag();
                    $json_string_user = get_data('https://graph.facebook.com/v4.0/' . $id_user . '?fields=first_name%2Cname%2Cgender&access_token=' . $token_eaag);
                    $json =  json_decode($json_string_user, true);
                    $uid_member_first_name = $json['first_name'];
                    $uid_member_full_name = $json['name'];
                    if ($json['gender'] == 'female') {
                        $uid_member_gender = 4;
                    }
                    if ($json['gender'] == 'male') {
                        $uid_member_gender = 1;
                    }
                    $sql = "INSERT INTO `vn_log_chat_id_fanpage`(`mem`, `team`, `uid_fanpage`, `uid_member`, `uid_member_first_name`, `uid_member_full_name`, `uid_member_gender`, `noi_dung`, `da_xem`, `sl_ib`, `time_update`,`timestamp`) VALUES  ('$id_member','$id_team','$id_fan','$id_user','$uid_member_first_name','$uid_member_full_name','$uid_member_gender','$noi_dung','$da_xem','0','$time_php','$timestamp')";
                    $conn->exec($sql);

                    //Tiếp tục tìm kiếm UID log
                    $stmt1 =  $conn->prepare("SELECT * FROM vn_log_chat_id_fanpage WHERE uid_fanpage=$id_fan AND uid_member=$id_user AND mem=$id_member");
                    $stmt1->execute();
                    $id_up1_a = $stmt1->fetch(PDO::FETCH_ASSOC);
                    $uid_log = (int) $id_up1_a['id'];
                }

                $stmt =  $conn->prepare("SELECT `id` FROM vn_log_chat_fanpage WHERE `uid_log`='$uid_log' AND `timestamp`='$timestamp'");
                $stmt->execute();
                $id_check_a = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($id_check_a['id'] < 1) {
                    $sql = "INSERT INTO `vn_log_chat_fanpage`(`uid_log`, `timestamp`, `admin_fan`, `noi_dung`, `update`, `time_add`) VALUES ('$uid_log','$timestamp','$admin_fan','$noi_dung','$stt','$time_php')";
                    $conn->exec($sql);
                } else {
                    $sql = "UPDATE vn_log_chat_fanpage SET `noi_dung`='$noi_dung' WHERE id='" . $id_check_a['id'] . "'";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                }
            }
        } catch (Exception $e) {
        }
    }
    exit;
}

function _size_string_num($t)
{
    $t = strtolower(convert_vi_to_en($t));
    $kq_chuoi = 0;
    $key_kg_cm = "kg|cm|cao|nang|1m|1.|can|ky|met|ki|ok|Ok|0k|1k|2k|3k|4k|5k|6k|7k|8k|9k|0 k|1 k|2 k|3 k|4 k|5 k|6 k|7 k|8 k|9 k";
    $key_a = explode('|', $key_kg_cm);
    for ($ka = 0; $ka < count($key_a); $ka++) {
        $ndka = trim($key_a[$ka]);
        if ($ndka != "") {
            if (check_string($ndka, $t) > 0) {
                $kq_chuoi++;
            }
        }
    }
    $pattern = '/^[0-9 |.,\/:;-]+$/';
    if (preg_match($pattern, $t, $matches)) {
        if (_sql_num($t) < 100000) {
            $kq_chuoi++;
        }
    }

    if ($kq_chuoi == 0) {
        return 'null';
    }

    for ($a2 = 3; $a2 < 10; $a2++) {
        $t = str_replace($a2 . "o", $a2 . "0", $t);
        $t = str_replace($a2 . "chuc", $a2 . "0", $t);
        $t = str_replace($a2 . "ch", $a2 . "0", $t);
    }
    $t = str_replace(array("met", " m ", "m ", "m", ","), "", $t);
    $t_1 = str_replace(" ", "", $t);
    $t = str_replace(array(" ", "|", ".", ",", "/", ":", ";", "-"), "|", $t);
    $t = preg_replace('/[^0-9 |]/', '', $t);
    for ($ia = 0; $ia < 100; $ia++) {
        $t = str_replace(array("||"), "|", $t);
    }
    $a1 = explode("|", trim($t));
    $chuoi_num = "";
    for ($i1 = 0; $i1 < count($a1); $i1++) {
        if (($a1[$i1] > 10 and $a1[$i1] < 20) or ($a1[$i1] > 30)) {
            if ($chuoi_num == "") {
                $chuoi_num = $a1[$i1];
            } else {
                $chuoi_num = $chuoi_num . '|' . $a1[$i1];
            }
        }
    }
    $a2 = explode('|', $chuoi_num);
    $cao = $a2[0];
    $nang = $a2[1];
    if (check_string($cao . 'k', $t_1) > 0) {
        $cao = $a2[1];
        $nang = $a2[0];
    }
    if (check_string($cao . 'can', $t_1) > 0) {
        $cao = $a2[1];
        $nang = $a2[0];
    }
    if ($cao < 130) {
        for ($a3 = 0; $a3 < 10; $a3++) {
            if ($a2[$a3] > 130 and $a2[$a3] < 190) {
                $cao = $a2[$a3];
                break;
            }
        }
    }
    if ($nang < 1) {
        $nang = $cao;
        $cao = 0;
    }
    if ($cao < 20 and $cao > 0) {
        $cao = $cao * 10;
    }
    if ($cao < 100 and $cao > 0) {
        $cao = $cao + 100;
    }
    $cao = $cao > 0 ? $cao : 0;
    $nang = $nang > 0 ? $nang : 0;
    if ($cao < 130 and $cao > 190) {
        $cao = 0;
    }
    if ($nang < 35 and $cao > 99) {
        $nang = 0;
    }
    if ($cao == 0 and $nang == 0) {
        return;
        exit;
    }
    return $cao . '|' . $nang;
}
function _size_nu($cao, $nang)
{
    $size_n = "0";
    if ($nang >= 35 and $nang < 43) {
        $size_n = "S";
    }
    if ($nang >= 43 and $nang < 46) {
        $size_n = "M";
    }
    if ($nang >= 46 and $nang < 53) {
        $size_n = "L";
    }
    if ($nang >= 53 and $nang < 57) {
        $size_n = "XL";
    }
    if ($nang >= 57 and $nang < 66) {
        $size_n = "XXL";
    }
    if ($nang >= 66 and $nang <= 80) {
        $size_n = "XXXL";
    }
    if ($nang > 80 and $nang < 150) {
        $size_n = "not";
    }
    if ($nang > 150) {
        $size_n = "error";
    }
    if ($nang > 53 and $nang < 57 and $cao < 150) {
        $size_n = "XXL";
    }
    if ($nang > 57 and $nang < 66 and $cao < 150) {
        $size_n = "XXXL";
    }
    return $size_n;
}

function hoi_bot_tra_loi($ma_hang, $cau_hoi_bot, $uid_facebook = null, $uid_fanpage = null, $time = 0)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM vn_san_pham WHERE id =:id");
    $stmt->execute(array(":id" => $ma_hang));
    $ma_sp = $stmt->fetch(PDO::FETCH_ASSOC);
    // ---> Begin: Khởi đầu các biến cơ bản
    $name_sp = $ma_sp['name'];
    $gia_ban = number_format($ma_sp['gia_ban'], 0) . 'đ';
    $don_vi = $ma_sp['don_vi'];
    $phi_ship = number_format($ma_sp['phi_ship'], 0) . 'đ';
    $quang_cao_sp = $ma_sp['quang_cao_sp'];
    $hinh_anh = $ma_sp['hinh_anh'];
    $sl_mien_ship = $ma_sp['sl_mien_ship'];
    $video = $ma_sp['video'];
    $xin_sdt = "{khach} cho em xin sđt để em lên tạo đơn hàng luôn ạ!";
    $chat_lieu_sp = $ma_sp['chat_lieu_sp'];
    $shop = $ma_sp['shop'];
    $shop_name = $ma_sp['shop_name'];
    // ---> End: Khởi đầu các biến cơ bản
    $tin_nhan_kh = _sql01(strtolower(convert_vi_to_en($cau_hoi_bot)));
    $bot_rep = "";
    $stmt1 =  $conn->prepare("SELECT * FROM vn_chat_bot ORDER BY id DESC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $vn_chat_bot) {
        $key_a = explode('|', $vn_chat_bot['key_a']);
        for ($ka = 0; $ka < count($key_a); $ka++) {
            $ndka = trim($key_a[$ka]);
            if ($ndka != "") {
                if (check_string($ndka, $tin_nhan_kh) > 0) {
                    $bot_rep = $vn_chat_bot['key_full'];
                    break;
                }
            }
            if ($bot_rep != "") {
                break;
            }
        }
        if ($bot_rep != "") {
            break;
        }
    }
    $sdt_khach = _check_sdt($cau_hoi_bot);
    //---> Begin: tính tiền theo số đơn vị khách đặt hàng
    $tinh_tien_ad = tinh_tien_don_vi($cau_hoi_bot, $ma_sp['gia_ban'], $don_vi, $ma_sp['phi_ship'], $sl_mien_ship);
    //---> End: tính tiền theo số đơn vị khách đặt hàng

    //---> Begin: Check xem có thông tin chiều cao - cân nặng của khách không
    $cm_kg_ssja = _size_string_num($cau_hoi_bot);
    $bot_cm_kg_sdjs = "";
    if ($cm_kg_ssja != 'null') {
        $a1sds = explode('|', $cm_kg_ssja);
        $size_sdje = _size_nu($a1sds[0], $a1sds[1]);
        $bot_cm_kg_sdjs = 'Cao ' . $a1sds[0] . 'cm, nặng ' . $a1sds[1] . 'kg mặc size ' . $size_sdje . ' phù hợp nhất đó {khach}';
        if ($a1sds[0] < 130) {
            $bot_cm_kg_sdjs = 'Nặng ' . $a1sds[1] . 'kg mặc size ' . $size_sdje . ' phù hợp nhất đó {khach}';
        }
        if ($size_sdje == "not") {
            $bot_cm_kg_sdjs = 'Hiện {shop} không có Size phù hợp với {khach}, {khach} thông cảm giúp {shop} nhé!';
        }
        if ($size_sdje == "error") {
            $bot_cm_kg_sdjs = "{shop} không hiểu, {khach} cho {shop} xin lại cân nặng và chiều cao chính xác với ạ.\nVí dụ: Cao 158cm, nặng 55kg!";
        }
        if ($a1sds[0] == 0 and $a1sds[1] == 0) {
            $bot_cm_kg_sdjs = "";
            // $bot_cm_kg_sdjs = '{khach} {ten_khach} cho {shop} xin chiều cao, cân nặng để {shop} chọn Size chuẩn cho {khach} ạ!';
        }
    }
    //--->End: Check xem có thông tin chiều cao - cân nặng của khách không

    if ($bot_rep != "" or $sdt_khach > 100000 or $bot_cm_kg_sdjs != "" or $tinh_tien_ad != '0') {
        $bot_rep = str_replace('{name_sp}', $name_sp, $bot_rep);
        $bot_rep = str_replace('{gia_ban}', $gia_ban, $bot_rep);
        $bot_rep = str_replace('{don_vi}', $don_vi, $bot_rep);
        $bot_rep = str_replace('{phi_ship}', $phi_ship, $bot_rep);
        $bot_rep = str_replace('{quang_cao_sp}', $quang_cao_sp, $bot_rep);
        $ar_size = explode('|', $ma_sp['size']);
        $size_sl = count($ar_size);
        $bot_rep = str_replace('{size_sl}', $size_sl, $bot_rep);
        $chi_tiet_size = "";
        $size_chuan_chung = "";
        $dia_chi_shop = $ma_sp['dia_chi_shop'];
        $sdt_shop = $ma_sp['sdt_shop'];
        for ($i = 0; $i < $size_sl; $i++) {
            $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id AND sex =:sex");
            $stmt->execute(array(":id" => (int) $ar_size[$i], ":sex" => (int) $ma_sp['sex']));
            $mau = $stmt->fetch(PDO::FETCH_ASSOC);
            $ngan_cach = "\n";
            if ($chi_tiet_size == "") {
                $chi_tiet_size = $mau['name_1'];
            } else {
                $chi_tiet_size = $chi_tiet_size . ' - ' . $mau['name_1'];
            }
            $size_chuan_chung = $size_chuan_chung . $ngan_cach . $mau['name'] . ' phù hợp Cao: ' . $mau['chieu_cao'] . ' - Nặng: ' . $mau['can_nang'] . ' - Eo: ' . $mau['vong_eo'] . ' - Ngực: ' . $mau['vong_nguc'] . '';
        }
        $bot_rep = str_replace('{chi_tiet_size}', $chi_tiet_size, $bot_rep);
        $bot_rep = str_replace("{size_chuan_chung}", $size_chuan_chung . "\n", $bot_rep);
        $ar_mau = explode('|', $ma_sp['mau_sac']);
        $chi_tiet_ms = "";
        for ($i = 0; $i < count($ar_mau); $i++) {
            $stmt =  $conn->prepare("SELECT * FROM vn_mau_sac WHERE id =:id");
            $stmt->execute(array(":id" => (int) $ar_mau[$i]));
            $mau = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($chi_tiet_ms == "") {
                $chi_tiet_ms = trim($mau['name']);
            } else {
                $chi_tiet_ms = trim($chi_tiet_ms . ' - ' . $mau['name']);
            }
        }
        $chat_lieu_sp = $ma_sp['chat_lieu_sp'];
        $shop = $ma_sp['shop'];
        $shop_name = $ma_sp['shop_name'];
        $ten_nhan_vien = 'Lan';
        $bot_tu_van = 'Về giá bán, chất liệu, khuyến mãi, giao hàng, màu sắc... {khach} quan tâm đến cái nào để em tư vấn nhé!';
        $bot_rep = str_replace("{chi_tiet_ms}", $chi_tiet_ms, $bot_rep);
        $bot_rep = str_replace("{dia_chi_shop}", $dia_chi_shop . " ", $bot_rep);
        $bot_rep = str_replace("{sdt_shop}", $sdt_shop, $bot_rep);
        $bot_rep = str_replace("{chat_lieu_sp}", $chat_lieu_sp, $bot_rep);
        $bot_rep = str_replace("{shop_name}", $shop_name, $bot_rep);
        $a_khach = explode('|', $ma_sp['khach']);
        if ($uid_facebook > 1000000 and $uid_fanpage > 100000) {
            $stmt =  $conn->prepare("SELECT * FROM vn_log_chat_id_fanpage WHERE `uid_member`='$uid_facebook' AND `uid_fanpage`='$uid_fanpage'");
            $stmt->execute();
            $id_up1_a = $stmt->fetch(PDO::FETCH_ASSOC);

            // --------> Begin: Nếu phát hiện hội thoại có chứa SDT thì đưa số đó vào SQL            
            if ($sdt_khach > 100000) {
                $sql = "UPDATE vn_log_chat_id_fanpage SET sdt='$sdt_khach' WHERE id='" . $id_up1_a['id'] . "'";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }
            // --------> End: Nếu phát hiện hội thoại có chứa SDT thì đưa số đó vào SQL

            //---> Begin: Update chiều chao cận nặng nếu tìm thấy chúng
            if ($bot_cm_kg_sdjs != "") {
                $chieu_cao_sja = (int)$a1sds[0];
                $can_nang_shas = (int)$a1sds[1];
                if ($chieu_cao_sja > 130 and $can_nang_shas > 30) {
                    $sql = "UPDATE vn_log_chat_id_fanpage SET cao=$chieu_cao_sja, nang=$can_nang_shas WHERE id='" . $id_up1_a['id'] . "'";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                }
                if ($chieu_cao_sja > 130 and $can_nang_shas < 30) {
                    $sql = "UPDATE vn_log_chat_id_fanpage SET cao=$chieu_cao_sja WHERE id='" . $id_up1_a['id'] . "'";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                }
                if ($chieu_cao_sja < 130 and $can_nang_shas > 30) {
                    $sql = "UPDATE vn_log_chat_id_fanpage SET nang=$can_nang_shas WHERE id='" . $id_up1_a['id'] . "'";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                }
            }
            //---> End: Update chiều chao cận nặng nếu tìm thấy chúng

            $khach = $id_up1_a['uid_member_gender'] == 1 ? $a_khach[1] : $a_khach[2];
            $bot_rep = str_replace(array(" " . $vn_chat_bot['question'] . ",", " " . $vn_chat_bot['question']), "", $bot_rep);

            //Xin chiều cao cân nặng nếu chưa có dữ liệu
            if ($id_up1_a['sl_ib'] > 0 and $id_up1_a['cao'] == 0 and $id_up1_a['nang'] == 0 and $sdt_khach < 1000) {
                $bot_tu_van = '{khach} {ten_khach} cho {shop} xin chiều cao - cân nặng để shop chọn size giúp mình?';
            }

            //Xin chiều số điện thoại khi biết chiều cao, cân nặng
            if ($id_up1_a['sl_ib'] > 1 and ($id_up1_a['cao'] != 0 or $id_up1_a['nang'] != 0)) {
                $bot_tu_van = '{khach} {ten_khach} cho em xin sđt để em tạo đơn và tư vấn kỷ hơn nhé!';
            }

            //Thay đổi lời chào mặc định khi gặp khách quen tạo độ thân thiện
            if ($vn_chat_bot['id'] == 14 and $id_up1_a['sl_ib'] > 2) {
                $bot_rep = "Em {ten_nhan_vien} ở đây!\n{khach} {ten_khach} cần em tư vấn gì đây nào?";
            }
            //BOT không chủ động đặt câu hỏi khi đã có số đt của khách
            if ($id_up1_a['sdt'] > 10000 or $sdt_khach > 1000) {
                $xin_sdt = "{shop} có lưu sđt 0" . $id_up1_a['sdt'] . " của {khach}.\n" . time_chat_bot($time);
                $bot_tu_van = '';
            }
            //Khách gửi số điện thoại, trả lời lịch sự
            if ($sdt_khach > 100000) {
                $bot_rep = time_chat_bot($time);
            }
            if ($bot_cm_kg_sdjs != "") {
                if ($id_up1_a['sdt'] > 1000 or $sdt_khach > 10000) {
                    $bot_rep = $bot_cm_kg_sdjs . "\n" . $bot_rep;
                } else {
                    $bot_rep = $bot_cm_kg_sdjs . "\n" . $bot_rep . "\nCho {shop} xin sđt để {shop} lên đơn giúp mình ạ.";
                }
            }

            $bot_rep = str_replace("{bot_tu_van}", $bot_tu_van, $bot_rep);
            $bot_rep = str_replace("{ten_khach}", $id_up1_a['uid_member_first_name'], $bot_rep);
        } else {
            $bot_rep = str_replace(" {ten_khach}", "", $bot_rep);
            $khach = $a_khach[0];
        }
        if ($bot_rep == "" or ($vn_chat_bot['id'] and $tinh_tien_ad != '0') and $sdt_khach < 100000) {
            $bot_rep = $tinh_tien_ad . "\n{bot_tu_van}";
        }
        for ($i_l = 0; $i_l < 1; $i_l++) {
            $bot_rep = str_replace("{bot_tu_van}", $bot_tu_van, $bot_rep);
            $bot_rep = str_replace('{xin_sdt}', $xin_sdt, $bot_rep);
            $bot_rep = str_replace('{name_sp}', $name_sp, $bot_rep);
            $bot_rep = str_replace('{gia_ban}', $gia_ban, $bot_rep);
            $bot_rep = str_replace('{don_vi}', $don_vi, $bot_rep);
            $bot_rep = str_replace('{phi_ship}', $phi_ship, $bot_rep);
            $bot_rep = str_replace('{quang_cao_sp}', $quang_cao_sp, $bot_rep);
            $bot_rep = str_replace("{chi_tiet_ms}", $chi_tiet_ms, $bot_rep);
            $bot_rep = str_replace("{dia_chi_shop}", $dia_chi_shop . " ", $bot_rep);
            $bot_rep = str_replace("{sdt_shop}", $sdt_shop, $bot_rep);
            $bot_rep = str_replace("{chat_lieu_sp}", $chat_lieu_sp, $bot_rep);
            $bot_rep = str_replace("{shop_name}", $shop_name, $bot_rep);
            $bot_rep = str_replace("{shop}", $shop, $bot_rep);
            $bot_rep = str_replace("{khach}", $khach, $bot_rep);
            $bot_rep = str_replace("{ten_nhan_vien}", $ten_nhan_vien, $bot_rep);
            $bot_rep = str_replace("{ten_khach}", $id_up1_a['uid_member_first_name'], $bot_rep);
        }
    } else {
        $bot_rep = "...";
    }
    return $bot_rep;
    exit;
}
