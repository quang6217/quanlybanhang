<?php

if (isset($_POST['name'])) {
    $name = _sql01(strip_tags(addslashes($_POST['name'])));
    $facebook = _sql01(strip_tags(addslashes($_POST['facebook'])));
    $phone = _sql01(strip_tags(addslashes($_POST['phone'])));
    $dia_chi = _sql01(strip_tags(addslashes($_POST['dia_chi'])));
    if (isset($_POST['pass_1'])) {
        $pass_1 = _sql01(strip_tags(addslashes($_POST['pass_1'])));
        $pass_2 = _sql01(strip_tags(addslashes($_POST['pass_2'])));
        $pass_3 = _sql01(strip_tags(addslashes($_POST['pass_3'])));

        if ($pass_2 != $pass_3) {
            echo '<title>Mật khẩu mới không khớp</title>Mật khẩu mới không khớp<br><a href="' . $_SERVER['REQUEST_URI'] . '">Bấm vào đây để làm lại</a>';
            exit;
        }
        if (md5(md5($pass_1)) != $member['pass']) {
            echo '<title>Mật khẩu củ không đúng</title>Mật khẩu củ không đúng<br><a href="' . $_SERVER['REQUEST_URI'] . '">Bấm đây vào để làm lại</a>';
            exit;
        }
        $pass_moi = md5(md5($pass_3));
        $sql = "UPDATE member SET name='$name', facebook='$facebook', phone='$phone', dia_chi='$dia_chi', pass='$pass_moi' WHERE id='" . $id_member . "'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        setcookie('id_member', '', $time_php - 100 * 60 * 60);
        setcookie('id_pass', '', $time_php - 100 * 60 * 60);
        session_destroy();
        header('Location: /login');
        exit;
    } else {
        $sql = "UPDATE member SET name='$name', facebook='$facebook', phone='$phone', dia_chi='$dia_chi' WHERE id='" . $id_member . "'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    }
    header('Location: ' . $_SERVER['REQUEST_URI']);
    exit;
}


$title = 'Đổi mật khẩu';
require 'site/widget/header.php';
?>

<main class="content">
    <div class="container-fluid p-0">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header bg_xanh_duong_nhat">
                    <h5 class="card-title">Thông tin cá nhân</h5>
                    <h6 class="card-subtitle text-muted">Điền đúng thông tin để ADMIN tiện liên hệ khi cần</h6>
                </div>
                <div class="card-body">
                    <form method="post" action="<?= $_SERVER['REQUEST_URI'] ?>">

                        <div class="form-group">
                            <label for="inputAddress">Họ Và Tên</label>
                            <input type="text" class="form-control" name="name" value="<?= $member['name'] ?>" placeholder="ví dụ: Vũ Bảo Dân" required>
                        </div>

                        <div class="form-group">
                            <label for="inputAddress">Email đăng nhập</label>
                            <input type="text" class="form-control" value="<?= $member['email'] ?>" disabled>
                        </div>


                        <div class="form-group">
                            <label for="inputAddress2">URL facebook</label>
                            <input type="text" class="form-control" name="facebook" value="<?= $member['facebook'] ?>" placeholder="ví dụ: https://www.facebook.com/vudan55" required>
                        </div>


                        <div class="form-group">
                            <label for="inputAddress2">Số điện thoại</label>
                            <input type="text" class="form-control" name="phone" value="<?= $member['phone'] ?>" placeholder="ví dụ: 0822.161.999" required>
                        </div>

                        <div class="form-group">
                            <label for="inputAddress2">Địa chỉ</label>
                            <input type="text" class="form-control" name="dia_chi" value="<?= $member['dia_chi'] ?>" placeholder="ví dụ: 19 - Đinh Công Trứ - Sơn Trà - ĐN" required>
                        </div>

                        <div class="form-group">
                            <label class="custom-control custom-checkbox m-0">
                                <input type="checkbox" id="action" class="custom-control-input">
                                <span class="custom-control-label">Đổi mật khẩu</span>
                            </label>
                        </div>
                        <p id="demo"></p>





                        <button type="submit" class="btn btn-info">Submit</button>
                    </form>
                </div>
            </div>
        </div>

        <script id="myScript" src="demo_script_src.js"></script>
        <script language="javascript">
            document.getElementById('action').onclick = function(e) {
                if (this.checked) {
                    var x = document.getElementById("myScript").src;
                    document.getElementById("demo").innerHTML = '<div class="form-row"><div class="form-group col-md-4"><label>Mật khẩu củ</label><input type="password" name="pass_1" class="form-control" required></div><div class="form-group col-md-4"><label>Mật khẩu mới</label><input type="password" name="pass_2" class="form-control" required></div><div class="form-group col-md-4"><label>Nhập lại mật khẩu mới</label><input type="password" name="pass_3" class="form-control" required></div></div>';
                } else {
                    var x = document.getElementById("myScript").src;
                    document.getElementById("demo").innerHTML = '';
                }
            };
        </script>