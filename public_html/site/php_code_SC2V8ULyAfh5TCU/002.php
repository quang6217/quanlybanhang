<?php
if ($_GET['exit'] == 'ok') {
    setcookie('id_member', '', $time_php - 100 * 60 * 60);
    setcookie('id_pass', '', $time_php - 100 * 60 * 60);
    session_destroy();
    header('Location: /login');
    exit;
}

//Khai báo các biến ban đầu
$error_1 = '';
$error_2 = '';
$_SESSION['email'] = isset($_SESSION['email']) ? _sql01($_SESSION['email']) : '';

//Hàm xữ lý khi người dùng nhấn POST
if (isset($_POST['login'])) {
    //Lưu lại giá trị post    
    $email = _sql01(($_POST['login_name']));
    $_SESSION['email'] = $email;
    $pass = _sql01(($_POST['pass']));
    $pass_md5 = md5(md5($pass));
    //Lấy thông tin thành viên
    $stmt =  $conn->prepare("SELECT * FROM member WHERE `email`='$email'");
    $stmt->execute(array());
    $referral = $stmt->fetch(PDO::FETCH_ASSOC);
    $id_member = $referral['id'];

    //Nếu đã có lịch sữ sai pass xấu, chặn lại
    if ($referral['pass_error'] > 2 and $referral['time_login'] > $time_php - 300) {
        setcookie('id_member', '', $time_php - 100 * 60 * 60);
        setcookie('id_pass', '', $time_php - 100 * 60 * 60);
        session_destroy();
        $so_giay =  $referral['time_login'] - ($time_php - 300);
?>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
        <script>
            $(document).ready(function() {
                var interval;
                $("#processbar").val("0");
                $("#count-down").html("<?= $so_giay ?>");
                clearInterval(interval);
                interval = null;
                var count = <?= $so_giay ?>;

                function loadlink() {
                    window.location = "/?get=login_new";
                }
                interval = setInterval(function() {
                    count--;
                    if (count === 0) {
                        count = <?= $so_giay ?>;
                        loadlink()
                    }
                    $("#count-down").html(count);
                    $("#processbar").val(<?= $so_giay ?> - count);
                }, 1000);
            });
        </script>
        <center>
            <title>Bảo mật đăng nhập</title>
            <div class="content"><span class="has-text-grey is-size-7">Xin thử đăng nhập lại sau: <span id="count-down">30s</span> giây</span><br>
                <progress value="0" max="<?= $so_giay ?>" id="processbar"></progress></div>
            Lý do: Bạn nhập sai mật khẩu nhiều lần!
        </center>
<?php
        exit;
    }

    // Thông báo các lỗi khi gặp phải
    $error_1 = $id_member < 1 ? '<small style="float: right; color:red">Email is incorrect</small>' : '';
    $error_2 = $pass_md5 != $referral['pass'] ? '<small style="float: right; color:red">Password is incorrect</small>' : '';
    $error_2 = $error_1 == '' ? $error_2 : '';


    //Nếu đầy đủ các điều kiện cho phép đăng nhập
    if ($id_member > 0 and $pass_md5 == $referral['pass']) {

        // Bắt very email mới cho đăng nhập
        if ($referral['time_login'] < 1000000 and $referral['time_login'] > 1000) {
            header('Location: /very_email&id=' . $referral['id']);
            exit;
        }



        setcookie('id_member', $referral['id'], $time_php + 10 * 60 * 60 * 24);
        setcookie('id_pass', $referral['pass'], $time_php + 10 * 60 * 60 * 24);
        $code_1 = substr($referral['pass'], 0, 10);
        $code_2 = substr($referral['pass'], 10, 20);
        $token_v2 = md5(md5($code_1)) . '' . md5(md5(md5($code_2)));
        setcookie('token', $token_v2, $time_php + 10 * 60 * 60 * 24);
        $_SESSION['id_member'] = $referral['id'];
        $_SESSION['id_pass'] = $referral['pass'];

        //Xóa lịch sữ đăng nhập sai mật khẩu
        $sql = "UPDATE member SET time_login='$time_php', pass_error='0' WHERE id='" . $id_member . "'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $_SESSION['REQUEST_URI'] = isset($_SESSION['REQUEST_URI']) ? $_SESSION['REQUEST_URI'] : '/';
        $_SESSION['REQUEST_URI'] = $_SESSION['REQUEST_URI'] == '/login' ? '/' : $_SESSION['REQUEST_URI'];
        header('Location: ' . $_SESSION['REQUEST_URI']);
        exit;
    } else {
        //Ghi lại lỗi login sai mật khẩu
        if ($id_member > 0) {
            $so_pass_error = $referral['pass_error'] + 1;
            $sql = "UPDATE member SET pass_error='$so_pass_error'  WHERE id='" . $id_member . "'";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
        }
    }
}
$title = 'Đăng nhập';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $title ?> - <?= $_SERVER['HTTP_HOST'] ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="css/csslogin/images/icons/favicon.ico" />
    <link rel="stylesheet" type="text/css" href="css/csslogin/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <script src="css/csslogin/vendor/jquery/jquery-3.2.1.min.js"></script>
    <script src="css/csslogin/vendor/bootstrap/js/popper.js"></script>
    <script src="css/csslogin/vendor/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/lienhe.css">
</head>

<body>
    <div class="limiter">
        <div class="container-fluid">
            <div class="container-login100">
                <div class="wrap-login100">
                    <div id="contacts-new" class="container minh220 image">
                        <div class="white-card rel">
                            <div id="main_" class="row">
                                <div id="contacts" class="col-sm-4">
                                    <div class="left-side side c1 image ">
                                        <h2 class="fs38r mt0 mb40r">Contacts</h2>
                                        <div class="icon-box d-flex pb30">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <div class="text-block">
                                                <a class="db pb10 a8" href="mailto:khachhang@hiryo.vn">khachhang@hiryo.vn</a>
                                                <a class="db pb10 a8" target="_blank" href="https://t.me/danplaynet">Zalo: 0844.156.888</a>
                                                <a class="db pb10 a8 " href="">Phone: 0844.156.888</a>
                                            </div>
                                        </div>
                                        <div class="icon-box d-flex pb30">
                                            <i class="fa fa-map" aria-hidden="true"></i>
                                            <div class="text-block">
                                                <p class="c1 lh15">
                                                    108 Lý Tự Trọng,<br>
                                                    phường Hà Huy Tập <br>
                                                    Thành phố Vinh, Nghệ An
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="message" class="col-sm-8">
                                    <div class="right-side side">
                                        <h2 class="fs38r mt0 mb40r" style="text-align: center; color: #0083b4">Đăng nhập</h2>
                                        <form id="contact_form" class="login100-form validate-form" method="post" action="<?= _sql01($_SERVER['REQUEST_URI']) ?>">
                                            <?= $error_1 ?>
                                            <div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
                                                <label style="text-align: left;">Email:</label>
                                                <input class="input100" type="text" name="login_name" value="<?= _sql01($_SESSION['email']) ?>" placeholder="Email" required>
                                            </div>
                                            <?= $error_2 ?>
                                            <div class="wrap-input100 validate-input" data-validate="Password is required">
                                                <label style="text-align: left;">Mật khẩu:</label>
                                                <input class="input100" type="password" name="pass" placeholder="Mật khẩu" required>
                                            </div>
                                            <div class="container-login100-form-btn" style="padding-bottom: 21px; ">
                                                <button class="login100-form-btn button--primary button button--icon button--icon--login" style="height: 50px;" type="submit" name="login">
                                                    Đăng nhập
                                                </button>
                                            </div>
                                            <div class="text-center p-t-12">
                                                <a href="/forgotten" data-xf-click="overlay">Quên mật khẩu</a> hoặc <a class="txt2" href="/registration"> đăng ký mới</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                    var onresize = function() {
                        var widthclinet = document.body.clientWidth;
                        var contacts = '<div id="contacts" class="col-sm-4">' + document.getElementById("contacts").innerHTML + '</div>';
                        var message = ' <div id="message" class="col-sm-8">' + document.getElementById("message").innerHTML + '</div>';
                        if (widthclinet < 770) {
                            $("#contacts").remove();
                            $("#main_").append(contacts);
                        } else {
                            $("#message").remove();
                            $("#main_").append(message);
                        }
                    }
                    window.addEventListener("resize", onresize);
                    onresize();
                </script>