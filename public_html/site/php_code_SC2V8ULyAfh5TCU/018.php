<?php
$title = "Thêm đơn hàng";
$tao_don_hang = 'Tạo đơn hàng';
$tao_don_hang_ms = 'info';
//----------> Begin: Lựa chọn tỉnh thành
$selected_tinh = '<option value="0">Chọn Tỉnh/Thành phố</option>';
$selected_huyen = '<option>Chọn Huyện/Quận</option>';
$selected_xa = '<option>Chọn Phường/Xã</option>';
$stmt1 =  $conn->prepare("SELECT * FROM vn_tinh ORDER BY id ASC");
$stmt1->execute();
$list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
foreach ($list_code as $show) {
    $selected_tinh = $selected_tinh . '<option value="' . $show['id'] . '">' . $show['name'] . '</option>';
}
//----------> End: Lựa chọn tỉnh thành
$sdt = '';
$nmang_sdt = '';
$nameKH = '';
$linkFB = '';
$address_detail = '';
$ghi_chu = '';
$block_edit = '';
$ct_xa_kh = '';
$in_id_sp = '';
$in_mau_sac = '<option value="0">Chọn màu sắc</option>';
$in_size = '<option value="0">Chọn size</option>';
$html_i_ms = '<select class="custom-select flex-grow-1" id="vn_mau_sac" name="mau_sac" onchange="vn_mau_sac()" required>' . $in_mau_sac . '</select>';
$html_i_sz = '<select class="custom-select flex-grow-1" id="vn_size" name="size" onchange="vn_size()" required>' . $in_size . '</select>';
$in_name_sp = '';
$in_sl = '';
$in_gia_sp = '';
$button_a = 'Xác nhận';
if (isset($_GET['edit_id'])) {
    $block_edit = 'readonly';
    $id_edit = (int) $_GET['edit_id'];
    $stmt =  $conn->prepare("SELECT * FROM vn_don_hang_ship WHERE id =:id");
    $stmt->execute(array(":id" => $id_edit));
    $edit = $stmt->fetch(PDO::FETCH_ASSOC);
    if (isset($_POST['xac_nhan'])) {
        $sdt = (int) $_POST['sdt'];
        $nameKH = _sql($_POST['nameKH']);
        $linkFB = _sql($_POST['linkFB']);
        $ma_hang = (int) $_POST['ma_hang'];
        $nameSP = _sql($_POST['nameSP']);
        $sl_mua = (int) $_POST['sl_mua'];
        $total_money = (int) $_POST['total_money'] * 1000;
        $mau_sac = (int) $_POST['mau_sac'];
        $size = (int) $_POST['size'];
        for ($a = 0; $a < 100; $a++) {
            $ms_i = isset($_POST['mau_sac_' . $a]) ? '|' . (int) $_POST['mau_sac_' . $a] : '';
            $sz_i = isset($_POST['size_' . $a]) ? '|' . (int) $_POST['size_' . $a] : '';
            $mau_sac = $mau_sac . '' . $ms_i;
            $size = $size . '' . $sz_i;
        }
        // echo $mau_sac; exit;
        $xaKH = (int) $_POST['xaKH'];
        $address_detail = _sql($_POST['address_detail']);
        $ghi_chu = _sql($_POST['ghi_chu']);
        if ($xaKH > 0 and $mau_sac > 0 and $sl_mua > 0 and $ma_hang > 0) {
            sql_kho_hang($edit['mau_sac'], $edit['size'], $edit['ma_hang'], $id_team, 4); //Xóa dữ liệu hàng hóa ở kho
            sql_kho_hang($mau_sac, $size, $ma_hang, $id_team); //Đưa giữ liệu vào kho hàng
            $sql = "UPDATE `vn_don_hang_ship` SET `sdt`='$sdt',`nameSP`='$nameSP',`nameKH`='$nameKH',`linkFB`='$linkFB',`ma_hang`='$ma_hang',`sl_mua`='$sl_mua',`total_money`='$total_money',`mau_sac`='$mau_sac',`size`='$size',`xaKH`='$xaKH',`address_detail`='$address_detail',`ghi_chu`='$ghi_chu' WHERE id=$id_edit";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
            setcookie('don_hang', 'off', $time_php + 10 * 60 * 60 * 24);
            header('Location: /don_hang&id=' . $ma_hang);
            exit;
        }
    }

    setcookie('don_hang', 'show', $time_php + 10 * 60 * 60 * 24);
    if ($_COOKIE['don_hang'] != 'show') {
        header('Location: ' . _sql($_SERVER['REQUEST_URI']));
        exit;
    }
    $tao_don_hang_ms = 'secondary';
    $title = "Chỉnh sữa đơn hàng";
    $button_a = 'Update';
    //Các kết quả đã nhập
    $sdt = '0' . _sql00($edit['sdt']);
    $nmang_sdt = check_nha_mang($sdt);
    $nameKH = _sql01($edit['nameKH']);
    $tao_don_hang = 'Chỉnh sữa đơn: #' . $id_edit . ' - KH: ' . $nameKH;
    $linkFB = _sql00($edit['linkFB']);
    $in_id_sp = (int) $edit['ma_hang'];

    // Bảo mật không cho phép sữa chữa đơn hàng của người khác.
    if ($id_level < 4 and $id_member != $edit['mem']) {
        header('Location: /don_hang&id=' . $in_id_sp);
        exit;
    }

    $in_name_sp = _sql($edit['nameSP']);
    $in_sl = (int) $edit['sl_mua'];
    $in_gia_sp = (int) $edit['total_money'] / 1000;
    $address_detail = _sql($edit['address_detail']);
    $ghi_chu = _sql($edit['ghi_chu']);
    $json_xa = json_decode(info_vn_xa((int) $edit['xaKH']), true);
    $ct_xa_kh = $json_xa['full_name'];

    //select
    $stmt =  $conn->prepare("SELECT * FROM vn_san_pham WHERE id =:id");
    $stmt->execute(array(":id" => $in_id_sp));
    $sanp = $stmt->fetch(PDO::FETCH_ASSOC);
    $in_name_sp = _sql01($sanp['name']);
    $in_gia_sp1 = (int) $sanp['gia_ban'] / 1000;
    $in_tien_ship = (int) $sanp['phi_ship'] / 1000;


    // ------------> Lấy dữ liệu màu sắc
    $html_i_ms = '';
    $ar_ms1 = explode('|', $edit['mau_sac']);
    for ($a = 0; $a < count($ar_ms1); $a++) {
        $ar_mau = explode('|', $sanp['mau_sac']);
        $in_mau_sac = '';
        for ($i = 0; $i < count($ar_mau); $i++) {
            $stmt =  $conn->prepare("SELECT * FROM vn_mau_sac WHERE id =:id");
            $stmt->execute(array(":id" => (int) $ar_mau[$i]));
            $mau = $stmt->fetch(PDO::FETCH_ASSOC);
            $selected_a = $mau['id'] == $ar_ms1[$a] ? 'selected' : '';
            $in_mau_sac = $in_mau_sac . '<option value="' . $mau['id'] . '" title="' . $mau['mo_ta'] . ' - ' . $ar_ms1[$a] . '" ' . $selected_a . '>' . $mau['mau'] . '</option>';
        }
        if ($a == 0) {
            $name_ms_s = 'mau_sac';
        } else {
            $a1 = $a - 1;
            $name_ms_s = 'mau_sac_' . $a1;
        }
        $html_i_ms = $html_i_ms . '<select class="custom-select flex-grow-1" id="vn_mau_sac" name="' . $name_ms_s . '" onchange="vn_mau_sac()" required>' . $in_mau_sac . '</select>';
    }
    // ------------> Lấy dữ liệu size

    $html_i_sz = '';
    $ar_ms1 = explode('|', $edit['size']);
    for ($a = 0; $a < count($ar_ms1); $a++) {
        $ar_size = explode('|', $sanp['size']);
        $in_size = '';
        for ($i = 0; $i < count($ar_size); $i++) {
            $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id AND sex =:sex");
            $stmt->execute(array(":id" => (int) $ar_size[$i], ":sex" => (int) $sanp['sex']));
            $mau = $stmt->fetch(PDO::FETCH_ASSOC);
            $selected_a = $mau['id'] == $ar_ms1[$a] ? 'selected' : '';
            $in_size = $in_size . '<option value="' . $mau['id'] . '" title="Cao: ' . $mau['chieu_cao'] . ' - Nặng: ' . $mau['can_nang'] . ' - Eo: ' . $mau['vong_eo'] . ' - Ngực: ' . $mau['vong_nguc'] . '" ' . $selected_a . '>' . $mau['name'] . '</option>';
        }
        $name_ms_s = '';
        if ($a == 0) {
            $name_ms_s = 'size';
        } else {
            $a1 = $a - 1;
            $name_ms_s = 'size_' . $a1;
        }
        $html_i_sz = $html_i_sz . '<select class="custom-select flex-grow-1" id="vn_size" name="' . $name_ms_s . '" onchange="vn_size()" required>' . $in_size . '</select>';
    }
    $ar_size = explode('|', $sanp['size']);
    $in_size = '';
    for ($i = 0; $i < count($ar_size); $i++) {
        $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id AND sex =:sex");
        $stmt->execute(array(":id" => (int) $ar_size[$i], ":sex" => (int) $sanp['sex']));
        $mau = $stmt->fetch(PDO::FETCH_ASSOC);
        $selected_a = $i + 1 == $edit['size'] ? 'selected' : '';
        $in_size = $in_size . '<option value="' . $mau['id'] . '" title="Cao: ' . $mau['chieu_cao'] . ' - Nặng: ' . $mau['can_nang'] . ' - Eo: ' . $mau['vong_eo'] . ' - Ngực: ' . $mau['vong_nguc'] . '" ' . $selected_a . '>' . $mau['name'] . '</option>';
    }
    // ------------> Lấy dữ liệu tỉnh
    $selected_tinh = '';
    $selected_huyen = '<option value="' . $json_xa['id_huyen'] . '">' . $json_xa['name_huyen'] . '</option>';
    $selected_xa = '<option value="' . $json_xa['id_xa'] . '">' . $json_xa['name_xa'] . '</option>';
    $stmt1 =  $conn->prepare("SELECT * FROM vn_tinh ORDER BY id ASC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $show) {
        $selected_ck_t = $show['id'] == $json_xa['id_tinh'] ? 'selected' : '';
        $selected_tinh = $selected_tinh . '<option value="' . $show['id'] . '" ' . $selected_ck_t . '>' . $show['name'] . '</option>';
    }
}
if (isset($_GET['delfp'])) {
    $id_xoa = (int) $_GET['delfp'];
    $ma_hang = (int) $_GET['id'];
    $stmt =  $conn->prepare("SELECT * FROM vn_don_hang_ship WHERE id =:id");
    $stmt->execute(array(":id" => $id_xoa));
    $edit = $stmt->fetch(PDO::FETCH_ASSOC);
    sql_kho_hang($edit['mau_sac'], $edit['size'], $edit['ma_hang'], $id_team, 4); //Xóa dữ liệu hàng hóa ở kho
    $sql = "UPDATE vn_don_hang_ship SET trang_thai=44 WHERE id=$id_xoa AND mem=$id_member";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    setcookie('don_hang', 'off', $time_php + 10 * 60 * 60 * 24);
    if ($ma_hang > 9999) {
        header('Location: /don_hang&id=' . $ma_hang);
    } else {
        header('Location: /don_hang');
    }
    exit;
}

// --->Begin: Ghi nhớ lại số ID để thao tác nhanh hơn
if (isset($_GET['id'])) {
    $block_edit = 'readonly';
    $in_id_sp = (int) $_GET['id'];
    if ($in_id_sp > 999 and $in_id_sp < 10000) {
        $stmt =  $conn->prepare("SELECT * FROM vn_san_pham WHERE id =:id");
        $stmt->execute(array(":id" => $in_id_sp));
        $sanp = $stmt->fetch(PDO::FETCH_ASSOC);
        $in_name_sp = _sql01($sanp['name']);
        $in_sl = 1;
        $in_gia_sp1 = (int) $sanp['gia_ban'] / 1000;
        $in_tien_ship = (int) $sanp['phi_ship'] / 1000;
        $in_gia_sp = (int) $sanp['gia_ban'] / 1000 + $in_tien_ship;
        // ------------> Lấy dữ liệu màu sắc
        $ar_mau = explode('|', $sanp['mau_sac']);
        $in_mau_sac = '';
        for ($i = 0; $i < count($ar_mau); $i++) {
            $stmt =  $conn->prepare("SELECT * FROM vn_mau_sac WHERE id =:id");
            $stmt->execute(array(":id" => (int) $ar_mau[$i]));
            $mau = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($mau['mau'] != "") {
                $in_mau_sac = $in_mau_sac . '<option value="' . $mau['id'] . '" title="' . $mau['mo_ta'] . '">' . $mau['mau'] . '</option>';
            }
        }
        if ($in_mau_sac == "") {
            $in_mau_sac = '<option value="4444">Không có màu sắc để chọn</option>';
        }
        // ------------> Lấy dữ liệu size
        $ar_size = explode('|', $sanp['size']);
        $in_size = '';
        for ($i = 0; $i < count($ar_size); $i++) {
            $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id AND sex =:sex");
            $stmt->execute(array(":id" => (int) $ar_size[$i], ":sex" => (int) $sanp['sex']));
            $mau = $stmt->fetch(PDO::FETCH_ASSOC);
            $in_size = $in_size . '<option value="' . $mau['id'] . '" title="Cao: ' . $mau['chieu_cao'] . ' - Nặng: ' . $mau['can_nang'] . ' - Eo: ' . $mau['vong_eo'] . ' - Ngực: ' . $mau['vong_nguc'] . '">' . $mau['name'] . '</option>';
        }
        $html_i_ms = '<select class="custom-select flex-grow-1" id="vn_mau_sac" name="mau_sac" onchange="vn_mau_sac()" required>' . $in_mau_sac . '</select>';
        $html_i_sz = '<select class="custom-select flex-grow-1" id="vn_size" name="size" onchange="vn_size()" required>' . $in_size . '</select>';
    }
}
// --->End: Ghi nhớ lại số ID để thao tác nhanh hơn

if (isset($_POST['xac_nhan'])) {
    $sdt = (int) $_POST['sdt'];
    $nameKH = _sql($_POST['nameKH']);
    $linkFB = _sql($_POST['linkFB']);
    $ma_hang = (int) $_POST['ma_hang'];
    $nameSP = _sql($_POST['nameSP']);
    $sl_mua = (int) $_POST['sl_mua'];
    $total_money = (int) $_POST['total_money'] * 1000;
    $mau_sac = (int) $_POST['mau_sac'];
    $size = (int) $_POST['size'];
    for ($a = 0; $a < 100; $a++) {
        $ms_i = isset($_POST['mau_sac_' . $a]) ? '|' . (int) $_POST['mau_sac_' . $a] : '';
        $sz_i = isset($_POST['size_' . $a]) ? '|' . (int) $_POST['size_' . $a] : '';
        $mau_sac = $mau_sac . '' . $ms_i;
        $size = $size . '' . $sz_i;
    }
    $xaKH = (int) $_POST['xaKH'];
    $address_detail = _sql($_POST['address_detail']);
    $ghi_chu = _sql($_POST['ghi_chu']);

    $time_giam_sdt =  $time_php - 120 * 60;
    $sqlAll = "SELECT COUNT(`sdt`) FROM `vn_don_hang_ship` WHERE `sdt` = $sdt and `time_add`>$time_giam_sdt AND `trang_thai`!='44' AND `ma_hang`=$ma_hang";
    $stmt = $conn->query($sqlAll);
    $sum_sdt_a = $stmt->fetchColumn();
    // echo 'sdt: '.$sdt.' - trùng: '.$sum_sdt_a; exit;
    if ($xaKH > 0 and $mau_sac > 0 and $sl_mua > 0 and $ma_hang > 0 and $sum_sdt_a < 1) {
        sql_kho_hang($mau_sac, $size, $ma_hang, $id_team);
        $sql = "INSERT INTO `vn_don_hang_ship`(`mem`, `team`, `sdt`, `nameSP`, `nameKH`, `linkFB`, `ma_hang`, `sl_mua`, `total_money`, `mau_sac`, `size`, `xaKH`, `address_detail`, `ghi_chu`, `trang_thai`, `time_add`, `time_num`) VALUES ('$id_member','$id_team','$sdt','$nameSP','$nameKH','$linkFB','$ma_hang','$sl_mua','$total_money','$mau_sac','$size','$xaKH','$address_detail','$ghi_chu','0','$time_php','$time_num')";
        $conn->exec($sql);
        //Lấy thông tin id đơn hàng để check
        $stmt =  $conn->prepare("SELECT * FROM vn_don_hang_ship WHERE sdt =:sdt ORDER BY id DESC limit 1");
        $stmt->execute(array(":sdt" => $sdt));
        $id_dn_sdt = $stmt->fetch(PDO::FETCH_ASSOC);
        $id_dh_k = (int)$id_dn_sdt['id'];
        $sotien_lend = sql_sale($id_team, 'add_dh');
        if ($sotien_lend > 0) {
            lich_su_giao_dich($sotien_lend, 9, $id_member, 4, 'ĐH ' . $id_dh_k, '', 8, $time_php);
        }
        setcookie('don_hang', 'off', $time_php + 10 * 60 * 60 * 24);
        header('Location: /don_hang&id=' . $ma_hang);
        exit;
    }
    $title = "Lổi đơn hàng";
    $tao_don_hang = 'Bạn chưa điền đầy đủ thông tin';
    if ($sum_sdt_a > 0) {
        $tao_don_hang = 'Số: 0' . $sdt . ' đã lên đơn';
    }
    $sdt = '0' . $sdt;
    $tao_don_hang_ms = 'danger';
}
require 'site/widget/header.php'; ?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div id="id_button_td">
                <h3 class="mt-3 ml-3 btn btn-<?= $tao_don_hang_ms ?> mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_TaoDonHang" aria-expanded="false" aria-controls="collapseTwo" onclick="show_hiden_p()">
                        <?= $tao_don_hang ?>
                    </a>
                </h3>
            </div>
            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_TaoDonHang" class="card collapse <?= $_COOKIE['don_hang'] ?>" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-row">
                                        <div class="form-group mb-3  col-md-3">
                                            <input class="form-control h38" type="number" id="id_c_sdta" name="sdt" value="<?= $sdt ?>" placeholder="SĐT khách hàng" onchange="check_sdt()" autocomplete="off" required></input>
                                        </div>
                                        <div class="form-group mb-3  col-md-3">
                                            <input class="form-control h38" type="text" id="id_check_mang_v2" name="check_mang" value="<?= $nmang_sdt ?>" placeholder="Mạng di động" readonly></input>
                                        </div>
                                        <div class="form-group mb-3 col-md-6">
                                            <input class="form-control h38" name="nameKH" id="id_a_namekh" value="<?= $nameKH ?>" placeholder="Tên khách hàng" autocomplete="off" required></input>
                                        </div>
                                        <div class="form-group mb-3  col-md-6">
                                            <input class="form-control h38" type="text" id="id_ma_hang" value="<?= $in_id_sp ?>" name="ma_hang" placeholder="Mã hàng" onkeyup="sc_ma_hang()" required <?= $block_edit ?>></input>
                                        </div>
                                        <div class="form-group mb-3  col-md-6">
                                            <input class="form-control h38" type="text" name="address_detail" id="id_address_detail" value="<?= $address_detail ?>" placeholder="Nhập địa chỉ chi tiết (nhà, ngõ, đường)" autocomplete="off" required></input>
                                        </div>
                                        <div class="input-group mb-3 col-md-12">
                                            <input class="form-control h38" id="goi_y_dia_chi" value="<?= $ct_xa_kh ?>" placeholder="Gõ tên Phường/Xã, Quận/Huyện, Tỉnh để tìm kiếm nhanh chóng" onkeyup="dx_dia_chi()" onchange="dx_dia_chi()" autocomplete="off"></input>
                                        </div>

                                        <div class="form-group mb-3  col-md-12" id="dia_chi_giao_hang">
                                        </div>

                                        <div class="form-group mb-3  col-md-6">
                                            <input class="form-control h38" type="text" id="name_sp" value="<?= $in_name_sp ?>" name="nameSP" placeholder="Tên sản phẩm" required <?= $block_edit ?>></input>
                                        </div>
                                        <div class="form-group mb-3  col-md-2">
                                            <input class="form-control h38" type="number" min="1" max="50" id="sl_mua" value="<?= $in_sl ?>" name="sl_mua" placeholder="Số lượng" onchange="edit_sl1()" onkeyup="edit_sl1()" required></input>
                                        </div>
                                        <div class="form-group mb-3  col-md-2">
                                            <input class="form-control h38" type="number" id="tien_ship_hang" value="<?= $in_tien_ship ?>" name="tien_ship_hang" placeholder="Tiền ship hàng" onchange="tien_ship_ne()" onkeyup="tien_ship_ne()"></input>
                                            <input hidden class="form-control h38" type="number" id="tien_ship_hang_v1" value="<?= $in_tien_ship ?>" name="tien_ship_hang_v1"></input>
                                        </div>
                                        <div class="form-group mb-3  col-md-2">
                                            <input hidden class="form-control h38" id="gia_tien_cd" value="<?= $in_gia_sp1 ?>"></input>
                                            <input class="form-control h38" id="tong_tien" value="<?= $in_gia_sp ?>" name="total_money" type="number" placeholder="Tổng tiền" required></input>
                                        </div>

                                        <div class="form-group mb-3  col-md-6" id="html_mau_sach">
                                            <?= $html_i_ms ?>
                                        </div>
                                        <div class="form-group mb-3  col-md-6" id="html_size_a">
                                            <?= $html_i_sz ?>
                                        </div>
                                        <div class="form-group mb-3  col-md-6">
                                            <select class="custom-select flex-grow-1" id="vn_tinh" onchange="my_tinh()">
                                                <?= $selected_tinh ?>
                                            </select>
                                        </div>
                                        <div class="form-group mb-3  col-md-6">
                                            <select class="custom-select flex-grow-1" id="vn_huyen" onchange="my_huyen()">
                                                <?= $selected_huyen ?>
                                            </select>
                                        </div>
                                        <div class="form-group mb-3  col-md-6">
                                            <select id="vn_xa" onchange="my_xa()" name="xaKH" class="custom-select flex-grow-1" required>
                                                <?= $selected_xa ?>
                                            </select>
                                        </div>
                                        <div class="form-group mb-3  col-md-6">
                                            <input class="form-control h38" name="linkFB" value="<?= $linkFB ?>" placeholder="Link or id Face" autocomplete="off"></input>
                                        </div>

                                        <div class="form-group  mb-3 col-md-12" id="id_edit_ghi_chu">
                                            <textarea class="form-control" rows="4" name="ghi_chu" placeholder="Mô tả đơn hàng (ghi chú)"><?= $ghi_chu ?></textarea>
                                        </div>
                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit" name="xac_nhan" id="button_xn_k" class="btn btn-block btn-flat margin-top-10 btn-info"><?= $button_a ?></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-header alert-info">
                        <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                            Danh sách đơn hàng
                        </div>
                    </div>
                    <div class="">
                        <div class="table-responsive flex-row flex-nowrap">
                            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align:center; width:5%;">#</th>
                                        <th style="text-align:center;">Tên KH</th>
                                        <th style="text-align:center;">SĐT</th>
                                        <th style="text-align:center;">Sản phẩm</th>
                                        <th style="text-align:center;">Tổng tiền (Sl)</th>
                                        <th style="text-align:center;">Ghi chú</th>
                                        <th style="text-align:center;">Ngày thêm</th>
                                        <th style="text-align:center; width:7%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sqlAll = "SELECT COUNT(`mem`) FROM `vn_don_hang_ship` WHERE mem=$id_member AND trang_thai!=44";
                                    $stmt5 = $conn->query($sqlAll);
                                    $total_records  = $stmt5->fetchColumn();
                                    $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                                    $total_page = ceil($total_records / $limit);
                                    $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                                    $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                                    if ($total_page > 0) {
                                        $total_page_max = $total_page - 1;
                                    } else {
                                        $total_page_max = $total_page;
                                    }
                                    $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                                    $start_page = $_GET['page'] * $limit;
                                    $num_1 = 0;

                                    $stmt1 =  $conn->prepare("SELECT * FROM vn_don_hang_ship WHERE mem=$id_member AND trang_thai!=44 ORDER BY id DESC LIMIT $start_page, $limit");
                                    $stmt1->execute(array());
                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                    $num = 0;
                                    foreach ($list_code as $show_fp) {
                                        $arr = (array) $show_fp;
                                        $num_1 = $num_1 + 1;
                                        $num = $num_1 + $_GET['page'] * $limit;
                                        $ngay_hien_tai = date('d/m/Y', $time_php);
                                        $ngay_them = date('d/m/Y', $show_fp['time_add']);
                                        $ngay_show = $ngay_them == $ngay_hien_tai ? date('H:i:s', $show_fp['time_add']) : $ngay_them;
                                        $sho_ghi = _sql($show_fp['ghi_chu']) != '' ? _sql($show_fp['ghi_chu']) : _sql($show_fp['address_detail']);
                                        $sho_ghi_a = mb_substr($sho_ghi, 0, 20, 'UTF-8');
                                        if ($id_member == $show_fp['mem']) {
                                            $quyen_edit = '<a href="don_hang&edit_id=' . $show_fp['id'] . '"><i class="align-middle" data-feather="edit" style="color:#495057"></i></a>
                                            <a href="' . _sql03($_SERVER['REQUEST_URI']) . '&delfp=' . $show_fp['id'] . '" onclick="return confirm(\'#' . $num . ' - Đơn hàng của: ' . $show_fp['nameKH'] . ', sẽ bị xóa?\')"><i class="align-middle" data-feather="trash" style="color:#495057"></i></a>';
                                        } else {
                                            $quyen_edit = '<i class="align-middle" data-feather="alert-circle"></i>';
                                        }
                                        echo '<tr>
                                        <td style="text-align:center;">' . $num . '</td>                                        
                                        <td style="text-align:center;">' . $show_fp['nameKH'] . '</td>                                        
                                        <td style="text-align:center;">0' . $show_fp['sdt'] . ' - (' . check_nha_mang('0' . $show_fp['sdt'], 2) . ')</td>
                                        <td style="text-align:center;">' . $show_fp['nameSP'] . '</td>                                        
                                        <td style="text-align:center;">' . number_format($show_fp['total_money'], 0) . ' - (' . $show_fp['sl_mua'] . ')</td>
                                        <td style="text-align:center;" title="' . $sho_ghi . '">' . $sho_ghi_a . '</td>
                                        <td style="text-align:center;" title="' . date('d/m/Y H:i:s', $show_fp['time_add']) . '">' .  $ngay_show . '</td>
                                        <td style="text-align:center;">' . $quyen_edit . '</td>
                                        </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
                load_page(_sql02($_SERVER['REQUEST_URI']), $total_page, $limit, $total_records, $total_page_max);
                load_dialog($total_page_max, $member['id']);
                ?>
            </div>
        </div>
    </div>
</main>
<script src="/js/don_hang.js"></script>