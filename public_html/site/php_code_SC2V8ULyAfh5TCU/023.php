<?php
$title = "Thống kê";
require 'site/widget/header.php'; ?>
<div class="content">
    <div class="row">
        <div class="col-12 col-lg-7">
            <div class="card mb-3">
                <div class="card-header alert-info">
                    <h5 class="card-title mb-0  text-white fs_18" style="font-weight: bold;">Đơn hàng theo ngày</small></h5>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:10%; text-align:center;">#</th>
                            <th style="text-align:center;">Ngày</th>
                            <th style="text-align:center;">Đơn</th>
                            <th style="text-align:center;">Bộ</th>
                            <th style="text-align:center;">Hoàn</th>
                            <th style="text-align:center;">COD</th>
                        </tr>
                    </thead>
                    <tbody id="top_online">
                        <?php
                        for ($i = 0; $i < 20; $i++) {
                            $a_time = date('Ymd', $time_php - $i * 24 * 60 * 60);
                            $sqlAll = "SELECT COUNT(id) FROM `vn_don_hang_ship`WHERE team=$id_team AND `time_num`=$a_time AND `trang_thai`=0";
                            $stmt = $conn->query($sqlAll);
                            $num_rows = $stmt->fetchColumn();

                            $sqlAll = "SELECT SUM(`sl_mua`) FROM `vn_don_hang_ship`WHERE team=$id_team AND `time_num`=$a_time AND `trang_thai`=0";
                            $stmt = $conn->query($sqlAll);
                            $num_rows1 = $stmt->fetchColumn();

                            $sqlAll = "SELECT SUM(`gh_pick_money`) FROM `vn_don_hang_ship`WHERE team=$id_team AND (`gh_status`=6 OR `gh_status`=5) AND `time_num`=$a_time AND `trang_thai`=0";
                            $stmt = $conn->query($sqlAll);
                            $num_rows2 = $stmt->fetchColumn();


                            $sqlAll = "SELECT COUNT(`id`) FROM `vn_don_hang_ship`WHERE team=$id_team AND (`gh_status`=6 OR `gh_status`=5) AND `time_num`=$a_time AND `trang_thai`=0";
                            $stmt = $conn->query($sqlAll);
                            $num_rows_oke = $stmt->fetchColumn();

                            $sqlAll = "SELECT COUNT(`id`) FROM `vn_don_hang_ship`WHERE team=$id_team AND (`gh_status`=7 OR `gh_status`=11 or `gh_status`=21) AND `time_num`=$a_time AND `trang_thai`=0";
                            $stmt = $conn->query($sqlAll);
                            $num_rows_hoan = $stmt->fetchColumn();


                            echo '<tr>
                          <td style="text-align:center;">' . number_format($i + 1, 0) . '</td>
                          <td style="text-align:center;">' . date('d.m.Y', $time_php - $i * 24 * 60 * 60) . '</td>
                          <td style="text-align:center;">' . number_format($num_rows, 0) . '</td>
                          <td style="text-align:center;">' . number_format($num_rows1, 0) . '</td>
                          <td style="text-align:center;">' . number_format((int)($num_rows_hoan/($num_rows_oke+$num_rows_hoan)*100), 0) . '%</td>
                          <td style="text-align:center;">' . number_format($num_rows2, 0) . '</td>
                          </tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="col-12 col-lg-5">
            <div class="card mb-3">
                <div class="card-header alert-info">
                    <h5 class="card-title mb-0  text-white fs_18" style="font-weight: bold;">Top đơn hôm nay <small>(Rich kid)</small></h5>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:10%; text-align:center;">#</th>
                            <th style="width:50%; text-align:center;">Thành viên</th>
                            <th style="text-align:center;">Số đơn</th>
                            <th style="text-align:center;">Sản phẩm</th>
                        </tr>
                    </thead>
                    <tbody id="top_online">
                        <?php
                        $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`sl_mua` ) as `total_don_hang`, count(`a`.`sl_mua` ) as `count_don_hang` FROM `vn_don_hang_ship` AS  `a` WHERE team=$id_team AND `time_num`='$time_num' AND `trang_thai`=0 GROUP BY  `mem`  ORDER BY `total_don_hang` DESC LIMIT 5");
                        $stmt1->execute(array());
                        $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                        $num = 0;
                        foreach ($list_code as $show_code) {
                            $num++;
                            echo '<tr>
                          <td style="text-align:center;">' . $num . '</td>
                          <td style="text-align:center;">' . ucwords(sql_member($show_code['mem'], 'name')) . '</td>
                          <td style="text-align:center;">' . number_format($show_code['count_don_hang'], 0) . '</td>
                          <td style="text-align:center;">' . number_format($show_code['total_don_hang'], 0) . '</td>
                          </tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>

            <div class="card mb-3">
                <div class="card-header alert-info">
                    <h5 class="card-title mb-0  text-white fs_18" style="font-weight: bold;">Top đơn hôm qua</h5>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:10%; text-align:center;">#</th>
                            <th style="width:50%; text-align:center;">Thành viên</th>
                            <th style="text-align:center;">Số đơn</th>
                            <th style="text-align:center;">Sản phẩm</th>
                        </tr>
                    </thead>
                    <tbody id="top_online">
                        <?php
                        $time_num_1 = date('Ymd', $time_php - 24 * 60 * 60);
                        $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`sl_mua` ) as `total_don_hang`, count(`a`.`sl_mua` ) as `count_don_hang` FROM `vn_don_hang_ship` AS  `a` WHERE team=$id_team AND `time_num`='$time_num_1' AND `trang_thai`=0 GROUP BY  `mem`  ORDER BY `total_don_hang` DESC LIMIT 5");
                        $stmt1->execute(array());
                        $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                        $num = 0;
                        foreach ($list_code as $show_code) {
                            $num++;
                            echo '<tr>
                          <td style="text-align:center;">' . $num . '</td>
                          <td style="text-align:center;">' . ucwords(sql_member($show_code['mem'], 'name')) . '</td>
                          <td style="text-align:center;">' . number_format($show_code['count_don_hang'], 0) . '</td>
                          <td style="text-align:center;">' . number_format($show_code['total_don_hang'], 0) . '</td>
                          </tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>

            <div class="card mb-3">
                <div class="card-header alert-info">
                    <h5 class="card-title mb-0  text-white fs_18" style="font-weight: bold;">Top đơn tháng này</h5>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width:10%; text-align:center;">#</th>
                            <th style="width:50%; text-align:center;">Thành viên</th>
                            <th style="text-align:center;">Số đơn</th>
                            <th style="text-align:center;">Số sản phẩm</th>
                        </tr>
                    </thead>
                    <tbody id="top_online">
                        <?php
                        $time_num_2 = date('Ym', $time_php - 24 * 60 * 60);
                        $dau_thang = $time_num_2 . '00';
                        $cuoi_thang = $time_num_2 . '40';
                        $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`sl_mua` ) as `total_don_hang`, count(`a`.`sl_mua` ) as `count_don_hang` FROM `vn_don_hang_ship` AS  `a` WHERE team=$id_team AND `time_num`>'$dau_thang' AND `time_num`<'$cuoi_thang' AND `trang_thai`=0 GROUP BY  `mem`  ORDER BY `total_don_hang` DESC LIMIT 5");
                        $stmt1->execute(array());
                        $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                        $num = 0;
                        foreach ($list_code as $show_code) {
                            $num++;
                            echo '<tr>
                          <td style="text-align:center;">' . $num . '</td>
                          <td style="text-align:center;">' . ucwords(sql_member($show_code['mem'], 'name')) . '</td>
                          <td style="text-align:center;">' . number_format($show_code['count_don_hang'], 0) . '</td>
                          <td style="text-align:center;">' . number_format($show_code['total_don_hang'], 0) . '</td>
                          </tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>        
    </div>
</div>