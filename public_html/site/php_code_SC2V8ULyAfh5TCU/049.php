<?php
$trangthai[0] = "****";
$trangthai[70] = "Gom thành công";
$trangthai[91] = "Phát chưa thành công";
$trangthai[100] = "Phát thành công";
$trangthai[110] = "Chờ trả tiền";
$trangthai[120] = "Đã trả tiền";
$trangthai[161] = "Phát hoàn chưa TC";
$trangthai[170] = "Phát hoàn thành công";
if ($id_level == 5) {
    header('Location: /');
    exit;
}

function _sql_date($t)
{
    return preg_replace('/[^0-9-]/', '', $t);
}
if (isset($_POST['check_team_on'])) {
    $team = $_POST['check_team_on'];
    $stmt1 =  $conn->prepare("SELECT * FROM member WHERE id!=9 AND team=$team ORDER BY id  ASC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    echo '<option value="0">Lọc theo MEMBER</option>';
    foreach ($list_code as $show) {
        $selected_ck_2 = $arr_t[4] == $show['id'] ? 'selected' : '';
        echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name'] . '</option>';
    }
    exit;
}
if (isset($_GET['search'])) {
    if ($_GET['search'] == 0) {
        $_SESSION['tim_kiem_list_hang'] = '1=1';
        $_SESSION['data_tim_kiem'] = '';
        header('Location: /list_hang');
        exit;
    }
}
if (isset($_POST['button_xn_tim_kiem'])) {
    $in_sdt_name = '1=1';
    if (is_numeric($_POST['in_sdt_name'])) {
        $data_sdt_name = (int)$_POST['in_sdt_name'];
        $in_sdt_name = '(`sdt` LIKE \'%' . $data_sdt_name . '%\')';
    } else {
        $data = trim(_sql01($_POST['in_sdt_name']));
        $data_sdt_name = preg_replace('/\s+/', ' ', $data);
        $in_sdt_name = '(`nameKH` LIKE \'%' . $data_sdt_name . '%\' OR `so_hieu_don_hang` LIKE \'%' . $data_sdt_name . '%\')'; //OrderStatusId
    }

    $in_member = (int)$_POST['in_member'];
    $data_in_member = '1=1';
    if ($in_member > 0) {
        $data_in_member = '(`mem`=' . $in_member . ')';
    }

    $data_in_team = '1=1';
    $in_team = (int)$_POST['in_team'];
    if ($in_team > 0) {
        $data_in_team = 'team=' . $in_team;
    }
    $data_nguon_khach = '1=1';
    $id_nguon_khach = (int)$_POST['id_nguon_khach'];
    if ($id_nguon_khach > 0) {
        $data_nguon_khach = 'nguonKH=' . $id_nguon_khach;
    }
    $data_status_ly_do = '1=1';
    $id_status_ly_do = (int)$_POST['id_status_ly_do'];
    if ($id_status_ly_do > 0) {
        if ($id_status_ly_do < 1000) {
            $data_status_ly_do = 'type_dh=' . $id_status_ly_do;
        } else {
            $id_stt = $id_status_ly_do - 1000;
            $data_status_ly_do = 'trang_thai=' . $id_stt;
        }
    }
    $a_date_bd = '1=1';
    if ($_POST['date_bd'] != '') {
        $i_date_bd = _sql_date($_POST['date_bd']);
        $date_bd = _sql_num($_POST['date_bd']);
        $a_date_bd = '(`time_num`>=' . (int)$date_bd . ')';
    }
    $a_date_kt = '1=1';
    if ($_POST['date_kt'] != '') {
        $i_date_kt = _sql_date($_POST['date_kt']);
        $date_kt = _sql_num($_POST['date_kt']);
        $a_date_kt = '(`time_num`<=' . (int)$date_kt . ')';
    }

    $sql_buu_dien = '1=1';
    $tt_buu_dien = 0;
    if ($_POST['trang_thai_buu_dien'] > 0) {
        $tt_buu_dien = (int)$_POST['trang_thai_buu_dien'];
        $sql_buu_dien = 'OrderStatusId=' . $tt_buu_dien;
    }
    $_SESSION['data_tim_kiem'] = $data_sdt_name . '|' . $in_team . '|' . $id_nguon_khach . '|' . $id_status_ly_do . '|' . $in_member . '|' . $i_date_bd . '|' . $i_date_kt . '|' . $tt_buu_dien;
    $_SESSION['tim_kiem_list_hang'] = $in_sdt_name . ' AND ' . $data_in_team . ' AND ' . $data_nguon_khach . ' AND ' . $data_status_ly_do . ' AND ' . $data_in_member . ' AND ' . $a_date_bd . ' AND ' . $a_date_kt . ' AND ' . $sql_buu_dien;
}
if (isset($_POST['chi_tiet_dh'])) {
    $uid = (int)$_POST['chi_tiet_dh'];
    $stmt =  $conn->prepare("SELECT * FROM sale_sanpham_03 WHERE id=$uid AND trang_thai!=44 AND (team=$id_team or $id_level > 6)  ORDER BY id DESC");
    $stmt->execute();
    // echo 
    $id_sq01 = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($id_sq01['id'] > 0) {
        $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_04 WHERE sp_03=$uid ORDER BY id ASC");
        $sql =  "SELECT * FROM sale_sanpham_04 WHERE sp_03=$uid ORDER BY id ASC";
        if ($id_sq01['type_dh'] > 0 and $id_sq01['type_dh'] < 19 ) {
            $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_04_xit WHERE sp_03=$uid ORDER BY id ASC");
            $sql =  "SELECT * FROM sale_sanpham_04_xit WHERE sp_03=$uid ORDER BY id ASC";
        }
        // echo $sql;
        $stmt1->execute();
        $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
        echo '<div class="table-responsive flex-row flex-nowrap">
        <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
        <thead>
        <tr>
        <th style="text-align:center;">Sản phẩm</th>
        <th style="text-align:center;">Số lượng</th>
        <th style="text-align:center;">Tổng tiền</th>
        </tr>
        </thead>
        <tbody>';
        foreach ($list_code as $show_fp) {
            echo '<tr>
                <td style="text-align:center;">' . sql_sanpham_02($show_fp['sp_02'], 'ma_kem') . '</td>
                <td style="text-align:center;">  ' . number_format($show_fp['sl']) . '</td>
                <td style="text-align:center;">' . number_format($show_fp['sl'] * $show_fp['tien']) . '</td>
                </tr>';
        }
        echo '<tr><td style="text-align:center;" colspan="2">Tổng đơn</td><td style="text-align:center;">  ' . number_format($id_sq01['tong_tien']) . '</td></tr>';
        echo '<tr><td style="text-align:center;" colspan="2">VAT</td><td style="text-align:center;">  ' . number_format($id_sq01['vat']) . '</td></tr>';
        echo '<tr><td style="text-align:center;" colspan="2">Giảm giá</td><td style="text-align:center;">  ' . number_format($id_sq01['giam_gia']) . '</td></tr>';
        echo '<tr><td style="text-align:center;" colspan="2">Ship</td><td style="text-align:center;">  ' . number_format($id_sq01['ship']) . '</td></tr>';
        echo '<tr><td style="text-align:center; color:red" colspan="2">Thành tiền</td><td style="text-align:center; color:red">  ' . number_format($id_sq01['thuc_nhan']) . '</td></tr>';
        if ($id_sq01['ghi_chu'] != '') {
            echo '<tr><td style="text-align:center;">Ghi chú</td><td style="text-align:center;" colspan="2">  ' . _sql($id_sq01['ghi_chu']) . '</td></tr>';
        }
        $json_xa = json_decode(info_vn_xa($id_sq01['id_xa']), true);
        echo '<tr><td style="text-align:center;" colspan="3">Đ/c: ' . _sql($id_sq01['dia_chi']) . ' - ' . $json_xa['full_name'] . '</td></tr>';
        echo '<tr><td style="text-align:center;">Team</td><td style="text-align:center;" colspan="2">  ' . sql_web($id_sq01['team'], 'danh_sach_team', 'name_team') . '</td></tr>';
        if ($id_sq01['nguonKH'] > 0) {
            echo '<tr><td style="text-align:center;">Nguồn</td><td style="text-align:center;" colspan="2">  ' . sql_web($id_sq01['nguonKH'], 'admin_nguon_khach_hang', 'name') . '</td></tr>';
        }
        if($id_sq01['type_dh'] == 0)
        {
            echo '<tr><td style="text-align:center;">Chốt xịt</td><td style="text-align:center;" colspan="2">  Đã chốt</td></tr>';
        }
        else{
            if($id_sq01['type_dh'] == 20)
            {
                echo '<tr><td style="text-align:center;">Chốt xịt</td><td style="text-align:center;" colspan="2">  Đơn nội thành</td></tr>';
            }
            else
            {
                echo '<tr><td style="text-align:center;">Chốt xịt</td><td style="text-align:center;" colspan="2">  ' . sql_web($id_sq01['type_dh'], 'admin_ly_do_chot_xit', 'name') . '</td></tr>';
            }
        }
        echo '<tr><td style="text-align:center;">Mã vận đơn</td><td style="text-align:center;" colspan="2">  ' . $id_sq01['so_hieu_don_hang'] . '</td></tr>';
        echo '<tr><td style="text-align:center;" colspan="3">
        <div class="d-flex justify-content-between">
        <a href="add_donhang&edit=' . $id_sq01['id'] . '"><i class="far fa-fw fa-edit"></i> Điều chỉnh</a>
        <a href="' . _sql03($_SERVER['REQUEST_URI']) . '&delfp=' . $id_sq01['id'] . '" onclick="return confirm(\'0' . $id_sq01['sdt'] . ' - Đơn hàng của: ' . $id_sq01['nameKH'] . ', sẽ bị xóa?\')"><i class="fas fa-fw fa-trash-alt"></i> Xoá bỏ</a></div></td></tr>';
        
    } 
    else 
    {
        echo 'Yêu cầu quyền ADMIN để xem dữ liệu';
    }
    exit;
}
if (isset($_GET['delfp'])) {
    $id_xoa = (int) $_GET['delfp'];
    //Bảo mật ko cho xóa nếu có gì đó ko ổn
    $stmt =  $conn->prepare("SELECT * FROM sale_sanpham_03 WHERE id =$id_xoa AND trang_thai!=44 AND (mem='$id_member' OR $id_level>5)");
    $stmt->execute();
    $anpham_03 = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($anpham_03['id'] < 1) {
        header('Location: /list_hang');
        exit;
    }
    //Update trạng thái xóa
    $sql = "UPDATE sale_sanpham_03 SET trang_thai=44 WHERE id=$id_xoa AND mem=$id_member";
    if ($id_level > 3) {
        $sql = "UPDATE sale_sanpham_03 SET trang_thai=44 WHERE id=$id_xoa";
    }
    $stmt = $conn->prepare($sql);
    $stmt->execute();

    //Lưu lại bản log để biết ai đã xóa, xóa cái gì    
    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_04 WHERE sp_03=$id_xoa ORDER BY id ASC"); //Có thể chọn RAND ()
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $show) {
        $json[] = ['uid' => (int)$show['id'], 'name' => sql_sanpham_02($show['sp_02'], 'ma_kem'), 'price' => (int)$show['tien'], 'count' => (int)$show['sl']];
    }
    $json_a = _sql(json_encode($json));
    $sql = "INSERT INTO `sale_sanpham_04_del`(`sp03`, `mem`, `list_json`, `time_add`) VALUES ('$id_xoa','$id_member','$json_a','$time_php')";
    $conn->exec($sql);
    //Xóa dữ liệu ở bảng chi tiết sản phẩm
    $sql = "DELETE FROM sale_sanpham_04 WHERE sp_03='" . $id_xoa . "' AND (mem=$id_member OR $id_level>5)";
    $conn->exec($sql);
    header('Location: /list_hang');
    exit;
}
if (!isset($_SESSION['tim_kiem_list_hang'])) {
    $_SESSION['tim_kiem_list_hang'] = '1=1';
    $class_show = '';
} else {
    if ($_SESSION['tim_kiem_list_hang'] != '1=1') {
        $arr_t = explode('|', $_SESSION['data_tim_kiem']);
        $class_show = 'show';
    }
}
$timkiem = $_SESSION['tim_kiem_list_hang'];
$title = 'Danh sách đơn hàng';
require 'site/widget/header.php'; ?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-12 mb-3" id="accordion">
                <div id="collapse_TaoDonHang" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card" style="margin-bottom: -0.5rem;">
                        <div class="card-body">
                            <form method="post">
                                <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12">
                                    <div class="row">
                                        <div class="col-md mb-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">Số đt, tên khách, mã đơn</span>
                                                </div>
                                                <input class="form-control h38" type="text" value="<?= $arr_t[0] ?>" name="in_sdt_name"></input>
                                            </div>
                                        </div>
                                        <div class="col-md mb-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Trạng thái</span>
                                                </div>
                                                <select class="custom-select flex-grow-1" name="trang_thai_buu_dien">
                                                    <?php
                                                    $selected_ck_7 = $arr_t[7] == 0 ? 'selected' : '';
                                                    echo '<option value="0" ' . $selected_ck_7 . '>...</option>';
                                                    $selected_ck_7 = $arr_t[7] == 70 ? 'selected' : '';
                                                    echo '<option value="70" ' . $selected_ck_7 . '>Gom thành công</option>';
                                                    $selected_ck_7 = $arr_t[7] == 91 ? 'selected' : '';
                                                    echo '<option value="91" ' . $selected_ck_7 . '>Phát chưa thành công</option>';
                                                    $selected_ck_7 = $arr_t[7] == 100 ? 'selected' : '';
                                                    echo '<option value="100" ' . $selected_ck_7 . '>Phát thành công</option>';
                                                    $selected_ck_7 = $arr_t[7] == 110 ? 'selected' : '';
                                                    echo '<option value="110" ' . $selected_ck_7 . '>Chờ trả tiền</option>';
                                                    $selected_ck_7 = $arr_t[7] == 120 ? 'selected' : '';
                                                    echo '<option value="120" ' . $selected_ck_7 . '>Đã trả tiền</option>';
                                                    $selected_ck_7 = $arr_t[7] == 161 ? 'selected' : '';
                                                    echo '<option value="161" ' . $selected_ck_7 . '>Phát hoàn chưa TC</option>';
                                                    $selected_ck_7 = $arr_t[7] == 170 ? 'selected' : '';
                                                    echo '<option value="170" ' . $selected_ck_7 . '>Phát hoàn thành công</option>';
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12">
                                    <div class="row">
                                        <div class="col-md mb-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">Nguồn KH</span>
                                                </div>
                                                <select class="custom-select flex-grow-1" name="id_nguon_khach">
                                                    <?php
                                                    $stmt1 =  $conn->prepare("SELECT * FROM admin_nguon_khach_hang WHERE an=0 ORDER BY id ASC");
                                                    $stmt1->execute();
                                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                    echo '<option value="0">...</option>';
                                                    foreach ($list_code as $show) {
                                                        $selected_ck_2 = $arr_t[2] == $show['id'] ? 'selected' : '';
                                                        echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md mb-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Lý do chốt</span>
                                                </div>
                                                <select class="custom-select flex-grow-1" name="id_status_ly_do">
                                                    <?php
                                                    $stmt1 =  $conn->prepare("SELECT * FROM admin_ly_do_chot_xit WHERE an=0 ORDER BY id ASC");
                                                    $stmt1->execute();
                                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                    echo '<option value="0">...</option>';
                                                    foreach ($list_code as $show) {
                                                        $selected_ck_2 = $arr_t[3] == $show['id'] ? 'selected' : '';
                                                        echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12">
                                    <div class="row">
                                        <div class="col-md mb-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">Team</span>
                                                </div>
                                                <select class="custom-select flex-grow-1" name="in_team" id="check_team_on" onchange="check_member_team()">
                                                    <?php
                                                    $stmt1 =  $conn->prepare("SELECT * FROM danh_sach_team ORDER BY id ASC");
                                                    $stmt1->execute();
                                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                    echo '<option value="0">...</option>';
                                                    foreach ($list_code as $show) {
                                                        $selected_ck_2 = $arr_t[1] == $show['id'] ? 'selected' : '';
                                                        echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name_team'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md mb-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Member</span>
                                                </div>
                                                <select class="custom-select flex-grow-1" name="in_member" id="member_js_sha">
                                                    <?php
                                                    $stmt1 =  $conn->prepare("SELECT * FROM member WHERE id!=9 ORDER BY id  ASC");
                                                    $stmt1->execute();
                                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                    echo '<option value="0">...</option>';
                                                    foreach ($list_code as $show) {
                                                        $selected_ck_2 = $arr_t[4] == $show['id'] ? 'selected' : '';
                                                        echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12 mb-3">
                                    <div class="row">
                                        <div class="col-md mb-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">Ngày bắt đầu</span>
                                                </div>
                                                <input type="date" name="date_bd" value="<?= $arr_t[5] ?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md mb-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Kết thúc</span>
                                                </div>
                                                <input type="date" name="date_kt" value="<?= $arr_t[6] ?>" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-xs-right">
                                    <button type="submit" name="button_xn_tim_kiem" id="button_xn_tim_kiem" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-header alert-info d-flex justify-content-between">
                        <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                            <div>Danh sách đơn hàng</div> <i class="align-middle mr-2 fas fa-fw fa-search" data-toggle="collapse" data-target="#collapse_TaoDonHang"></i>
                        </div>
                    </div>
                    <div class="">
                        <div class="table-responsive flex-row flex-nowrap">
                            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align:center; width:5%;">#</th>
                                        <th style="text-align:center;">Member</th>
                                        <th style="text-align:center;">SĐT</th>
                                        <th style="text-align:center;">Tên KH</th>
                                        <th style="text-align:center;">Tổng tiền</th>
                                        <th style="text-align:center;">Trạng thái</th>
                                        <th style="text-align:center;">Ghi chú</th>
                                        <th style="text-align:center;">Ngày thêm</th>
                                        <th style="text-align:center; width:7%">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sqlAll = "SELECT COUNT(`mem`) FROM `sale_sanpham_03` WHERE trang_thai!=44 AND $timkiem"; //(team=$id_team OR $id_level>7) AND
                                    $stmt5 = $conn->query($sqlAll);
                                    $total_records  = $stmt5->fetchColumn();
                                    $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                                    $total_page = ceil($total_records / $limit);
                                    $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                                    $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                                    if ($total_page > 0) {
                                        $total_page_max = $total_page - 1;
                                    } else {
                                        $total_page_max = $total_page;
                                    }
                                    $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                                    $start_page = $_GET['page'] * $limit;
                                    $num_1 = 0;

                                    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_03 WHERE trang_thai!=44 AND $timkiem ORDER BY id DESC LIMIT $start_page, $limit");
                                    $stmt1->execute(array());
                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                    $num = 0;
                                    foreach ($list_code as $show_fp) {
                                        $mas_tk = "";
                                        $arr = (array) $show_fp;
                                        $num_1 = $num_1 + 1;
                                        $num = $num_1 + $_GET['page'] * $limit;
                                        $ngay_hien_tai = date('d-m-Y', $time_php);
                                        $ngay_them = date('d-m-Y', $show_fp['time_add']);
                                        $ngay_show = $ngay_them == $ngay_hien_tai ? date('H:i:s', $show_fp['time_add']) : $ngay_them;
                                        $sho_ghi = _sql($show_fp['ghi_chu']) != '' ? _sql($show_fp['ghi_chu']) : _sql($show_fp['address_detail']);
                                        $sho_ghi_a = mb_substr($sho_ghi, 0, 20, 'UTF-8');
                                        if ($show_fp['team'] == $id_team or $id_level > 6) {
                                            $d_sdt = $show_fp['sdt'];
                                        } else {
                                            $d_sdt = substr($show_fp['sdt'], 0, 7) . '***';
                                        }
                                        if ($id_level > 7) {
                                            $d_sdt = $show_fp['sdt'];
                                        }
                                        $trang_thai_v1 = 'Chưa xuất hàng';
                                        // setup hiển thị trang thái
                                        if ($show_fp['type_dh'] == "0" && (int)$show_fp["OrderStatusId"] == 0) {
                                            $mas_tk = "color:blue;";
                                            $ten_trang_thai = "Đã chốt(chưa giao)";
                                        } 
                                        else 
                                        {
                                            if($show_fp['type_dh'] == "20" && (int)$show_fp["OrderStatusId"] == 0)
                                            {
                                                $mas_tk = "color:blue;";
                                                $ten_trang_thai = "Đơn nội thành";
                                            }else
                                            {
                                                if((int)$show_fp["OrderStatusId"] != 0)
                                                {
                                                    $mas_tk = "color:blue;";
                                                    $ten_trang_thai =  $trangthai[(int)$show_fp["OrderStatusId"]];
                                                }
                                                else
                                                {
                                                    $mas_tk = "color:red;";
                                                    $ten_trang_thai = get_chot_xit($show_fp['type_dh']);
                                                }
                                                
                                            }
                                        }
                                        $ghi_chu_s = $show_fp['ghi_chu'] != "" ? $show_fp['ghi_chu'] : info_vn_tinh($show_fp['id_tinh']);
                                        echo '<tr>
                                        <td style="text-align:center; ' . $mas_tk . '">' . $num . '</td>                                        
                                        <td style="text-align:center; ' . $mas_tk . '">' . ucwords(sql_member($show_fp['mem'], 'name')) . '</td>
                                        <td style="text-align:center; ' . $mas_tk . '">0' . $d_sdt . '</td>
                                        <td style="text-align:center; ' . $mas_tk . '">' . $show_fp['nameKH'] . '</td>
                                        <td style="text-align:center; ' . $mas_tk . '">' . number_format($show_fp['thuc_nhan'], 0) . '</td>
                                        <td style="text-align:center; ' . $mas_tk . '">' . $ten_trang_thai . '</td>
                                        <td style="text-align:center; ' . $mas_tk . '">' . _sql($ghi_chu_s) . '</td>
                                        <td style="text-align:center; ' . $mas_tk . '" title="' . date('d-m-Y H:i:s', $show_fp['time_add']) . '">' .  $ngay_show . '</td>
                                        <td style="text-align:center; ' . $mas_tk . '" onclick="chi_tiet(' . $num . ',' . $show_fp['id'] . ',\'' . $show_fp['nameKH'] . '\',\'0' . $d_sdt . '\')"><i class="far fa-fw fa-folder-open" style="color:#495057"></i></td>
                                        </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
                load_page(_sql02($_SERVER['REQUEST_URI']), $total_page, $limit, $total_records, $total_page_max);
                load_dialog($total_page_max, $member['id']);
                ?>
            </div>
        </div>
    </div>
</main>
<script>
    function check_member_team() {
        abc = document.getElementById("check_team_on").value;
        $.post("list_hang", {
            check_team_on: abc
        }, function(data) {
            document.getElementById("member_js_sha").innerHTML = data;
        });

    }

    function chi_tiet(num, uid, ten, sdt) {
        document.getElementById("popup_title").innerHTML = "# " + num + " -->" + ten + " -->" + sdt;
        $.post("list_hang", {
            chi_tiet_dh: uid
        }, function(data) {
            // console.log(data);
            document.getElementById("popup_noidung").innerHTML = data;
            $('#popup_ne').modal('show');
        });
    }

    function js_doi_tac() {
        abc = document.getElementById("js_name_doi_tac").value;
        if (abc == "mem" || abc == "team") {
            $.post("ke_toan", {
                js_doi_tac: abc
            }, function(data, status) {
                document.getElementById("js_doi_tac").innerHTML = data;
            });
        }
    }
</script>
<div class="modal fade" id="popup_ne" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg_xanh_duong_nhat">
                <h2 style="text-align:center;" id="popup_title"></h2>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="popup_noidung"></div>
        </div>
    </div>
</div>