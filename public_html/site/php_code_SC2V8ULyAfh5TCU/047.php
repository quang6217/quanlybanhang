<?php
if (isset($_POST['name'])) {
    $id_edita = (int)explode('edit=', $_POST['url_xl_as'])[1];
    $json = json_decode($_POST['cart'], true);
    $tong_tien = 0;
    foreach ($json as $key => $value) {
        $tong_tien = $tong_tien + ($value['count'] * $value['price']);
    }
    $giam_gia = $tong_tien * $_POST['discount'] / 100;
    $vat = $tong_tien * $_POST['vat'] / 100;
    $ship = (int)$_POST['ship'];
    $thanh_tien = $tong_tien - $giam_gia + $vat + $ship;
    //echo $giam_gia +" " + $vat + " " + $ship +" " + $thanh_tien;
    $id_xa = (int)$_POST['id_xa'];
    $id_huyen = (int)$_POST['id_huyen'];
    $id_tinh = (int)$_POST['id_tinh'];
    $customer_tel = (int)$_POST['customer_tel'];
    $customer_fullname = _sql($_POST['customer_fullname']);
    $customer_first_address = _sql($_POST['customer_first_address']);
    $message = _sql($_POST['message']);
    $stmt =  $conn->prepare("SELECT * FROM sale_sanpham_03 WHERE sdt =:sdt ORDER BY id DESC");
    $stmt->execute(array(":sdt" => $customer_tel));
    $id_sq01 = $stmt->fetch(PDO::FETCH_ASSOC);
    // $time_chong_trung = $time_php - 60 * 60;
    $time_chong_trung = $time_php ;
    $id_ly_do_xit = (int)$_POST['id_ly_do_xit'];
    $id_nguon_khach = (int)$_POST['id_nguon_khach'];
    if ($id_edita > 0) 
    {
        $stmt_xuat =  $conn->prepare("SELECT * from sale_sanpham_03 where id = $id_edita");
        $stmt_xuat->execute();
        $msx =  $stmt_xuat->fetch()["ma_so_xuat"];
        if($msx != 0)
        {
            $sql_mess = "UPDATE sale_sanpham_03 SET ghi_chu = '$message' WHERE id='$id_edita'";
            $stmt_mess = $conn->prepare($sql_mess);
            $stmt_mess->execute();
            echo "update_mess";
            exit;
        }
    }
    // Nếu mà đang ở url edit thì update dữ liệu xong thoát
    if ($id_edita > 0) {
        $sql = "UPDATE sale_sanpham_03 SET nguonKH='$id_nguon_khach', type_dh='$id_ly_do_xit',`id_huyen`='$id_huyen',`time_num` = '$time_num',`time_add` = '$time_php',
        `id_tinh`='$id_tinh',`log_edit`='$id_member',nameKH=:nameKH,
        `sdt`='$customer_tel',dia_chi=:dia_chi,`id_xa`='$id_xa',
        `tong_tien`='$tong_tien',`giam_gia`='$giam_gia',`vat`='$vat',
        `ship`='$ship',`thuc_nhan`='$thanh_tien',ghi_chu=:ghi_chu WHERE id='" . $id_edita . "' AND (mem=$id_member OR $id_level>5)";
        $stmt = $conn->prepare($sql);
        $stmt->execute(array('nameKH' => $customer_fullname, 'dia_chi' => $customer_first_address, 'ghi_chu' => $message));
        // //Lưu lại bản log để biết ai đã xóa, xóa cái gì    
        $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_04 WHERE sp_03=$id_edita ORDER BY id ASC"); //Có thể chọn RAND ()
        $stmt1->execute();
        $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
        foreach ($list_code as $show) {
            $json_edit[] = ['uid' => (int)$show['id'], 
            'name' => sql_sanpham_02($show['sp_02'], 'ma_kem'), 
            'price' => (int)$show['tien'],
             'count' => (int)$show['sl']];
        }
        $json_a = _sql(json_encode($json_edit));
        
        $sql = "INSERT INTO `sale_sanpham_04_edit`(`sp03`, `mem`, `list_json`, `time_add`) VALUES
         ('$id_edita','$id_member','$json_a','$time_php')";
        $conn->exec($sql);

        //Xóa hết đi để lưu trữ lại từ đầu
        $sql = "DELETE FROM sale_sanpham_04 WHERE sp_03='" . $id_edita . "' AND (mem=$id_member OR $id_level>5)";
        $conn->exec($sql);
        $sql = "DELETE FROM sale_sanpham_04_xit WHERE sp_03='" . $id_edita . "' AND (mem=$id_member OR $id_level>5)";
        $conn->exec($sql);
        // //Lưu trữ lại từ đầu
        foreach ($json as $key => $value) 
        {
            $id_sp02 = (int)$value['uid'];
            // echo $id_sp02." " ;
            $sl = (int)$value['count'];
            $gia_ban = (int)$value['price'];
            $id_sp01 = (int)sql_sanpham_02($id_sp02, 'id_san_pham');
            $id_gia_tien = get_id_so_lan_max($id_sp02)["id"];
            if ($id_ly_do_xit == 0 || $id_ly_do_xit == 20) {
                $sql = "INSERT INTO `sale_sanpham_04`(`mem`, `team`, `sp_03`, `sp_02`, `sp_01`, `sl`, `tien`, `time_num`,`id_gia_tien`,`nguonKH`) VALUES 
                ('$id_member','$id_team','$id_edita','$id_sp02','$id_sp01','$sl','$gia_ban','$time_num','$id_gia_tien','$id_nguon_khach')";
                // echo $sql;
                $conn->exec($sql);
            } else {
                $sql = "INSERT INTO `sale_sanpham_04_xit`(`mem`, `team`, `sp_03`, `sp_02`, `sp_01`, `sl`, `tien`, `time_num`,`id_gia_tien`,`nguonKH`) VALUES 
                ('$id_member','$id_team','$id_edita','$id_sp02','$id_sp01','$sl','$gia_ban','$time_num','$id_gia_tien','$id_nguon_khach')";
                $conn->exec($sql);
                echo $sql;
            }
        }
        echo 'update';
        exit;
    }
    if ($id_sq01['time_add'] < $time_chong_trung  and $id_xa > 0 and $tong_tien > 0 and $customer_tel > 0) {
        try {
            $stmt = $conn->prepare('INSERT INTO sale_sanpham_03 (`mem`, `team`, `nameKH`, `sdt`, `dia_chi`, `id_xa`, `link_facebook`, `tong_tien`, `giam_gia`, `vat`, `ship`, `thuc_nhan`, `ghi_chu`, `trang_thai`, `time_add`, `log_edit`, `time_num`, `type_dh`, `id_huyen`, `id_tinh`, `nguonKH`, `ma_GH`) values
             (:mem, :team, :nameKH, :sdt, :dia_chi, :id_xa, :link_facebook, :tong_tien, :giam_gia, :vat, :ship, :thuc_nhan, :ghi_chu, :trang_thai, :time_add, :log_edit, :time_num, :type_dh, :id_huyen, :id_tinh, :nguonKH, :ma_GH)');
            $data = array('mem' => $id_member, 'team' => $id_team, 'nameKH' => $customer_fullname, 'sdt' => $customer_tel, 'dia_chi' => $customer_first_address, 'id_xa' => $id_xa, 'link_facebook' => '', 'tong_tien' => $tong_tien, 'giam_gia' => $giam_gia, 'vat' => $vat, 'ship' => $ship, 'thuc_nhan' => $thanh_tien, 'ghi_chu' => $message, 'trang_thai' => 0, 'time_add' => $time_php, 'log_edit' => 0, 'time_num' => $time_num, 'type_dh' => $id_ly_do_xit, 'id_huyen' => $id_huyen, 'id_tinh' => $id_tinh, 'nguonKH' => $id_nguon_khach, 'ma_GH' => '');
            $stmt->execute($data);
        } catch (Exception $e) {
        }
        $stmt =  $conn->prepare("SELECT * FROM sale_sanpham_03 WHERE sdt =:sdt ORDER BY id DESC");
        $stmt->execute(array(":sdt" => $customer_tel));
        $id_mii_them = $stmt->fetch(PDO::FETCH_ASSOC);
        $id_sq03 = (int)$id_mii_them['id'];
        //var_dump($json);
        foreach ($json as $key => $value) {
            $id_sp02 = (int)$value['uid'];
           // echo $id_sp02;
            $sl = (int)$value['count'];
            $gia_ban = (int)$value['price'];
            $id_sp01 = (int)sql_sanpham_02($id_sp02, 'id_san_pham');
            $id_gia_tien = get_id_so_lan_max($id_sp02)["id"];
            if ($id_ly_do_xit == 0 || $id_ly_do_xit == 20) {
                $sql = "INSERT INTO `sale_sanpham_04`(`mem`, `team`, `sp_03`, `sp_02`, `sp_01`, `sl`, `tien`, `time_num`,`id_gia_tien`,`nguonKH`) VALUES
                                             ('$id_member','$id_team','$id_sq03','$id_sp02','$id_sp01','$sl','$gia_ban','$time_num','$id_gia_tien','$id_nguon_khach')";
                $conn->exec($sql);
            } else {
                $sql = "INSERT INTO `sale_sanpham_04_xit`(`mem`, `team`, `sp_03`, `sp_02`, `sp_01`, `sl`, `tien`, `time_num`,`id_gia_tien`,`nguonKH`) VALUES
                 ('$id_member','$id_team','$id_sq03','$id_sp02','$id_sp01','$sl','$gia_ban','$time_num','$id_gia_tien','$id_nguon_khach')";
                $conn->exec($sql);
                
            }
        }
        echo 'oke';
    } else {
        if ($id_sq01['time_add'] > $time_chong_trung) {
            echo 'trung_don';
            exit;
        }
        echo 'error';
    }
    //  echo "Tổng tiền: " . $tong_tien . "\nGiảm giá: " . $giam_gia . "\nVAT: " . $vat . "\nShip: " . $ship . "\nThanh toán: " . $thanh_tien . "\nid_xa: " . $id_xa . "\ncustomer_tel: " . $customer_tel . "\ncustomer_fullname: " . $customer_fullname . "\ncustomer_first_address: " . $customer_first_address . "\nmessage: " . $message . "\n";
    exit;
}
if (isset($_GET['edit'])) {
    $id_edit = (int)$_GET['edit'];
    if ($id_edit > 0) {
        $stmt =  $conn->prepare("SELECT * FROM sale_sanpham_03 WHERE id=$id_edit AND trang_thai!=44 AND ($id_level>6 OR team=$id_team ) ORDER BY id DESC");
        $stmt->execute();
        $id_sq01 = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($id_sq01['id'] < 1) 
        {
            header('Location: /list_hang');
            exit;
        }
    }
}
if ($id_level == 5) {
    header('Location: /');
    exit;
}
$title = "Tạo đơn hàng";
require 'site/widget/header.php';
kho_update();
if ($id_sq01['id'] > 0) {
    $id_sp03 = (int)$id_sq01['id'];
    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_04 WHERE sp_03=$id_sp03 ORDER BY id ASC");
    if ($id_sq01['type_dh'] > 0 && $id_sq01['type_dh']<19 )
     {
        $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_04_xit WHERE sp_03=$id_sp03 ORDER BY id ASC");
    }
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    // var_dump(json_encode($stmt1));
    foreach ($list_code as $show_1552) {
        $json[] = ['uid' => (int)$show_1552['sp_02'], 'name' => sql_sanpham_02($show_1552['sp_02'], 'ma_kem'), 'price' => (int)$show_1552['tien'], 'count' => (int)$show_1552['sl']];
    }
    $giam_gia = (int)$id_sq01['giam_gia'] / $id_sq01['tong_tien'] * 100;
    $vat = (int)$id_sq01['vat'] / $id_sq01['tong_tien'] * 100;
    // var_dump(json_encode($json));
    // var_dump(base64_encode((json_encode($json))));
    // echo '<body onload="load_from_edit(\'' . base64_en(json_encode($json)) . '\',\'' . $giam_gia . '\',\'' . $vat . '\',\'' . $id_sq01['ship'] . '\',\'' . $id_sq01['ghi_chu'] . '\',\'' . $id_sq01['sdt'] . '\',\'' . $id_sq01['nameKH'] . '\',\'' . $id_sq01['dia_chi'] . '\',\'' . $id_sq01['id_xa'] . '\')"></body>';
    echo '<body onload="load_from_edit(\'' . base64_encode((json_encode($json))) . '\',\'' . $giam_gia . '\',\'' . $vat . '\',\'' . $id_sq01['ship'] . '\',\'' . $id_sq01['ghi_chu'] . '\',\'' . $id_sq01['sdt'] . '\',\'' . $id_sq01['nameKH'] . '\',\'' . $id_sq01['dia_chi'] . '\',\'' . $id_sq01['id_xa'] . '\')"></body>';

}
$timkiem = '1=1';
?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<link rel="stylesheet" type="text/css" href="/css/dh/dh.css">
<div class="content" onclick=my_vitri()>
    <div class="row">
        <div class="col-12 col-lg-7">
            <div class="card mb-3">
                <div class="card-header bg_xanh_duong_nhat">
                    <h5 class="card-title mb-0">Tìm kiếm mã sản phẩm..!</h5>
                </div>
                <div class="card-body">
                    <div class="input-group mb-3">
                        <input class="form-control" type="text" value="" id="search_sp_001" placeholder="Gõ tên hoặc id mã sản phẩm" autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="card mb-3">
                <div id="show_khach_quen"></div>
            </div>
            <div id="search_sp_002">
            </div>
        </div>
        <div class="col-12 col-lg-5">
            <div class="card mb-3">
                <div class="card-header bg_xanh_duong_nhat">
                    <h5 class="card-title mb-0" style="float: left;"><button type="button" class="vbd_01 btn btn_primary">Tổng sản phẩm: (<span class="total-count"></span>)</button> <button class="clear-cart btn btn-danger">Xóa giỏ hàng</button></h5>
                </div>
                <div class="card-body">
                    <div id="popover_content_wrapper">
                        <span id="cart_details">
                            <div class="table-responsive" id="order_table">
                                <table class="table show-cart mb-2">
                                </table>
                            </div>
                        </span>
                        <div class="btnbox-vbd">
                            <div class="form-group">
                                <div class="form-row align-items-center">
                                    <div class="col-md-4 input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Giảm:</div>
                                        </div>
                                        <input type="number" class="form-control" name="discount" style="font-size: 13px;" onchange="displayCart()" id="discount" placeholder="%" value="">
                                    </div>
                                    <div class="col-md-4 input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">VAT:</div>
                                        </div>
                                        <input type="number" class="form-control" name="vat" style="font-size: 13px;" onchange="displayCart()" id="vat" placeholder="%" value="">
                                    </div>
                                    <div class="col-md-4 input-group mb-2">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text">Ship: </div>
                                        </div>
                                        <input type="number" class="form-control" name="ship" style="font-size: 13px;" onchange="displayCart()" id="ship" placeholder="vnđ" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <textarea id="message" class="form-control" name="message" placeholder="Ghi chú"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-header bg_xanh_duong_nhat">
                    <h5 class="card-title mb-0">Thông tin người nhận hàng</h5>

                </div>
                <div class="card-body">
                    <div class="aui-group form-row">

                        <div class="input-icon row-field" style="margin-left: 15px; margin-top: 5px;">
                            <i class="align-middle icon-phone" data-feather="phone"></i>
                            <input id="customer_tel" required="required" maxlength="15" placeholder="Nhập SĐT để tự động điền thông tin khách hàng quen" type="tel" onchange="check_sdt()"></div>
                        <div class="input-icon row-field customer-fullname-text" style="margin-left: 15px; margin-top: 0px;">
                            <i class="align-middle icon-user" data-feather="user-plus"></i>
                            <input id="customer_fullname" required="required" class="s-tooltip customer-fullname" maxlength="500" placeholder="Tên khách hàng" type="text" id="Package0CustomerFullname" data-original-title="Tên khách hàng"> </div>
                        <div id="customer-address-detect" class="input-icon row-field customer-first-address-group" style="margin-left: 15px; margin-top: 0px;">
                            <i class="align-middle icon-home" data-feather="home"></i>
                            <input id="customer_first_address" required="required" placeholder="Địa chỉ chi tiết (nhà/ngõ/ngách...)" class="left-stripe s-tooltip customer-first-address" maxlength="100" type="text" id="Package0CustomerFirstAddress" data-original-title="Địa chỉ chi tiết giúp chúng tôi giao hàng chính xác hơn">
                            <div class="loader-address"></div>
                        </div>
                        <div class="row-field customer-province-item" style="margin-left: 15px; margin-top: 0px;">
                            <select class="vn_tinh left-stripe s-tooltip flex-grow-1" type="text" style="width:100% !important" name="vn_tinh" id="vn_tinh"></select>
                        </div>
                        <div class="row-field customer-province-item" style="margin-left: 15px; margin-top: 10px;">
                            <select class="vn_huyen left-stripe s-tooltip flex-grow-1" type="text" style="width:100% !important" name="vn_huyen" id="vn_huyen"></select>
                        </div>
                        <div class="row-field customer-province-item" style="margin-left: 15px; margin-top: 10px;">
                            <select class="vn_xa left-stripe s-tooltip flex-grow-1" type="text" style="width:100% !important" name="vn_xa" id="vn_xa"></select>
                        </div>
                        <div class="input-icon row-field" style="margin-left: 15px; margin-top: 5px;">
                            <select class="custom-select flex-grow-1" id="id_ly_do_xit">
                                <?php
                                $stmt1 =  $conn->prepare("SELECT * FROM admin_ly_do_chot_xit WHERE an=0 ORDER BY id ASC");
                                $stmt1->execute();
                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                echo '<option value="0">Chốt thành công</option>';
                                if (isset($id_sq01['type_dh']))
                                {
                                    if($id_sq01['type_dh'] == 20)
                                    {
                                        echo '<option value="20" selected >Hàng nội thành</option>';
                                    }else
                                    {
                                        echo '<option value="20" >Hàng nội thành</option>';
                                    }
                                }else
                                {
                                    echo '<option value="20" >Hàng nội thành</option>';
                                }
                                foreach ($list_code as $show) {
                                    $selected_ck_t = $id_sq01['type_dh'] == $show['id'] ? 'selected' : '';
                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_t . '>' . $show['name'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="input-icon row-field" style="margin-left: 15px; margin-top: 5px;">
                            <select class="custom-select flex-grow-1" id="id_nguon_khach">
                                <?php
                                $stmt1 =  $conn->prepare("SELECT * FROM admin_nguon_khach_hang WHERE an=0 ORDER BY id ASC");
                                $stmt1->execute();
                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                echo '<option value="0">Thêm nguồn khách hàng</option>';
                                foreach ($list_code as $show) {
                                    $selected_ck_2 = $id_sq01['nguonKH'] == $show['id'] ? 'selected' : '';
                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name'] . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="row-field customer-province-item" style="margin-left: 15px; margin-top: 10px;">
                            <button class="js_post_dh btn btn-block btn-flat margin-top-10 btn-info" id="ska_5">Đăng đơn hàng</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/js/d_h/add_dh.js"></script>
<script>
    function chi_tiet(uid, ten, sdt) {
        document.getElementById("popup_title").innerHTML = ten + " -->" + sdt;
        $.post("js_047", {
            chi_tiet_dh: uid
        }, function(data) {
            document.getElementById("popup_noidung").innerHTML = data;
        });
    }
</script>
<div class="modal fade" id="popup_ne" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg_xanh_duong_nhat">
                <h2 style="text-align:center;" id="popup_title"></h2>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="popup_noidung"></div>
        </div>
    </div>
</div>