<?php
if (isset($_POST['input_001'])) {
    if (is_numeric($_POST['input_001'])) {
        $data = (int)$_POST['input_001'];
        $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_02 WHERE id_san_pham=$data AND `hien_an`!=44 ORDER BY id ASC LIMIT 100");
    } else {
        $data = trim(_sql01($_POST['input_001']));
        $data = preg_replace('/\s+/', ' ', $data);
        $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_02 WHERE `ma_kem` LIKE '%$data%' AND `hien_an`!=44 ORDER BY gia_tien ASC LIMIT 100");
    }
    echo '<div class="card mb-3">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="width:5%; text-align:center;">#</th>
                        <th style="text-align:left;">Các mã kèm</th>
                        <th style="text-align:center;">Loại</th>
                        <th style="text-align:center;">Giá tiền (đ)</th>                        
                        <th style="text-align:center;">Kho</th>
                        <th style="text-align:center;width:2%"><i class="fas fa-fw fa-shopping-cart"></i></th>
                    </tr>
                </thead>
                <tbody>';
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    foreach ($list_code as $show) {
        $num++;
        echo '<tr>
                <td style="text-align:center;">' . $num . '</td>
                <td style="text-align:left;">' . $show['ma_kem'] . '</td>
                <td style="text-align:center;">' . _sql($show['phan_loai']) . '</td>                          
                <td style="text-align:center;">' . number_format($show['gia_tien']) . '</td>                
                <td style="text-align:center;">'.number_format(sql_web($show['id'],'kho_update','sl')).'</td> 
                <td style="text-align:center;width:2%"><a href="javascript:void(0)" onclick="add_to_cart(this)" data-uid="' . $show['id'] . '" data-price="' . $show['gia_tien'] . '" data-name="' . $show['ma_kem'] . '"><i class="fas fa-fw fa-shopping-cart text-info"></i></a></td> 
                </tr>';
    }
    echo '</tbody></table></div>';
    exit;
}

if (isset($_GET['tinh'])) {
    $json = [];
    $url = $_SERVER['REQUEST_URI'];
    $parts = parse_url($url);
    parse_str($parts['query'], $query);
    $search_tinh =  _sql01($query['searchTinh']);
    $stmt1 =  $conn->prepare("SELECT * FROM vn_tinh where `name` like'%" . $search_tinh . "%' ORDER BY id ASC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $show) {
        $json[] = ['id' => $show['id'], 'text' => $show['name']];
    }
    echo json_encode($json);
    exit;
}
if (isset($_GET['huyen'])) {
    $json = [];
    $url = $_SERVER['REQUEST_URI'];
    $parts = parse_url($url);
    parse_str($parts['query'], $query);
    $search_tinh =  _sql01($query['searchTerm']);
    $id_tinh = (int)$query['vn_tinh'];
    $stmt1 =  $conn->prepare("SELECT * FROM vn_tinh_huyen where `id_tinh`=$id_tinh and `name` like'%" . $search_tinh . "%' ORDER BY id ASC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $show) {
        $json[] = ['id' => $show['id'], 'text' => $show['name']];
    }
    echo json_encode($json);
    exit;
}
if (isset($_GET['xa'])) {
    $json = [];
    $url = $_SERVER['REQUEST_URI'];
    $parts = parse_url($url);
    parse_str($parts['query'], $query);
    $search_tinh =  _sql01($query['searchTerm']);
    $id_huyen = (int)$query['vn_huyen'];
    $stmt1 =  $conn->prepare("SELECT * FROM vn_tinh_huyen_xa where `id_huyen`=$id_huyen and `name` like'%" . $search_tinh . "%' ORDER BY id ASC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $show) {
        $json[] = ['id' => $show['id'], 'text' => $show['name']];
    }
    echo json_encode($json);
    exit;
}
if (isset($_POST['vnXa'])) {
    $idXa = (int)$_POST['vnXa'];
    $stmt =  $conn->prepare("SELECT * FROM vn_tinh_huyen_xa WHERE id =:id");
    $stmt->execute(array(":id" => (int) $idXa));
    $xa = $stmt->fetch(PDO::FETCH_ASSOC);
    $selected_tinh = '<option value="' . $xa['id_tinh'] . '">' . info_vn_tinh($xa['id_tinh']) . '</option>';
    $selected_huyen = '<option value="' . $xa['id_huyen'] . '">' . info_vn_huyen($xa['id_huyen']) . '</option>';
    $selected_xa = '<option value="' . $xa['id'] . '">' . $xa['name'] . '</option>';
    echo $selected_xa . '||' . $selected_huyen . '||' . $selected_tinh;
    exit;
}

if (isset($_POST['sale_sanpham_01'])) {
    if ($id_level < 8) {
        exit;
    }
    $uid = (int)$_POST['sale_sanpham_01'];
    $name = _sql($_POST['name']);
    $sql = "UPDATE sale_sanpham_01 SET name='$name' WHERE id='" . $uid . "'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    exit;
}

if (isset($_POST['sale_sanpham_02'])) {
    if ($id_level < 8) {
        exit;
    }
    $uid = (int)$_POST['sale_sanpham_02'];
    $name = _sql($_POST['name']);
    $loai = _sql($_POST['loai']);
    $gia = _sql_num($_POST['gia']);
    $ship = _sql_num($_POST['ship']);
    $so_lan = get_id_so_lan_max($uid)["so_lan"];
    insert_gia_xuat_nhap($uid,$gia,$ship,$so_lan+1);
    $sql = "UPDATE sale_sanpham_02 SET ma_kem='$name',phan_loai='$loai',gia_tien='$gia',gia_nhap='$ship' WHERE id='" . $uid . "'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    // var_dump($sql);
    exit;
}

if (isset($_POST['type_toggle'])) {
    if ($id_level < 8) {
        exit;
    }
    $uid = (int)$_POST['uid'];
    $type = (int)$_POST['type_toggle'];
    $sql = "UPDATE sale_sanpham_01 SET hien_an='$type' WHERE id='" . $uid . "'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    exit;
}

if (isset($_POST['type_toggle2'])) {
    if ($id_level < 8) {
        exit;
    }
    $uid = (int)$_POST['uid'];
    $type = (int)$_POST['type_toggle2'];
    $sql = "UPDATE sale_sanpham_02 SET hien_an='$type' WHERE id='" . $uid . "'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    exit;
}

if (isset($_POST['edit_team_ne'])) {
    if ($id_level < 8) {
        exit;
    }
    $uid = (int)$_POST['edit_team_ne'];
    $id_mem = (int)$_POST['id_mem'];
    $name_team = _sql01($_POST['name_team']);
    $sql = "UPDATE danh_sach_team SET name_team='$name_team', admin = '$id_mem' WHERE id='" . $uid . "'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    // Xoá các admin khác
    $sql = "UPDATE member SET level='0' WHERE team='" . $uid . "' AND level<8 AND level>4 AND id!=$id_member";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    //Update bạn mới lên chức admin
    $sql = "UPDATE member SET level='7' WHERE id=$id_mem AND id!=$id_member AND level<8";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    exit;
}

if (isset($_POST['sdt_quen'])) {
    $sdt = (int)$_POST['sdt_quen'];
    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_03 WHERE sdt='$sdt' AND trang_thai!=44 ORDER BY id DESC LIMIT 5");
    $stmt1->execute(array());
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    if (count($list_code) > 0) {
        echo '<div class="card-header alert-danger"><h5 class="card-title mb-0" style="color:white;">Lịch sử mua hàng của khách</h5></div><div class="table-responsive flex-row flex-nowrap"><table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%"><thead><tr>
        <th style="text-align:center;">Member</th>
        <th style="text-align:center;">Tổng tiền</th>
        <th style="text-align:center;">Ghi chú</th>
        <th style="text-align:center;">Ngày thêm</th>
        <th style="text-align:center; width:7%">Actions</th></tr></thead><tbody>';
        $dia_chi = '';
        foreach ($list_code as $show_fp) {
            if ($dia_chi == '') {
                $dia_chi = $show_fp['nameKH'] . '|'.$show_fp['dia_chi'] . '|' . $show_fp['id_xa'];
            }
            if ($show_fp['type_dh'] == "0") {
                $mas_tk = "color:blue;";
            } else {
                $mas_tk = "color:red;";
            }
            $ghi_chu_s = $show_fp['ghi_chu'] != "" ? mb_substr($show_fp['ghi_chu'], 0, 20, 'UTF-8') : info_vn_tinh($show_fp['id_tinh']);
            echo '<tr>                                    
            <td style="text-align:center; ' . $mas_tk . '">' . ucwords(sql_member($show_fp['mem'], 'name')) . '</td>                                                                                                                       
            <td style="text-align:center; ' . $mas_tk . '">' . number_format($show_fp['thuc_nhan'], 0) . '</td>      
            <td style="text-align:center; ' . $mas_tk . '">' . _sql($ghi_chu_s) . '</td>      
            <td style="text-align:center; ' . $mas_tk . '">' . date('H:i d/m', $show_fp['time_add']) . '</td>
            <td style="text-align:center; ' . $mas_tk . '" data-toggle="modal" data-target="#popup_ne" onclick="chi_tiet(' . $show_fp['id'] . ',\'' . $show_fp['nameKH'] . '\',\'0' . $show_fp['sdt'] . '\')"><i class="far fa-fw fa-folder-open" style="color:#495057"></i></td>
            </tr>';
        }
        echo '</tbody></table></div>';
        $arr_dc = explode('|',$dia_chi);
        echo '||'.$arr_dc[0].'||'.$arr_dc[1].'||';
        $idXa = (int)$arr_dc[2];
        $stmt =  $conn->prepare("SELECT * FROM vn_tinh_huyen_xa WHERE id =:id");
        $stmt->execute(array(":id" => (int) $idXa));
        $xa = $stmt->fetch(PDO::FETCH_ASSOC);
        $selected_tinh = '<option value="' . $xa['id_tinh'] . '">' . info_vn_tinh($xa['id_tinh']) . '</option>';
        $selected_huyen = '<option value="' . $xa['id_huyen'] . '">' . info_vn_huyen($xa['id_huyen']) . '</option>';
        $selected_xa = '<option value="' . $xa['id'] . '">' . $xa['name'] . '</option>';
        echo $selected_xa . '||' . $selected_huyen . '||' . $selected_tinh;
    }
    exit;
}

if (isset($_POST['chi_tiet_dh'])) {
    $uid = (int)$_POST['chi_tiet_dh'];
    $stmt =  $conn->prepare("SELECT * FROM sale_sanpham_03 WHERE id=$uid AND trang_thai!=44 AND  (mem='$id_member' OR $id_level>5) ORDER BY id DESC");
    $stmt->execute();
    $id_sq01 = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($id_sq01['id'] > 0) {
        $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_04 WHERE sp_03=$uid ORDER BY id ASC");
        if ($id_sp01['type_dh'] > 0 and $id_sp01['type_dh'] <19 ) {
            $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_04_xit WHERE sp_03=$uid ORDER BY id ASC");
        }
        $stmt1->execute();
        $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
        echo '<div class="table-responsive flex-row flex-nowrap">
        <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
        <thead>
        <tr>
        <th style="text-align:center;">Sản phẩm</th>
        <th style="text-align:center;">Số lượng</th>
        <th style="text-align:center;">Tổng tiền</th>
        </tr>
        </thead>
        <tbody>';
        foreach ($list_code as $show_fp) {
            echo '<tr>
                <td style="text-align:center;">' . sql_sanpham_02($show_fp['sp_02'], 'ma_kem') . '</td>
                <td style="text-align:center;">  ' . number_format($show_fp['sl']) . '</td>                                                   
                <td style="text-align:center;">' . number_format($show_fp['sl'] * $show_fp['tien']) . '</td>
                </tr>';
        }
        echo '<tr><td style="text-align:center;" colspan="2">Tổng đơn</td><td style="text-align:center;">  ' . number_format($id_sq01['tong_tien']) . '</td></tr>';
        echo '<tr><td style="text-align:center;" colspan="2">VAT</td><td style="text-align:center;">  ' . number_format($id_sq01['vat']) . '</td></tr>';
        echo '<tr><td style="text-align:center;" colspan="2">Giảm giá</td><td style="text-align:center;">  ' . number_format($id_sq01['giam_gia']) . '</td></tr>';
        echo '<tr><td style="text-align:center;" colspan="2">Ship</td><td style="text-align:center;">  ' . number_format($id_sq01['ship']) . '</td></tr>';
        echo '<tr><td style="text-align:center; color:red" colspan="2">Thành tiền</td><td style="text-align:center; color:red">  ' . number_format($id_sq01['thuc_nhan']) . '</td></tr>';
        if ($id_sq01['ghi_chu'] != '') {
            echo '<tr><td style="text-align:center;">Ghi chú</td><td style="text-align:center;" colspan="2">  ' . _sql($id_sq01['ghi_chu']) . '</td></tr>';
        }
        $json_xa = json_decode(info_vn_xa($id_sq01['id_xa']), true);
        echo '<tr><td style="text-align:center;" colspan="3">Đ/c: ' . _sql($id_sq01['dia_chi']) . ' - ' . $json_xa['full_name'] . '</td></tr>';
        echo '<tr><td style="text-align:center;">Team</td><td style="text-align:center;" colspan="2">  ' . sql_web($id_sq01['team'], 'danh_sach_team', 'name_team') . '</td></tr>';
        if ($id_sq01['nguonKH'] > 0) {
            echo '<tr><td style="text-align:center;">Nguồn</td><td style="text-align:center;" colspan="2">  ' . sql_web($id_sq01['nguonKH'], 'admin_nguon_khach_hang', 'name') . '</td></tr>';
        }
        if ($id_sq01['type_dh'] > 0) {
            echo '<tr><td style="text-align:center;">Chốt xịt</td><td style="text-align:center;" colspan="2">  ' . sql_web($id_sq01['type_dh'], 'admin_ly_do_chot_xit', 'name') . '</td></tr>';
        }        
    } else {
        echo 'Yêu cầu quyền ADMIN để xem dữ liệu';
    }
    exit;
}

if (isset($_POST['ke_toan_01'])) {
    if ($id_level < 8) {
        exit;
    }
    $uid = (int)$_POST['ke_toan_01'];
    $name = _sql($_POST['name']);
    $sql = "UPDATE ke_toan_01 SET name='$name' WHERE id='" . $uid . "'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    exit;
}

if (isset($_POST['ke_toan_02'])) {
    if ($id_level < 8) {
        exit;
    }
    $uid = (int)$_POST['ke_toan_02'];
    $name = _sql($_POST['name']);
    $sql = "UPDATE admin_nguon_khach_hang SET name='$name' WHERE id='" . $uid . "'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    exit;
}