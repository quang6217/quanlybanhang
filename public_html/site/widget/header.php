<?php
if (!isset($mac_dinh)) {
    $mac_dinh = 'sidebar sidebar-sticky';
}
if (!isset($_COOKIE['id_member'])) {
    header('Location: /login');
    exit;
}
if ($member['money'] < 0) {
    $show_sss = ' <font color=red>(' . number_format($member['money'], 0) . 'đ)</font>';
} else {
    $show_sss = '- ' . number_format($member['money'], 0) . 'đ';
}
$chinhsuamail ="";
if(check_admin($member['email']))
{
    $chinhsuamail = '<li class="sidebar-item"><a class="sidebar-link" href="/edit_email"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Chỉnh sửa email</a></li>';
}
$head_yes = 99;
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $title ?> - <?= $_SERVER['HTTP_HOST'] ?></title>
    <link rel="shortcut icon" type="image/png" href="img/like88.ico" sizes="192x192">
    <script src="/js/settings.js"></script>
    <link rel="stylesheet" type="text/css" href="css/dp_css/dashmix2.css">
    <link rel="stylesheet" type="text/css" href="css/dp_css/dataTables.bootstrap4.css">
    <link rel="stylesheet" type="text/css" href="css/danplay.css" media="all">
    <link rel="stylesheet" type="text/css" href="css/dp_css/lq_color.css">
    <link rel="stylesheet" type="text/css" href="css/dp_css/lq1.css">
    <style type="text/css">
        .nav-item>.btn-hero-success {
            border-radius: 1.25rem;
        }
    </style>
</head>

<body>
    <div class="wrapper">
        <nav class="<?= $mac_dinh ?>">
            <div class="sidebar-content  js-simplebar">
                <div class="sidebar-brand" style="padding-bottom: 0px!important; margin-bottom: -2px;" href="/">
                    <div id="img_logo">
                        <a class="link-fx font-w600 font-size-lg text-white" href="/" style="padding-bottom: 0px!important; margin-bottom: -6px;">
                            <span class="smini-visible "><span class="text-white-75">F</span><span class="text-white">B</span></span>
                            <span class="smini-hidden align-middle text_logo"><span class=""></span><span class="">HIRYO.VN</span></span>
                        </a>
                    </div>
                </div>
                <?php
                $home_ac = $_SERVER['REQUEST_URI'] == '/' ? 'active' : '';
                $add_donhang = check_string('/add_donhang', $_SERVER['REQUEST_URI']) ==  0 ? '' : 'active';
                $list_hang = check_string('/list_hang', $_SERVER['REQUEST_URI']) ==  0 ? '' : 'active';
                echo '<ul class="sidebar-nav">
                    <li class="sidebar-item ' . $home_ac . '">
                        <a class="sidebar-link" href="/">
                            <i class="align-middle" data-feather="home"></i> <span class="align-middle">Trang chủ</span>
                        </a>
                    </li>
                    <li class="sidebar-item ' . $add_donhang . '">
                        <a class="sidebar-link" href="/add_donhang">
                            <i class="align-middle mr-2 fas fa-fw fa-shopping-cart"></i> <span class="align-middle">Tạo đơn hàng</span>
                        </a>
                    </li>
                    <li class="sidebar-item ' . $list_hang . '">
                        <a class="sidebar-link" href="/list_hang&search=0">
                            <i class="align-middle mr-2 fas fa-fw fa-tasks"></i> <span class="align-middle">Danh sách đơn</span>
                        </a>
                    </li>';
                $tk_don_ngay = $_SERVER['REQUEST_URI'] == '/tk_don_ngay' ? 'active' : '';
                $tk_don_team = $_SERVER['REQUEST_URI'] == '/tk_don_team' ? 'active' : '';
                $tk_don_mem = $_SERVER['REQUEST_URI'] == '/tk_don_mem' ? 'active' : '';
                $tk_don_nguon = $_SERVER['REQUEST_URI'] == '/tk_don_nguon' ? 'active' : '';
                $cp_ngay = $_SERVER['REQUEST_URI'] == '/cp_ngay' ? 'active' : '';
                $cp_tong_quat = $_SERVER['REQUEST_URI'] == '/cp_tong_quat' ? 'active' : '';
                echo ' <li class="sidebar-item ' . $list_tk . '"><a href="#list_tk" data-toggle="collapse" class="sidebar-link collapsed">
                    <i class="align-middle mr-2 fas fa-fw fa-robot"></i> <span class="align-middle">Thống kê</span></a>
                    <ul id="list_tk" class="sidebar-dropdown list-unstyled collapse ' . $list_tk_1 . '">
                    <li class="sidebar-item ' . $tk_don_ngay . '"><a class="sidebar-link" href="/tk_don_ngay&search=0"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Đơn hàng theo ngày</a></li>
                    <li class="sidebar-item ' . $tk_don_team . '"><a class="sidebar-link" href="/tk_don_team&search=0"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Đơn hàng team</a></li>
                    <li class="sidebar-item ' . $tk_don_mem . '"><a class="sidebar-link" href="/tk_don_mem&search=0"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Đơn hàng nhân viên</a></li>
                    <li class="sidebar-item ' . $tk_don_nguon . '"><a class="sidebar-link" href="/tk_don_nguon&search=0"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Đơn hàng theo nguồn</a></li>
                    <li class="sidebar-item ' . $cp_ngay . '"><a class="sidebar-link" href="/cp_ngay&search=0"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Chi phí theo ngày</a></li>
                    <li class="sidebar-item ' . $cp_tong_quat . '"><a class="sidebar-link" href="/cp_tong_quat&search=0"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Chi phí trực quan</a></li></ul></li>
                    ';
                echo '  <li class="sidebar-item">
                    <a href="#van_hanh" data-toggle="collapse" class="sidebar-link collapsed"><i class="align-middle mr-2 fas fa-fw fa-shipping-fast"></i> <span class="align-middle">Điều hành hệ thống</span></a>
                    <ul id="van_hanh" class="sidebar-dropdown list-unstyled collapse">                            
                    <li class="sidebar-item"><a class="sidebar-link" href="/ke_toan"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Kế toán - chi tiêu</a></li>';
                if ($id_level > 4) {
                    echo '
                    <li class="sidebar-item"><a class="sidebar-link" href="/xuat_hang"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Xuất đơn hàng</a>
                    <li class="sidebar-item"><a class="sidebar-link" href="/ql_member"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Quản lý Member</a></li>
                    <li class="sidebar-item"><a class="sidebar-link" href="/ql_chat"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Quản lí chat</a></li>
                    <li class="sidebar-item"><a class="sidebar-link" href="/kho_hang"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Tình trạng kho hàng</a></li></li>';
                }
                echo '</ul></li>';
                if ($id_level > 7) {
                    echo '  <li class="sidebar-item ' . $Admin . '">
                        <a href="#Admin" data-toggle="collapse" class="sidebar-link collapsed"><i class="align-middle" data-feather="settings"></i> <span class="align-middle">Admin</span></a>
                        <ul id="Admin" class="sidebar-dropdown list-unstyled collapse">                        
                        <li class="sidebar-item"><a class="sidebar-link" href="/admin_nap_tien"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Nạp tiền ADMIN</a></li>
                        <li class="sidebar-item"><a class="sidebar-link" href="/tong_giao_dich"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Tổng giao dịch</a></li>
                        <li class="sidebar-item"><a class="sidebar-link" href="/nhap_thong_bao"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Nhập thông báo</a></li>
                        '.$chinhsuamail.'
                        </ul></li>
                        
                        <li class="sidebar-item ' . $user_cog . '">
                        <a href="#user_cog" data-toggle="collapse" class="sidebar-link collapsed">
                        <i class="align-middle mr-2 fas fa-fw fa-user-cog"></i> <span class="align-middle">Cấu hình dữ liệu</span>
                        </a>
                        <ul id="user_cog" class="sidebar-dropdown list-unstyled collapse">
                        <li class="sidebar-item"><a class="sidebar-link" href="/ql_team"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Quản lý TEAM</a></li>
                        <li class="sidebar-item"><a class="sidebar-link" href="/add_sanpham"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Quản lý sản phẩm</a></li>
                        <li class="sidebar-item"><a class="sidebar-link" href="/ql_chot_xit"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Lý do chốt ko đc</a></li>
                        <li class="sidebar-item"><a class="sidebar-link" href="/al_nguon_kh"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Nguồn khách hàng</a></li>
                        <li class="sidebar-item"><a class="sidebar-link" href="/khoan_chi"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Thêm khoản chi</a></li></ul></li>';
                } ?>
                <li class="sidebar-item">
                    <a href="#Member" data-toggle="collapse" class="sidebar-link collapsed">
                        <i class="align-middle" data-feather="user"></i> <span class="align-middle">Member</span>
                    </a>
                    <ul id="Member" class="sidebar-dropdown list-unstyled collapse">
                        <li class="sidebar-item"><a class="sidebar-link" href="/profile"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>ID tài khoản: <strong style="color:red"><?= $member['id'] ?></strong></a></li>
                        <li class="sidebar-item"><a class="sidebar-link" href="/chuyen_tien"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Chuyển tiền</a></li>
                        <li class="sidebar-item"><a class="sidebar-link" href="/lich_su_giao_dich"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Lịch sử giao dịch</a></li>
                        <li class="sidebar-item"><a class="sidebar-link" href="/login&exit=ok"><i class="align-middle" style="padding:4px;" data-feather="arrow-right"></i>Đăng xuất</a></li>
                    </ul>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link" target="_blank">
                        <i class="align-middle" data-feather="globe" style="color: white !important;"></i> <span style="color: white !important;" class="align-middle">IP: <?= $_SERVER['REMOTE_ADDR'] ?></span>
                    </a>
                    </button>
                </li>
                </ul>
            </div>
        </nav>
        <div class="main">
            <nav class="navbar navbar-expand " style="background-color: #1f75d5; color:white;">
                <a class="sidebar-toggle d-flex mr-2" onclick="show_hide_menu()">
                    <i class="fa fa-align-left fa-lg align-self-center"></i>
                </a>
                <div class="navbar-collapse collapse" style="background-color: #1f75d5; color:white;">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-icon dropdown-toggle d-inline-block d-sm-none" href="#" data-toggle="dropdown">
                                <span class="text-white" style="font-size: 80%;"><?= $member['name'] ?> <?= $show_sss ?></span> <i class="text-white" style="color: white;margin-left: 5px;" data-feather="arrow-down-circle"></i>
                            </a>
                            <a class="nav-link  d-none d-sm-inline-block" href="#" data-toggle="dropdown">
                                <img src="img/avatars/avatar.jpg" class="avatar img-fluid rounded-circle mr-1" /><span class="text-white" style="font-size: 100%;"><?= $member['name'] ?> <?= $show_sss ?></span> <i class="text-white" style="color: white;margin-left: 10px;" data-feather="arrow-down-circle"></i>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item"><i class="align-middle mr-1" data-feather="user"></i> ID: <?= $id_member ?></a>
                                    <a class="dropdown-item" href="/lich_su_giao_dich"><i class="align-middle mr-1" data-feather="dollar-sign"></i> <?= number_format($member['money'], 0) ?>đ</a>
                                    <a class="dropdown-item" href="/lich_su_giao_dich"><i class="align-middle mr-1" data-feather="pie-chart"></i> Lịch sữ giao dịch</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="/profile">Thông tin & bảo mật</a>
                                    <a class="dropdown-item" href="/login&exit=ok">Đăng xuất</a>
                                </div>
                        </li>
                    </ul>
                </div>
            </nav>