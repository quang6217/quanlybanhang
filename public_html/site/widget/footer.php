<script src="js/app.js"></script>
<script>
    $(function() {
        $('.sidebar-toggle.d-flex.mr-2').click(function() {
            $('footer.footer').toggleClass('no-padding');
        });
    });
</script>
<style>
    .footer {
        position: fixed;
        left: 0;
        bottom: 0;
        width: 100%;
        text-align: center;
    }
</style>
<footer class="footer alert-info">
    <div class="container-fluid">
        <div class="row text-muted">
            <div class="col-6 text-left">
                <span>Support: <a class="db pb10 a8" target="_blank">0962.808.788</a></span>
            </div>
            <div class="col-6 text-right">

                <p class="mb-0">
                    © 2020 - HIRYO
                </p>
            </div>
        </div>
    </div>
</footer>
</div>