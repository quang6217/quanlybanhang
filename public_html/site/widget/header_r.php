
<!DOCTYPE html>
<html lang="en">
    <head>
    	<title><?=$title?> - <?=$_SERVER['HTTP_HOST']?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="csslogin/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="csslogin/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="csslogin/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="csslogin/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="csslogin/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="csslogin/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="csslogin/css/util.css">
	<link rel="stylesheet" type="text/css" href="csslogin/css/main.css?v1">
<!--===============================================================================================-->
</head>
<body>	
    	<div class="limiter">
    		<div class="container-login100">
    			<div class="wrap-login100">
    				<div class="login100-pic js-tilt" data-tilt>
    					<img src="csslogin/images/img-01.png" alt="IMG">
				</div>